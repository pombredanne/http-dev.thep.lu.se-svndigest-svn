$Id: readme.txt 84 2006-03-13 22:04:34Z jari $

Copyright (C) 2005, 2006 Jari H�kkinen

This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

svnstat is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svnstat is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.



svnstat traverses a directory structure (controlled by subversion) and
calculates developer statistics for all subversion controlled
entries. The result is written to a sub-directory, svnstat_output,
that will be created in the current working directory.

To understand what statistics is calculated by svnstat this definition
is needed: The developer who made the latest change to a line still in
use in the latest (checked out) revision, is considered as the
contributor of that line regardless of who actually originally created
that line.

The statistics calculated is the number of lines contributed from each
contributing developer in the latest (checked out) revision. Also, the
number of lines still in use from each revision is calculated.

There are many different types of files and for many file types it
does not make sense to define lines. Source code, documentation, and
other human readable files can be treated on line basis whereas
symbolic links and binary files cannot. svnstat treats non-line based
files as one-line files, i.e. the developer that made the last change
to this type of files gets a one-line credit for it.

How should we exclude un-reasonable credit for large line based files?
We could add a property to that file, tagging it as a file that should
be treated as an non-line base file.

The current design assumes that the subversion repository to be
analysed is checked out, maybe one would want to run the analysis
agains a subversion repositoy directly.

The current flow of the program is.

i) Extract the directory structure starting from the directory given
   at the command line.

ii) Parse the directory structure, removing files and direcotries that
   is not in subversion control. After this step, only valid
   subversion entries will be allowed in the tree. Beware, 'svn blame'
   chokes on directories, and the directories are still a part of the
   tree.

iii) Walk through the directory structure and calculate statistics for
   each entry.

iv) Compile statistics as discussed. This step may be intermingled
   with step iii).

v) Create the html presentation as discussed.
