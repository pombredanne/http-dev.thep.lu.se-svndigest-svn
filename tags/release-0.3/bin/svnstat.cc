// $Id: svnstat.cc 130 2006-08-02 18:46:27Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"

#include "Directory.h"
#include "GnuplotFE.h"
#include "rmdirhier.h"
#include "SVN.h"
#include "SVNinfo.h"

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

int main(const int argc,const char* argv[])
{
	using namespace theplu::svnstat;
	Parameter* option=NULL;
	try {
		option = new Parameter(argc,argv);
	}
	catch (std::runtime_error e) {
		std::cerr << e.what() << std::endl;
		exit(-1);
	}

	// Make sure that root directory is under subversion control.
	SVN* svn=SVN::instance();
	try {
		svn->setup_wc_adm_access(option->root());
	}
	catch (SVNException e) {
		std::cerr << "\nsvnstat: " << e.what() << "\nsvnstat: " << option->root()
							<< " is not under subversion control\n" << std::endl;
		exit(-1);
	}

	// Extract repository location
	std::string repo;
	try {
		repo=SVNinfo(option->root()).repos_root_url();
	}
	catch (SVNException e) {
		std::cerr << "\nsvnstat: " << e.what()
							<< "\nsvnstat: Failed to determine repository for "
							<< option->root() << '\n' << std::endl;
		exit(-1);
	}

	// Retrieve commit dates.
	std::vector<std::string> commit_dates;
	try {
		svn->setup_ra_session(repo);
		commit_dates=svn->commit_dates(repo);
	}
	catch (SVNException e) {
		std::cerr << "\nsvnstat: " << e.what() << std::endl;
		exit(-1);
	}

	// check if target already exists and behave appropriately
	std::string root_path(option->targetdir()+'/'+file_name(option->root()));
	struct stat buf;
	if (!stat(root_path.c_str(),&buf))
		// root_path already exists
		if (option->force()) {
			// force true -> removal of root_path
			if (option->verbose())
				std::cout << "rm -rf " << root_path << "\n";
			rmdirhier(root_path);
		}
		else {
			// force false -> exit 
			std::cerr << "\nsvnstat: " << root_path << ": directory already exists"
								<< std::endl;
			exit(-1);
		}

	if (!option->revisions())
		GnuplotFE::instance()->set_dates(commit_dates);

	Directory tree(0,option->root(),"");
	tree.parse(option->verbose());

	GnuplotFE::instance()->command(string("cd '")+option->targetdir()+"'");
	chdir(option->targetdir().c_str());
	try {
		tree.print(option->verbose());
	}
	catch (const std::runtime_error& x) {
		std::cerr << "svnstat: " << x.what() << std::endl;
	}
	std::string css(file_name(option->root())+"/svnstat.css");
	std::ofstream os(css.c_str());
	print_css(os);
	os.close();

	delete option;
	exit(0);				// normal exit
}


