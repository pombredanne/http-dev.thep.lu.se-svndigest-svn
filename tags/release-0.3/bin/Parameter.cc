// $Id: Parameter.cc 122 2006-07-23 07:30:20Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svnstat {

	Parameter::Parameter(const int argc,const char *argv[])
	{
		defaults();
		for (int i=1; i<argc; i++) {
			bool ok=false;
			std::string myargv(argv[i]);
			if (myargv=="-f" || myargv=="--force"){
					force_=true;
					ok=true;
			}
			else if (myargv=="-h" || myargv=="--help"){
				help();
				exit(0);			// always exit after printing help
			}
			else if (myargv=="-r" || myargv=="--root"){
				if (++i<argc){
					root_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-rev" || myargv=="--revisions") {
					revisions_=true;
					ok=true;
			}
			else if (myargv=="-t" || myargv=="--target"){
				if (++i<argc){
					targetdir_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-v" || myargv=="--verbose"){
					verbose_=true;
					ok=true;
			}
			else if (myargv=="--version"){
					version();
					exit(0);
			}

			if (!ok)
				throw std::runtime_error("svnstat: invalid option: " + myargv +
																 "\nType 'svnstat --help' for usage.");
		}

		analyse();
	}


	void Parameter::analyse(void)
	{
		// making root absolute
		std::string workdir(pwd());
		chdir(root_.c_str());
		root_ = pwd();
		// remove trailing '/'
		if (*root_.rbegin()=='/')
			root_.erase(root_.begin()+root_.size()-1);
		chdir(workdir.c_str());

		// making targetdir absolute
		chdir(targetdir_.c_str());
		targetdir_ = pwd();
		// remove trailing '/'
		if (*targetdir_.rbegin()=='/')
			root_.erase(targetdir_.begin()+targetdir_.size()-1);
		chdir(workdir.c_str());

		struct stat buf;
		// check that root directory exists
		if (stat(root_.c_str(),&buf))
			throw std::runtime_error("\nsvnstat: " + root_ + ": No such directory.");

		// check that target directory exists
		if (stat(targetdir_.c_str(),&buf)){
			throw std::runtime_error("\nsvnstat: " + targetdir_ + 
															 ": No such directory.");
		}

	}


	void Parameter::defaults(void)
	{
		force_=false;
		revisions_=false;
		root_=".";
		targetdir_=".";
		verbose_=false;
	}


	void Parameter::help(void)
	{
		defaults();
		std::cout << "\n"
							<< "usage: svnstat [options]\n"
							<< "\n"
							<< "svnstat traverses a directory structure (controlled by\n"
							<< "subversion) and calculates developer statistics entries.\n"
							<< "The top level directory of the directory structure to\n"
							<< "traverse is set with the -r option. The result is written to\n"
							<< "the current working directory or a directory set with the\n"
							<< "-t option.\n"
							<< "\n"
							<< "Valid options:\n"
							<< "  -f [--force]   : remove target directory/file if it exists\n"
							<< "                   [no force]. NOTE recursive delete.\n"
							<< "  -h [--help]    : display this help and exit\n"
							<< "  -r [--root] arg : svn controlled directory to perform\n"
							<< "                    statistics calculation on [" 
							<< root_ << "]\n"
							<< "  -rev [--revisions]: Use revision numbers as time scale\n"
							<< "                      instead of dates [dates].\n"
							<< "  -t [--target] arg : output directory [" 
							<< targetdir_ << "]\n"
							<< "  -v [--verbose] : explain what is being done\n"
							<< "  --version      : print version information and exit\n"
							<< std::endl;
	}


	void Parameter::version(void) const
	{
		using namespace std;
		cout << PACKAGE_STRING
				 << "\nCopyright (C) 2006 Jari H�kkinen and Peter Johansson.\n\n"
				 << "This is free software; see the source for copying conditions.\n"
				 << "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR\n"
				 << "A PARTICULAR PURPOSE." << endl;
	}

}} // of namespace wenni and namespace theplu
