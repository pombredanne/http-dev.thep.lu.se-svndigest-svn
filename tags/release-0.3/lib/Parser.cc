// $Id: Parser.cc 118 2006-07-03 07:44:49Z peter $

/*
	Copyright (C) 2006 Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parser.h"
#include "utility.h"

#include <algorithm>
#include <functional>
#include <fstream>
#include <iostream>
#include <string>

namespace theplu{
namespace svnstat{


	Parser::Parser(const std::string& path)
	{
		std::ifstream is(path.c_str());
		cc_mode(is);
		is.close();
	}


	void Parser::cc_mode(std::istream& is)
	{
		std::string str;
		while(getline(is,str)) {
			std::string::iterator where;
			where = std::find_if(str.begin(), str.end(), std::not1(WhiteSpace()));
			if (where==str.end())
				type_.push_back(empty);
			else if (match_begin(where, str.end(), "//")) {
				where = std::find_if(where, str.end(), AlphaNum());
				if (where==str.end())
					type_.push_back(empty);
				else
					type_.push_back(comment);
			}
			else {
				type_.push_back(code);
			}
		}
	}


}} // end of namespace svnstat and namespace theplu
