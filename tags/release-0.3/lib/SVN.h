// $Id: SVN.h 141 2006-08-04 10:11:26Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svnstat_svn_
#define _theplu_svnstat_svn_

#include <stdexcept>
#include <string>
#include <vector>

#include <svn_client.h>
#include <svn_types.h>

namespace theplu {
namespace svnstat {

	///
	/// If something goes wrong in the use of the different SVN classes,
	/// an SVNException is thrown.
	///
	struct SVNException : public std::runtime_error
	{ inline SVNException(const std::string& msg) : runtime_error(msg) {} };

	///
	/// The SVN class is a front end to the subversion API.
	///
	/// SVN provides one single global access point to the underlying
	/// subversion API and makes sure that there is only one point of
	/// access for the binary.
	///
	/// @see Design Patterns (the singleton pattern). Subversion API
	/// documents.
	///
	class SVN {
	public:

		enum vc_status {
			unversioned=0,
			uptodate,
			unresolved
		};

		///
		/// @brief The destructor.
		///
		~SVN(void);

		///
		/// @brief Call the underlying svn_client_blame3 for \a path with
		/// \a receiver and \a baton.
		///
		/// This function is called from SVNblame to do 'svn blame' on an
		/// item. The \a receiver and \a baton is defined in SVNblame and
		/// the \a receiver is called by the underlying subversion API for
		/// every line in \a path provided it the item is under subversion
		/// control. The \a baton is used to communicate anonymous
		/// information through the API to the \a receiver. If \a path is
		/// a binary object an error is returned, all other errors will
		/// generate an SVNException.
		///
		/// @return SVN_NO_ERROR or SVN_ERR_CLIENT_IS_BINARY_FILE, the
		/// latter can be used to trigger on binary files. Note that
		/// errors return from underlying subversion API must be cleared
		/// by the receiver.
		///
		/// @see Subversion API (svn_error_clear).
		///
		svn_error_t * client_blame(const std::string& path,
															 svn_client_blame_receiver_t receiver,
															 void *baton);

		///
		/// @brief Call the underlying svn_client_info for \a path with \a
		/// receiver and \a baton.
		///
		/// This function is called from SVNinfo to do 'svn info' on an
		/// item. The \a receiver and \a baton is defined in SVNinfo and
		/// the \a receiver is called by the underlying subversion API if
		/// \a path is under subversion control. The \a baton is used to
		/// communicate anonymous information through the API to the
		/// \a receiver.
		///
		/// @see Subversion API documentation, SVNinfo
		///
		void client_info(const std::string& path, svn_info_receiver_t receiver,
										 void *baton);

		///
		/// @brief Get revision dates.
		///
		/// Get dates for all commits to the repository. \a root can be a
		/// repository path or a path within a working copy. In the latter
		/// case the corresponding repository will be used to retrieve
		/// commit dates.
		///
		/// @return Revision dates in a vector where revision is
		/// implicitly defined by vector index, i.e., element 56 in the
		/// resulting vector implies revision 56.
		///
		/// @note Currently revision 0 (repositoy creation) is not
		/// supported.
		///
		/// @throw Various error messages generated from the subversion
		/// API.
		///
		std::vector<std::string> commit_dates(const std::string& path);

		///
		/// @brief Get an instance of SVN.
		///
		/// @throw Throws an SVNException if initialization fails in the
		/// underlying subversion API calls.
		///
		static SVN* instance(void)
		{ if (!instance_) instance_=new SVN; return instance_; }

		///
		/// @throws SVNException if session setup fails.
		///
		void setup_ra_session(const std::string& path);

		///
		/// @throws SVNException if access setup fails.
		///
		void setup_wc_adm_access(const std::string& path);

		///
		/// @brief Check if entry \a path is under version control
		///
		/// @return True if \a path is under version control, false
		/// otherwise.
		///
		vc_status version_controlled(const std::string& path);


	private:
		///
		/// @brief Constructor
		///
		/// The only way to create a object of SVN type is by calling
		/// SVN::instance.
		///
		SVN(void);

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVN(const SVN&);

		///
		/// Free resources when failing to reach end of constructor. 
		///
		void cleanup_failed_initialization(svn_error_t *err);

		static SVN* instance_;

		// Subversion API stuff

		// Log message receiver
		struct log_receiver_baton {
			std::vector<std::string> commit_dates;
		};
		static svn_error_t *
		log_message_receiver(void *baton, apr_hash_t *changed_paths,
												 svn_revnum_t rev, const char *author, const char *date,
												 const char *msg, apr_pool_t *pool);

		svn_wc_adm_access_t* adm_access_;
		apr_allocator_t* allocator_;
		svn_client_ctx_t* context_;
		apr_pool_t* pool_;
		svn_ra_session_t* ra_session_;
	};

}} // end of namespace svnstat and namespace theplu

#endif
