// $Id: Parser.h 128 2006-08-02 16:39:54Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svnstat_parser_
#define _theplu_svnstat_parser_

#include <iosfwd>
#include <string>
#include <vector>

namespace theplu{
namespace svnstat{

	///
	/// Class parsing files
	///
	class Parser
	{
	public:
		///
		/// A line is considered empty if it only contains spaces and tabs.
		///
		/// A line is considered a comment if first and second non-white
		/// space characters are '/'.
		///
		/// All other lines are considered code.
		///
		enum line_type {
			empty,
			comment,
			code
		};

		/// 
		/// @brief Constructor 
		///
		explicit Parser(const std::string&);

		///
		/// @return const ref to vector with the line types
		///
		inline const std::vector<line_type>& type(void) const { return type_; }

	private:
		///
		/// Copy constructor (not implemented)
		///
		Parser(const Parser& other);

		void cc_mode(std::istream&);

		std::vector<line_type> type_;

	};

	///
	/// @return true if @a c is [a-z], [A-Z] or numerical.
	///
	struct AlphaNum : public std::unary_function<char,bool>
	{
		inline bool operator()(const char c) const
		{ 
			return isalnum(c); 
		}
	};

	///
	/// Functor for white space identification
	///
	/// @see isspace
	/// 
	/// @return true if @a c is '\t', '\n', '\v', '\f' or ' '.
	///
	struct WhiteSpace : public std::unary_function<char,bool>
	{
		inline bool operator()(const char c) const
		{ 
			return isspace(c); 
		}
	};

}} // end of namespace svnstat end of namespace theplu

#endif 
