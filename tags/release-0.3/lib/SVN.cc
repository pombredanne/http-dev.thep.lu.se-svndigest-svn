// $Id: SVN.cc 142 2006-08-08 19:23:11Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVN.h"

#include <string>

#include <apr_allocator.h>
#include <apr_hash.h>
#include <svn_client.h>
#include <svn_cmdline.h>
#include <svn_path.h>
#include <svn_pools.h>
#include <svn_wc.h>

namespace theplu {
namespace svnstat {


	SVN* SVN::instance_=NULL;


	SVN::SVN(void)
		: adm_access_(NULL), allocator_(NULL), context_(NULL), pool_(NULL)
	{
		svn_error_t* err=NULL;

		// initialize something (APR subsystem and more). The APR
		// subsystem is automatically destroyed at program exit. In case
		// of several calls to svn_cmdline_init (ie. several exceptions
		// thrown and caught with subsequent reinitializatios is safe
		// memorywise but what about APR internal counters?)
		if (svn_cmdline_init("svnstat",stderr) != EXIT_SUCCESS)
			throw SVNException("SVN(void): svn_cmdline_init failed");

		/// create top-level pool
		if (apr_allocator_create(&allocator_))
			throw SVNException("SVN(void): apr_allocator_create failed");
		apr_allocator_max_free_set(allocator_,SVN_ALLOCATOR_RECOMMENDED_MAX_FREE);
		pool_ = svn_pool_create_ex(NULL, allocator_);
		apr_allocator_owner_set(allocator_, pool_);

		// initialize the repository access library
		if ((err=svn_ra_initialize(pool_))) {
			cleanup_failed_initialization(err);
			throw SVNException("SVN(void): svn_ra_initialize failed");
		}

		// Check that users .subversion exist. Should this really be done?
		// If this check is to be done, we might be forced to support a
		// command line option to change the location of the .subversion
		// stuff (compare with the svn binary).
		if ((err=svn_config_ensure(NULL, pool_))) {
			cleanup_failed_initialization(err);
			throw SVNException("SVN(void): svn_config_ensure failed");
		}

		// create a client context object
		if ((err=svn_client_create_context(&context_, pool_))) {
			cleanup_failed_initialization(err);
			throw SVNException("SVN(void): svn_client_create_context failed");
		}

		if ((err=svn_config_get_config(&(context_->config), NULL, pool_))) {
			cleanup_failed_initialization(err);
			throw SVNException("SVN(void): svn_config_get_config failed");
		}

		// set up authentication stuff
		if ((err=svn_cmdline_setup_auth_baton(&(context_->auth_baton), false, NULL,
																					NULL, NULL, false,
						static_cast<svn_config_t*>(apr_hash_get(context_->config,
																										SVN_CONFIG_CATEGORY_CONFIG,
																										APR_HASH_KEY_STRING)),
																					context_->cancel_func,
																					context_->cancel_baton, pool_))) {
			cleanup_failed_initialization(err);
			throw SVNException("SVN(void): svn_cmdline_setup_auth_baton failed");
		}
	}


	SVN::~SVN(void)
	{
		if (adm_access_)
			svn_error_clear(svn_wc_adm_close(adm_access_));
		svn_pool_destroy(pool_);
		apr_allocator_destroy(allocator_);
		// other apr resources acquired in svn_cmdline_init are destroyed
		// at program exit, ok since SVN is a singleton
		delete instance_;
	}


	svn_error_t* SVN::client_blame(const std::string& path,
																 svn_client_blame_receiver_t receiver,
																 void *baton)
	{
		// Setup to use all revisions
		svn_opt_revision_t peg, start, head;
		peg.kind=svn_opt_revision_unspecified;
		start.kind=svn_opt_revision_number;
		start.value.number=0;
		head.kind=svn_opt_revision_head;
		apr_pool_t *subpool = svn_pool_create(pool_);
		svn_error_t* err=svn_client_blame3(path.c_str(), &peg, &start, &head,
																			 svn_diff_file_options_create(subpool),
																			 false, receiver, baton, context_,
																			 subpool);
		if (err && err->apr_err!=SVN_ERR_CLIENT_IS_BINARY_FILE) {
			svn_handle_error2(err, stderr, false, "svnstat: ");
			svn_error_clear(err);
			svn_pool_destroy(subpool);
			throw SVNException("SVN::client_blame: svn_client_blame3 failed");
		}
		svn_pool_destroy(subpool);
		return err;
	}


	void SVN::client_info(const std::string& path, svn_info_receiver_t receiver,
												void *baton)
	{
		apr_pool_t *subpool = svn_pool_create(pool_);
		if (svn_error_t *err=svn_client_info(path.c_str(), NULL, NULL, receiver,
																				 baton, false, context_, subpool)) {
			svn_handle_error2(err, stderr, false, "svnstat: ");
			svn_error_clear(err);
			svn_pool_destroy(subpool);
			throw SVNException("repository: svn_client_info failed");
		}
		svn_pool_destroy(subpool);
	}


	std::vector<std::string> SVN::commit_dates(const std::string& path)
	{
		// Allocate space in subpool to pool_ for apr_path (here a string).
		apr_pool_t *subpool = svn_pool_create(pool_);
		apr_array_header_t* apr_path=apr_array_make(subpool,1,4);
		// Copy path to apr_path.
		(*((const char **) apr_array_push(apr_path))) =
			apr_pstrdup(subpool, svn_path_internal_style(path.c_str(),subpool));

		// Setup to retrieve all commit logs.
		svn_opt_revision_t peg, start, head;
		peg.kind=svn_opt_revision_unspecified;
		start.kind=svn_opt_revision_number;
		start.value.number=0;
		head.kind=svn_opt_revision_head;
		svn_error_t* err=NULL;
		// Retrieving the last revision is only needed for the reserve
		// call below, not needed for the functionality here.
		if ((err=svn_ra_get_latest_revnum(ra_session_, &(head.value.number),
																			subpool))) {
			svn_handle_error2(err, stderr, false, "svnstat: ");
			svn_error_clear(err);
			svn_pool_destroy(subpool);
			throw SVNException("commit_dates: svn_ra_get_latest_revnum failed");
		}
		// The struct we want to pass through to all log_message_receiver
		// calls, here we only want to push all commit dates into a
		// std::vector<std::string>.
		struct log_receiver_baton lb;
		lb.commit_dates.reserve(head.value.number+1); // revision 0 is also stored.
		if ((err=svn_client_log3(apr_path, &peg, &start, &head, 0, false, true,
														 log_message_receiver, static_cast<void*>(&lb),
														 context_, subpool))) {
			svn_handle_error2(err, stderr, false, "svnstat: ");
			svn_error_clear(err);
			svn_pool_destroy(subpool);
			throw SVNException("commit_dates: svn_client_log3 failed");
		}
		svn_pool_destroy(subpool);
		return lb.commit_dates;
	}


	void SVN::cleanup_failed_initialization(svn_error_t *err)
	{
		svn_handle_error2(err,stderr,false,"svnstat:");
		svn_error_clear(err);
		svn_pool_destroy(pool_);
		apr_allocator_destroy(allocator_);
	}


	svn_error_t *
	SVN::log_message_receiver(void *baton, apr_hash_t *changed_paths,
														svn_revnum_t rev, const char *author,
														const char *date, const char *msg, apr_pool_t *pool)
	{
		struct log_receiver_baton *lb=static_cast<struct log_receiver_baton*>(baton);
		if (date && date[0])
			lb->commit_dates.push_back(date);
		else
			throw SVNException("No date defined for revision: " + rev); 
    return SVN_NO_ERROR;
	}


	void SVN::setup_ra_session(const std::string& path) {
		// get a session to the repository
		if (svn_error_t *err=svn_client_open_ra_session(&ra_session_, path.c_str(),
																										context_,pool_)) {
			svn_handle_error2(err,stderr,false,"svnstat:");
			svn_error_clear(err);
			throw SVNException("setup_ra_session: svn_client_open_ra_session failed");
		}
	}


	void SVN::setup_wc_adm_access(const std::string& path)
	{
		// Set up svn administration area access. The whole structure is
		// setup, maybe this is unnecessary?
		const char* canonical_path=svn_path_internal_style(path.c_str(), pool_);
		if (svn_error_t *err=svn_wc_adm_open3(&adm_access_, NULL, canonical_path,
																					false, -1, context_->cancel_func,
																					context_->cancel_baton, pool_)) {
			svn_handle_error2(err,stderr,false,"svnstat:");
			svn_error_clear(err);
			throw SVNException("setup_wc_adm_access: svn_wc_adm_open3 failed");
		}
	}


	SVN::vc_status SVN::version_controlled(const std::string& path)
	{
		svn_wc_status2_t* status=NULL;
		apr_pool_t *subpool = svn_pool_create(pool_);
		if (svn_error_t *err=
				svn_wc_status2(&status,svn_path_internal_style(path.c_str(), subpool),
											 adm_access_, subpool)) {
			svn_handle_error2(err,stderr,false,"svnstat:");
			svn_error_clear(err);
			svn_pool_destroy(subpool);
			throw SVNException("version_controlled(): svn_config_get_config failed");
		}
		svn_pool_destroy(subpool);

		if ((status->text_status==svn_wc_status_none) ||
				(status->text_status==svn_wc_status_unversioned))
			return unversioned;
		else if (status->text_status==svn_wc_status_normal)
			return uptodate;

		return unresolved;
	}


}} // end of namespace svnstat and namespace theplu
