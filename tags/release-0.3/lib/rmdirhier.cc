// $Id: rmdirhier.cc 100 2006-06-19 09:53:16Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "rmdirhier.h"

#include <dirent.h>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>


namespace theplu {
namespace svnstat {

	void rmdirhier(const std::string& path)
	{
		int fd=open(".",O_RDONLY);	// remember "original" cwd
		void rmdirhier__(const std::string&);

		try {
			rmdirhier__(path);
		}
		catch(const BadDirectory& x) {
			std::cerr << "Invalid directory: " << x.what() << std::endl;
		}
		catch(const DirectoryOpenError& x) {
			std::cerr << "Error opening directory: " << x.what() << std::endl;
		}
		catch(const FileDeleteError& x) {
			std::cerr << "Error deleting file: " << x.what() << std::endl;
		}
		catch(const DirectoryDeleteError& x) {
			std::cerr << "Error deleting directory: " << x.what() << std::endl;
		}
		catch(const DirectoryError& x) {
			std::cerr << "Directory error:" << x.what() << std::endl;
		}

		fchdir(fd);	// return to "original" cwd
	}


	void rmdirhier__(const std::string& dir)
	{
		if (chdir(dir.c_str()))
      throw BadDirectory(dir);

   // Delete any remaining directory entries
		DIR* dp;
		struct dirent *entry;
		if (!(dp=opendir(".")))
			throw DirectoryOpenError(dir);
		while ((entry=readdir(dp)) != NULL) {
			struct stat buf;
      if ((std::string(entry->d_name) == ".") ||
					(std::string(entry->d_name) == ".."))
				continue;
      stat(entry->d_name,&buf);
      if (buf.st_mode & S_IFDIR)
				rmdirhier__(entry->d_name);      // sub-directory
      else {
				// Make sure file is removable before removing it
				chmod(entry->d_name,S_IWRITE);
				if (unlink(entry->d_name))
					throw FileDeleteError(entry->d_name);
      }
		}
		closedir(dp);

		// Remove the directory from its parent
		chdir("..");
		if (rmdir(dir.c_str()))
      throw DirectoryDeleteError(dir);
	}

}} // of namespace svnstat and namespace theplu
