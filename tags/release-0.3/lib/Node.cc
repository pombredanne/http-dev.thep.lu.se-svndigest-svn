// $Id: Node.cc 129 2006-08-02 17:56:18Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Node.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

namespace theplu{
namespace svnstat{

	Node::Node(const u_int level, const std::string& path, 
						 const std::string& output="")
		: level_(level), path_(path), stats_(path), svninfo_(path)
	{ 
		output_name_ = output+file_name(path_); 
	}

	bool Node::dir(void) const
	{
		return false;
	}

	std::string Node::html_tablerow(const std::string& css_class) const
	{
		std::stringstream ss;
		ss << "<tr class=\"" << css_class << "\">\n"
			 << "<td" << html_link() << "</td>\n" 
			 << "<td>" << stats_.lines() << "</td>\n"
			 << "<td>" << stats_.code() << "</td>\n"
			 << "<td>" << stats_.comments() << "</td>\n"
			 << "<td>" << stats_.last_changed_rev() << "</td>\n"
			 << "<td>" << author() << "</td>\n"
			 << "</tr>\n";
		return ss.str();
	}

	void Node::print_footer(std::ostream& os) const
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		os << "<p align=center><font size=-2>\nGenerated on "
			 << asctime (timeinfo) 
			 << "by <a href=http://lev.thep.lu.se/trac/svnstat/>"
			 << PACKAGE_STRING << "</a>"
			 << "</font>\n</p>\n</div>\n</body>\n</html>\n";
	}


	void Node::print_header(std::ostream& os) const
	{
		os << "<!DOCTYPE html\n"
			 << "PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
			 << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			 << "<html xmlns=\"http://www.w3.org/1999/xhtml\""
			 << " xml:lang=\"en\" lang=\"en\"><head>\n"
			 << "<head>\n"
			 << "<title> svnstat " << name() << "</title>\n"
			 << "</head>\n"
			 << "<link rel=\"stylesheet\" "
			 << "href=\"";
		for (u_int i=0; i<level_; ++i)
			os << "../";
		os << "svnstat.css\" type=\"text/css\" />\n"
			 << "<body>\n"
			 << "<div id=\"menu\">"
			 << "<ul><li></li>"
			 << "<li><a href=\"";
		for (u_int i=0; i<level_; ++i)
			os << "../";
		os << "index.html\">Home</a></li>"
			 << "</ul></div>"
			 << "<div id=\"main\">"
			 << "<h2>" << output_name() << "</h2>\n";
	}

}} // end of namespace svnstat and namespace theplu
