// $Id: rmdirhier.h 100 2006-06-19 09:53:16Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svnstat_rmdirhier_
#define _theplu_svnstat_rmdirhier_

#include <stdexcept>
#include <string>

namespace theplu {
namespace svnstat {

	struct DirectoryError : public std::runtime_error
	{ inline DirectoryError(const std::string& s) : runtime_error(s) {} };

	struct BadDirectory : public DirectoryError
	{ inline BadDirectory(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryOpenError : public DirectoryError
	{ inline DirectoryOpenError(const std::string& s) : DirectoryError(s) {} };

	struct FileDeleteError : public DirectoryError
	{ FileDeleteError(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryDeleteError : public DirectoryError
	{ DirectoryDeleteError(const std::string& s) : DirectoryError(s) {} };

	void rmdirhier(const std::string& path);

}} // of namespace svnstat and namespace theplu

#endif
