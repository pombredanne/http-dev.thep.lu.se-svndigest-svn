// $Id: Gnuplot.h 145 2006-08-09 21:36:31Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat

	svnstat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svnstat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svnstat_gnuplot_
#define _theplu_svnstat_gnuplot_

#include <cassert>
#include <cstdio>
#include <list>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>


#include <iostream>

namespace theplu {
namespace svnstat {

	///
	/// If something goes wrong in the use of the Gnuplot class, a
	/// GnuplotException is thrown.
	///
	struct GnuplotException : public std::runtime_error
	{ inline GnuplotException(const std::string& msg) : runtime_error(msg) {} };

	///
	/// The Gnuplot class creates a pipe to 'gnuplot' and facilitates
	/// communication with the gnuplot binary.
	///
	/// @see GnuplotFE
	///
	class Gnuplot
	{
	public:
		///
		/// This constructor sets up the pipe to the first gnuplot
		/// executable found in the PATH environment variable. The PATH
		/// variable must exist.
		///
		/// @throw Throws a GnuplotException if a no PATH variable is
		/// found, if the gnuplot binary cannot be found, or if a pipe
		/// cannot to the gnuplot binary cannot be established.
		///
		Gnuplot(void);

		///
		/// The destructor, closes the pipe to the gnuplot binary.
		///
		virtual ~Gnuplot(void);

		///
		/// @brief Send arbitrary commands to Gnuplot.
		///
		/// @return 0 on success (underlying fputs, fputc, and fflush are
		/// succesful), EOF otherwise.
		///
		int command(const std::string&);

		///
		/// Set the \a style of the line in the subsequent plot or
		/// replot calls. The setting applies until this function is
		/// called again.
		///
		/// @note The \a style is not checked to be valid.
		///
		inline void linestyle(const std::string& style) { linestyle_=style; }

		///
		/// Set the \a title of the line in the subsequent plot or
		/// replot calls. The setting applies until this function is
		/// called again.
		///
		inline void linetitle(const std::string& title) { linetitle_=title; }

		///
		/// Plot the data \a y as a function of \a x using the Gnuplot
		/// 'plot' command. The \a x vector can be omitted as in normal
		/// Gnuplot usage.
		///
		/// @return 0 on success, EOF otherwise.
		///
		template <class T1, class T2>
		int plot(const std::vector<T1>& y, const std::vector<T2>& x)
		{ return plot(y,x,"plot"); }

		template <class T1>
		int plot(const std::vector<T1>& y,const std::vector<T1>& x=std::vector<T1>())
		{ return plot(y,x,"plot"); }

		///
		/// Plot the data \a y as a function of \a x using the Gnuplot
		/// 'replot' command. The \a x vector can be omitted as in normal
		/// Gnuplot usage.
		///
		/// @return 0 on success, EOF otherwise.
		///
		template <class T1, class T2>
		int replot(const std::vector<T1>& y, const std::vector<T2>& x)
		{ return plot(y,x,"replot"); }

		template <class T1>
		int replot(const std::vector<T1>& y,
							const std::vector<T1>& x=std::vector<T1>())
		{ return plot(y,x,"replot"); }

	private:
		///
		/// Copy constructor, not implemented.
		///
		Gnuplot(const Gnuplot&);

		void acquire_program_path(const std::string&);

		///
		/// @param \a plotcmd must be "plot" or "replot".
		///
		template <class T1, class T2>
		int plot(const std::vector<T1>& y, const std::vector<T2>& x,
						 const std::string& plotcmd)
		{
			assert(x.size()==y.size() || x.empty());
			assert(plotcmd=="plot" || plotcmd=="replot");

			std::ostringstream cmdstring;
			cmdstring << plotcmd << " '-' u 1:2 title '" << linetitle_ << "' with "
								<< linestyle_ << '\n';
			for (size_t i=0; i<y.size(); ++i) {
				if (!x.empty())
					cmdstring << x[i] << '\t';
				else
					cmdstring << i << '\t';
				cmdstring << y[i] << '\n';
			}
			cmdstring << "e\n";

 			return command(cmdstring.str());
		}

		void tokenizer(const std::string& in, std::list<std::string>& tokens,
									 const std::string& delimiters = ":");

		std::string gnuplot_binary_;
		std::string linestyle_;
		std::string linetitle_;
		FILE* pipe_;
};

}} // end of namespace svnstat and namespace theplu

#endif
