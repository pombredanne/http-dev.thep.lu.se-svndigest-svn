#!/bin/bash

# $Id: svnstat.sh 110 2006-06-28 14:46:52Z jari $

# Copyright (C) 2006 Jari H�kkinen
#
# This file is part of svnstat, http://lev.thep.lu.se/trac/svnstat
#
# svnstat is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# svnstat is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

cd /export/home/trac/tracs/svnstat/htdocs/pristine_svnstat
svn diff -rHEAD > tmp.txt
if [ -s "tmp.txt" ]; then
        svn update
	svnstat -f \
		-r /export/home/trac/tracs/svnstat/htdocs/pristine_svnstat \
		-t /export/home/trac/tracs/svnstat/htdocs/svnstat
fi
rm tmp.txt
