$Id: README.developer 731 2008-12-15 19:03:04Z peter $

= Developer Information =

This file contains developer specific information.


=== Coding style ===

Developers should follow the coding style outlined in the
[http://www.thep.lu.se/~jari/documents/c++_coding_guidelines/index.html C++ coding guidelines].


=== Subversion usage ===

Commits should be minimalistic and the projects should always compile
(`make` and `make test`) when someone makes a clean checkout. There is
a short introduction to subversion and its usage available as
[http://www.thep.lu.se/~jari/documents/subversion_guidelines/index.html Subversion guidelines].
We follow these guidelines.


=== bootstrap === 

At first checkout from the repository, or when `configure.ac` is
changed, you must run

  `./bootstrap` 

to setup autoconf files.


=== configure ===

Run the generated configure script with 

  `./configure --enable-debug --enable-wctests`

The option flag `--enable-debug` turns on debug options (e.g. enables
assertions). The option flag `--enable-wctests` turns on some
additional tests. All tests are not enabled by default since some of
the tests depend on being run from a subversion WC.


=== Testing ===

Test programs should be created and added to the `make check`
execution. Always check that the test runs before committing code to
the repository, i.e., run `make check`.

There is a test repository in source:trunk/test/repo that is a part of
the svndigest repository. The logic behind this is that it is nice to
have a common test repository that can be used off-line. Changes to
the repository are usually made to create new test cases and it is
important that these changes are committed to svndigest repository as
well. All changes to the test repository are not automatically
propagated to the svndigest repository and it is important that some
files created in the test repository are manually added to the
svndigest repository. Assuming that `make check` is run before a
commit, a warning is generated if the test repository is inconsistent
with the svndigest repository. There will be a diagnostic message and
a resolution hint, however the developer must examine and resolve the
problem manually.

The problem is simply that the file source:trunk/test/repo/db/current
is changed compared to the svndigest repository. The `current` file
indicates a specific number of change sets within the test repository
and the directories source:trunk/test/repo/db/revs and
source:trunk/test/repo/db/revprops contains a matching number of
change sets. However, the change sets added to the test repository are
not automatically added to the svndigest repository, therefore the new
files in the `revs` and `revprops` directories must be added manually
to the svndigest repository.

There is also a race condition when developers make changes to the
test repository simultaneously: It is easy to create severe conflicts
in the test repository. Imaging the case when two developers adds test
commits to the test repository, and then tries to add these to the
svndigest repository. Since there are different versions of the same
test repository revisions, the second developer committing to the
svndigest will get messages about files being in the way. This is just
one situation with possible conflicts. This conflict is of course
resolved by the second developer having to throw away his changes and
redo them after a new checkout of the test repository.

The take home message is that developers should communicate with each
other when changing the test repository in order to avoid to large
repository conflicts. AND read the output of `make check`.


=== Create a distribution ===

Create a distribution package with `make dist`. Remember to test
the package with `make distcheck`. If required packages are installed
in non-standard locations you need to pass this information to
`distcheck`. This is done by using the variable
DISTCHECK_CONFIGURE_FLAGS as

   `DISTCHECK_CONFIGURE_FLAGS="--with-apr=/usr/local/apr" make distcheck`

Usually you need to set DISTCHECK_CONFIGURE_FLAGS to whatever options
you pass to configure with the exception of `--enable-wctests`. 
Passing `--enable-wctests` to distcheck will always make the `make` to
fail because a distribution does not contain all files needed for WC
tests.


== Releasing ==

See http://dev.thep.lu.se/svndigest/wiki/ReleaseProcedure


-------------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari H�kkinen
Copyright (C) 2007 Jari H�kkinen, Peter Johansson
Copyright (C) 2008 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

}}}
