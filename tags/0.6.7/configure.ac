## Process this file with autoconf to produce a configure script.
##
## $Id: configure.ac 764 2009-01-31 19:41:08Z peter $
##
## If you grabbed the source from subversion you should, at top-level,
## execute:
##          ./bootstrap

# Copyright (C) 2005 Jari H�kkinen, Peter Johansson
# Copyright (C) 2006 Jari H�kkinen
# Copyright (C) 2007 Jari H�kkinen, Peter Johansson
# Copyright (C) 2008, 2009 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

AC_PREREQ(2.57)
AC_INIT([svndigest],[0.6.7],[jari@thep.lu.se])
AC_CONFIG_SRCDIR([lib/File.h])
AC_CONFIG_AUX_DIR([autotools])
AC_PREFIX_DEFAULT([/usr/local])
test $prefix = NONE && prefix=/usr/local

AM_CONFIG_HEADER([config.h])
AM_INIT_AUTOMAKE([std-options check-news])

AC_LANG([C++])

# Checks for programs.
AC_PROG_CXXCPP
AC_PROG_CXX
AC_PROG_INSTALL
AC_PROG_LIBTOOL
AC_PROG_RANLIB
AC_CHECK_PROG([GNUPLOT],[gnuplot],[ok])

# -Wno-long-long is needed to suppress compiler diagnostics regarding
# using extension beyond the C++ standard (usage of non C++ standard
# 'long long' types).
CXXFLAGS="-Wall -pedantic -Wno-long-long"

AC_ARG_ENABLE(debug,
	[AS_HELP_STRING([--enable-debug],[turn on debug options and code])],
	[CXXFLAGS="$CXXFLAGS -g -O"], 
	[CPPFLAGS="$CPPFLAGS -DNDEBUG" CXXFLAGS="$CXXFLAGS -O3"])

AC_ARG_ENABLE(wctests,
	[AS_HELP_STRING([--enable-wctests],[turn on all tests, tests
	requiring a subversion WC are disabled by default])], [wctests=true])
AM_CONDITIONAL(WCTESTS, test x$wctests = xtrue)

# optionally prepare for building static libraries.
AC_ARG_ENABLE(staticbin,
	[AS_HELP_STRING([--enable-staticbin], [create a static binary,
	at least as static as the available underlying libraries
	allows])])
if test "$enable_staticbin" = "yes"; then
	case $host in
	*-apple-darwin*)
		# At the time of creating this libsvn_subr uses
		# Keychain on Mac OSX. In consequence the below
		# frameworks are needed for succesful static builds.
		LIBS="$LIBS -framework Security"
		LIBS="$LIBS -framework CoreFoundation"
		LIBS="$LIBS -framework CoreServices"
		;;
	esac
	STATICFLAG=-static
	AC_SUBST(STATICFLAG)
fi

# Apache Portable Runtime (APR) API checks
# The next three lines are not needed as long as APR_FIND_APR is used.
# AC_ARG_WITH(apr,
#  [  --with-apr=DIR          prefix for installed APR or path to APR build
#                          tree [[PREFIX]]])
# Include APR_FIND_APR macro distributed within the APR project. If
# the usage of the APR macro is to be omitted then the construct for
# setting the CXXFLAGS (header file location) and LDFLAGS (linking
# informaion) for APR must be changed. The latter can be achieved with
# AC_SEARCH_LIBS([apr_allocator_create],[apr-0],,apr_found="no") but
# apr-0 must be prior knowledge.
sinclude(./build_support/find_apr.m4)
APR_FIND_APR(,,1)
if test "$apr_found" = "yes" ; then
		LDFLAGS="`$apr_config --link-ld` $LDFLAGS"
		CPPFLAGS="`$apr_config --includes` $CPPFLAGS"
		AC_CHECK_HEADER([apr_allocator.h],,apr_found="no")
fi

# Subversion API checks
svn_found="yes"
AC_ARG_WITH(svn,
  [AS_HELP_STRING([--with-svn=DIR],[prefix for svn developer files [[PREFIX]]])],
  [ LDFLAGS="-L$withval/lib $LDFLAGS" CPPFLAGS="-I$withval/include $CPPFLAGS"])
AC_CHECK_HEADER([subversion-1/svn_types.h],,svn_found="no")
# The library checks below may match shared libs even when
# --enable-staticbin is given to configure. This should probably not
# pose any problems since in a properly installed system the shared
# and static libraries should be the same.
AC_SEARCH_LIBS([svn_cmdline_setup_auth_baton],[svn_subr-1],,svn_found="no")
AC_SEARCH_LIBS([svn_ra_initialize],[svn_ra-1],,svn_found="no")
AC_SEARCH_LIBS([svn_wc_adm_open3],[svn_wc-1],,svn_found="no")
AC_SEARCH_LIBS([svn_diff_file_options_create],[svn_diff-1],,svn_found="no")
AC_SEARCH_LIBS([svn_client_log3],[svn_client-1],,svn_found="no")

AC_CONFIG_FILES([Makefile
								bin/Makefile
								lib/Makefile
								test/Makefile])

# Print failure status information about selected items, and exit if
# fatal errors were encountered. No output will be created if
# configure is halted prematurely.

# used to trigger exit before creation of output
all_reqs_ok="true"

# Non-existing APR is fatal -- sub-sequent compilation will fail.
if (test "$apr_found" = "no") ; then
		AC_MSG_WARN([APR not found. The Apache Portable Runtime
		(APR) library cannot be found. Please make sure APR is installed
		and supply the appropriate --with-apr option to 'configure'.])
		all_reqs_ok="false"
fi

# Non-existing subversion API is fatal -- sub-sequent compilation will fail.
if (test "$svn_found" = "no") ; then
		svn_msg="Subversion API not found. Subversion API libraries cannot
		be found. Make sure the APIs are installed and supply the
		appropriate --with-svn option to 'configure'."
if (test "$apr_found" = "no") ; then
		svn_msg="$svn_msg
		Note, APR was not found. Failure to locate APR affects the
		location of the subversion API. Please fix the APR problem before
		trying to resolve the subversion related issues."
fi
		AC_MSG_WARN([$svn_msg])
		all_reqs_ok="false"
fi

if (test "$all_reqs_ok" = "false") ; then
		AC_MSG_ERROR([Some pre-requisites were not fulfilled, aborting
		configure. Please consult the 'README' file for more information
		about what is needed to compile svndigest and refer to above
		warning messages. Needed files were NOT created.])
fi

# Create output.
AC_OUTPUT

# Some more messages.
AC_MSG_NOTICE([])
AC_MSG_NOTICE([   Ready to compile the executables of svndiget])
AC_MSG_NOTICE([   The following flags and libs will be used:])
AC_MSG_NOTICE([   +++++++++++++++++++++++++++++++++++++++++++++++])
AC_MSG_NOTICE([    Preprocessor flags: CPPFLAGS=\"$CPPFLAGS\"])
AC_MSG_NOTICE([    C++ flags:          CXXFLAGS=\"$CXXFLAGS\"])
AC_MSG_NOTICE([    Linker flags:       LDFLAGS=\"$LDFLAGS\"])
AC_MSG_NOTICE([    LIBS:               LIBS=\"$LIBS\"])
AC_MSG_NOTICE([   +++++++++++++++++++++++++++++++++++++++++++++++])
AC_MSG_NOTICE([])

if (test "$wctests") ; then
		AC_MSG_NOTICE([WC dependent tests are enabled])
else
		AC_MSG_NOTICE([WC dependent tests are disabled ... okay for most users])
		AC_MSG_NOTICE([Developers, use --enable-wctests to enable WC tests])
fi
AC_MSG_NOTICE([])

# Failure to locate gnuplot is not considered fatal
if (test "$GNUPLOT" != "ok") ; then
		AC_MSG_WARN([Gnuplot was not found. svndigest will compile
		without gnuplot but will throw an exception at run-time. Please
		install gnuplot (available for a wide range of operating systems
		at http://www.gnuplot.info).])
		AC_MSG_NOTICE([])
fi

if test "$enable_staticbin" = "yes"; then
		AC_MSG_NOTICE([A statically linked 'svndigest' binary will be created.])
else
		AC_MSG_NOTICE([A dynamically linked 'svndigest' binary will be created.])
fi
		AC_MSG_NOTICE([])

AC_MSG_NOTICE([Now type 'make ; make check'.])
