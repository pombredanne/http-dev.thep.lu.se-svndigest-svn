// $Id: html_utility.cc 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson
	Copyright (C) 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "html_utility.h"

#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include <config.h>	// this header file is created by configure

#include <sstream>
#include <string>

namespace theplu{
namespace svndigest{

	std::string anchor(const std::string& url,
										 const std::string& name, u_int level, 
										 const std::string& title)
	{
		std::stringstream ss;
		HtmlStream hs(ss);
		ss << "<a title=\"";
		hs << title; 
		ss << "\" href=\"";
		for (size_t i=0; i<level; ++i)
			ss << "../";
		ss << url; 
		ss << "\">"; 
		hs << name; 
		ss << "</a>";
		return ss.str();
	}

	
	void print_footer(std::ostream& os)
	{
		Date date;
		os << "<p class=\"footer\">\nGenerated on "
			 << date("%a %b %d %H:%M:%S %Y") << " (UTC) by "
			 << anchor("http://dev.thep.lu.se/svndigest/", 
								 PACKAGE_STRING, 0, "")
			 << "\n</p>\n</div>\n</body>\n</html>\n";
	}


	void print_header(std::ostream& os, std::string title, u_int level,
										std::string user, std::string item, std::string path)
	{
		os << "<!DOCTYPE html\n"
			 << "PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
			 << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			 << "<html xmlns=\"http://www.w3.org/1999/xhtml\""
			 << " xml:lang=\"en\" lang=\"en\">\n"
			 << "<head>\n"
			 << "<title> " << title << " - svndigest</title>\n"
			 << "<link rel=\"stylesheet\" "
			 << "href=\"";
		for (u_int i=0; i<level; ++i)
			os << "../";
		os << "svndigest.css\" type=\"text/css\" />\n"
			 << "</head>\n"
			 << "<body>\n"
			 << "<div id=\"menu\">"
			 << "<ul><li></li>";
		if (item=="main")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor("index.html", "Main", level, "Main page");
		os << "</li>";

		if (item=="total")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(user+"/total/"+path, "Total", level, 
					 "View statistics of all lines");
		os << "</li>";

		if (item=="code")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(user+"/code/"+path, "Code", level, 
								 "View statistics of code lines");
		os << "</li>";

		if (item=="comments")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(user+"/comments/"+path, "Comment", level, 
								 "View statistics of comment lines");
		os << "</li>";


		if (item=="empty")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(user+"/empty/"+path, "Other", level, 
								 "View statistics of other lines");
		os << "</li>"
			 << "</ul></div>"
			 << "<div id=\"main\">\n";
	}


	std::string trac_revision(size_t r)
	{
		const Configuration& conf = Configuration::instance();
		std::stringstream ss;
		if (conf.trac_root().empty())
			ss << r; 
		else {
			std::stringstream rev;
			rev << r;
			ss << anchor(conf.trac_root()+"changeset/"+rev.str(), rev.str());
		}
		return ss.str();
	}

}} // end of namespace svndigest and namespace theplu
