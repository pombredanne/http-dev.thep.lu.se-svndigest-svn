// $Id: Commitment.cc 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2007, 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Commitment.h"
#include "Date.h"

#include <string>

namespace theplu {
namespace svndigest {


	Commitment::Commitment(void)
	{
	}


	Commitment::Commitment(std::string author, const Date& date, 
												 std::string msg, size_t rev)
		: author_(author), date_(date), msg_(msg), rev_(rev)
	{
	}


}} // end of namespace svndigest and namespace theplu
