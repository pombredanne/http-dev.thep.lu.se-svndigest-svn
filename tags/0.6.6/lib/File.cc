// $Id: File.cc 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2005, 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "File.h"

#include "Alias.h"
#include "Date.h"
#include "GnuplotFE.h"
#include "html_utility.h"
#include "HtmlStream.h"
#include "Stats.h"
#include "SVNblame.h"
#include "SVNlog.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <string>

namespace theplu{
namespace svndigest{


	File::File(const u_int level, const std::string& path, 
						 const std::string& output) 
		: Node(level,path,output) 
	{
		output_dir_=output;
		if (!output_dir_.empty())
			output_dir_+='/';
	}


	std::string File::href(void) const
	{ 
		return name()+".html"; 
	}


	std::string File::node_type(void) const
	{
		return std::string("file");
	}


	std::string File::output_path(void) const
	{
		return output_dir()+name()+".html";
	}


	const Stats& File::parse(const bool verbose)
	{
		if (verbose)
			std::cout << "Parsing " << path_ << std::endl; 
		stats_.reset();
		stats_.parse(path_);
		return stats_;
	}


	void File::print_blame(std::ofstream& os) const
	{
		os << "<br /><h3>Blame Information</h3>";
		os << "<table class=\"blame\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th class=\"rev\">Rev</th>\n";
		os << "<th class=\"date\">Date</th>\n";
		os << "<th class=\"author\">Author</th>\n";
		os << "<th class=\"line\">Line</th>\n";
		os << "<th></th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";
		HtmlStream hs(os);
		SVNblame blame(path_);
		Parser parser(path_);
		std::vector<Parser::line_type>::const_iterator 
			line_type(parser.type().begin());
		int last=0;
		int first=0;
		bool using_dates=true;
		if (GnuplotFE::instance()->dates().empty()){
			using_dates=false;
			last = stats_.revision();
		}
		else {
			last = Date(GnuplotFE::instance()->dates().back()).seconds();
			// earliest date corresponds either to revision 0 or revision 1 
			first = std::min(Date(GnuplotFE::instance()->dates()[0]).seconds(),
											 Date(GnuplotFE::instance()->dates()[1]).seconds());
		}
		// color is calculated linearly on time, c = kt + m
		// brightest color (for oldest rev in log) is set to 192.
		double k = 192.0/(first-last);
		double m = -last*k; 
		while (blame.valid()) {
			std::string color;
			if (using_dates)
				color = hex(static_cast<int>(k*Date(blame.date()).seconds()+m),2);
			else
				color = hex(static_cast<int>(k*blame.revision()+m),2);
			os << "<tr>\n<td class=\"rev\"><font color=\"#" << color 
				 << color << color << "\">" << blame.revision()
				 << "</font></td>\n<td class=\"date\"><font color=\"#" << color 
				 << color << color << "\">" ;
			hs << Date(blame.date())("%d %b %y");
			os << "</font></td>\n<td class=\"author\">";
			hs << blame.author();
			os << "</td>\n<td class=\"";
			assert(line_type!=parser.type().end());
			if (*line_type==Parser::empty)
				os << "line-other";
			else if (*line_type==Parser::comment)
				os << "line-comment";
			else
				os << "line-code";
			os << "\">" << blame.line_no()+1
				 << "</td>\n<td>";
			hs << blame.line();
			os << "</td>\n</tr>\n";
			blame.next_line();
			++line_type;
		}
		os << "</tbody>\n";
		os << "</table>\n";
	}


	void File::print_copyright(std::map<std::string, Alias>& alias) const 
	{
		if (ignore())
			return;
		using namespace std;

		SVNlog log(path());

		map<int, set<Alias> > year_authors;

		assert(log.author().size()==log.date().size());
		vector<string>::const_iterator author=log.author().begin();
		for (vector<string>::const_iterator date=log.date().begin();
				 date!=log.date().end(); ++date, ++author) {
			time_t sec = str2time(*date);
			tm* timeinfo = gmtime(&sec);

			// find username in map of aliases
			std::map<string,Alias>::iterator name(alias.lower_bound(*author));

			// if alias exist insert alias
			if (name != alias.end() && name->first==*author)
				year_authors[timeinfo->tm_year].insert(name->second);
			else {
				// else insert user name
				Alias a(*author,alias.size());
				year_authors[timeinfo->tm_year].insert(a);
				std::cerr << "svndigest: warning: no copyright alias found for `" 
									<< *author << "`\n";
				// insert alias to avoid multiple warnings.
				alias.insert(name, std::make_pair(*author, a));
			}
		}

		// Code copied from Gnuplot -r70
		char tmpname[]="/tmp/svndigestXXXXXX";
		int fd=mkstemp(tmpname);	// mkstemp return a file descriptor
		if (fd == -1)
			throw std::runtime_error(std::string("Failed to get unique filename: ") +
															 tmpname);
		// Jari would like to do something like 'std::ofstream tmp(fd);'
		// but has to settle for (which is stupid since the file is
		// already open for writing.
		std::ofstream tmp(tmpname);

		ifstream is(path().c_str());
		assert(is.good());
		string line;
		bool found_copyright = false;
		bool after_copyright = false;
		string prefix;
		while(getline(is, line)){
			if (after_copyright) {
				tmp << line;
				if (is.good()) // not end of file
					tmp << "\n";
				else { // check if file ends with newline character
					is.unget();
					char c = is.get();
					if (c=='\n') 
						tmp << "\n";
				}
			}
			else if (found_copyright){
				// check if line is end of copyright statement, i.e. contains
				// no alphanumerical character
				after_copyright = true;
				for (size_t i=0; i<line.size()&&after_copyright; ++i)
					if (isalnum(line[i]))
						after_copyright = false;
				if (after_copyright)
					tmp << line << "\n";
					
			}
			else {
				// check whether copyright starts on this line
				string::iterator i = search(line.begin(), line.end(), "Copyright (C)");
				if (i==line.end()) {
					tmp << line << "\n";
				}			
				else {
					prefix = line.substr(0, distance(line.begin(), i));
					found_copyright = true;
					// Printing copyright statement
					for (map<int, set<Alias> >::const_iterator i(year_authors.begin());
							 i!=year_authors.end();) {
					tmp << prefix << "Copyright (C) "
							<< 1900+i->first;
					map<int, set<Alias> >::const_iterator j = i;
					assert(i!=year_authors.end());
					while (++j!=year_authors.end() && 
								 i->second == j->second){
						tmp << ", " << 1900+(j->first);
					}
					// printing authors
					std::vector<Alias> vec_alias;
					back_insert_iterator<std::vector<Alias> > ii(vec_alias);
					std::copy(i->second.begin(), i->second.end(), ii);
					// sort with respect to id
					std::sort(vec_alias.begin(), vec_alias.end(), IdCompare());
					for (std::vector<Alias>::iterator a=vec_alias.begin();
							 a!=vec_alias.end(); ++a){
						if (a!=vec_alias.begin())
							tmp << ",";
						tmp << " " << a->name();
					}
					tmp << "\n";
					i = j;
					}
				}
			}
		}
		is.close();
		tmp.close();
		close(fd);
		// finally copy temporary file to replace original file, and
		// remove the temporary file
		try {
			copy_file(tmpname, path());
		}
		catch (std::runtime_error e) {
			// catch exception, cleanup, and rethrow
			std::cerr << "File::print_copyright: Exception caught, "
								<< "removing temporary file " << tmpname << std::endl;
			if (unlink(tmpname))
				throw runtime_error(std::string("File::print_copyright: ") +
														"failed to unlink temporary file" + tmpname);
			throw;
		}
		if (unlink(tmpname))
			throw runtime_error(std::string("File::print_copyright: ") +
													"failed to unlink temporary file" + tmpname);
	}


	void File::print_core(const bool verbose) const 
	{
	}


	void File::print_core(const std::string& user, const std::string& line_type,
												const SVNlog& log) const 
	{
		std::string outpath = user+"/"+line_type+"/"+local_path();
		std::string imagefile = "images/"+line_type+"/"+local_path_+".png";
		std::string html_name(outpath + ".html");
		std::ofstream os(html_name.c_str());
		print_header(os, name(), level_+2, user, line_type, local_path()+".html");
		path_anchor(os);

		os << "<p class=\"plot\">\n<img src='"; 
		for (size_t i=0; i<level_; ++i)
			os << "../";
		os << "../../";
		if (user=="all")
			os << stats_.plot(imagefile,line_type);
		else
			os << imagefile;
		os << "' alt='[plot]' />\n</p>";

		print_author_summary(os, line_type, log);
		os << "\n";

		print_blame(os);

		print_footer(os);
		os.close();	
	}

}} // end of namespace svndigest and namespace theplu
