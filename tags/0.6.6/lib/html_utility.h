#ifndef _theplu_svndigest_html_utility_
#define _theplu_svndigest_html_utility_

// $Id: html_utility.h 685 2008-08-04 14:53:58Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson
	Copyright (C) 2008 Jari H�kkinen

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <fstream>
#include <iosfwd>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	///
	/// @brief create anchor
	///
	/// @param url address to link to
	/// @param name text visible on page
	/// @param level '../' is added @a level times before @a href
	/// @param title title of anchor
	///
	std::string anchor(const std::string& url,
										 const std::string& name, unsigned int level=0, 
										 const std::string& title="");

	///
	/// @Brief print html footer of page
	///
	void print_footer(std::ostream&);
		
	///
	/// @brief print html header of page
	///
	/// \param os stream to print to
	/// \param name
	/// \param level
	/// \param 
	void print_header(std::ostream& os, std::string name, unsigned int level, 
										std::string user, std::string item, std::string path);


	///
	/// @return if trac-revision is set in config file anchor to trac is
	/// given otherwise just a string corresponding to passed parameter.
	///
	std::string trac_revision(size_t);

}} // end of namespace svndigest end of namespace theplu

#endif 
