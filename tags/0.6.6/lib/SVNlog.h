#ifndef _theplu_svndigest_svnlog_
#define _theplu_svndigest_svnlog_

// $Id: SVNlog.h 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVN.h"

#include "Commitment.h"

#include <string>
#include <vector>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	/**
		 The SVNlog class is a utility class for taking care of 'svn
		 log' information. An 'svn log' is performed on an item, the
		 log information for each revision is stored in vectors.
	*/
	class SVNlog {
	public:

		/**
			 \brief The contructor.
			 
			 The constructor performs an 'svn log' on \a path and
			 stores the information for later access.
		*/
		explicit SVNlog(const std::string& path);

		/**
			 \brief The destructor.
		*/
		~SVNlog(void);

		/**
			 \return Authors
		*/
		inline const std::vector<std::string>& author(void) const
		{ return lb_.authors; }

		/**
			 \return Dates
		*/
		inline const std::vector<std::string>& date(void) const
		{ return lb_.commit_dates; }

		/**
			 \return true if \a author appears in log.
		*/
		bool exist(std::string author) const;

		/**
			 \return Messages
		*/
		inline const std::vector<std::string>& message(void) const
		{ return lb_.msg; }

		/**
			 \return Latest commit
		*/
		Commitment latest_commit(void) const;

		/**
			 \return Latest commit \a author did. If no author is found an
			 empty Commitment (default constructor) is returned.
		*/
		Commitment latest_commit(std::string author) const;

		/**
			 \return Revisions
		*/
		inline const std::vector<size_t>& revision(void) const
		{ return lb_.rev; }

	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNlog(const SVNlog&);

		/**
			 log information is stored in the log_receiver_baton. The
			 information is retrieved with the info_* set of member
			 functions. The struct is filled in the info_receiver function.

			 \see info_receiver
		*/
		struct log_receiver_baton {
			std::vector<std::string> authors;
			std::vector<std::string> commit_dates;
			std::vector<std::string> msg;
			std::vector<size_t> rev;
		} lb_;

		/**
			 info_receiver is the function passed to the underlying
			 subversion API. This function is called by the subversion API
			 for every item matched by the conditions of the API call.

			 \see Subversion API documentation
		*/
		// The return type should be svn_log_message_receiver_t but I
		// cannot get it to compile. The svn API has a typedef like
		// typedef svn_error_t*(* svn_log_message_receiver_t)(void *baton,
		// apr_hash_t *changed_paths, svn_revnum_t revision, const char
		// *author, const char *date,const char *message, apr_pool_t
		// *pool) for svn_log_message_receiver_t.
		static svn_error_t*
		log_message_receiver(void *baton, apr_hash_t *changed_paths,
												 svn_revnum_t rev, const char *author, const char *date,
												 const char *msg, apr_pool_t *pool);
	};

}} // end of namespace svndigest and namespace theplu

#endif
