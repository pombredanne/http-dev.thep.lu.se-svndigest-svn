// $Id: HtmlStream.cc 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "HtmlStream.h"

#include <string>

namespace theplu{
namespace svndigest{

	HtmlStream::HtmlStream(std::ostream& os)
		: os_(os)
	{
		map_['"']=std::string("\"");
		map_['\'']=std::string("\'");
		map_['\n']=std::string("<br />");
		map_['<']=std::string("&lt;");
		map_['>']=std::string("&gt;");
		map_['&']=std::string("&amp;");
		map_[' ']=std::string("&nbsp;");
		// This should be configurable, but for now indentation is two spaces.
		map_['\t']=std::string("&nbsp;&nbsp;");
		map_['�']=std::string("&aring;");
		map_['�']=std::string("&auml;");
		map_['�']=std::string("&ouml;");
		map_['�']=std::string("&Aring;");
		map_['�']=std::string("&Auml;");
		map_['�']=std::string("&Ouml;");
		map_['�']=std::string("&eacute;");
		map_['�']=std::string("&Eacute;");
		map_['�']=std::string("&aacute;");
		map_['�']=std::string("&Aacute;");
	}


	HtmlStream::~HtmlStream(void)
	{
	}


	void HtmlStream::print(std::stringstream& ss)
	{
		char c;
		while (true){
			ss.get(c);
			if (!ss.good())
				return;
			std::map<char, std::string>::const_iterator i = map_.find(c);
			if (i==map_.end())
				os_ << c;
			else
				os_ << i->second;
		}
	}

	
	std::ostream& HtmlStream::stream(void)
	{
		return os_;
	}

}} // end of namespace svndigest and namespace theplu
