// $Id: utility.cc 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "utility.h"

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/param.h>
#include <unistd.h>

#include <iostream>

namespace theplu{
namespace svndigest{

	int access_rights(const std::string& path, const std::string& bits)
	{
		if (access(path.c_str(),F_OK)) {
			throw std::runtime_error(std::string("access_rights: ") + path +
															 "' does not exist.");
		}
		int mode=0;
		for (u_int i=0; i<bits.length(); ++i)
			switch (bits[i]) {
					case 'r':
						mode|=R_OK;
						break;
					case 'w':
						mode|=W_OK;
						break;
					case 'x':
						mode|=X_OK;
						break;
			}
		return access(path.c_str(),mode);
	}


	void copy_file(const std::string& source, const std::string& target)
	{
		std::ofstream o(target.c_str());
		std::ifstream i(source.c_str());
		while (i.good()) {
			char ch=i.get();
			if (i.good())
				o.put(ch);
			if (!o.good())
				throw std::runtime_error(std::string("copy_file: ") +
																 "writing target file failed '" + target + "'");
		}
 		if (!i.eof() && (i.bad() || i.fail()))	// fail on everything except eof
			throw std::runtime_error(std::string("copy_file: ") +
															 "error reading source file '" + source + "'");
		i.close(); o.close();
	}


	std::string	file_name(const std::string& full_path)
	{
		std::stringstream ss(full_path);
		std::string name;
		while (getline(ss,name,'/')) {}
		return name;
	}


	std::string getenv(const std::string& var)
	{
		char* buffer=std::getenv(var.c_str());
		if (!buffer)
			throw std::runtime_error("Environment variable "+var+" is not set");
		return std::string(buffer);
	}


	std::string hex(int x, u_int width)
	{
		std::stringstream ss;
		ss << std::hex << x;
		if (!width)
			return ss.str();
		if (ss.str().size()<width) 
			return std::string(width-ss.str().size(), '0') + ss.str();
		return ss.str().substr(0, width);
	}


	std::string htrim(std::string str)
	{
		size_t length=str.size();
		while(length && isspace(str[length-1]))
			--length;
		return str.substr(0,length);
	}


	bool is_int(std::string s)
	{
		std::stringstream ss(s);
		int a;
		ss >> a;
		if(ss.fail()) 
			return false;
		// Check that nothing is left on stream
		std::string b;
		ss >> b;
		return b.empty();
	}


	std::string ltrim(std::string str)
	{
		size_t i = 0;
		while(i<str.size() && isspace(str[i]))
			++i;
		return str.substr(i);
	}


	int percent(int a, int b)
	{
		if (b)
			return (100*a)/b;
		return 0;
	}


	bool node_exist(const std::string& path)
	{
		struct stat buf;
		return !stat(path.c_str(),&buf);
	}


	std::string pwd(void)
	{
		char buffer[MAXPATHLEN];
		if (!getcwd(buffer, MAXPATHLEN))
			throw std::runtime_error("Failed to get current working directory");
		return std::string(buffer);
	}


	void touch(std::string str)
	{
		if (!node_exist(str)) {
			std::ofstream os(str.c_str());
			os.close();
		}
	}

	time_t str2time(const std::string& str)
	{
		//	str in format 2006-09-09T10:55:52.132733Z
		std::stringstream sstream(str);
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo =  gmtime ( &rawtime );

		u_int year, month, day, hour, minute, second;
		std::string tmp;
		getline(sstream,tmp,'-');
		year=atoi(tmp.c_str());
		timeinfo->tm_year = year - 1900;

		getline(sstream,tmp,'-');
		month=atoi(tmp.c_str());
		timeinfo->tm_mon = month - 1;

		getline(sstream,tmp,'T');
		day=atoi(tmp.c_str());
		timeinfo->tm_mday = day;

		getline(sstream,tmp,':');
		hour=atoi(tmp.c_str());
		timeinfo->tm_hour = hour;

		getline(sstream,tmp,':');
		minute=atoi(tmp.c_str());
		timeinfo->tm_min = minute;

		getline(sstream,tmp,'.');
		second=atoi(tmp.c_str());
		timeinfo->tm_sec = second;

		return mktime(timeinfo);
	}


	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										std::string str)
	{
		if (match_begin(first, last, str)){
			first+=str.size();
			return str;
		}
		return std::string();
	}

	notChar::notChar(char c)
		: char_(c)
	{}


	not2Char::not2Char(char c1, char c2)
		: char1_(c1), char2_(c2)
	{}
		

	not2Str::not2Str(std::string s1, std::string s2)
		: str1_(s1), str2_(s2)
	{}
		
}} // end of namespace svndigest and namespace theplu
