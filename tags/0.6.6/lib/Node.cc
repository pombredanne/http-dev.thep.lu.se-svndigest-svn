// $Id: Node.cc 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2005, 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Node.h"
#include "html_utility.h"
#include "SVNlog.h"
#include "SVNproperty.h"
#include "utility.h"

#include <cassert>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	std::string Node::project_=std::string();

	Node::Node(const u_int level, const std::string& path, 
						 const std::string& local_path)
		: level_(level), path_(path), stats_(path), 
			svninfo_(path)
	{ 
		SVNproperty property(path);
		binary_=property.binary();
		svndigest_ignore_=property.svndigest_ignore();
		if (Node::project_==std::string()) // no root directory in local path
			Node::project_ = file_name(path);
		else if (local_path.empty())
			local_path_ = file_name(path);
		else
			local_path_ = local_path + "/" + file_name(path);

		struct stat nodestat;                // C api from sys/stat.h
		lstat(path.c_str(),&nodestat);   // C api from sys/stat.h
		link_ = S_ISLNK(nodestat.st_mode);
	}


	bool Node::dir(void) const
	{
		return false;
	}


	void Node::html_tablerow(std::ostream& os, 
													 const std::string& css_class,
													 const std::string& user) const
	{
		os << "<tr class=\"" << css_class << "\">\n"
			 << "<td class=\"" << node_type() << "\">";
		if (svndigest_ignore())
			os << name() << " (<i>svndigest:ignore</i>)";
		else if (binary())
			os << name() << " (<i>binary</i>)";
		else if (link_)
			os << name() << " (<i>link</i>)";
		// there is no output for nodes when user has zero contribution
		else if (user!="all" && !stats_.lines(user))
			os << name();
		else
			os << anchor(href(), name()); 
		os << "</td>\n"; 
		if (user=="all") {
			os << "<td>" << stats_.lines() << "</td>\n"
				 << "<td>" << stats_.code() << "</td>\n"
				 << "<td>" << stats_.comments() << "</td>\n"
				 << "<td>" << stats_.empty() << "</td>\n";
		}
		else {
			os << "<td>" << stats_.lines(user); 
			if (stats_.lines(user)) 
				os << " (" << percent(stats_.lines(user),stats_.lines()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.code(user); 
			if (stats_.code(user)) 
				os << " (" << percent(stats_.code(user),stats_.code()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.comments(user); 
			if (stats_.comments(user)) 
				os << " (" << percent(stats_.comments(user),stats_.comments()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.empty(user); 
			if (stats_.empty(user)) 
				os << " (" << percent(stats_.empty(user),stats_.empty()) << "%)"; 
			os << "</td>\n";

		}
		os << "<td>" << trac_revision(stats_.last_changed_rev()) << "</td>\n"
			 << "<td>" << author() << "</td>\n"
			 << "</tr>\n";
	}


	std::string Node::output_dir(void) const
	{
		return output_dir_;
	}


	void Node::path_anchor(std::ostream& os) const
	{
		os << "<h2 class=\"path\">\n";
		std::vector<std::string> words;
		words.reserve(level_+1);
		std::string word;
		words.push_back(Node::project_);
		std::stringstream ss(local_path());
		while(getline(ss,word,'/'))
			if (!word.empty()) // ignore double slash in path
				words.push_back(word);
		if (words.size()==1)
			os << anchor("index.html", Node::project_,0, "View " + Node::project_);
		else {
			for (size_t i=0; i<words.size()-1; ++i){
				os << anchor("index.html", words[i], level_-i, "View " + words[i]);
				os << "<span class=\"sep\">/</span>\n";
			}
			os << anchor(href(), words.back(), level_+2-words.size(), 
						 "View " + words.back()); 
		}
		os << "\n</h2>\n";
	}


	void Node::print(const bool verbose) const
	{
		if (ignore())
			return;
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		SVNlog log(path_);
		print_core("all", "total", log);
		print_core("all", "code", log);
		print_core("all", "comments", log);
		print_core("all", "empty", log);

		for (std::set<std::string>::const_iterator i = stats_.authors().begin();
				 i!=stats_.authors().end(); ++i) {
			print_core(*i, "total", log);
			print_core(*i, "code", log);
			print_core(*i, "comments", log);
			print_core(*i, "empty", log);
		}
		
		print_core(verbose);
	}


	void Node::print_author_summary(std::ostream& os, std::string line_type,
																	const SVNlog& log) const
	{ 
		os << "<h3>Author Summary</h3>";
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Author</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Date</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (!dir()) {
			os << "<tr class=\"" << color << "\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("index.html", "../");
			os << "</td>\n</tr>\n";
		}

		// print authors
		const std::string timefmt("%e/%m/%y %H:%M:%S");
		for (std::set<std::string>::const_iterator i=stats_.authors().begin();
				 i!=stats_.authors().end(); ++i){
			if (color=="dark")
				color="light";
			else
				color="dark";
			os << "<tr class=\"" << color << "\"><td>"; 
			os << anchor(*i+"/"+line_type+"/"+output_path()
									 ,*i, level_+2, "View statistics for "+*i); 
			os << "</td><td>" << stats_.lines(*i)
				 << "</td><td>" << stats_.code(*i)
				 << "</td><td>" << stats_.comments(*i)
				 << "</td><td>" << stats_.empty(*i);
			if (log.exist(*i)) {
				Commitment lc(log.latest_commit(*i));
				os << "</td>" << "<td>" << trac_revision(lc.revision()) 
					 << "</td>" << "<td>" << lc.date()(timefmt);
			}
			else {
				os << "</td>" << "<td>N/A"
					 << "</td>" << "<td>N/A";
			}
			os << "</td></tr>\n";
		}

		os << "<tr class=\"" << color << "\">\n";
		os << "<td>"; 
		if (dir())
			if (local_path().empty())
				os << anchor("all/"+line_type+"/index.html"
										 ,"Total", level_+2, "View statistics for all"); 
			else
				os << anchor("all/"+line_type+"/"+local_path()+"/index.html"
										 ,"Total", level_+2, "View statistics for all"); 
		else
			os << anchor("all/"+line_type+"/"+local_path()+".html"
									 ,"Total", level_+2, "View statistics for all"); 
		os << "</td>\n";
		os << "<td>" << stats_.lines() << "</td>\n";
		os << "<td>" << stats_.code() << "</td>\n";
		os << "<td>" << stats_.comments() << "</td>\n";
		os << "<td>" << stats_.empty() << "</td>\n";
		Commitment lc(log.latest_commit());
		os << "<td>" << trac_revision(lc.revision()) << "</td>\n";
		os << "<td>" << lc.date()(timefmt)<< "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
	}

	
	std::string Node::url(void) const
	{
		return svninfo_.url();
	}

}} // end of namespace svndigest and namespace theplu
