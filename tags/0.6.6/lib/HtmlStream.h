#ifndef _theplu_svndigest_html_stream_
#define _theplu_svndigest_html_stream_

// $Id: HtmlStream.h 677 2008-07-28 16:09:26Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <map>
#include <fstream>
#include <sstream>

namespace theplu{
namespace svndigest{

	///
	/// Filtering text to html code.
	///
	class HtmlStream
	{
	public:

		///
		/// @brief Constructor
		///
		HtmlStream(std::ostream&);

		///
		/// @brief Destructor
		///
		~HtmlStream(void);

		///
		/// \brief filter and print stringstream
		///
		void print(std::stringstream&);

		///
		/// \return reference to underlying ostream
		///
		std::ostream& stream(void);

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		HtmlStream(const HtmlStream&);

		std::map<char, std::string> map_;
		std::ostream& os_;
	};


	template <typename T>
	HtmlStream& operator<<(HtmlStream& html, const T& rhs)
	{
		std::stringstream ss;
		ss << rhs;
		html.print(ss);
		return html;
	}

}} // end of namespace svndigest and namespace theplu

#endif
