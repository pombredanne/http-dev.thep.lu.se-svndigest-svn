// $Id: utility_test.cc 685 2008-08-04 14:53:58Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari Häkkinen


	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "utility.h"

#include <iostream>
#include <string>

bool test_hex(int, unsigned int, std::string);

int main(const int argc,const char* argv[])
{
	bool ok=true;
	
	ok &= test_hex(15,2, "0f"); 
	ok &= test_hex(17,1, "1"); 
	ok &= test_hex(16,2, "10"); 

	if (ok)
		return 0;
  return 1;
}

bool test_hex(int x, unsigned int w, std::string facit)
{
	if (theplu::svndigest::hex(x,w)==facit)
		return true;
	std::cerr << "hex(" << x << ", " << w << ") results "
						<< theplu::svndigest::hex(x,w) << ". Expects " << facit 
						<< std::endl;
	return false;
}
