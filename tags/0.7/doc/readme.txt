$Id: readme.txt 847 2009-11-17 01:38:52Z peter $
{{{
Copyright (C) 2005 Jari H�kkinen
Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
Copyright (C) 2009 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
}}}


= About svndigest =

Svndigest traverses a directory structure (controlled by subversion)
and calculates developer statistics for all subversion controlled
entries. The result is written to a sub-directory of a user
specifiable target directory (default is the current working
directory).

== Statistics ==

To understand what statistics is calculated by svndigest this
definition is needed: The developer who made the latest change to a
line still in use in the latest revision, is considered as the
contributor of that line regardless of who actually originally created
that line.

For each developer svndigest calculates the number of contributed
lines in the latest (checked out) revision. Svndigest calculates for
each revision, by checking when each line was last changed, how many
lines each developer had contributed at that specific revision and
still are in use. 

see also: http://dev.thep.lu.se/svndigest/wiki/StatsType

== Different linetypes ==

Svndigest parses each file to decide whether a line is ''code'',
''comment'', or ''other''. Depending on the file name different
parsing modes are used, which means different sets of rules what is
''code'' or ''comment'' are employed. Common for all modes is that
comment blocks are identified by a start code (e.g. '/*' in a C file)
and a stop code (e.g. '*/' in a C file). If a line contains
visible characters being outside comment blocks, the line is
considered to be ''code''. Otherwise, if the line contains
alphanumeric characters inside a comment block, the line is considered
to be a line of ''comment''. Otherwise the line is considered to be
''other''. 

From svndigest 0.7, it is possible to configure which rules are used
for different files. For more on how to configure svndigest see
below. If no configuration file is given or no rules are given, a set
of default rules are used. For files named `*.h`, for example, rules
allowing to detect comments as either `// ... \n` or `/*
... */`. These rules can be set in the configuration file using a
one-liner like:

`*.h = "//":"\n"  ;  "/*":"*/"`

The first string (`*.h`) is a file-name-pattern describing which files
this rule will be used on. The second block, `"//":"\n"`, is the first
rule saying one way to mark comments is to start with a ''start
code'', `//`, and end with an ''end code'', `\n`. The start and end
codes are surrounded by `""` and separated by `:`. Similarily, the
second block describes that comments also could come as `/* ... */`.

For a more detailed illustration, please have a look at `config` that
can be found in directory `.svndigest`, and the svndigest screenshots
that can be reached through http://dev.thep.lu.se/svndigest/.

Sometimes it might be useful if a file could be parsed as though it
was going under a different name. It could, for example, be useful if
files named `Makefile.in` could be parsed as `Makefile` or files named
`test_repo.sh.in` could be parsed as though it was named
`test_repo.sh`. More generally, it would be useful if files named
`<file-name-pattern>.in` could be parsed as though it was named
`<file-name-pattern>`. For this reason it is possible to set rules on
how a filename should be interpreted (see section
[file-name-dictionary] in configuration file). Default behaviour is
that there is only one such a rule in use, namely the one described
above that trailing `.in` in file names are discarded.


== Caching Statistics ==

To avoid retrieving the same data repeatedly from the repository,
statistics is cahed in files located in `.svndigest`. Subsequent
svndigest runs read the cache files and retrieve information from the
repository only for the revisions that have been committed since the
cache file was created. Obviously, it is important that the cache
files reflect the history of the current working copy. If that is not
the case, for example, if you have switched working copy using `svn
switch`, you can make svndigest ignore the cache through option
`--ignore-cache`.


== Different file types ==

There are many different types of files and for many file types it
does not make sense to define lines. Source code, documentation, and
other human readable files can be treated in single line basis whereas
symbolic links and binary files cannot. svndigest treats binary files
and symbolic links as zero-line files. There is a possibility to
exclude files from the statistics, the use of the property
svndigest:ignore. 

Sometimes large test files and XML files are added to the repository
that should not really be counted in the statistics. This is solved
with the svndigest:ignore property. Files with this property are
excluded from statistics. Setting the svndigest:ignore property to a
directory will exclude all siblings to that directory from svndigest
treatment.

To set the property on file `FILE`, issue `svn propset
svndigest:ignore "" FILE`. For more detailed information refer to
http://svnbook.red-bean.com/.

== Configuration ==

The configuration file may be used to define various behaviors for
svndigest. By default svndigest looks for a config file named `config`
in directory `<root-dir>/.svndigest` (<root-dir> is directory set by
option `--root`). If no such file is found, svndigest behaves as
default. Which file to be used as config file may also be set with
option `--config-file`. To update an old config file you can use the
option `--generate-config`. With this option set the used
configuration will be written to standard output. If new variables has
been added to configuration of svndigest, these variables will be
written with default values. Note: if you have added a comment (e.g. a
subversion keyword) to your old config file, this will not be
inherited to the new config file. To preserve such a comment you must
merge the new config file with the old config file manually.

== Copyright update ==

Using the option `--copyright` svndigest will try to update the
copyright statement in each of the parsed files. The copyright
statement is detected as the first line containing `Copyright
(C)`. The copyright statement block is defined to start at this line
and ends with the first following line containing no alphanumerical
characters (excluding `prefix` string preceeding `Copyright (C)`). This
copyright statement block is replaced with a new copyright statement
generated from analyzing `svn log`. An author is considered to have
copyright of the file if (s)he has modified the file and thereby
occurs in the log. For an example of the format of the generated
copyright statement, please have a look at the bottom of this file. By
default the `svn user name` of the author is printed in the copyright
statement. This may be overridden by setting a `copyright-alias` in
the config file. In svndigest, e.g., user name `jari` is set to
copyright-alias ''Jari H�kkinen'' and user name `peter` is set to
copyright-alias ''Peter Johansson''. If two (or several) authors are
given the same copyright-alias they are considered as one person in
the copyright statement (i.e. their alias is not repeated). This may
be useful if you want to give copyright to an organization rather than
to individual developers.

== TracLinks ==

From svndigest 0.6 there is support for TracLinks. The root trac
location may be set in the configuration, in which case various links
to the trac site will be included in the resulting report. This
includes links from revision number to the corresponding revision at
the trac site, and various kinds of links are detected in the messages
in `Recent Log`.

== Prerequisites ==

Svndigest runs against a working copy (WC), i.e., svndigest will not
run directly against a repository. Svndigest requires that the WC is
pristine before running the analysis, i.e., no files are allowed to be
modified. We also recommend that the WC is in synch with the
repository. Issue `svn update` before running svndigest.

== Flow of the program ==
The current flow of the program is.

  * Check that we are working with a WC in subversion control.

  * Build the requested directory structure ignoring not subversion 
    controlled items. During the directory structure creation a check 
    is made that the WC is up to date with the repository.

  * Walk through the directory structure and calculate statistics for
    each entry.

  * Create the plots and HTML presentation.

