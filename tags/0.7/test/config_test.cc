// $Id: config_test.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Configuration.h"

namespace theplu{
namespace svndigest{
	bool test_codon(void);
}} // end of namespace svndigest and theplu


int main( int argc, char* argv[])
{
	using namespace theplu::svndigest;
	bool ok=true;

	ok &= test_codon();
                                                                                
	if (ok)
		return 0;
  return 1;
}


namespace theplu{
namespace svndigest{

	bool test_codon(void)
	{
		const Configuration& c(Configuration::instance());
		bool ok =true;
		if (!c.codon("foo.h")){
			std::cerr << "No codon for foo.h\n";
			ok = false;
		}
		if (!c.codon("../dir/test.cc")){
			std::cerr << "No codon for test.cc\n";
			ok = false;
		}

		return ok;
	}
	
}} // end of namespace svndigest and theplu
