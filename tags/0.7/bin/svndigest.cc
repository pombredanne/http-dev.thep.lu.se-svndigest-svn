// $Id: svndigest.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Parameter.h"

#include "Configuration.h"
#include "css.h"
#include "Directory.h"
#include "first_page.h"
#include "GnuplotFE.h"
#include "html_utility.h"
#include "rmdirhier.h"
#include "Stats.h"
#include "StatsCollection.h"
#include "SVN.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "../lib/utility.h"

#include "yat/Exception.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

void create_file_struct(std::string stats_type, 
												const theplu::svndigest::Stats& stats);

int main( int argc, char* argv[])
{
	using namespace theplu;
	using namespace svndigest;

	// Reading commandline options
	Parameter* option=NULL;
	try {
		option = new Parameter(argc,argv);
		if (option->verbose())
			std::cout << "Done parsing parameters" << std::endl;
	}
	catch (yat::utility::cmd_error& e) {
		std::cerr << e.what() << std::endl;
		exit(-1);
	}
	assert(option);

	// Reading configuration file
	Configuration& config = Configuration::instance();
	if (node_exist(option->config_file())) {
		std::ifstream is(option->config_file().c_str());
		if (!is.good()){
			is.close();
			std::cerr << "\nsvndigest: Cannot open config file " 
								<< option->config_file() << std::endl; 
			exit(-1);
		}
		try { 
			config.load(is);
		}
		catch (std::runtime_error e) {
			std::cerr << "svndigest: invalid config file\n"
								<< e.what() << std::endl;
			exit(-1);
		}
		is.close();
	}
	
	// write configuration
	if (option->generate_config()) {
		std::cout << config;
		exit(0);
	}

	SVN* svn=NULL;
	try {
		if (option->verbose())
			std::cout << "Initializing SVN singleton." << std::endl;
		svn=SVN::instance(option->root());
	}
	catch (SVNException e) {
		std::cerr << "svndigest: " << e.what() << "\n";
		exit(-1);
	}

	// check if target already exists and behave appropriately
	bool need_to_erase_target=false;
	std::string target_path=option->targetdir() + '/' + file_name(option->root());
	if (option->report()) {
		if (option->verbose())
			std::cout << "Checking target directory" << std::endl;
		need_to_erase_target = node_exist(target_path);
		if (need_to_erase_target && !option->force()) {
			std::cerr << "svndigest: directory `" 
								<< target_path << "' already exists\n";
			exit(-1);
		}
	}

	// Extract repository location
	std::string repo;
	try {
		if (option->verbose())
			std::cout << "Acquiring repository information" << std::endl;
		repo=SVNinfo(option->root()).repos_root_url();
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what()
							<< "\nsvndigest: Failed to determine repository for "
							<< option->root() << '\n' << std::endl;
		exit(-1);
	}

	// build directory tree already here ... if WC is not upto date with
	// repo an exception is thrown. This avoids several costly
	// statements below and will not remove a digest tree below if a
	// tree already exists.
	if (option->verbose())
		std::cout << "Building directory tree" << std::endl;
	Directory* tree=NULL;
	try {
		tree = new Directory(0,option->root(),"");
	}
	catch (NodeException e) {
		std::cerr << "svndigest: " << e.what() << std::endl;
		exit(-1);
	}
	assert(tree);

	if (option->verbose())
		std::cout << "Parsing directory tree" << std::endl;
	StatsCollection stats(tree->parse(option->verbose(), option->ignore_cache()));

	if (option->report()) {
		// remove target if needed
		if (need_to_erase_target) {
			if (option->verbose())
				std::cout << "Removing old target tree: " << target_path << "\n";
			rmdirhier(target_path);
			// exit if remove failed
			if (node_exist(target_path)) {
				std::cerr << "svndigest: remove failed\n";
				exit(-1);
			}
		}


		if (option->verbose())
			std::cout << "Generating output" << std::endl;
		if (!option->revisions()) {
			SVNlog log(repo);
			std::vector<std::string> dates;
			dates.reserve(log.commits().size());
			for (SVNlog::container::const_iterator iter=log.commits().begin();
					 iter!=log.commits().end(); ++iter) {
				assert(static_cast<svn_revnum_t>(dates.size())==iter->revision());
				dates.push_back(iter->date());
			}
			GnuplotFE::instance()->set_dates(dates); 
		}
		if (chdir(option->targetdir().c_str()) ) {
			std::cerr << "svndigest: chdir " << option->targetdir() << " failed\n";
			exit(-1);
		}
		mkdir(tree->name());
		if (chdir(tree->name().c_str()) ) {
			std::cerr << "svndigest: chdir " << tree->name() << " failed\n";
			exit(-1);
		}
		GnuplotFE::instance()->command(std::string("cd '")+option->targetdir()+"/"
																	 +tree->name()+"'");
		print_css("svndigest.css");
		print_main_page(tree->name(), tree->log(), stats, tree->url());
		// create structure StatsType/Author/LineType
		for (std::map<std::string, Stats*>::const_iterator i(stats.stats().begin());
				 i!=stats.stats().end(); ++i)
			create_file_struct(i->first+std::string("/"), *i->second);
		try {
			tree->print(option->verbose());
		}
		catch (const std::runtime_error& x) {
			std::cerr << "svndigest: " << x.what() << std::endl;
			exit(-1);
		}
	}

	if (option->copyright()){
		try {
			if (option->verbose())
				std::cout << "Updating copyright statements" << std::endl;
			std::map<std::string, Alias> alias(config.copyright_alias());
			tree->print_copyright(alias, option->verbose());
		}
		catch (const std::runtime_error& x) {
			std::cerr << "svndigest: " << x.what() << std::endl;
			exit(-1);
		}
	}

	if (option->verbose())
		std::cout << "Finalizing" << std::endl;

	delete tree;
	if (option->verbose())
		std::cout << "Done!" << std::endl;
	delete option;
	exit(0);				// normal exit
}

	void create_file_struct(std::string stats_type, 
													const theplu::svndigest::Stats& stats)
	{
		using namespace theplu::svndigest;
		mkdir(stats_type);
		touch(stats_type+std::string("index.html"));
		mkdir(stats_type+std::string("all"));
		mkdir(stats_type+std::string("images"));
		touch(stats_type+std::string("all/index.html"));
		touch(stats_type+std::string("images/index.html"));
		for (std::set<std::string>::const_iterator i = stats.authors().begin();
				 i!=stats.authors().end(); ++i) {
			mkdir(stats_type+*i);
			touch(stats_type+*i+std::string("/index.html"));
		}
	}
