#ifndef _theplu_svndigest_copyright_year_
#define _theplu_svndigest_copyright_year_

// $Id: copyright_year.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

namespace theplu{
namespace svndigest{

	/**
		 \return year svndigest was last changed
	 */
	std::string svn_year(void);

}} // end of namespace svndigest and namespace theplu

#endif 
