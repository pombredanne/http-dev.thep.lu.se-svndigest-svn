// $Id: Stats.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Stats.h"

#include "Functor.h"
#include "GnuplotFE.h"
#include "SVNblame.h"
#include "SVNinfo.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	Stats::Stats(const std::string& path)
		:	stats_(std::vector<Author2Vector>(LineTypeParser::total+1))
	{
		// Make sure latest revision is set properly
		SVNinfo svn_info(path);
		revision_=svn_info.rev();
		last_changed_rev_=svn_info.last_changed_rev();
		reset();
	}


	Stats::~Stats(void)
	{
	}


	void Stats::accumulate(std::vector<unsigned int>& vec,
												 svn_revnum_t rev) const
	{
		assert(rev>0);
		if (vec.empty()){
			// just to allow call to vec.back() below
      vec.resize(1,0);
		}
		else if (vec.begin()+rev-1 < vec.end())
			std::partial_sum(vec.begin()+rev-1,vec.end(),vec.begin()+rev-1);
		// static_cast to remove annoying compiler warning
		if (vec.size() < static_cast<size_t>(revision()+1))
			vec.resize(revision()+1, vec.back());
	}


	void Stats::accumulate_stats(svn_revnum_t rev)
	{
		if (!rev)
			rev = 1;
		for (std::set<std::string>::const_iterator iter(authors().begin());
				 iter!=authors().end(); ++iter) {
			std::vector<unsigned int>& code = code_stats()[*iter];
			accumulate(code, rev);
			std::vector<unsigned int>& comments = comment_stats()[*iter];
			accumulate(comments, rev);
			std::vector<unsigned int>& other = other_stats()[*iter];
			accumulate(other, rev);
			std::vector<unsigned int>& copyright = copyright_stats()[*iter];
			accumulate(copyright, rev);
		}
	}


  void Stats::add(const std::string& user, const unsigned int& rev, 
									const LineTypeParser::line_type& lt, unsigned int n)
  {
		assert(user.size());
		add_author(user);

		add(code_stats()[user], rev, lt==LineTypeParser::code, n);
		add(comment_stats()[user], rev, lt==LineTypeParser::comment, n);
		add(other_stats()[user], rev, lt==LineTypeParser::other, n);
		add(copyright_stats()[user], rev, lt==LineTypeParser::copyright, n);
  }


	void Stats::add(std::vector<unsigned int>& vec, unsigned int rev, bool x,
									unsigned int n)
	{	
    if (vec.size() < rev+1){
			vec.reserve(rev+1);
			vec.resize(rev);
			assert(vec.size()+1<vec.max_size());
			if (x) {
				vec.push_back(n);
			}
			else {
				vec.push_back(0);
			}
    }
    else if (x)
			vec[rev]+=n;
	}


	void Stats::add_author(std::string name)
	{
		authors_.insert(name);
	}


	void Stats::add_authors(std::set<std::string>::const_iterator first, 
													std::set<std::string>::const_iterator last)
	{
		authors_.insert(first, last);
	}


	const std::set<std::string>& Stats::authors(void) const
	{
		return authors_;
	}


	void Stats::calc_all(void)
	{
		std::vector<unsigned int> init(revision()+1);
		for (int lt=0; lt <= 4; ++lt) {
			stats_[lt]["all"].clear();
			stats_[lt]["all"] =
				std::accumulate(stats_[lt].begin(), 
												stats_[lt].end(), init,
												PairValuePlus<std::string,unsigned int>());
		}
		VectorPlus<unsigned int> vp;
		comment_or_copy_stats()["all"] = 
			vp(comment_stats()["all"], copyright_stats()["all"]);

		total_stats()["all"] = 
			vp(vp(code_stats()["all"], comment_or_copy_stats()["all"]), 
				 other_stats()["all"]);
	}


	void Stats::calc_total(void)
	{
		for (std::set<std::string>::const_iterator iter(authors().begin());
				 iter!=authors().end(); ++iter) {
			std::vector<unsigned int>& code = code_stats()[*iter];
			std::vector<unsigned int>& comments = comment_stats()[*iter];
			std::vector<unsigned int>& other = other_stats()[*iter];
			std::vector<unsigned int>& copy = copyright_stats()[*iter];

			VectorPlus<unsigned int> vp;
			total_stats()[*iter] = vp(vp(vp(code, comments),other),copy);
		}

	}


	void Stats::calc_comment_or_copy(void)
	{
		for (std::set<std::string>::const_iterator iter(authors().begin());
				 iter!=authors().end(); ++iter) {
			std::vector<unsigned int>& comments = comment_stats()[*iter];
			std::vector<unsigned int>& copy = copyright_stats()[*iter];

			VectorPlus<unsigned int> vp;
			comment_or_copy_stats()[*iter] = vp(comments, copy);
		}

	}


	unsigned int Stats::code(const std::string& user) const
	{
		return get_back(code_stats(), user);
	}


	unsigned int Stats::comments(const std::string& user) const
	{
		return get_back(comment_or_copy_stats(), user);
	}


	unsigned int Stats::empty(const std::string& user) const
	{
		return get_back(other_stats(), user);
	}


	unsigned int Stats::get_back(const Author2Vector& m, std::string user) const
	{
		A2VConstIter iter(m.find(std::string(user)));
		if (iter==m.end() || iter->second.empty()) 
			return 0;
		return iter->second.back();
	}


	const std::vector<unsigned int>& Stats::get_vector(const Author2Vector& m, 
																							std::string user) const
	{
		A2VConstIter iter(m.find(std::string(user)));
		if (iter==m.end()) 
			throw std::runtime_error(user+std::string(" not found i Stats"));
		return iter->second;
	}


	svn_revnum_t Stats::last_changed_rev(void) const
	{
		return last_changed_rev_;
	}


	unsigned int Stats::lines(const std::string& user) const
	{
		return get_back(total_stats(), user);
	}


	void Stats::load(std::istream& is, Author2Vector& m)
	{
		while (m.size() < authors().size()+1 && is.good()) {
			std::string name;
			std::getline(is, name);
			std::vector<unsigned int>& vec=m[name];
			std::string line;
			std::getline(is, line);
			std::stringstream ss(line);
			while (ss.good()) {
				svn_revnum_t rev=0;
				unsigned int count=0;
				ss >> rev;
				ss >> count;
				assert(rev<=revision_);
				if (!count)
					break;
				vec.resize(std::max(vec.size(),static_cast<size_t>(rev+1)));
				vec[rev]=count;
			}
			accumulate(vec);
		}
	}


	svn_revnum_t Stats::load_cache(std::istream& is)
	{
		std::string str;
		getline(is, str);
		if (str!=cache_check_str())
			return 0;
		svn_revnum_t rev;
		is >> rev;
		reset();
		size_t a_size=0;
		is >> a_size;
		while (authors().size()<a_size && is.good()){
			getline(is, str);
			if (str.size())
				add_author(str);
		}
		getline(is, str);
		if (str!=cache_check_str()) {
			return 0;
		}
		for (size_t i=0; i<stats_.size(); ++i){
			load(is, stats_[i]);
			getline(is, str);
			if (str!=cache_check_str()) {
				return 0;
			}
		}
		return rev;
	}


	void Stats::map_add(A2VConstIter first1, A2VConstIter last1, 
											Author2Vector& map)
	{
		A2VIter first2(map.begin());
		Author2Vector::key_compare compare;
		while ( first1 != last1) { 
			// key of first1 less than key of first2
			if (first2==map.end() || compare(first1->first,first2->first)) {
				first2 = map.insert(first2, *first1);
				++first1;
			}
			// key of first2 less than key of first1
			else if ( compare(first2->first, first1->first)) {
				++first2;
			}
			// keys are equivalent
			else {
				VectorPlus<Author2Vector::mapped_type::value_type> vp;
				first2->second = vp(first1->second, first2->second);
				++first1;
				++first2;
			}
    }
	}


	void Stats::parse(const std::string& path, svn_revnum_t rev)
	{
		// reset stats to zero for [rev, inf)
		for (size_t i=0; i<stats_.size(); ++i)
			for (A2VIter iter=stats_[i].begin(); iter!=stats_[i].end(); ++iter) {
				iter->second.resize(rev,0);
				iter->second.resize(revision(),0);
			}
		do_parse(path, rev);
		calc_comment_or_copy();
		calc_total();
		calc_all();
		assert(total_stats().size());
		assert(code_stats().size());
		assert(comment_or_copy_stats().size());
		assert(other_stats().size());
	}

	std::string Stats::plot(const std::string& filename,
													const std::string& linetype) const
	{
		assert(total_stats().size());
		plot_init(filename);
		GnuplotFE* gp=GnuplotFE::instance();
		const Author2Vector* stat=NULL;
		if (linetype=="total")
			stat = &total_stats();
		else if (linetype=="code")
			stat = &code_stats();
		else if (linetype=="comments")
			stat = &comment_or_copy_stats();
		else if (linetype=="empty")
			stat = &other_stats();
		assert(stat);
		assert(stat->size());
		assert(stat->find("all")!=stat->end());
		std::vector<unsigned int> total=get_vector(*stat, "all");		
		double yrange_max=1.03*total.back()+1;
		gp->yrange(yrange_max);

		typedef std::vector<std::pair<std::string, std::vector<unsigned int> > > vec_type;
		vec_type author_cont;
		author_cont.reserve(stat->size());
		for (std::set<std::string>::const_iterator i=authors_.begin(); 
				 i != authors_.end(); ++i) {
			if (lines(*i)) {
				assert(stat->find(*i)!=stat->end());
				author_cont.push_back(std::make_pair(*i,get_vector(*stat,*i)));
			}
		}

		LessReversed<std::vector<unsigned int> > lr;
		PairSecondCompare<std::string, std::vector<unsigned int>, 
			LessReversed<std::vector<unsigned int> > > compare(lr);
		std::sort(author_cont.begin(), author_cont.end(), compare);

		size_t plotno=author_cont.size();
		std::stringstream ss;
		vec_type::iterator end(author_cont.end());
		for (vec_type::iterator i(author_cont.begin()); i!=end; ++i) {
			ss.str("");
			ss << "set key height " << 2*plotno;
			gp->command(ss.str());
			ss.str("");
			ss << get_back(*stat, i->first) << " " << i->first;
			gp->yrange(yrange_max);
			gp->linetitle(ss.str());
			ss.str("");
			ss << "steps " << --plotno+2;
			gp->linestyle(ss.str());
 			gp->plot(i->second);
		}
		ss.str("");
		ss << get_back(*stat, "all") << " total";
		gp->command("set key height 0");
		gp->linetitle(ss.str());
		gp->linestyle("steps 1");
		gp->plot(total);

		gp->command("unset multiplot");
		gp->yrange();

		return filename;
	}


	void Stats::plot_init(const std::string& filename) const
	{
		GnuplotFE* gp=GnuplotFE::instance();
		gp->command("set term png");
		gp->command("set output '"+filename+"'");
		gp->command("set xtics nomirror");
		gp->command("set ytics nomirror");
		gp->command("set key default");
		gp->command("set key left Left reverse");
		gp->command("set multiplot");
	}


	void Stats::plot_summary(const std::string& filename) const
	{
		plot_init(filename);
		GnuplotFE* gp=GnuplotFE::instance();
		std::vector<unsigned int> total = get_vector(total_stats(), "all");
		double yrange_max=1.03*total.back()+1;
		gp->yrange(yrange_max);
		std::stringstream ss;
		
		ss.str("");
		std::vector<unsigned int> x(get_vector(code_stats(), "all"));
		ss << x.back() << " code";
		gp->command("set key height 2");
		gp->linetitle(ss.str());
		gp->linestyle("steps 2");
		gp->plot(x);

		ss.str("");
		x = get_vector(comment_or_copy_stats(), "all");
		ss << x.back() << " comment";
		gp->command("set key height 4");
		gp->linetitle(ss.str());
		gp->linestyle("steps 3");
		gp->plot(x);

		ss.str("");
		x = get_vector(other_stats(), "all");
		ss << x.back() << " other";
		gp->command("set key height 6");
		gp->linetitle(ss.str());
		gp->linestyle("steps 4");
		gp->plot(x);

		ss.str("");
		ss << total.back() << " total";
		gp->command("set key height 0");
		gp->linetitle(ss.str());
		gp->linestyle("steps 1");
		gp->plot(total);

		gp->command("unset multiplot");
		gp->yrange();
	}


	void Stats::print(std::ostream& os) const
	{
		os << cache_check_str() << "\n";
		os << last_changed_rev() << " ";
		os << authors().size() << "\n";

		std::copy(authors().begin(), authors().end(), 
							std::ostream_iterator<std::string>(os, "\n"));
		os << cache_check_str() << "\n";
		for (size_t i=0; i<stats_.size(); ++i){
			print(os, stats_[i]);
			os << cache_check_str() << "\n";
		}
	}


	void Stats::print(std::ostream& os, const Author2Vector& m) const
	{
		for (A2VConstIter i(m.begin()); i!=m.end(); ++i){
			os << i->first << "\n";
			assert(i->second.size());
			if (i->second[0])
				os << 0 << " " << i->second[0] << " ";
			for (size_t j=1; j<i->second.size(); ++j) {
				// only print if stats changes in this rev
				if (i->second[j] != i->second[j-1]) {
					os << j << " " << i->second[j] - i->second[j-1] << " ";
				}
			}
			os << "\n";
		}
	}

	void Stats::reset(void)
	{
		for (size_t i=0; i<stats_.size(); ++i){
			stats_[i].clear();
			std::vector<unsigned int>& tmp = stats_[i]["all"];
			std::fill(tmp.begin(), tmp.end(), 0);
			tmp.resize(revision_+1);
		}
		authors_.clear();
	}


	Stats& Stats::operator+=(const Stats& rhs)
  {
		revision_ = std::max(revision_, rhs.revision_);
		last_changed_rev_ = std::max(last_changed_rev_, rhs.last_changed_rev_);
		add_authors(rhs.authors().begin(), rhs.authors().end());
		assert(stats_.size()==rhs.stats_.size());
		for (size_t i=0; i<stats_.size(); ++i)
			map_add(rhs.stats_[i].begin(), rhs.stats_[i].end(), stats_[i]);
		
    return *this;
  }

	
	size_t Stats::operator()(int linetype, std::string author, 
													 svn_revnum_t rev) const
	{
		assert(linetype<=LineTypeParser::total);
		assert(static_cast<size_t>(linetype) < stats_.size());
		assert(rev>=0);
		A2VConstIter i = stats_[linetype].find(author);
		if (i==stats_[linetype].end()){
			std::stringstream msg;
			msg << __FILE__ << ": author: " << author << " does not exist";  
			throw std::runtime_error(msg.str());
		}
		assert(rev < static_cast<svn_revnum_t>(i->second.size()));
		return i->second[rev];
	}

}} // end of namespace svndigest and namespace theplu
