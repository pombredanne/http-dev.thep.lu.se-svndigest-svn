// $Id: HtmlStream.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "HtmlStream.h"
#include "HtmlBuf.h"

#include <string>

namespace theplu{
namespace svndigest{

	HtmlStream::HtmlStream(std::ostream& os)
		: std::ostream(), hbuf_(*os.rdbuf()), os_(os)
	{
		// setting member variable in base class
		rdbuf(&hbuf_);
	}


	HtmlStream::~HtmlStream(void)
	{
	}


	std::ostream& HtmlStream::stream(void)
	{
		return os_;
	}

}} // end of namespace svndigest and namespace theplu
