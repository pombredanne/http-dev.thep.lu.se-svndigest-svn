// $Id: Trac.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Trac.h"

#include "Configuration.h"
#include "Functor.h"
#include "HtmlStream.h"
#include "html_utility.h"
//#include "utility.h"

namespace theplu{
namespace svndigest{

	Trac::Trac(HtmlStream& hs)
		: hs_(hs) 
	{}

	std::string Trac::anchor_text(std::string::const_iterator first,
																std::string::const_iterator last,
																std::string::const_iterator last_trunc)
	{
		if (last>last_trunc) { // truncate
			std::string str(first, last_trunc);
			str+="...";
			return str;
		}
		return std::string(first, last);
	}


	bool Trac::changeset(const std::string::const_iterator& first, 
											 std::string::const_iterator& iter,
											 const std::string::const_iterator& last,
											 const std::string::const_iterator& last_trunc)
	{
		if (changeset1(first, iter, last, last_trunc))
			return true;
		if (changeset2(iter, last, last_trunc))
			return true;
		if (changeset3(iter, last, last_trunc))
			return true;
		return false;
	}


	bool Trac::changeset1(const std::string::const_iterator& first, 
												std::string::const_iterator& iter,
												const std::string::const_iterator& last,
												const std::string::const_iterator& last_trunc)
	{
		if (iter==last)
			return false;
		if (*iter != 'r')
			return false;
		if (iter!=first && isalnum(*(iter-1)))
			return false;
		const std::string::const_iterator iter_orig(iter);
		++iter;
		std::string rev = match(iter, last, Digit());
		if (rev.empty() || (iter!=last && (std::isalnum(*iter) || *iter==':')) ){
			iter = iter_orig;
			return false;
		}
		std::string href(Configuration::instance().trac_root()+"changeset/"+rev);
		hs_.stream() << anchor(href, anchor_text(iter_orig, iter, last_trunc)); 
		return true;
	}


	bool Trac::changeset2(std::string::const_iterator& first, 
												const std::string::const_iterator& last,
												const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;
		if (*first != '[')
			return false;
		const std::string::const_iterator first_orig(first);
		++first;
		std::string rev = match(first, last, Digit());
		if (rev.empty() || first==last || *first!=']'){
			first = first_orig;
			return false;
		}
		++first;
		std::string href(Configuration::instance().trac_root()+"changeset/"+rev);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::changeset3(std::string::const_iterator& first, 
												const std::string::const_iterator& last,
												const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;
		const std::string::const_iterator first_orig(first);
		if (match(first, last, std::string("changeset:")).empty()){
			first = first_orig;
			return false;
		}
		std::string rev = match(first, last, Digit());
		if (rev.empty()){
			first = first_orig;
			return false;
		}
		std::string href(Configuration::instance().trac_root()+"changeset/"+rev);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::comment(std::string::const_iterator& first, 
										 const std::string::const_iterator& last,
										 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (match(first, last, std::string("comment:ticket:")).empty()){
			first = first_orig;
			return false;
		}
			
		std::string ticket = match(first, last, AlNum());
		if (ticket.empty() || first == last || *first != ':') {
			first = first_orig;
			return false;
		}
		++first;
		std::string comment = match(first, last, Digit());
		if (comment.empty() ) {
			first = first_orig;
			return false;
		}
		std::string href(Configuration::instance().trac_root()+"ticket/"+ticket+
										 "#comment:"+comment);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::diff(std::string::const_iterator& first, 
									const std::string::const_iterator& last,
									const std::string::const_iterator& last_trunc)
	{
		if (diff3(first, last, last_trunc))
			return true;
		if (diff1(first, last, last_trunc))
			return true;
		if (diff2(first, last, last_trunc))
			return true;
		return false;
	}


	bool Trac::diff1(std::string::const_iterator& first, 
									 const std::string::const_iterator& last,
									 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (match(first, last, std::string("diff:")).empty()){
			first = first_orig;
			return false;
		}
		std::string node = match(first, last, notChar('@'));
		if (first==last){
			first = first_orig;
			return false;
		}
		++first; 
		std::string old_rev = match(first, last, AlNum());
		if (old_rev.empty() || first == last || *first != ':') {
			first = first_orig;
			return false;
		}
		++first;
		std::string new_rev = match(first, last, AlNum());
		if (new_rev.empty() ) {
			first = first_orig;
			return false;
		}
		std::string href;
		if (node.empty())
			href = std::string(Configuration::instance().trac_root()+
												 "changeset?new="+new_rev+"&amp;old="+old_rev);
		else
			href = std::string(Configuration::instance().trac_root()+
												 "changeset?new="+new_rev+"&amp;new_path="+node+
												 "&amp;old="+old_rev+"&amp;old_path="+node);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::diff2(std::string::const_iterator& first, 
									 const std::string::const_iterator& last,
									 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (match(first, last, std::string("diff:")).empty()){
			first = first_orig;
			return false;
		}
		std::string old_path(match(first, last, not2Str("//", " ")));
		std::string new_path;
		if (first==last || *first==' ')
			new_path = old_path;
		else {
			first += 2;
			new_path = match(first, last, notChar(' '));
		}
		if (new_path.empty() || old_path.empty()){
			first = first_orig;
			return false;
		}
		std::string href(Configuration::instance().trac_root()+
										 "changeset?new_path="+new_path+"&amp;old_path="+old_path);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::diff3(std::string::const_iterator& first, 
									 const std::string::const_iterator& last,
									 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (match(first, last, std::string("diff:")).empty()){
			first = first_orig;
			return false;
		}
		std::string old_path(match(first, last, not2Char('@', ' ')));
		if (*first!='@'){
			first = first_orig;
			return false;
		}
		++first;
		std::string old_rev(match(first, last, AlNum()));
		if (match(first, last, std::string("//")).empty()) {
			first = first_orig;
			return false;
		}
		std::string new_path = match(first, last, not2Char('@', ' '));
		if (*first!='@'){
			first = first_orig;
			return false;
		}
		++first;
		std::string new_rev(match(first, last, AlNum()));
		if (new_rev.empty()){
			first = first_orig;
			return false;
		}
		
		std::string href(Configuration::instance().trac_root()+
										 "changeset?new="+new_rev+"&amp;new_path="+new_path+
										 "&amp;old="+old_rev+"&amp;old_path="+old_path);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::log(const std::string::const_iterator& first,
								 std::string::const_iterator& iter, 
								 const std::string::const_iterator& last,
								 const std::string::const_iterator& last_trunc)
	{
		if (log1(first, iter, last, last_trunc))
			return true;
		if (log2(iter, last, last_trunc))
			return true;
		if (log3(iter, last, last_trunc))
			return true;
		return false;
	}


	bool Trac::log1(const std::string::const_iterator& first,
									std::string::const_iterator& iter, 
									const std::string::const_iterator& last,
									const std::string::const_iterator& last_trunc)
	{
		if (iter==last)
			return false;

		const std::string::const_iterator iter_orig(iter);
		if (*iter != 'r')
			return false;
		if (iter!=first && isalnum(*(iter-1)))
			return false;
		++iter;

		std::string stop_rev = match(iter, last, Digit());
		if (stop_rev.empty() || iter == last || *iter != ':') {
			iter = iter_orig;
			return false;
		}
		++iter;
		std::string rev = match(iter, last, Digit());
		if (rev.empty() || (iter!=last && std::isalnum(*iter) ) ){
			iter = iter_orig;
			return false;
		}
		std::string href(Configuration::instance().trac_root()+"log/?rev="+
										 rev+"&amp;stop_rev="+stop_rev);
		hs_.stream() << anchor(href, anchor_text(iter_orig,iter, last_trunc)); 
		return true;
	}


	bool Trac::log2(std::string::const_iterator& first, 
									const std::string::const_iterator& last,
									const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (*first != '[')
			return false;
		++first;

		std::string stop_rev = match(first, last, AlNum());
		if (stop_rev.empty() || first == last || *first != ':') {
			first = first_orig;
			return false;
		}
		++first;
		std::string rev = match(first, last, AlNum());
		if (rev.empty() || first == last || *first != ']') {
			first = first_orig;
			return false;
		}
		++first; // eating ']'
		std::string href(Configuration::instance().trac_root()+"log/?rev="+
										 rev+"&amp;stop_rev="+stop_rev);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::log3(std::string::const_iterator& first, 
									const std::string::const_iterator& last,
									const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);

		const std::string log_str("log:"); 
		if (!match_begin(first, last, log_str)) {
			first = first_orig;
			return false;
		}
		first += log_str.size();
		std::string node = match(first, last, not2Char('#', '@'));
		++first;
		std::string stop_rev = match(first, last, AlNum());
		if (stop_rev.empty() || first == last || *first != ':') {
			first = first_orig;
			return false;
		}
		++first;
		std::string rev = match(first, last, AlNum());
		if (rev.empty() ) {
			first = first_orig;
			return false;
		}
		std::string href;
		if (!node.empty() && node[0]=='/')
			href = std::string(Configuration::instance().trac_root()+"log"+node+
												 "?rev="+rev+"&amp;stop_rev="+stop_rev);
		else
			href = std::string(Configuration::instance().trac_root()+"log/"+node+
												 "?rev="+rev+"&amp;stop_rev="+stop_rev);
			
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::milestone(std::string::const_iterator& first, 
											 const std::string::const_iterator& last,
											 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);

		if (match(first, last, std::string("milestone:")).empty()){
			first = first_orig;
			return false;
		}

		const std::string::const_iterator milestone_begin(first);

		// find the last alphanumerical char before next space (or last)
		for (std::string::const_iterator i(first); i!=last && *i!=' '; ++i)
			if (isalnum(*i)) 
				first = i+1;

		std::string milestone(milestone_begin, first);
		if (milestone.empty()){
			first = first_orig;
			return false;
		}

		const Configuration& conf = Configuration::instance();
		std::string href(conf.trac_root()+"milestone/"+milestone);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	void Trac::print(std::string str, size_t width)
	{
		std::string::const_iterator first(str.begin());
		std::string::const_iterator last_trunc(str.end());
		if (width<=str.size()) // truncate
			last_trunc = first+width;
		while (first<last_trunc) {
			if (log(str.begin(), first, str.end(), last_trunc))
				continue;
			if (comment(first, str.end(), last_trunc))
				continue;
			if (ticket(first, str.end(), last_trunc))
				continue;
			if (changeset(str.begin(), first, str.end(), last_trunc))
				continue;
			if (diff(first, str.end(), last_trunc))
				continue;
			if (milestone(first, str.end(), last_trunc))
				continue;
			if (source(first, str.end(), last_trunc))
				continue;
			hs_ << *first;
			++first;
		}
		if (last_trunc!=str.end())
			hs_ << "...";
	}


	bool Trac::source(std::string::const_iterator& first, 
										const std::string::const_iterator& last,
										const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);

		if (match(first, last, std::string("source:")).empty()){
			first = first_orig;
			return false;
		}

		std::string node;
		std::string rev;
		std::string line;
		const std::string::const_iterator node_begin(first);

		node = match(first, last, not2Char('@', ' '));
		if (!node.empty() && node[0]=='/')
			node = node.substr(1, node.size()-1); 
	
		if (*first == '@'){
			++first;
			rev = match(first, last, AlNum());
			if (*first == '#') {
				++first;
				line = match(first, last, notChar(' '));
			}
		}

		const Configuration& conf = Configuration::instance();
		std::string href(conf.trac_root()+"browser/"+node);
		if (!rev.empty()) {
			href += "?rev="+rev;
			if (!line.empty())
				href += "#"+line;
		}
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::ticket(std::string::const_iterator& first, 
										const std::string::const_iterator& last,
										const std::string::const_iterator& last_trunc)
	{
		if (ticket1(first, last, last_trunc))
			return true;
		if (ticket2(first, last, last_trunc))
			return true;
		return false;
	}


	bool Trac::ticket1(std::string::const_iterator& first, 
										 const std::string::const_iterator& last,
										 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);
		if (*first != '#')
			return false;
		++first;
		std::string ticket = match(first, last, Digit());

		if (ticket.empty()) {
			first = first_orig;
			return false;
		}

		const Configuration& conf = Configuration::instance();
		hs_.stream() << anchor(conf.trac_root()+"ticket/"+ticket,
													 anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


	bool Trac::ticket2(std::string::const_iterator& first, 
										 const std::string::const_iterator& last,
										 const std::string::const_iterator& last_trunc)
	{
		if (first==last)
			return false;

		const std::string::const_iterator first_orig(first);

		if (match(first, last, std::string("ticket:")).empty()){
			first = first_orig; 
			return false;
		}
		std::string ticket = match(first, last, Digit());

		const Configuration& conf = Configuration::instance();
		std::string href(conf.trac_root()+"ticket/"+ticket);
		hs_.stream() << anchor(href, anchor_text(first_orig,first, last_trunc)); 
		return true;
	}


}} // end of namespace svndigest and namespace theplu
