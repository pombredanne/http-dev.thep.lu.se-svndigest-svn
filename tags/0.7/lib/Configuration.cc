// $Id: Configuration.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Configuration.h"

#include "Functor.h"

#include <cassert>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <stdexcept>
#include <utility>

namespace theplu{
namespace svndigest{

	Configuration* Configuration::instance_=NULL;


	Configuration::Configuration(void)
	{
	}


	void Configuration::add_codon(std::string key, std::string start, 
																std::string end)
	{
		std::pair<std::string, std::string> p(start,end);
		String2Codons::iterator iter = string2codons_.end();
		for (String2Codons::iterator i=string2codons_.begin();
				 i!=string2codons_.end(); ++i)
			if (i->first == key)
				iter = i;
		
		if (iter==string2codons_.end())
			string2codons_.push_back(std::make_pair(key, VectorPair(1,p)));
		else
			iter->second.push_back(p);
	}


	std::string Configuration::author_str_color(const std::string& author) const
	{
		std::string res;
		std::map<std::string, std::string>::const_iterator iterator;
		if ( (iterator=author_color_.find(author)) != author_color_.end())
			res = iterator->second;
		return res;
	}


	const std::vector<std::pair<std::string, std::string> >* 
	Configuration::codon(std::string file_name) const 
	{
		if (const std::pair<std::string,std::string>* dict=dictionary(file_name))
			file_name = translate(file_name, *dict);
		for (String2Codons::const_iterator i(string2codons_.begin());
				 i!=string2codons_.end(); ++i) {
			if (svndigest::equal(file_name.begin(), file_name.end(), 
													 i->first.begin(), i->first.end()) ) {
				return &i->second;
			}
		}
		return NULL;
	}


	const std::map<std::string,Alias>& Configuration::copyright_alias(void) const
	{
		return copyright_alias_;
	}


	const std::pair<std::string,std::string>* 
	Configuration::dictionary(std::string lhs) const
	{
		for (size_t i=0; i<dictionary_.size(); ++i)
			if (svndigest::equal(lhs.begin(), lhs.end(), 
													 dictionary_[i].first.begin(),
													 dictionary_[i].first.end()))
				return &dictionary_[i];
		return NULL;
	}


	bool Configuration::equal_false(const std::string& str) const
	{
		return str=="false" || str=="False" || str=="FALSE" ||
			str=="no" || str=="No" || str=="NO";
	}


	bool Configuration::equal_true(const std::string& str) const
	{
		return str=="true" || str=="True" || str=="TRUE" ||
			str=="yes" || str=="Yes" || str=="YES";
	}


	void Configuration::load(void)
	{
		set_default();
		validate_dictionary();
	}


	void Configuration::load(std::istream& is)
	{
		assert(is.good());
		set_default();

		bool parsing_found=false;
		bool dictionary_found=false;
		std::string line;
		std::string section;
		std::string tmp;
		while (getline(is, line)) {
			line = ltrim(line);
			if (line.empty() || line[0]=='#')
				continue;
			std::stringstream ss(line);
			if (line[0] == '[') {
				getline(ss, tmp, '[');
				getline(ss, section, ']');
				continue;
			}
			std::string lhs;
			getline(ss, lhs, '=');
			lhs = trim(lhs);
			std::string rhs;
			getline(ss, rhs);
			rhs = trim(rhs);
			if (rhs.empty()){
				throw Config_error(line, "expected format: <lhs> = <rhs>");
			}
			if (section == "copyright-alias"){
				std::map<std::string,Alias>::iterator iter = 
					copyright_alias_.lower_bound(lhs);
				if (iter!=copyright_alias_.end() && iter->first==lhs){
					std::stringstream mess;
					mess << "in copright-alias section " << lhs << " defined twice.";
					throw Config_error(line, mess.str());
				}
				
				// insert alias
				copyright_alias_.insert(iter,std::make_pair(lhs, Alias(rhs,copyright_alias_.size())));
			}
			else if (section == "trac"){
				if (lhs=="trac-root")
					trac_root_=rhs;
				else {
					std::stringstream mess;
					mess << "in trac section" << lhs + " is invalid option.";
					throw Config_error(line, mess.str());
				}
			}
			else if (section == "copyright") {
				if (lhs=="missing-copyright-warning") {
					if (equal_false(rhs))
						missing_copyright_warning_ = false;
					else if (equal_true(rhs))
						missing_copyright_warning_ = true;
					else {
						throw Config_error(line, "");
					}
				}
			}
			else if (section == "author-color") {
				author_color_[lhs] = rhs;
			}			
			else if (section == "parsing-codons") {
				if (!parsing_found) {
					parsing_found=true;
					// clearing the default setting
					string2codons_.clear();
				}
				
				if (codon(lhs)) {
					std::stringstream mess;
					mess << "clashes with previous given file name pattern: ";
					// find previous file-name-pattern
					for (String2Codons::const_iterator i(string2codons_.begin());
							 i!=string2codons_.end(); ++i) {
						if (svndigest::equal(lhs.begin(), lhs.end(), 
																 i->first.begin(), i->first.end()) ) {
							mess << "`" << i->first << "'";
							break;
						}
					}
					throw Config_error(line, mess.str());
				}
				std::stringstream ss(rhs);
				std::string start;
				while (getline(ss, start, ':')) {
					start = trim(start);
					std::string end;
					getline(ss, end, ';');
					end = trim(end);
					if (start.empty() && end.empty())
						continue;
					try {
						if (start.empty() || start=="\"\"") {
							throw std::runtime_error("start-code is empty");
						}
						else if (start.size()<3) {
							std::stringstream mess;
							mess << "start-code `" << start << "' is invalid";
							throw std::runtime_error(mess.str());
						}
						start = trim(start, '"');
						if (end.empty() || end=="\"\"") {
							throw std::runtime_error("end-code is empty");
						}
						else if (end.size()<3) {
							std::stringstream mess;
							mess << "end-code `" << end << "' is invalid";
							throw std::runtime_error(mess.str());
						}
						end = trim(end, '"');
					}
					catch (std::runtime_error& e){
						throw Config_error(line, e.what());
					}
					replace(start, "\\n", "\n");
					replace(end, "\\n", "\n");
					add_codon(lhs, start, end);
				}
			} 
			else if (section == "file-name-dictionary") {
				if (!dictionary_found) {
					dictionary_found=true;
					// clearing the default setting
					dictionary_.clear();
				}
				
				if (const std::pair<std::string, std::string>* entry=dictionary(lhs)) {
					std::stringstream mess;
					mess << "clashes with previous given file name pattern: "
							 << "`" << entry->first << "'";
					throw Config_error(line, mess.str());
				}
				lhs = trim(lhs);
				rhs = trim(rhs);
				if (!lhs.empty() && !rhs.empty()) 
					dictionary_.push_back(std::make_pair(lhs, rhs));
				else if (!lhs.empty() || !rhs.empty()) {
					throw Config_error(line, "");
				}
			} 
		}
		validate_dictionary();
	}


	Configuration& Configuration::instance(void)
	{
		if (!instance_){
			instance_ = new Configuration;
			instance_->load();
		}
		return *instance_;
	}


	bool Configuration::missing_copyright_warning(void) const
	{
		return missing_copyright_warning_;
	}


	std::string 
	Configuration::translate(const std::string& str,
													 const std::pair<std::string, std::string>& dic) const
	{
		assert(svndigest::equal(str.begin(), str.end(),
														dic.first.begin(), dic.first.end()));
		std::string res;
		std::vector<std::string> vec;
		regexp(str.begin(), str.end(), dic.first.begin(), dic.first.end(), vec);
		for (std::string::const_iterator i(dic.second.begin()); 
				 i!=dic.second.end(); ++i) {
			if (*i == '$') {
				std::stringstream ss(std::string(i+1, dic.second.end()));
				size_t n = 0;
				ss >> n;
				if (n>vec.size() || n==0){
					std::stringstream mess;
					mess << "svndigest: invalid config file: "
							 << "expression " << dic.second << " is invalid";
					if (n)
						mess << "because " << n << " is a too large.";
					throw std::runtime_error("");				
				}
				res += vec[n-1];
				++i;
				if (n>9){
					++i;
					if (n>99)
						++i;

				}
			}
			else
				res += *i;
		}

		return res;
	}


	std::string trans_end_code(std::string str)
	{
		if (str.size()>0 && str[str.size()-1]=='\n')
			return str.substr(0, str.size()-1) + std::string("\\n");
		return str;
	}


	std::string trans_beg_code(std::string str)
	{
		if (str.size()>0 && str[0]=='\n')
			return std::string("\\n") + str.substr(1); 
		return str;
	}


	std::string trim(std::string str, char c)
	{
		if (str.size()<2 || str[0]!=c || str[str.size()-1]!=c){
			std::stringstream mess;
			mess << "expected `" << str << "' to be surrounded by `" << c << "'";
			throw std::runtime_error(mess.str());
		}
		return str.substr(1, str.size()-2);
	}


	void Configuration::set_default(void)
	{
		copyright_alias_.clear();
		missing_copyright_warning_=false;
		trac_root_ = "";

		add_codon("*.ac", "#", "\n");
		add_codon("*.ac", "dnl", "\n");
		add_codon("*.am", "#", "\n");
		add_codon("*.m4", "#", "\n");
		add_codon("*.m4", "dnl", "\n");
		add_codon("*.c", "//", "\n");
		add_codon("*.c", "/*", "*/");
		add_codon("*.cc", "//", "\n");
		add_codon("*.cc", "/*", "*/");
		add_codon("*.cpp", "//", "\n");
		add_codon("*.cpp", "/*", "*/");
		add_codon("*.cxx", "//", "\n");
		add_codon("*.cxx", "/*", "*/");
		add_codon("*.h", "//", "\n");
		add_codon("*.h", "/*", "*/");
		add_codon("*.hh", "//", "\n");
		add_codon("*.hh", "/*", "*/");
		add_codon("*.hpp", "//", "\n");
		add_codon("*.hpp", "/*", "*/");
		add_codon("*.java", "//", "\n");
		add_codon("*.java", "/*", "*/");
		add_codon("*.pl", "#", "\n");
		add_codon("*.pm", "#", "\n");
		add_codon("*.sh", "#", "\n");
		add_codon("*config", "#", "\n");
		add_codon("bootstrap", "#", "\n");
		add_codon("Makefile", "#", "\n");
		add_codon("*.tex", "%", "\n");
		add_codon("*.m", "%", "\n");
		add_codon("*.jsp", "<!--", "-->");
		add_codon("*.html", "<%--", "--%>");
		add_codon("*.xml", "<!--", "-->");
		add_codon("*.xsl", "<!--", "-->");
		add_codon("*.xsd", "<!--", "-->");
		add_codon("*.xhtml", "<!--", "-->");
		add_codon("*.shtml", "<!--", "-->");
		add_codon("*.xml", "<!--", "-->");
		add_codon("*.css", "<!--", "-->");
		add_codon("*.rss", "<!--", "-->");
		add_codon("*.sgml", "<!--", "-->");
		add_codon("*.bat", "\nREM", "\n");
		add_codon("*.bat", "\nrem", "\n");

		dictionary_ = VectorPair(1, std::make_pair("*.in", "$1"));
	}


	std::string Configuration::trac_root(void) const
	{
		return trac_root_;
	}


	void Configuration::validate_dictionary(void) const
	{
		VectorPair::const_iterator end(dictionary_.end());
		for (VectorPair::const_iterator iter(dictionary_.begin());iter!=end;++iter){
			std::string word(iter->first);
			replace(word, "*", "");
			replace(word, "?", "");
			// throws if dictionary is invalid
			translate(word, *iter);
		}
	}


	std::ostream& operator<<(std::ostream& os, const Configuration& conf)
	{
		os << "### This file configures various behaviors for svndigest\n"
			 << "### The commented-out below are intended to demonstrate how to use\n"
			 << "### this file.\n"
			 << "\n"
			 << "### Section for setting behaviour of copyright update\n"
			 << "[copyright]\n"
			 << "# if true svndigest will warn if file has no copyright statement.\n"
			 << "missing-copyright-warning = ";
		
		if (conf.missing_copyright_warning())
			os << "yes\n";
		else
			os << "no\n";

		os << "\n"
			 << "### Section for setting aliases used in copyright update\n"
			 << "[copyright-alias]\n"
			 << "# jdoe = John Doe\n";

		typedef std::vector<std::pair<std::string, Alias> > vector;
		vector vec;
		std::back_insert_iterator<vector> back_insert_iterator(vec);
		vec.reserve(conf.copyright_alias().size());
		std::copy(conf.copyright_alias().begin(), conf.copyright_alias().end(),
							back_insert_iterator);
		// sort with respect to Alias.id
		IdCompare id;
		PairSecondCompare<const std::string, Alias, IdCompare> comp(id);
		std::sort(vec.begin(),vec.end(), comp);
							

		for (vector::const_iterator i(vec.begin()); i!=vec.end(); ++i) {
			os << i->first << " = " << i->second.name() << " \n";
		}

		os << "\n"
			 << "### Section for author color in blame output\n"
			 << "[author-color]\n"
			 << "# jdoe = 000000\n";
		typedef std::map<std::string,std::string> str_map;
		for (str_map::const_iterator i(conf.author_color_.begin());
				 i!=conf.author_color_.end(); ++i) {
			os << i->first << " = " << i->second << " \n";
		}

		os << "\n"
			 << "### Section for setting trac environment\n"
			 << "[trac]\n"
			 << "# If trac-root is set, svndigest will create anchors to "
			 << "the Trac page.\n"
			 << "# trac-root = http://dev.thep.lu.se/svndigest/\n";
		if (!conf.trac_root().empty())
			os << "trac-root = " << conf.trac_root() << "\n";

		if (!conf.dictionary_.empty()) {
			os << "\n"
				 << "### Section for setting dictionary for file names.\n"
				 << "### Prior looking for file name pattern in section " 
				 << "[parsing-codons],\n"
				 << "### the file name may be translated according to the rules \n"
				 << "### in this section. In default setting there is, for example,\n"
				 << "### a rule to translate `<FILENAME>.in' to `<FILENAME>'.\n"
				 << "### The format of the entries is:\n"
				 << "###    file-name-pattern = new-name\n"
				 << "### Left hand side may contain wildcards (such as '*' and '?').\n"
				 << "### Right hand side may contain \"$i\", which will be replaced \n"
				 << "### with the ith wild card in lhs string.\n"
				 << "[file-name-dictionary]\n";
			for (size_t i=0; i<conf.dictionary_.size(); ++i)
				os << conf.dictionary_[i].first << " = " 
					 << conf.dictionary_[i].second << "\n"; 
		}
		if (!conf.string2codons_.empty()) {
			os << "\n"
				 << "### Section for setting parsing modes\n"
				 << "### The format of the entries is:\n"
				 << "###   file-name-pattern = \"start-code\" : \"end-code\"\n"
				 << "### The file-name-pattern may contain wildcards (such as '*' "
				 << "and '?').\n"
				 << "### String \"\\n\" can be used for codons containing newline"
				 << "\n### character.\n"
				 << "[parsing-codons]\n";
			for (size_t i=0; i<conf.string2codons_.size(); ++i) {
				os << conf.string2codons_[i].first << " = "; 
				for (size_t j=0; j<conf.string2codons_[i].second.size(); ++j) {
					if (j)
						os << "  ;  ";
					os << "\"" << trans_beg_code(conf.string2codons_[i].second[j].first) 
						 << "\":\"" 
						 << trans_end_code(conf.string2codons_[i].second[j].second) 
						 << "\""; 
				}
				os << "\n";
			}
		}
		return os;
	}

	
	Config_error::Config_error(const std::string& line,const std::string& message)
		: std::runtime_error(std::string("line: `") + line + 
												 std::string("' is invalid.\n") + message)
	{}

}} // end of namespace svndigest and namespace theplu
