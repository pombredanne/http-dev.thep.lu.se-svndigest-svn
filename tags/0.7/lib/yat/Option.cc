// $Id: Option.cc 1797 2009-02-12 18:07:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Option.h"
#include "CommandLine.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace theplu {
namespace yat {
namespace utility {


	Option::Option(CommandLine& cmd, std::string flag, std::string desc)
		: cmd_(cmd), description_(desc), present_(false)
	{
		if (flag.empty())
			throw std::runtime_error("yat: Option: given flag is empty");
		if (flag.size()==1 || (flag.size()==2 && flag[1]==','))
			short_name_ = flag[0];
		else if (flag[1]==','){
			short_name_ = flag[0];
			long_name_ = flag.substr(2);
			if (long_name_.size()==1)
				long_name_="";
		}
		else {
			short_name_ = '\0';
			long_name_=flag;
		}
		cmd.add(*this);
	}


	Option::~Option(void)
	{
	}


	const CommandLine& Option::cmd(void) const
	{
		return cmd_;
	}


	std::string Option::description(void) const
	{
		return description_;
	}


	std::string Option::long_name(void) const
	{
		return long_name_;
	}


	void Option::parse(std::vector<std::string>::iterator& first, 
										 const std::vector<std::string>::iterator& last)
	{
		present_=true;
		do_parse(first, last);
	}


	bool Option::present(void) const
	{
		if (!cmd().parsed()) {
			std::string s("Option::present called before Commandline was parsed");
			throw std::logic_error(s);
		}
		return present_;
	}


	std::string Option::print(void)
	{
		return print1()+print2()+print3()+std::string("\t")+print4();
	}


	std::string Option::print1(void) const
	{
		std::string str;
		if (short_name()){
			str = std::string("-")+short_name();
			if (!long_name().empty())
				str += std::string(",");
		}
		else
			str = std::string("   ");
		return str;
	}

		
	std::string Option::print2(void) const
	{
		if (long_name().size()) 
			return std::string(" --")+long_name();
		return std::string();
	}


	std::string Option::print3(void) const
	{
		return std::string();
	}


	std::string Option::print4(void) const
	{
		return description_;
	}


	void Option::reset(void)
	{
		present_=false;
	}


	char Option::short_name(void) const
	{
		return short_name_;
	}


	void Option::validate()
	{
		do_validate();
	}

}}} // of namespace utility, yat, and theplu
