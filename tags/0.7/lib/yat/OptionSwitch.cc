// $Id: OptionSwitch.cc 1797 2009-02-12 18:07:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "OptionSwitch.h"

#include <sstream>
#include <string>

namespace theplu {
namespace yat {
namespace utility {


	OptionSwitch::OptionSwitch(CommandLine& cmd, std::string flag, 
														 std::string desc, bool def)
		: Option(cmd, flag, desc), def_(def), switch_(def)
	{
	}


	void OptionSwitch::do_parse(std::vector<std::string>::iterator& first, 
															const std::vector<std::string>::iterator& last)
	{		
		if (first->size()>3 && first->substr(0,3)=="no-")
			switch_=false;
		else
			switch_=true;
		do_parse2(first, last);
	}


	void OptionSwitch::do_parse2(std::vector<std::string>::iterator first, 
															 std::vector<std::string>::iterator last)
	{		
	}


	std::string OptionSwitch::print2(void) const
	{
		std::string str;
		if (long_name().size()){ 
			str = " --";
			if (def_ && long_name().size()>3 && long_name().substr(0,3)!="no-")
				str += "no-";
			str += long_name();
		}
		return str;
	}


	void OptionSwitch::do_validate() const
	{		
	}


	bool OptionSwitch::value(void) const
	{ 
		return switch_; 
	}


}}} // of namespace utility, yat, and theplu
