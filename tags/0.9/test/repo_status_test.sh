#!/bin/sh

# $Id: repo_status_test.sh 978 2009-12-12 20:09:41Z peter $

# Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="wc"
. ./init.sh

set -e

# assume test repository and svndigest repository are synchronized
repository_status=1

# retrieve last revision in test repository
rev=`cat ${repo}/db/current`
rev=${rev%% *}

# check if the last test revision is a part of the svndigest repository
status_return=`$SVN status -v ${repo}/db/revs/$rev`
# logic: (file does not exist) || (file exists but not in revision control)
if [[ ${#status_return} -eq 0 || ${status_return:0:1} = "?" ]]; then
		repository_status=0
		echo "ERROR: test repository and svndigest repository are not synchronized:"
		echo "ERROR: test repository has entries not added to svndigest repository"
		echo "ERROR:"
		echo "ERROR: Please resolve issues before committing to svndigest repository"
		echo "ERROR: test revision(s) needs to be added the svndigest repository."
		echo "ERROR:"
		echo "ERROR: Issue these commands:"
fi

# loop until a test repository revision that is a part of svndigest
# repository is found, or break the test repository revision 0 is
# reached.
while [[ ${#status_return} -eq 0 || ${status_return:0:1} = "?" ]] && [ $rev -ge 0 ]; do
		echo "ERROR:    svn add test/repo/db/revs/$rev test/repo/db/revprops/$rev"
		let rev--
		status_return=`$SVN status -v ${repo}/db/revs/$rev`
done

if [ $repository_status -eq 0 ]; then
		exit_fail;		# test failed.
fi

exit_success