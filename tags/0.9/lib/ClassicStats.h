#ifndef _theplu_svndigest_classic_stats_
#define _theplu_svndigest_classic_stats_

// $Id: ClassicStats.h 1194 2010-10-03 23:39:12Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Stats.h"

namespace theplu{
namespace svndigest{

  ///
  /// Class taking care of statistics from svn.
  ///
  class ClassicStats : public Stats
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		explicit ClassicStats(const std::string& path);

    ClassicStats(const ClassicStats& other);

  private:
		void do_parse(const std::string&, svn_revnum_t);
		unsigned int max_element(const SumVector&) const; 

  };
}} // end of namespace svndigest end of namespace theplu

#endif 
