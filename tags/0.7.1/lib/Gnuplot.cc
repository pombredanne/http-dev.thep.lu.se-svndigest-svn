// $Id: Gnuplot.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "Gnuplot.h"

#include <cstdio>
#include <cstdlib>
#include <string>
#include <unistd.h>

#include <iostream>


namespace theplu {
namespace svndigest {


	Gnuplot::Gnuplot(void)
		: linestyle_("steps"), pipe_(NULL)
	{
		acquire_program_path("gnuplot");
		if (gnuplot_binary_.empty())
			throw GnuplotException("Can't find gnuplot in your PATH");

		pipe_=popen(gnuplot_binary_.c_str(),"w");
		if (!pipe_)
			throw GnuplotException("Could not open connection to gnuplot");
	}


	Gnuplot::~Gnuplot(void)
	{
		if (pclose(pipe_) == -1)
			throw GnuplotException("Problem closing communication to gnuplot");
	}


	void Gnuplot::acquire_program_path(const std::string& progname)
	{
		std::string tmp = GNUPLOT_PATH;
		if (tmp=="no")
			throw GnuplotException("no gnuplot binary available");
		if (!access(tmp.c_str(),X_OK)) {
			gnuplot_binary_=tmp;
		}

		if (gnuplot_binary_.empty())
			throw GnuplotException("Cannot find '" + tmp);
	}

	
	int Gnuplot::command(const std::string& cmdstr)
	{
		if (fputs(cmdstr.c_str(),pipe_)==EOF)
			return EOF;
		if ((*(cmdstr.rbegin())!='\n') && (fputc('\n',pipe_)==EOF))
			return EOF;
		return fflush(pipe_);
	}


	void Gnuplot::tokenizer(const std::string& in,
													std::list<std::string>& tokens,
													const std::string& delimiters)
	{
		std::string::size_type previous_pos=in.find_first_not_of(delimiters,0);
		std::string::size_type position=in.find_first_of(delimiters,previous_pos);
		while ((std::string::npos!=position) || (std::string::npos!=previous_pos)) {
			tokens.push_back(in.substr(previous_pos, position-previous_pos));
			previous_pos=in.find_first_not_of(delimiters,position);
			position=in.find_first_of(delimiters,previous_pos);
		}
	}


}} // end of namespace svndigest and namespace theplu
