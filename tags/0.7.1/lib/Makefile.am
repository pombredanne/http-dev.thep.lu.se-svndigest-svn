## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 867 2009-11-22 04:07:44Z peter $

# Copyright (C) 2005 Jari H�kkinen
# Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
# Copyright (C) 2009 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

MOVE_IF_CHANGE = $(SHELL) $(top_srcdir)/build_support/move-if-change
SVN_REVISION = `$(SVNVERSION) $(top_srcdir)`

SUBDIRS = yat .

AM_CPPFLAGS = $(SVNDIGEST_CPPFLAGS)
AM_CXXFLAGS = $(SVNDIGEST_CXXFLAGS)

noinst_LIBRARIES = libsvndigest.a

noinst_HEADERS = AddStats.h Alias.h BlameStats.h ClassicStats.h \
	Commitment.h Configuration.h copyright_year.h css.h\
	Date.h Directory.h File.h first_page.h Functor.h \
	Gnuplot.h GnuplotFE.h \
	HtmlBuf.h HtmlStream.h html_utility.h LineTypeParser.h \
	Node.h \
	OptionVersion.h rmdirhier.h \
	Stats.h StatsCollection.h subversion_info.h SVN.h SVNblame.h	\
	SVNinfo.h SVNlog.h SVNproperty.h Trac.h utility.h

libsvndigest_a_SOURCES = AddStats.cc Alias.cc BlameStats.cc \
	ClassicStats.cc \
	Commitment.cc Configuration.cc copyright_year.cc \
	css.cc Date.cc Directory.cc File.cc first_page.cc\
	Functor.cc Gnuplot.cc GnuplotFE.cc	HtmlBuf.cc HtmlStream.cc \
	html_utility.cc LineTypeParser.cc Node.cc \
	OptionVersion.cc \
	rmdirhier.cc Stats.cc StatsCollection.cc subversion_info.cc SVN.cc \
	SVNblame.cc SVNinfo.cc SVNlog.cc SVNproperty.cc Trac.cc utility.cc




clean-local: 
	rm -rf *~

all-local:

if HAVE_SVN_WC
$(srcdir)/subversion_info.cc: subversion_info.cc.tmp
	@$(MOVE_IF_CHANGE) subversion_info.cc.tmp $@

subversion_info.cc.tmp: FORCE
	@echo '// subversion_info.cc generated from subversion_info.cc.in.' > $@ ;\
	revision=$(SVN_REVISION);\
	$(SED) -e 's/sub_2_svn_revision/r'$$revision'/g' \
	$(srcdir)/subversion_info.cc.in >> $@ ;

# update copyright year automatically (if we build from svn wc)
$(srcdir)/copyright_year.cc: copyright_year.cc.tmp
	@$(MOVE_IF_CHANGE) copyright_year.cc.tmp $@

copyright_year.cc.tmp: FORCE
	@if (echo $(SVN_REVISION) | $(GREP) M); then \
	  $(SED) -e 's/"20[0-9][0-9]"/'\"`date -u "+%Y"`\"'/g' \
	    $(srcdir)/copyright_year.cc > $@ ; \
	else \
		cp $(srcdir)/copyright_year.cc $@; \
	fi

else
# this is needed in 'svn export' build
$(srcdir)/subversion_info.cc:
	$(SED) -e 's/sub_2_svn_revision//g' \
	$(srcdir)/subversion_info.cc.in >> $@ ;

endif

FORCE:
