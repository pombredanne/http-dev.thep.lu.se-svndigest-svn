#ifndef _theplu_yat_utility_deprecate_
#define _theplu_yat_utility_deprecate_ 

// $Id: deprecate.h 2062 2009-09-14 20:19:17Z peter $

/*
	Copyright (C) 2008, 2009 Peter Johansson

	This file is part of the yat library, http://trac.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

///
/// \file utility/deprecate.h
///

#include "config_public.h"

// allow user to define YAT_DEPRECATE and thereby turn off warning
// about deprecation
#ifndef YAT_DEPRECATE
#ifdef YAT_HAVE_GCC_DEPRECATED
/// if supported by compiler define attribute deprecated
#define YAT_DEPRECATE __attribute__((deprecated))
#else
/// otherwise define it as empty
#define YAT_DEPRECATE
#endif
#endif

// With GCC version < 4.3 deprecation of classes yields no
// warning. Therefore, we deprecate the constructors of those classes
// with this macro YAT_DEPRECATE_GCC_PRE4_3, which is mute for
// compiler GCC 4.3 and newer and for others compilers than GCC.
#ifndef YAT_DEPRECATE_GCC_PRE4_3
#if defined(__GNUC__) && ((__GNUC__<4) || (__GNUC__==4 && __GNUC_MINOR__<3)) 
/// if GCC version before 4.3 define as YAT_DEPRECATE defined above
#define YAT_DEPRECATE_GCC_PRE4_3 YAT_DEPRECATE
#else
/// with GCC 4.3 and newer (or other compilers) define it as empty
#define YAT_DEPRECATE_GCC_PRE4_3
#endif
#endif

#endif
