#ifndef _theplu_svndigest_gnuplotfe_
#define _theplu_svndigest_gnuplotfe_

// $Id: GnuplotFE.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Gnuplot.h"
#include <string>
#include <vector>


namespace theplu {
namespace svndigest {

	///
	/// The GnuplotFE class is a front end to the Gnuplot class. This is
	/// a utility class needed to communicate plotting related
	/// information between objects.
	///
	/// GnuplotFE provides one single global access point to the
	/// underlying gnuplot binary and makes sure that there is only one
	/// point of access to the binary.
	///
	/// @see Design Patterns (the singleton pattern).
	/// @see Gnuplot
	///
	class GnuplotFE : public Gnuplot
	{
	public:
		///
		/// The destructor.
		///
		~GnuplotFE(void) { delete instance_; }

		///
		/// @return dates.
		///
		const std::vector<std::string>& dates(void) const { return date_; }

		///
		/// Plot the data \a y. If a date vector is set with set_dates
		/// function before calling this function, the date vector will be
		/// used as x values using \a format as date format
		/// description. If no date vector is set then the \a y data will
		/// be plot as a function of subversion revision.
		///
		/// @see Gnuplot documentation for date formats.
		///
		/// @note \a y may change in this call: In some cases the date and
		/// \a y data vectors may have different length. This is an effect
		/// from subversions treatment of revisions, i.e., subversion
		/// controlled items may have different revisions but still up to
		/// date (at the items last changed revision). The date vector
		/// typically contains all revisions of the repository and thus a
		/// clash of different sized vectors may occur in gnuplot. This
		/// situation is resolved by adding elements to \a y such that the
		/// vector sizes match. The date vector is never expected to be
		/// shorter than \a y vector.
		///
		void plot(std::vector<unsigned int>& y,const std::string& format="%y-%b");

		///
		/// Plot the data \a y as a function of \a x using the Gnuplot
		/// 'replot' command, see plot documentation for information on x
		/// axis data and possible modifications of \a y.
		///
		void replot(std::vector<unsigned int>& y);

		///
		/// @throw Re-throws a GnuplotException from Gnuplot.
		///
		static GnuplotFE* instance(void)
		{ if (!instance_) instance_=new GnuplotFE; return instance_; }

		///
		/// sets format of date output.
		///
		inline void set_date_format(const std::string& format)
		{ date_output_format_ = format;}

		///
		/// Function setting the dates. \a format must comply with strings
		/// in date.
		///
		inline void set_dates(const std::vector<std::string>& date,
													const std::string& format="%Y-%m-%dT%H:%M:%SZ")
		{ date_=date; date_input_format_=format; }

		///
		/// Set the upper value for the y axis, the lower values is always
		/// zero. Call this function with an argument 0 (or without an
		/// argument) to cancel the current setting. Negative argument
		/// values are treated as zero value.
		///
		/// @return Returns the actual set upper value. A zero value is
		/// returned if the current setting was cancelled.
		///
		/// @see Gnuplot documentation for yrange
		///
		double yrange(double ymax=0.0);

	private:
		///
		/// @throw Re-throws a GnuplotException from Gnuplot.
		///
		GnuplotFE(void) {};

		///
		/// Copy constructor, not implemented.
		///
		GnuplotFE(const GnuplotFE&);

		std::vector<std::string> date_;
		std::string date_input_format_;
		std::string date_output_format_;
		static GnuplotFE* instance_;
};

}} // end of namespace svndigest and namespace theplu

#endif
