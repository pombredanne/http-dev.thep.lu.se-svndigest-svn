// $Id: utility_test.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "../lib/utility.h"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <string>

bool test_hex(int, unsigned int, std::string);
bool test_equal(bool, std::string, std::string);
bool test_regexp(bool, std::string, std::string, 
								 const std::vector<std::string>&);

int main(const int argc,const char* argv[])
{
	bool ok=true;
	
	ok &= test_hex(15,2, "0f"); 
	ok &= test_hex(17,1, "1"); 
	ok &= test_hex(16,2, "10"); 

	ok &= test_equal(true,"peter", "peter"); 
	ok &= test_equal(false,"peter", "peterj"); 
	ok &= test_equal(true,"p*", "peterj"); 
	ok &= test_equal(true,"peter", "p*"); 
	ok &= test_equal(false,"peter", "p*j"); 
	ok &= test_equal(true,"peter", "*peter"); 

	std::vector<std::string> vec;
	ok &= test_regexp(true,"abcde", "abcde", vec); 
	vec.push_back("");
	ok &= test_regexp(true,"abcde", "abcd?e", vec); 
	vec[0]="c";
	ok &= test_regexp(true,"abcde", "ab?de", vec); 
	vec[0] = "bcd";
	ok &= test_regexp(true,"abcde", "a*e", vec); 
	vec.push_back("");
	ok &= test_regexp(true,"abcddf", "a*d*f", vec); 
	vec[0] = "bc";
	vec[1] = "ef";
	ok &= test_regexp(true,"abcdefg", "a*d*g", vec); 
	vec.push_back("");
	ok &= test_regexp(true,"abcdefg", "a*d*?g", vec); 

	if (ok)
		return 0;
  return 1;
}

bool test_equal(bool answ, std::string a, std::string b)
{
	if (theplu::svndigest::equal(a.begin(), a.end(), b.begin(), b.end())==answ)
		return true;
	std::cerr << "equal(" << a << ", " << b << ") results "
						<< theplu::svndigest::equal(a.begin(), a.end(),b.begin(), b.end()) 
						<< ". Expects " << answ << std::endl;
	return false;
}

bool test_hex(int x, unsigned int w, std::string facit)
{
	if (theplu::svndigest::hex(x,w)==facit)
		return true;
	std::cerr << "hex(" << x << ", " << w << ") results "
						<< theplu::svndigest::hex(x,w) << ". Expects " << facit 
						<< std::endl;
	return false;
}

bool test_regexp(bool ans, std::string a, std::string b, 
								 const std::vector<std::string>& vec)
{
	using namespace theplu::svndigest;
	std::vector<std::string> v;
	bool res = regexp(a.begin(), a.end(), b.begin(), b.end(), v);
	if (res!=ans || v!=vec) {
		std::cerr << "regexp(" << a << ", " << b << ") results "
							<< res << ". Expected " << ans << "\n"
							<< "resulting vector:\n";
		std::copy(v.begin(), v.end(), 
							std::ostream_iterator<std::string>(std::cerr, "\n"));
		std::cerr << "expected:\n";
		std::copy(vec.begin(), vec.end(), 
							std::ostream_iterator<std::string>(std::cerr, "\n"));
		return false;
	}
	return true;

}
