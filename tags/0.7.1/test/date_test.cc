// $Id: date_test.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Date.h"

#include <iostream>
#include <string>

int main(const int argc,const char* argv[])
{
	using namespace theplu::svndigest;
	bool ok=true;

	Date date("2007-06-27T15:40:52.123456Z");
	if (date("%H:%M")!="15:40"){
		ok = false;
		std::cerr << "date(\"%H:%M\") returns: " << date("%H:%M")
							<< "\nexpected '15:40'" << std::endl;
	}
			
	if (ok)
		return 0;
  return -1;
}

