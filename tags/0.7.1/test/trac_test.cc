// $Id: trac_test.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Configuration.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "Trac.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

bool test(std::string mess, std::string href, std::ostream&);
bool test_no_anchor(std::string str, std::ostream&);

int main(const int argc,const char* argv[])
{
	using namespace theplu::svndigest;
	bool ok=true;
	std::ostream& my_out(std::cout);

	// faking a config file
	Configuration& conf = Configuration::instance();
	std::stringstream ss;
	ss << "[trac]\ntrac-root = http://trac.domain.org/\n";
	conf.load(ss);

	
	ok &= test("r123", conf.trac_root()+"changeset/123", my_out);
	ok &= test("[123]", conf.trac_root()+"changeset/123", my_out);
	ok &= test("changeset:123", conf.trac_root()+"changeset/123", my_out);
	ok &= test("comment:ticket:123:1", 
						 conf.trac_root()+"ticket/123#comment:1", my_out);
	ok &= test("diff:trunk@12:123", conf.trac_root()+
						 "changeset?new=123&amp;new_path=trunk&amp;"+
						 "old=12&amp;old_path=trunk", 
						 my_out);
	ok &= test("diff:tags/1.0", conf.trac_root()+
						 "changeset?new_path=tags/1.0&amp;old_path=tags/1.0", 
						 my_out);
	ok &= test("diff:tags/1.0//tags/1.0.1", conf.trac_root()+
						 "changeset?new_path=tags/1.0.1&amp;old_path=tags/1.0", 
						 my_out);
	ok &= test("diff:tags/1.0@123//trunk@236", conf.trac_root()+
						 "changeset?new=236&amp;new_path=trunk&amp;"+
						 "old=123&amp;old_path=tags/1.0", 
						 my_out);
	ok &= test("r123:236", conf.trac_root()+"log/?rev=236&amp;stop_rev=123", 
						 my_out);
	ok &= test("[123:236]",conf.trac_root()+"log/?rev=236&amp;stop_rev=123", 
						 my_out);
	ok &= test("log:trunk@123:236", 
						 conf.trac_root()+"log/trunk?rev=236&amp;stop_rev=123", my_out);
	ok &= test("milestone:1.0", conf.trac_root()+"milestone/1.0", my_out);
	ok &= test("source:trunk", conf.trac_root()+"browser/trunk", my_out);
	ok &= test("source:trunk@123", conf.trac_root()+"browser/trunk?rev=123", 
						 my_out);
	ok &= test("source:trunk@123#L3", 
						 conf.trac_root()+"browser/trunk?rev=123#L3", my_out);
	ok &= test("#65", conf.trac_root()+"ticket/65", my_out);
	ok &= test("ticket:65", conf.trac_root()+"ticket/65", my_out);
	ok &= test_no_anchor("Container2D", my_out);
	ok &= test_no_anchor("r2b", my_out);
	ok &= test_no_anchor("ar2", my_out);
	ok &= test_no_anchor("2r2", my_out);
	ok &= test_no_anchor("r2r3", my_out);
	ok &= test_no_anchor("ar2:3", my_out);
	ok &= test_no_anchor("r2:3a", my_out);

	if (ok)
		return 0;
  return -1;
}

bool test(std::string mess, std::string href, std::ostream& out)
{
	using namespace theplu::svndigest;
	std::stringstream ss;
	HtmlStream html(ss);
	Trac trac(html);
	trac.print(mess,80);
	if (ss.str()==anchor(href, mess))
		return true;
	out << "error:\n";
	out << "  message: " << mess << std::endl;
	out << "  trac generates output:\n     " << ss.str() << std::endl;
	out << "  expected:\n     " << anchor(href, mess) << std::endl;
	return false;
}

bool test_no_anchor(std::string str, std::ostream& os)
{
	using namespace theplu::svndigest;
	std::stringstream ss;
	HtmlStream html(ss);
	Trac trac(html);
	trac.print(str,80);
	if (ss.str()!=str) {
		os << "error:\n";
		os << "  message: " << str << std::endl;
		os << "  trac generates output:\n     " << ss.str() << std::endl;
		os << "  expected:\n     " << str << std::endl;
		return false;
	}
	return true;
}
