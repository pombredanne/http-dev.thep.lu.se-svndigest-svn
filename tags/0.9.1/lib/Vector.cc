// $Id: Vector.cc 1208 2010-10-07 03:33:36Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Vector.h"

namespace theplu{
namespace svndigest{


	void accumulate(const SparseVector& vec, SumVector& result)
	{
		result.clear();
		int value = 0;
		for (SparseVector::const_iterator iter = vec.begin(); 
				 iter!=vec.end();++iter) {
			value += iter->second;
			assert(value>=0);
			result.set(iter->first, value);
		}
	}

}} // end of namespace svndigest and namespace theplu
