#ifndef _theplu_svndigest_configuration_
#define _theplu_svndigest_configuration_

// $Id: Configuration.h 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Alias.h"

#include <iostream>
#include <map>
#include <string>
#include <utility>

namespace theplu{
namespace svndigest{

	///
	/// Configuration class takes care of all setting defined in the
	/// configuration file.
	///
	class Configuration
	{
	public:
		static Configuration& instance(void);

		/// 
		/// @brief load deafult configuration
		/// 
		void load(void);

		/// 
		/// throw if stream is not a valid config
		///
		/// @brief load configuration from stream
		/// 
		void load(std::istream&);

		///
		/// @brief Aliases for Copyright
		///
		const std::map<std::string, Alias>&	copyright_alias(void) const;

		///
		/// @return root for the trac envrionment, e.g.,
		/// http://trac.thep.lu.se/trac/svndigest/
		///
		std::string trac_root(void) const;

	private:
		/// 
		/// Creates a Config object with default settings.
		///
		/// @brief Default Constructor 
		/// 
		Configuration(void);
		// Copy Constructor not implemented
		Configuration(const Configuration&);
		// assignment not implemented because assignment is always self-assignment
		Configuration& operator=(const Configuration&);

		void clear(void);

		void set_default(void);

		static Configuration* instance_;

		std::map<std::string, Alias> copyright_alias_;

		std::string trac_root_;
	};

	///
	/// @brief Output operator
	///
	std::ostream& operator<<(std::ostream&, const Configuration&);

}} // end of namespace svndigest and namespace theplu

#endif


