// $Id: File.cc 74 2006-03-07 15:46:59Z jari $

#include "File.h"
#include "Node.h"
#include "Stats.h"
#include "utility.h"

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace theplu{
namespace svnstat{

  const Stats& File::parse(const bool verbose)
  {
		if (verbose)
			std::cout << "Parsing " << path_ << std::endl; 
    stats_.reset();

		std::map<std::string,std::string> svn_info = info(path_);
		author_ = svn_info["Last Changed Author"];
		std::stringstream ss(svn_info["Last Changed Rev"]);
		ss >> revision_;

		binary_ = stats_.parse(path_);
    return stats_;
  }

	void File::print(const bool verbose) const 
	{
		std::string output(output_name() + ".html");
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		std::ofstream os(output.c_str());
		print_header(os);
		os << "<p align=center>\n<img src='" << stats_.plot(output_name()+".png")
			 << "' alt='[plot]' border=0>\n</p>";
		print_footer(os);
		os.close();	

	}

}} // end of namespace svnstat and namespace theplu
