// $Id: Directory.cc 79 2006-03-10 11:44:45Z jari $

#include "Directory.h"
#include "File.h"
#include "Node.h"

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>

#include <cerrno>	// Needed to check error state below.
#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svnstat{

	Directory::Directory(const std::string& path, const std::string& output)
		: Node(path,output)
	{
		using namespace std;
		DIR* directory=opendir(path.c_str());    // C API from dirent.h
		if (!directory) {
			// Jari, throw exception
			cerr << "ERROR: opendir() failed; terminating" << endl;
			exit(1);
		}
		list<string> entries;
		struct dirent* entry;
		errno=0;	// Global variable used by C to track errors, from errno.h
		while ((entry=readdir(directory)))       // C API from dirent.h
			entries.push_back(string(entry->d_name));
		if (errno) {
			// Jari, throw exception
			cerr << "ERROR: readdir() failure; terminating" << endl;
			exit(1);
		}
		closedir(directory);

		// Jari, change this to some STL algo?
		for (list<string>::iterator i=entries.begin(); i!=entries.end(); i++)
			if ((*i)!=string(".") && (*i)!=string("..") && (*i)!=string(".svn")) {
				string fullpath(path_+'/'+(*i));
				struct stat nodestat;                // C api from sys/stat.h
				lstat(fullpath.c_str(),&nodestat);   // C api from sys/stat.h
				if (S_ISDIR(nodestat.st_mode))       // C api from sys/stat.h
					daughters_.push_back(new Directory(fullpath, output_name()+"_"));
				else
					daughters_.push_back(new File(fullpath,output_name()+"_"));
			}
	}


	Directory::~Directory(void)
	{
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); i++)
			delete *i;
	}


	const Stats& Directory::parse(const bool verbose)
	{
	  stats_.reset();

		// Directories give no contribution to statistics.
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); i++)
	    stats_ += (*i)->parse(verbose);
	  return stats_;
	}


	void Directory::print(const bool verbose) const
	{
		std::string output(output_name() + ".html");
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		std::ofstream os(output.c_str());
		print_header(os);
		os << "<p align=center>\n<img src='" << stats_.plot(output_name()+".png")
			 << "' alt='[plot]' border=0><br>\n";
		os << "<table>\n";
		os << "<tr><td><strong>Node</strong></td>\n";
		os << "<td><strong>Count</strong></td></tr>\n";
		os << "<tr><td>Total</td>\n";
		os << "<td align=right>" << stats_.rows() << "</td></tr>\n";
		// print html links to daughter nodes
		transform(daughters_.begin(), daughters_.end(), 
							std::ostream_iterator<std::string>(os," "), 
							std::mem_fun(&Node::html_tablerow));
		os << "</table>\n";
		os << "</p>";
		print_footer(os);
		os.close();	

		// print daughter nodes, i.e, this function is recursive
		std::for_each(daughters_.begin(), daughters_.end(),
									std::bind2nd(std::mem_fun(&Node::print),verbose));
	}

	void Directory::purge(const bool verbose) 
	{
		if (verbose)
			std::cout << "Purging " << path_ << std::endl;
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); )
			if (!((*i)->subversion_controlled())) {
				delete *i;
				i=daughters_.erase(i);
			}
			else {
				(*i)->purge(verbose);
				i++;
			}
	}

}} // end of namespace svnstat and namespace theplu
