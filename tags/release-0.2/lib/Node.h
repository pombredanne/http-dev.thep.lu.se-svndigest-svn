// $Id: Node.h 80 2006-03-10 16:52:04Z jari $

#ifndef _theplu_svnstat_node_
#define _theplu_svnstat_node_

#include "Stats.h"

#include <ostream>
#include <sstream>
#include <string>

namespace theplu{
namespace svnstat{

  ///
  /// Abstract Base Class for files.
  ///
  class Node
  {
  public:
    /// 
    /// @brief Constructor 
    /// 
		Node(const std::string& path, const std::string& output="")
			: path_(path), stats_(path)
			{ output_name_ = output + name(); }

		///
		/// @brief Destructor
		///
		virtual inline ~Node(void) {};

		///
		/// @return A properly formatted html link to this node.
		///
		inline std::string html_link(void) const
		{ return "<a href=\"" + output_name() + ".html\">" + name() + "</a>"; }

		inline std::string html_tablerow(void) const
		{
			std::stringstream ss;
			ss << "<tr><td>" << html_link() << "</td><td align=right>" << stats_.rows()
				 << "</td></tr>\n";
			return ss.str();
		}

		inline const std::string& output_name(void) const { return output_name_; }

    ///
    /// @brief parsing file using svn blame.
    ///
    virtual const Stats& parse(const bool verbose=false)=0;

		///
		/// Function printing HTML in current working directory
		///
		virtual void print(const bool verbose=false) const=0;

		///
		///
		///
		inline virtual void purge(const bool verbose=false) 
		{ /* Nothing to be done */ };

		///
		/// Check if the node is under subversion control. This is done by
		/// checking the return status of 'svn proplist <Node>.
		///
		/// @return True if subversion controlled, false otherwise.
		///
		bool subversion_controlled(void) const;

  protected:
		///
		/// Function returning everything after the last '/'
		///
		/// @return name of node (not full path)
		///
		std::string name(void) const;

		///
		/// @brief print html footer of page
		///
		void print_footer(std::ostream&) const;
		
		///
		/// @brief print html header of page
		///
		void print_header(std::ostream&) const;

		std::string output_name_; //without suffix
    std::string path_;
    Stats stats_;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		Node(const Node&);

	};

}} // end of namespace svnstat and namespace theplu

#endif
