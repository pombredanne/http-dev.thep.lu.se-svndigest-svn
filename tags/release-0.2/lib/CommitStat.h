//$Id: CommitStat.h 73 2006-03-04 18:10:07Z jari $

#ifndef _theplu_svnstat_commit_stat_
#define _theplu_svnstat_commit_stat_

#include <string>
#include <vector>

namespace theplu{
namespace svnstat{

  ///
  /// Class for parsing and storing information from svn log
  ///
  class CommitStat
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		inline CommitStat(void) {}

		inline const std::vector<std::string>& dates(void) { return date_; }

		///
		/// Function parsing output from log() \a path'.
		///
		/// @return return value from system call
		///
		int parse(const std::string& path);

		inline const std::vector<std::string>& date(void) const { return date_; }

  private:
		///
		/// @return return value from 'svn log -q -r 1:HEAD \a path'
		///
		int log(const std::string& path) const;

		void reset(void);

		std::vector<std::string> date_;

  };
}}
// end of namespace svnstat end of namespace theplu

#endif 
