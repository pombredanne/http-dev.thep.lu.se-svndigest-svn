//$Id: utility.h 71 2006-02-23 09:25:41Z peter $

#ifndef _theplu_svnstat_utility_
#define _theplu_svnstat_utility_

#include <algorithm>
#include <functional>
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace theplu{
namespace svnstat{

	///
	/// @return 0 if ok
	/// 
	int blame(const std::string&);

	///
	/// Extracts information from 'svn info <node>'
	///
	/// @note <node> must be in subversion control.
	///
	std::map<std::string, std::string> info(const std::string&);

//	int log(const std::string&);

	///
	/// Calculating sum of two vectors.
	///
	/// @return resulting vector
	///
	template <typename T >
	struct VectorPlus : 
		public std::binary_function<std::vector<T>,std::vector<T>,std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& u,
															const std::vector<T>& v) const 
		{
			if ( u.size() > v.size() ){
				std::vector<T> res(u.size());
				transform(u.begin(), u.end(), v.begin(), res.begin(), std::plus<T>());
				copy(u.begin()+v.size(), u.end(), res.begin()+v.size());
				return res;
			}
	
			std::vector<T> res(v.size());
			transform(v.begin(), v.end(), u.begin(), res.begin(), std::plus<T>());
			if ( v.size() > u.size() )
				copy(v.begin()+u.size(), v.end(), res.begin()+u.size());
			return res;
		}

	};

  ///
  /// @return resulting vector
  ///
  template <typename Key, typename T>
	struct PairValuePlus :
		public std::binary_function<std::vector<T>,
																std::pair<const Key, std::vector<T> >, 
																std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& sum, 
															const std::pair<const Key,std::vector<T> >& p)
		{
			return VectorPlus<T>()(sum, p.second);
		}
	};

	///
	/// Jari, is this obsolete? And thus erasable?
	///
	struct CodingMore :
		public std::binary_function<std::pair<std::string,std::vector<u_int> >,
																std::pair<std::string,std::vector<u_int> >,
																bool >
	{
		inline bool operator()
			(const std::pair<std::string,std::vector<u_int> >& a, 
			 const std::pair<std::string,std::vector<u_int> >& b)
		{
			if (a.second.back() > b.second.back())
				return true;
			else if (a.second.back() < b.second.back())
				return false;
			return a.first < b.first;
		}
	};

}}

// end of namespace svnstat end of namespace theplu
#endif 
