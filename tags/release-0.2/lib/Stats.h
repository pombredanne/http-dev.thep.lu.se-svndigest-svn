//$Id: Stats.h 80 2006-03-10 16:52:04Z jari $

#ifndef _theplu_svnstat_stats_
#define _theplu_svnstat_stats_

#include <iostream>
#include <map>
#include <ostream>
#include <string>
#include <vector>

namespace theplu{
namespace svnstat{

  ///
  /// Class taking care of statistics from svn.
  ///
  class Stats
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		explicit Stats(const std::string& path);

		///
		/// @return true if file is binary
		///
		bool parse(const std::string&);

		///
		/// Create statistics graph.
		///
		std::string plot(const std::string&) const;

		///
		/// @brief Clear all statistics
		///
    inline void reset(void) { map_.clear(); }

		///
		///
		///
    inline u_int rows(void) const { return accumulated().back(); }

    ///
    /// @return resulting Stats
    ///
    Stats& operator+=(const Stats&);

  private:
		///
		/// Copy constructor (not implemented)
		///
    Stats(const Stats& other);

    ///
    /// @return accumulated vector of total
    ///
    std::vector<u_int> accumulated(void) const;

    ///
    /// @return accumulated vector of stats_[user]
    ///
    std::vector<u_int> accumulated(const std::string& user) const;

    ///
    /// @brief adding a line to user from revision to the stats
    ///
    void add(const std::string& user, const u_int& revision); 


		u_int latest_revision_; // Should be the latest revision for whole project

    // Peter, if the vector is sparse make it a map
    typedef std::map<std::string, std::vector<u_int> > Map_;
    typedef Map_::iterator MapIter_;
    typedef Map_::const_iterator MapConstIter_;
    Map_ map_;
  };
}} // end of namespace svnstat end of namespace theplu

#endif 
