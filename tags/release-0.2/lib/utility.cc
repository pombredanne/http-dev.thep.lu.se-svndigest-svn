// $Id: utility.cc 80 2006-03-10 16:52:04Z jari $

#include "utility.h"

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace theplu{
namespace svnstat{

	int blame(const std::string& path)
	{
		std::string system_call="svn blame " + path + " 1> svnstat.tmp 2> /dev/null";
		int system_return = system(system_call.c_str());
		if (system_return)
			std::cerr << "Error: svn blame " << path << std::endl;     
		return system_return;
	}

	std::map<std::string, std::string> info(const std::string& path) 
	{
		std::string system_call="svn info " + path + " 1> svnstat.tmp 2> /dev/null";
		int system_return = system(system_call.c_str());
		if (system_return)
			return std::map<std::string, std::string>();

		std::ifstream is("svnstat.tmp");
		std::string line;
		std::map<std::string, std::string> svn_info;
		while (getline(is,line)){
			std::stringstream ss(line);
			std::string tag;
			getline(ss,tag,':');
			ss >> svn_info[tag];
		}
		return  svn_info;
  }

}} // end of namespace svnstat and namespace theplu
