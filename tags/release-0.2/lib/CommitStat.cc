// $Id: CommitStat.cc 73 2006-03-04 18:10:07Z jari $

#include "CommitStat.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace theplu{
namespace svnstat{

  int CommitStat::log(const std::string& path) const
  {
    std::string system_call = "svn log -q -r HEAD:1 " + path + 
			" > svnstat.log.tmp";
		int system_return = system(system_call.c_str());
    if (system_return)
      std::cerr << "Error: svn log " << path << std::endl;     
    return system_return;
  }


  int CommitStat::parse(const std::string& path)
	{
		reset();
		int system_return = log(path);
		
    std::ifstream is("svnstat.log.tmp");
		std::string line;
		typedef std::vector<std::string>::iterator DateIter;
		size_t rev_last_iteration=0;
		std::string date;
    while (getline(is,line, '\n')){
      if (!line.size()) // skip empty line
				continue;
      std::stringstream ss(line);

			if (ss.get() == 'r'){
				size_t revision;
				ss >> revision;
				assert(revision);
				std::string tmp;
				ss >> tmp;
				std::string user;
				getline(ss,user,'|');
				ss >> date; 
				std::string time;
				ss >> time;

				if (revision+1 > date_.size()){
					date_.resize(revision+1);
					rev_last_iteration=revision+1;
				}
				assert(revision<=rev_last_iteration);
				std::fill(date_.begin()+revision,
									date_.begin()+rev_last_iteration,
									date);
				rev_last_iteration=revision;
			}
			std::fill(date_.begin(),
								date_.begin()+rev_last_iteration,
								date);
    }
    is.close();

		assert(date_.size());
		assert(date_[0].size());
		return system_return;
	}


	void CommitStat::reset(void)
	{
		date_.clear();
	}

}} // end of namespace svnstat and namespace theplu
