// $Id: GnuplotFE.h 74 2006-03-07 15:46:59Z jari $

#ifndef _theplu_svnstat_gnuplotfe_
#define _theplu_svnstat_gnuplotfe_

#include "Gnuplot.h"
#include <string>
#include <vector>


namespace theplu {
namespace svnstat {

	///
	/// The GnuplotFE class is a front end to the Gnuplot class. This is
	/// a utility function needed to communicate plotting related
	/// information between objects.
	///
	/// GnuplotFE provides one single global access point to the
	/// underlying gnuplot binary and makes sure that there is only one
	/// point of access to the binary.
	///
	/// @see Design Patterns (the singleton pattern).
	///
	class GnuplotFE : public Gnuplot
	{
	public:
		///
		/// The destructor.
		///
		~GnuplotFE(void) { delete instance_; }

		///
		/// @return input format for date.
		///
		// Peter, obsolete ?
		const std::string& date_input_format(void) { return date_input_format_; }

		///
		///
		///
		void plot(const std::vector<u_int>& y,const std::string& format="%y-%b-%d");

		///
		///
		///
		void replot(const std::vector<u_int>& y);

		///
		/// @throw Re-throws a GnuplotException from Gnuplot.
		///
		static GnuplotFE* instance(void)
		{ if (!instance_) instance_=new GnuplotFE; return instance_; }

		///
		/// sets format of date output.
		///
		inline void set_date_format(const std::string& format)
		{ date_output_format_ = format;}

		///
		/// Function setting the dates. \a format must comply with strings
		/// in date.
		///
		inline void set_dates(const std::vector<std::string>& date,
													const std::string& format="%Y-%m-%d")
		{ date_=date; date_input_format_=format; }

		///
		/// Set the upper value for the y axis, the lower values is always
		/// zero. Call this function with an argument 0 (or without an
		/// argument) to cancel the current setting. Negative argument
		/// values are treated as zero value.
		///
		/// @return Returns the actual set upper value. A zero value is
		/// returned if the current setting was cancelled.
		///
		/// @see Gnuplot documentation for yrange
		///
		double yrange(double ymax=0.0);

	private:
		///
		/// @throw Re-throws a GnuplotException from Gnuplot.
		///
		GnuplotFE(void) {};

		///
		/// Copy constructor, not implemented.
		///
		GnuplotFE(const GnuplotFE&);

		std::vector<std::string> date_;
		std::string date_input_format_;
		std::string date_output_format_;
		static GnuplotFE* instance_;
};

}} // end of namespace svnstat and namespace theplu

#endif
