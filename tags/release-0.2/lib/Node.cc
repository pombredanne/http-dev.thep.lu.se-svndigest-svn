// $Id: Node.cc 79 2006-03-10 11:44:45Z jari $

#include "Node.h"
#include "utility.h"

#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

namespace theplu{
namespace svnstat{


	std::string Node::name(void) const
	{
		std::stringstream ss(path_);
		std::string name;
		while (getline(ss,name,'/')) {}
		return name;
	}


	void Node::print_footer(std::ostream& os) const
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		os << "<p align=center><font size=-2>Generated on "
			 << asctime (timeinfo) 
			 << "by <a href=http://lev.thep.lu.se/trac/svnstat/>svnstat</a>"
			 << "</font></p></body>\n</html>\n";
	}


	void Node::print_header(std::ostream& os) const
	{
		os << "<html>\n"
			 << "<head>\n"
			 << "<title> svnstat " << name() << "</title>\n"
			 << "</head>\n"
			 << "<body bgcolor='FFFBFB'>\n";
	}


	bool Node::subversion_controlled(void) const
	{
    std::string system_call = "svn proplist " + path_ + ">&/dev/null";
    return !system(system_call.c_str());
	}

}} // end of namespace svnstat and namespace theplu
