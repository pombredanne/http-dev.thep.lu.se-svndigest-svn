// $Id: Directory.h 60 2006-01-16 10:03:04Z peter $

#ifndef _theplu_svnstat_directory_
#define _theplu_svnstat_directory_

#include "Node.h"

#include <list>
#include <string>

namespace theplu{
namespace svnstat{

	///
	/// Class taking care of directories.
	///
	class Directory : public Node
	{
	public:
		///
		/// @brief Constructor
		///
		/// Recursively create a directory tree starting from \a path. All
		/// entries except explicit directories are treated as File nodes,
		/// i.e. symbolic links to directories are treated as File
		/// nodes. This will ensure that the directory structure is a tree
		/// and double counting of branches is avoided.
		///
		/// @note Nodes named '.', '..', and '.svn' are ignored and not
		/// traversed.
		///
		Directory(const std::string& path, const std::string& output="");

		///
		/// @brief Destructor
		///
		~Directory(void);

		const Stats& parse(const bool verbose=false);

		void print(const bool verbose=false) const;

		void purge(const bool verbose=false);

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		Directory(const Directory&);

		typedef std::list<Node*>::iterator NodeIterator;
		typedef std::list<Node*>::const_iterator NodeConstIter_;
		std::list<Node*> daughters_;
	};

}} // end of namespace svnstat and namespace theplu

#endif
