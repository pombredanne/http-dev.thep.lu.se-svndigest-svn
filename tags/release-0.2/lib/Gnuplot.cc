// $Id: Gnuplot.cc 74 2006-03-07 15:46:59Z jari $

#include "Gnuplot.h"

#include <cstdio>
#include <string>
#include <unistd.h>

#include <iostream>


namespace theplu {
namespace svnstat {


	Gnuplot::Gnuplot(void)
		: linestyle_("steps"), pipe_(NULL)
	{
		acquire_program_path("gnuplot");
		if (gnuplot_binary_.empty())
			throw GnuplotException("Can't find gnuplot in your PATH");

		pipe_=popen(gnuplot_binary_.c_str(),"w");
		if (!pipe_)
			throw GnuplotException("Could'nt open connection to gnuplot");
	}


	Gnuplot::~Gnuplot(void)
	{
		if (pclose(pipe_) == -1)
			throw GnuplotException("Problem closing communication to gnuplot");
	}


	void Gnuplot::acquire_program_path(const std::string& progname)
	{
		char* env_path=getenv("PATH");	// is there a need to free this memory?
		if (!env_path)
			throw GnuplotException("Environment variable PATH is not set");

		std::list<std::string> paths;
		tokenizer(env_path,paths);
		for (std::list<std::string>::const_iterator i=paths.begin();
				 i!=paths.end(); ++i) {
			std::string tmp((*i) + '/' + progname);
			if (!access(tmp.c_str(),X_OK)) {
				gnuplot_binary_=tmp;
				break;
			}
		}

		if (gnuplot_binary_.empty())
			throw GnuplotException("Cannot find '" + progname + "' in PATH");
	}

	
	void Gnuplot::command(const std::string& cmdstr)
	{
		fputs(cmdstr.c_str(),pipe_);
		if (*(cmdstr.rbegin())!='\n')
			fputc('\n',pipe_);
		fflush(pipe_);
	}


	void Gnuplot::tokenizer(const std::string& in,
													std::list<std::string>& tokens,
													const std::string& delimiters)
	{
		std::string::size_type previous_pos=in.find_first_not_of(delimiters,0);
		std::string::size_type position=in.find_first_of(delimiters,previous_pos);
		while ((std::string::npos!=position) || (std::string::npos!=previous_pos)) {
			tokens.push_back(in.substr(previous_pos, position-previous_pos));
			previous_pos=in.find_first_not_of(delimiters,position);
			position=in.find_first_of(delimiters,previous_pos);
		}
	}


}} // end of namespace svnstat and namespace theplu
