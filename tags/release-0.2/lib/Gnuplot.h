// $Id: Gnuplot.h 73 2006-03-04 18:10:07Z jari $

#ifndef _theplu_svnstat_gnuplot_
#define _theplu_svnstat_gnuplot_

#include <cassert>
#include <cstdio>
#include <list>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>


namespace theplu {
namespace svnstat {

	///
	/// If something goes wrong in the use of the Gnuplot class, a
	/// GnuplotException is thrown.
	///
	struct GnuplotException : public std::runtime_error
	{ inline GnuplotException(const std::string& msg) : runtime_error(msg) {} };

	///
	/// The Gnuplot class creates a pipe to 'gnuplot' and facilitates
	/// communication with the gnuplot binary.
	///
	class Gnuplot
	{
	public:
		///
		/// This constructor sets up the pipe to the first gnuplot
		/// executable found in the PATH environment variable. The PATH
		/// variable must exist.
		///
		/// @throw Throws a GnuplotException if a no PATH variable is
		/// found, if the gnuplot binary cannot be found, or if a pipe
		/// cannot to the gnuplot binary cannot be established.
		///
		Gnuplot(void);

		///
		/// The destructor, closes the pipe to the gnuplot binary.
		///
		virtual ~Gnuplot(void);

		///
		/// Send arbitrary commands to Gnuplot.
		///
		void command(const std::string&);

		///
		/// Set the \a style of the line in the subsequent plot or
		/// replot calls. The setting applies until this function is
		/// called again.
		///
		/// @note The \a style is not checked to be valid.
		///
		inline void linestyle(const std::string& style) { linestyle_=style; }

		///
		/// Set the \a title of the line in the subsequent plot or
		/// replot calls. The setting applies until this function is
		/// called again.
		///
		inline void linetitle(const std::string& title) { linetitle_=title; }

		///
		/// Plot the data \a y as a function of \a x using the Gnuplot
		/// 'plot' command. The \a x vector can be omitted as in normal
		/// Gnuplot usage.
		///
		template <class T1, class T2>
		void plot(const std::vector<T1>& y, const std::vector<T2>& x)
		{ plot(y,x,"plot"); }

		template <class T1>
		void plot(const std::vector<T1>& y,
							const std::vector<T1>& x=std::vector<T1>())
		{ plot(y,x,"plot"); }

		///
		/// Plot the data \a y as a function of \a x using the Gnuplot
		/// 'replot' command. The \a x vector can be omitted as in normal
		/// Gnuplot usage.
		///
		template <class T1, class T2>
		void replot(const std::vector<T1>& y, const std::vector<T2>& x)
		{ plot(y,x,"replot"); }

		template <class T1>
		void replot(const std::vector<T1>& y,
							const std::vector<T1>& x=std::vector<T1>())
		{ plot(y,x,"replot"); }

	private:
		///
		/// Copy constructor, not implemented.
		///
		Gnuplot(const Gnuplot&);

		void acquire_program_path(const std::string&);

		///
		/// @param \a plotcmd must be "plot" or "replot".
		///
		template <class T1, class T2>
		void plot(const std::vector<T1>& y, const std::vector<T2>& x,
							const std::string& plotcmd)
		{
			assert(x.size()==y.size() || x.empty());
			assert(plotcmd=="plot" || plotcmd=="replot");

			std::ostringstream cmdstring;
			cmdstring << plotcmd << " '-' u 1:2 title '" << linetitle_ << "' with "
								<< linestyle_ << '\n';
			for (size_t i=0; i<y.size(); ++i) {
				if (!x.empty())
					cmdstring << x[i] << '\t';
				else
					cmdstring << i << '\t';
				cmdstring << y[i] << '\n';
			}
			cmdstring << "e\n";

 			command(cmdstring.str());
		}

		void tokenizer(const std::string& in, std::list<std::string>& tokens,
									 const std::string& delimiters = ":");

		std::string gnuplot_binary_;
		std::string linestyle_;
		std::string linetitle_;
		FILE* pipe_;
};

}} // end of namespace svnstat and namespace theplu

#endif
