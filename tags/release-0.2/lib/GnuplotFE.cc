// $Id: GnuplotFE.cc 74 2006-03-07 15:46:59Z jari $

#include "GnuplotFE.h"
#include "Gnuplot.h"

#include <string>
#include <sstream>


namespace theplu {
namespace svnstat {


	GnuplotFE* GnuplotFE::instance_=NULL;


	void GnuplotFE::plot(const std::vector<u_int>& y, const std::string& format)
	{
		if (!date_.empty()) {
			command(std::string("set xdata time"));
			command("set timefmt '" + date_input_format_ + "'");
			command("set format x '" + format + "'");
			Gnuplot::plot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::plot(y);
	}


	void GnuplotFE::replot(const std::vector<u_int>& y)
	{
		if (!date_.empty()) {
			command(std::string("set xdata time"));
			Gnuplot::replot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::replot(y);
	}


	double GnuplotFE::yrange(double ymax)
	{
		if (ymax<0)
			ymax=0;
		std::ostringstream cmd;
		cmd << "set yrang[0:";
		if (ymax)
			cmd << ymax;
		cmd << "]";

		command(cmd.str());
		return ymax;
	}


}} // end of namespace svnstat and namespace theplu
