// $Id: File.h 72 2006-03-02 22:16:24Z peter $

#ifndef _theplu_svnstat_file_
#define _theplu_svnstat_file_

#include "Node.h"

#include <string>

namespace theplu{
namespace svnstat{

  class File : public Node
  {
  public:
    /// 
    /// @brief Default Constructor 
    /// 
		File(const std::string& path, const std::string& output="") 
			: Node(path,output), binary_(false), ignore_(false) {}

    ///
    /// Parsing out information from svn repository
    ///
    /// @return true if succesful
    ///
    const Stats& parse(const bool verbose=false);

		///
		///
		///
		void print(const bool verbose=false) const;

  private:
    ///
    /// @brief Parsing svn blame output
    ///
    /// @return true if parsing is succesful
    ///
    bool blame(void) const;
    
		///
		/// @brief Copy Constructor, not implemented
		///
		File(const File&);

    std::string author_;
    bool binary_;
		bool ignore_;
    u_int revision_;
  
  };

}} // end of namespace svnstat and namespace theplu

#endif


