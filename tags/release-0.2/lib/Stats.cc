//$Id: Stats.cc 80 2006-03-10 16:52:04Z jari $

#include "Stats.h"
#include "GnuplotFE.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <sstream>
#include <string>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svnstat{


	Stats::Stats(const std::string& path)
	{
		// Make sure latest revision is set properly
		std::map<std::string,std::string> svn_info = info(path);
		std::stringstream ss;
		ss << (svn_info.count("Revision") ? svn_info["Revision"] : "0");
		ss >> latest_revision_;
	}


  std::vector<u_int> Stats::accumulated(void) const
  {
		// sum of all users
		std::vector<u_int> sum(latest_revision_+1, 0);
		sum=std::accumulate(map_.begin(), map_.end(), sum,
												PairValuePlus<std::string,u_int>());

    // calculate accumulated sum
    std::vector<u_int> accum(sum.size());
		std::partial_sum(sum.begin(),sum.end(),accum.begin());
		assert(sum.size()==accum.size());
    return accum;
  }

  std::vector<u_int> Stats::accumulated(const std::string& user) const
  {
    if (!map_.count(user))
      return std::vector<u_int>();
    std::vector<u_int> vec=(map_.find(user))->second;
	
		if (vec.size() < latest_revision_+1)
			vec.insert(vec.end(), latest_revision_+1-vec.size(), 0);

    std::vector<u_int> accum(vec.size());
    std::partial_sum(vec.begin(),vec.end(),accum.begin());
    return accum;
  }

  void Stats::add(const std::string& user, const u_int& rev)
  {
    std::vector<u_int>* vec = &(map_[user]);
    if (vec->size() < rev+1){
			vec->reserve(latest_revision_ + 1);
			vec->insert(vec->end(), rev - vec->size(),0);
			vec->push_back(1);
    }
    else
      (*vec)[rev]++;
  }


	bool Stats::parse(const std::string& path)
	{
		// Calling svn blame
    if (blame(path))
      return true;

    // Check if file is binary
    std::ifstream is("svnstat.tmp");
    std::string line;
    getline(is,line,' ');
    if (line==std::string("Skipping")){
      getline(is,line,' ');
      if (line==std::string("binary")){
				is.close();
				return true;
      }
    }
    is.close();

    is.open("svnstat.tmp");
    while (getline(is,line, '\n')){
      if (!line.size()) // skip empty line
				continue;
      std::stringstream ss(line);
      u_int revision;
      std::string user;
      ss >> revision;
      ss >> user;
      add(user, revision);
    }
    is.close();
		return false;
	}


	std::string Stats::plot(const std::string& name) const
	{
		GnuplotFE* gp=GnuplotFE::instance();
		gp->command(std::string("set term png; set output '")+name+"'");
		gp->command(std::string("set title '")+name+"'");
		gp->command("set key default");
		gp->command("set key left Left reverse");
		gp->command("set multiplot");
		std::vector<u_int> total=accumulated();		
		double yrange_max=1.03*total.back()+1;
		gp->yrange(yrange_max);
		size_t plotno=1;
		std::stringstream ss;
		for (MapConstIter_ i= map_.begin(); i != map_.end(); i++) {
			ss.str("");
			ss << "set key height " << 2*plotno;
			gp->command(ss.str());
			std::vector<u_int> x=accumulated(i->first);
			ss.str("");
			ss << x.back() << " " << i->first;
			gp->yrange(yrange_max);
			gp->linetitle(ss.str());
			ss.str("");
			ss << "steps " << ++plotno;
			gp->linestyle(ss.str());
 			gp->plot(x);
		}
		ss.str("");
		ss << total.back() << " total";
		gp->command("set key height 0");
		gp->linetitle(ss.str());
		gp->linestyle("steps 1");
		gp->plot(total);

		gp->command("unset multiplot");
		gp->yrange();

		return name;
	}

  Stats& Stats::operator+=(const Stats& other)
  {
    for (MapConstIter_ o_i= other.map_.begin(); o_i != other.map_.end(); ++o_i)
    {
      std::pair<MapIter_,bool> result = map_.insert(*o_i);
      if (!result.second)
				map_[(*(result.first)).first] = 
					VectorPlus<u_int>()( (*(result.first)).second, (*o_i).second );
	
    }
    return *this;
  }

}} // end of namespace svnstat and namespace theplu
