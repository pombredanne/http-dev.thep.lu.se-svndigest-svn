// $Id: Parameter.h 73 2006-03-04 18:10:07Z jari $

#ifndef _theplu_svnstat_parameter_
#define _theplu_svnstat_parameter_

#include <string>

namespace theplu {
namespace svnstat {

  // class for command line options.
	class Parameter {
	public:
		Parameter(const int argc,const char *argv[]);
		inline bool force(void) const { return force_; }
		inline bool revisions(void) const { return revisions_; }
		inline const std::string& root(void) const { return root_; }
		inline const std::string& targetdir(void) const { return targetdir_; }
		inline bool verbose(void) const { return verbose_; }

	private:
		void analyse(void);
		void defaults(void);
		void help(void);
		void version(void);

		bool force_;
		bool revisions_;
		std::string root_;
		std::string targetdir_;
		bool verbose_;
	};

}} // of namespace svnstat and namespace theplu

#endif
