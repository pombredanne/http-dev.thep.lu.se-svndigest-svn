// $Id: rmdirhier.h 50 2006-01-15 00:33:37Z jari $

#ifndef _theplu_svnstat_rmdirhier_
#define _theplu_svnstat_rmdirhier_

#include <stdexcept>
#include <string>

namespace theplu {
namespace svnstat {

	struct DirectoryError : public std::runtime_error
	{ inline DirectoryError(const std::string& s) : runtime_error(s) {} };

	struct BadDirectory : public DirectoryError
	{ inline BadDirectory(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryOpenError : public DirectoryError
	{ inline DirectoryOpenError(const std::string& s) : DirectoryError(s) {} };

	struct FileDeleteError : public DirectoryError
	{ FileDeleteError(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryDeleteError : public DirectoryError
	{ DirectoryDeleteError(const std::string& s) : DirectoryError(s) {} };

	void rmdirhier(const std::string& path);

}} // of namespace svnstat and namespace theplu

#endif
