// $Id: Parameter.cc 73 2006-03-04 18:10:07Z jari $

#include "Parameter.h"

#include <iostream>
#include <stdexcept>
#include <string>

namespace theplu {
namespace svnstat {

	Parameter::Parameter(const int argc,const char *argv[])
	{
		defaults();
		for (int i=1; i<argc; i++) {
			bool ok=false;
			std::string myargv(argv[i]);
			if (myargv=="-f" || myargv=="--force"){
					force_=true;
					ok=true;
			}
			else if (myargv=="-h" || myargv=="--help"){
				help();
				exit(0);			// always exit after printing help
			}
			else if (myargv=="-r" || myargv=="--root"){
				if (++i<argc){
					root_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-rev" || myargv=="--revisions") {
					revisions_=true;
					ok=true;
			}
			else if (myargv=="-t" || myargv=="--target"){
				if (++i<argc){
					targetdir_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-v" || myargv=="--verbose"){
					verbose_=true;
					ok=true;
			}
			else if (myargv=="--version"){
					version();
					exit(0);
			}

			if (!ok)
				throw std::runtime_error("svnstat: invalid option: " + myargv +
																 "\nType 'svnstat --help' for usage.");
		}

		analyse();
	}


	void Parameter::analyse(void)
	{
		// should check that root is a directory
	}


	void Parameter::defaults(void)
	{
		force_=false;
		revisions_=false;
		root_=".";
		targetdir_="svnstat_output";
		verbose_=false;
	}


	void Parameter::help(void)
	{
		std::cout << "\n"
							<< "usage: svnstat [options]\n"
							<< "\n"
							<< "svnstat traverses a directory structure (controlled by\n"
							<< "subversion) and calculates developer statistics entries.\n"
							<< "The top level directory of the directory structure to\n"
							<< "traverse is set with the -t option."
							<< "The result is written to a\n"
							<< "sub-directory, svnstat, that is created in the current\n"
							<< "working directory.\n"
							<< "\n"
							<< "Valid options:\n"
							<< "  -f [--force]   : remove target directory/file if it exists\n"
							<< "                   [no force]. NOTE recursive delete.\n"
							<< "  -h [--help]    : display this help and exit\n"
							<< "  -r [--root] arg : svn controlled directory to perform\n"
							<< "                    statistics calculation on [.]\n"
							<< "  -rev [--revisions]: Use revision numbers as time scale\n"
							<< "                      instead of dates [dates].\n"
							<< "  -t [--target] arg : output directory [svnstat_output]\n"
							<< "  -v [--verbose] : explain what is being done\n"
							<< "  --version      : print version information and exit\n"
							<< std::endl;
	}


	void Parameter::version(void)
	{
		std::cout << "svnstat 0.1\n"
							<< "Written by Jari Hakkinen and Peter Johansson.\n"
							<< std::endl;
	}

}} // of namespace wenni and namespace theplu
