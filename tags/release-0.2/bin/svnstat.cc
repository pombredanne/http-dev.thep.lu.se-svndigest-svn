// $Id: svnstat.cc 73 2006-03-04 18:10:07Z jari $

#include "Parameter.h"
#include "CommitStat.h"
#include "Directory.h"
#include "GnuplotFE.h"
#include "rmdirhier.h"

#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

#include <iostream>

using namespace std;

///
/// Create directory \a dir. The call can fail in many ways, cf. 'man
/// mkdir'. If the \a dir exists, setting \a force to true will
/// trigger a recursive removal of the target \a dir.
///
int createdir(const string& dir, bool force=false);


int main(const int argc,const char* argv[])
{
	using namespace theplu::svnstat;
	Parameter option(argc,argv);

	if (createdir(option.targetdir(),option.force())) {
		std::cerr << "\nFailed to create target directory '" << option.targetdir()
							<< "'.\n" << std::endl;
		exit(-1);
	}

	if (!option.revisions()) {
		if (option.verbose())
			std::cout << "Parsing the log." << std::endl;
		CommitStat cs;
		cs.parse(option.root());
		GnuplotFE::instance()->set_dates(cs.date());
	}
	
	string prefix("svnstat_");
	Directory tree(option.root(),prefix);
	tree.purge(option.verbose());
	tree.parse(option.verbose());

	GnuplotFE::instance()->command(string("cd '")+option.targetdir()+"'");
	chdir(option.targetdir().c_str());
	try {
		tree.print(option.verbose());
	}
	catch (const std::runtime_error& x) {
		std::cerr << "svnstat: " << x.what() << std::endl;
	}
	string htmltopnode=tree.output_name()+".html";
	symlink(htmltopnode.c_str(),"index.html");

	exit(0);				// normal exit
}


int createdir(const string& dir, bool force)
{
	struct stat buf;
	if (force && !stat(dir.c_str(),&buf))
		theplu::svnstat::rmdirhier(dir);

	return mkdir(dir.c_str(),0777);
}
