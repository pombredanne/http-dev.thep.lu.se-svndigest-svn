// $Id: rmdirhier.cc 55 2006-01-15 01:34:04Z jari $

#include "rmdirhier.h"

#include <dirent.h>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>


namespace theplu {
namespace svnstat {

	void rmdirhier(const std::string& path)
	{
		int fd=open(".",O_RDONLY);	// remember "original" cwd
		void rmdirhier__(const std::string&);

		try {
			rmdirhier__(path);
		}
		catch(const BadDirectory& x) {
			std::cerr << "Invalid directory: " << x.what() << std::endl;
		}
		catch(const DirectoryOpenError& x) {
			std::cerr << "Error opening directory: " << x.what() << std::endl;
		}
		catch(const FileDeleteError& x) {
			std::cerr << "Error deleting file: " << x.what() << std::endl;
		}
		catch(const DirectoryDeleteError& x) {
			std::cerr << "Error deleting directory: " << x.what() << std::endl;
		}
		catch(const DirectoryError& x) {
			std::cerr << "Directory error:" << x.what() << std::endl;
		}

		fchdir(fd);	// return to "original" cwd
	}


	void rmdirhier__(const std::string& dir)
	{
		if (chdir(dir.c_str()))
      throw BadDirectory(dir);

   // Delete any remaining directory entries
		DIR* dp;
		struct dirent *entry;
		if (!(dp=opendir(".")))
			throw DirectoryOpenError(dir);
		while ((entry=readdir(dp)) != NULL) {
			struct stat buf;
      if ((std::string(entry->d_name) == ".") ||
					(std::string(entry->d_name) == ".."))
				continue;
      stat(entry->d_name,&buf);
      if (buf.st_mode & S_IFDIR)
				rmdirhier__(entry->d_name);      // sub-directory
      else {
				// Make sure file is removable before removing it
				chmod(entry->d_name,S_IWRITE);
				if (unlink(entry->d_name))
					throw FileDeleteError(entry->d_name);
      }
		}
		closedir(dp);

		// Remove the directory from its parent
		chdir("..");
		if (rmdir(dir.c_str()))
      throw DirectoryDeleteError(dir);
	}

}} // of namespace svnstat and namespace theplu
