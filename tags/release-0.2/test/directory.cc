// $Id: directory.cc 69 2006-02-13 00:46:30Z jari $

#include "Directory.h"

#include <string>

int main()
{
  using namespace theplu::svnstat;
  std::string path(".");
  Directory directory(path);
  return 0;
}
