// $Id: gnuplot_pipe.cc 36 2006-01-13 01:05:53Z jari $

#include "Gnuplot.h"

#include <vector>

int main(const int argc,const char* argv[])
{  
	using namespace theplu;

	bool ok = true;

	std::vector<double> x,y;
	for (int i=-10; i<=10; ++i) {
		x.push_back(i);
		y.push_back(i*i);
	}

	svnstat::Gnuplot gnuplot1;
	gnuplot1.command("set output 'test1.png'; set term png ; set title 'sine'");
	gnuplot1.command("plot sin(x) title 'sine of x' with lines");

	svnstat::Gnuplot gnuplot2;
	gnuplot2.command("set output 'test2.png'; set term png");
	gnuplot2.plot(x);

	svnstat::Gnuplot gnuplot3;
	gnuplot3.command("set output 'test3.png'; set term png");
	gnuplot3.linetitle("parabola");
	gnuplot3.plot(x,y);

	gnuplot3.command("set output 'test4.png'; set title 'composite'");
	gnuplot3.linestyle("linespoints");
	gnuplot3.linetitle("");
	gnuplot3.replot(x);

	/*
	// Jari, if we want to avoid the above usage of plot_x and plot_xy,
	// we need to do something like this. Of course 'filename.data'
	// needs to be created by the user (this is hidden for the user in
	// plot_x and plot_xy). Note, the below example compiles, but will
	// fail at run time if 'filename.data' does not, miraculously, exist.
	svnstat::Gnuplot gnuplot4;
	gnuplot4.command("set output 'test5.png'\nset term png");
	gnuplot4.command("plot 'filename.data' using 1:2 with linespoints");
	*/

	return (ok ? 0 : -1);
}
