// $Id: file.cc 69 2006-02-13 00:46:30Z jari $

#include "File.h"
#include "Directory.h"

#include <string>

int main()
{
  using namespace theplu::svnstat;
  std::string path("file.cc");
  File file(path);
  file.parse();
  return 0;
}
