#ifndef _theplu_svndigest_node_visitor_
#define _theplu_svndigest_node_visitor_

// $Id: NodeVisitor.h 1234 2010-10-23 16:41:33Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/


namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
		 Abstract Base Class for Visitors.
	*/
  class NodeVisitor
  {
  public:
		/**
			 \brief Destructor
		*/
		virtual ~NodeVisitor(void) {};

		/**
			 This function is called from Directory::traverse

			 This is a chance for the visitor to do some action before
			 traversing daughter nodes.

			 \return true if we should traverse daughter nodes
		 */
		virtual bool enter(Directory& dir)=0;

		/**
			 This function is called from Directory::traverse

			 This is a chance for the visitor to do some action after
			 daughter nodes have been traversed.
		 */
		virtual void leave(Directory& dir)=0;

		/**
			 Do some action on the File. This action typically overlaps
			 significantly with enter(1) and/or leave(1) functions above, in
			 which case it is recommended to implement these with underlying
			 functions. To keep the base class flexible, however, the
			 functions for directory and files are kept independent in this
			 base class.
		 */
		virtual void visit(File& dir)=0;
	};
}} // end of namespace svndigest and namespace theplu

#endif
