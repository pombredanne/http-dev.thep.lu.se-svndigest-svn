// $Id: File.cc 1239 2010-10-23 23:57:56Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "File.h"

#include "Alias.h"
#include "Colors.h"
#include "Configuration.h"
#include "Date.h"
#include "Graph.h"
#include "html_utility.h"
#include "HtmlStream.h"
#include "NodeVisitor.h"
#include "Stats.h"
#include "SVNblame.h"
#include "SVNlog.h"
#include "TinyStats.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	File::File(const unsigned int level, const std::string& path, 
						 const std::string& output) 
		: Node(level,path,output)
	{
		output_dir_=output;
		if (!output_dir_.empty())
			output_dir_+='/';
	}


	std::string File::blame_output_file_name(void) const
	{
		return "blame_output/" + local_path() + ".html";
	}


	std::string File::href(void) const
	{ 
		return name()+".html"; 
	}


	svn_revnum_t File::last_changed_rev(void) const
	{
		return svn_info().last_changed_rev();
	}


	void File::log_core(SVNlog&) const
	{
	}


	std::string File::node_type(void) const
	{
		return std::string("file");
	}


	std::string File::output_path(void) const
	{
		return output_dir()+name()+".html";
	}


	const StatsCollection& File::parse(bool verbose, bool ignore)
	{
		if (verbose)
			std::cout << "Parsing '" << path_ << "'" << std::endl; 
		stats_.reset();
		std::string cache_dir = directory_name(path()) + std::string(".svndigest/"); 
		std::string cache_file = cache_dir + name()+std::string(".svndigest-cache");
		if (!ignore && node_exist(cache_file)){
			std::ifstream is(cache_file.c_str());
			if (stats_.load_cache(is)) {
				is.close();
				return stats_;
			}
			is.close();
		}
		else 
			stats_.parse(path_);
		if (!node_exist(cache_dir))
			mkdir(cache_dir);
		std::string tmp_cache_file(cache_file+"~");
		std::ofstream os(tmp_cache_file.c_str());
		assert(os);
		stats_.print(os);
		os.close();
		rename(tmp_cache_file, cache_file);
		return stats_;
	}


	void File::print_blame(std::ofstream& os) const
	{
		os << "<br /><h3>" << local_path() << "</h3>";
		os << "<div class=\"blame_legend\">\n";
		os << "<dl>\n";
		os << "<dt class=\"code\"></dt><dd>Code</dd>\n";
		os << "<dt class=\"comment\"></dt><dd>Comments</dd>\n";
		os << "<dt class=\"other\"></dt><dd>Other</dd>\n";
		os << "</dl>\n</div>\n";
		os << "<table class=\"blame\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th class=\"rev\">Rev</th>\n";
		os << "<th class=\"date\">Date</th>\n";
		os << "<th class=\"author\">Author</th>\n";
		os << "<th class=\"line\">Line</th>\n";
		os << "<th></th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";
		HtmlStream hs(os);
		SVNblame blame(path_);
		LineTypeParser parser(path_);
		while (blame.valid()) {
			parser.parse(blame.line());
			blame.next_line();
		}
		blame.reset();

		std::vector<LineTypeParser::line_type>::const_iterator 
			line_type(parser.type().begin());
		int last=0;
		int first=0;
		bool using_dates=true;
		if (!Graph::date_xticks()) {
			using_dates=false;
			last = stats_["classic"].revision();
		}
		else {
			last = Graph::xticks().back();
			// earliest date corresponds either to revision 0 or revision 1 
			first = std::min(Graph::xticks()[0],Graph::xticks()[1]);
		}
		// color is calculated linearly on time, c = kt + m
		// brightest color (for oldest rev in log) is set to 192.
		double k = 192.0/(first-last);
		double m = -last*k; 
		while (blame.valid()) {
			std::string color;
			if (using_dates)
				color = hex(static_cast<int>(k*Date(blame.date()).seconds()+m),2);
			else
				color = hex(static_cast<int>(k*blame.revision()+m),2);
			os << "<tr>\n<td class=\"rev\">";
			std::stringstream color_ss;
			color_ss << "#" << color << color << color; 
			os << "<font color=\"" << color_ss.str() << "\">"
				 << trac_revision(blame.revision(), color_ss.str())
				 << "</font></td>\n<td class=\"date\"><font color=\"#" << color 
				 << color << color << "\">" ;
			hs << Date(blame.date())("%d %b %y");
			os << "</font></td>\n<td class=\"author\">";
			const std::string& author_color = 
				Colors::instance().color_str(blame.author());
			assert(!author_color.empty());
			os << "<font color=\"#" << author_color << "\">";
			hs << blame.author();
			os << "</td>\n<td class=\"";
			assert(line_type!=parser.type().end());
			if (*line_type==LineTypeParser::other)
				os << "line-other";
			else if (*line_type==LineTypeParser::comment || 
							 *line_type==LineTypeParser::copyright)				
				os << "line-comment";
			else if (*line_type==LineTypeParser::code)
				os << "line-code";
			else {
				std::string msg="File::print_blame(): unexpected line type found";
				throw std::runtime_error(msg);
			}
			os << "\">" << blame.line_no()+1
				 << "</td>\n<td>";
			hs << blame.line();
			os << "</td>\n</tr>\n";
			blame.next_line();
			++line_type;
		}
		os << "</tbody>\n";
		os << "</table>\n";
	}


	void File::print_core(const bool verbose) const 
	{
		mkdir_p(directory_name(blame_output_file_name()));
		std::ofstream os(blame_output_file_name().c_str());
		assert(os.good());
		print_html_start(os, "svndigest", level_+1);
		if (Configuration::instance().output_blame_information())
			print_blame(os);
		print_footer(os);
		os.close();
	}


	void File::print_core(const std::string& stats_type, 
												const std::string& user, const std::string& line_type,
												const SVNlog& log) const 
	{
		std::string lpath = local_path();
		if (lpath.empty())
			lpath = "index";
		std::string outpath = stats_type+"/"+user+"/"+line_type+"/"+lpath;
		std::string imagefile = stats_type+"/"+"images/"+line_type+"/"+lpath;
		std::string html_name(outpath + ".html");
		mkdir_p(directory_name(html_name));
		mkdir_p(directory_name(imagefile));
		std::ofstream os(html_name.c_str());
		assert(os);
		print_header(os, name(), level_+3, user, line_type, lpath+".html",
								 stats_type);
		path_anchor(os);

		std::stringstream ss;
		for (size_t i=0; i<level_; ++i)
			ss << "../";
		ss << "../../../";
		if (user=="all")
			ss << stats_[stats_type].plot(imagefile,line_type);
		else
			ss << imagefile;
		os << "<p class=\"plot\">\n"; 
		os << image(ss.str());
		os << "</p>\n";

		print_author_summary(os, stats_[stats_type], line_type, log);
		os << "\n";
		os << "<h3>"
			 << anchor(blame_output_file_name(), 
								 "Blame Information", level_+3) 
			 << "</h3>\n";

		print_footer(os);
		os.close();	
	}


	void File::traverse(NodeVisitor& visitor)
	{
		visitor.visit(*this);
	}

}} // end of namespace svndigest and namespace theplu
