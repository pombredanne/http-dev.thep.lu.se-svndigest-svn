## $Id: yat_add_flag.m4 2107 2009-11-07 15:53:27Z peter $

# SYNOPSIS
#
#   YAT_CPP_ADD_FLAG([FLAGS], [FLAG])
#
#   YAT_CXX_ADD_FLAG([FLAGS], [FLAG])
#
#   YAT_LD_ADD_FLAG([FLAGS], [FLAG])
#
# DESCRIPTION
#
#   Check that FLAG is not already included in FLAGS and test that
#   FLAG is supported by C++ compiler. If true FLAG is appended to
#   FLAGS.
#
# LAST MODIFICATION
#
#   $Date: 2009-11-07 10:53:27 -0500 (Sat, 07 Nov 2009) $
#
# COPYLEFT
#
#   Copyright (C) 2008, 2009 Peter Johansson
#
#   This file is part of the yat library, http://dev.thep.lu.se/yat
#
#   The yat library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 3 of the
#   License, or (at your option) any later version.
#
#   The yat library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with yat. If not, see <http://www.gnu.org/licenses/>.
#

#
# serial 5  
#
# see http://www.gnu.org/software/automake/manual/automake.html#Serials

AC_DEFUN([YAT_CPP_ADD_FLAG],
[
  AC_PREREQ([2.63]) dnl we use AS_VAR_IF
  AC_REQUIRE([AC_PROG_SED])
  for yat_flag in $2; do
    # ignore flag if
    # 1) it is -I/usr/include
    # 2) it is -I/usr/local/include and CXX is g++
    # 3) it is -D* and * is already defined in $1, $CPPFLAGS, or AC_DEFINE
    ignore=no
    AS_CASE([$yat_flag],
            [-I/usr/include], [ignore=yes],
            [-I/usr/local/include],[AS_IF([test "x$CXX" = "xg++"],[ignore=yes])],
            [-D*], [yat_def=`AS_ECHO([$yat_flag]) | $SED 's,^-D,,;s,=.*,,'`;
                    yat_save_CPPFLAGS="$CPPFLAGS"
                    m4_if([$1], [CPPFLAGS], [], [CPPFLAGS="$$1 $CPPFLAGS"])
                    AC_CHECK_DECL([$yat_def],[ignore=yes])
                    CPPFLAGS="$yat_save_CPPFLAGS"])

    AS_VAR_IF([ignore], [no],
          [YAT_FIND_STR([$1], [$yat_flag], , 
                        [AX_CXXCPP_CHECK_FLAG([$yat_flag], ,, 
                                              [$1="$$1 $yat_flag"])])])
  done
]) # YAT_CPP_ADD_FLAG


AC_DEFUN([YAT_CXX_ADD_FLAG],
[
  AC_PREREQ([2.61]) dnl we use AX_CXX_CHECK_FLAG 
  for yat_flag in $2; do
    YAT_FIND_STR([$1], [$yat_flag], , 
                 [AX_CXX_CHECK_FLAG([$yat_flag], ,, [$1="$$1 $yat_flag"])])
  done
]) # YAT_CXX_ADD_FLAG

AC_DEFUN([YAT_LD_ADD_FLAG],
[
  AC_PREREQ([2.61]) dnl we use AX_LD_CHECK_FLAG 
  for yat_flag in $2; do
    # ignore flag if
    # 1) it is -L/usr/lib
    # 2) it is -L/usr/local/lib and CXX is g++
    AS_IF([test "x$yat_flag" = "x-L/usr/lib"],,
          [test "x$CXX" = "xg++" && test "x$yat_flag" = "x-L/usr/local/lib"],,
          [YAT_FIND_STR([$1], [$yat_flag], , 
                        [AX_LD_CHECK_FLAG([$yat_flag], ,, 
                                          [$1="$$1 $yat_flag"])])])
  done
]) # YAT_LD_ADD_FLAG

AC_DEFUN([YAT_FIND_STR],
[
  # YAT_FIND_STR
  found=no;				
  for a in $$1; do 
    AS_IF([test "x$a" = "x$2"],[found=yes])
  done

  # ACTION
  AS_IF([test "$found" = "yes"], [m4_default([$3], [:])],
        [m4_default([$4], [:])])

]) # YAT_FIND_STR

