### $Id: config 1360 2011-06-03 03:20:16Z peter $
### This file configures various behaviors for svndigest
### The commented-out below are intended to demonstrate how to use
### this file.

### Section for setting output
[output]
# if true svndigest will output blame information for each file.
blame-information = yes
# if true report will have pages for files and not only directories.
file = yes
# svndigest uses this value to replace tabs with spaces in blame output
tab-size = 2

### Section for setting behaviour of copyright update
[copyright]
# warn if file has no copyright statement.
missing-copyright-warning = no
# defining start of copyright statement
copyright-string = Copyright (C)

### Section for setting aliases used in copyright update
[copyright-alias]
# jdoe = John Doe
jari = Jari Häkkinen
peter = Peter Johansson

### Section for images
[image]
format = png
anchor_format = png

### Section for author color in plots and blame output.
[author-color]
# jdoe = 000000
jari = 0000aa
peter = aa0000

### Section for overriding svn properties.
### The format is the same as for section auto-props in subversion
### config file
[svn-props]
# COPYING = svndigest:ignore

### Section for setting trac environment.
[trac]
# If trac-root is set, svndigest will create anchors to the Trac page.
# trac-root = http://dev.thep.lu.se/svndigest/
trac-root = http://dev.thep.lu.se/svndigest/

### Section for setting dictionary for file names.
### Prior looking for file name pattern in section [parsing-codons],
### the file name may be translated according to the rules 
### in this section. In default setting there is, for example,
### a rule to translate `<FILENAME>.in' to `<FILENAME>'.
### The format of the entries is:
###    file-name-pattern = new-name
### Left hand side may contain wildcards (such as '*' and '?').
### Right hand side may contain "$i", which will be replaced 
### with the ith wild card in lhs string.
[file-name-dictionary]
*.in = $1

### Section for setting parsing modes
### The format of the entries is:
###   file-name-pattern = "start-code" : "end-code"
### The file-name-pattern may contain wildcards (such as '*' and '?').
### String "\n" can be used for codons containing newline
### character.
[parsing-codons]
*.ac = "#":"\n"  ;  "dnl":"\n"
*.am = "#":"\n"
*.as = "#":"\n"  ;  "dnl":"\n"
*.bat = "\nREM":"\n"  ;  "\nrem":"\n"
*.c = "//":"\n"  ;  "/*":"*/"
*.cc = "//":"\n"  ;  "/*":"*/"
*.cpp = "//":"\n"  ;  "/*":"*/"
*.css = "<!--":"-->"
*.cxx = "//":"\n"  ;  "/*":"*/"
*.h = "//":"\n"  ;  "/*":"*/"
*.hh = "//":"\n"  ;  "/*":"*/"
*.hpp = "//":"\n"  ;  "/*":"*/"
*.html = "<%--":"--%>"
*.java = "//":"\n"  ;  "/*":"*/"
*.jsp = "<!--":"-->"
*.m = "%":"\n"
*.m4 = "#":"\n"  ;  "dnl":"\n"
*.pl = "#":"\n"
*.pm = "#":"\n"
*.R = "#":"\n"
*.rss = "<!--":"-->"
*.sgml = "<!--":"-->"
*.sh = "#":"\n"
*.shtml = "<!--":"-->"
*.tex = "%":"\n"
*.xhtml = "<!--":"-->"
*.xml = "<!--":"-->"
*.xsd = "<!--":"-->"
*.xsl = "<!--":"-->"
*config = "#":"\n"
bootstrap = "#":"\n"
Makefile = "#":"\n"
Portfile = "#":"\n"
