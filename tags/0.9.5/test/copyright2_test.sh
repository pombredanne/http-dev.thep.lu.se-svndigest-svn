#!/bin/sh

# $Id: copyright2_test.sh 1266 2010-11-02 03:39:22Z peter $

# Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009, 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. ./init.sh || exit 1

# exit if cmd fails
set -e

$SVN revert $rootdir -R
$SVN update $rootdir -r 45
$SVN update $rootdir/lib
# cannot use old config file because it's invalid
$SVN update $rootdir/.svndigest

SVNCOPYRIGHT_run 0 -r $rootdir --ignore-cache --verbose
rm -rf $rootdir

$GREP '^Parsing.*dir_to_be_ignored' stdout && exit_fail

exit_success;