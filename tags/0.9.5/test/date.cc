// $Id: date.cc 1164 2010-08-13 20:21:40Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Date.h"

#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv);

	Date date("2007-06-27T15:40:52.123456Z");
	if (date("%H:%M")!="15:40"){
		suite.add(false);
		std::cerr << "date(\"%H:%M\") returns: " << date("%H:%M")
							<< "\nexpected '15:40'" << std::endl;
	}
			
	return suite.exit_status();
}

