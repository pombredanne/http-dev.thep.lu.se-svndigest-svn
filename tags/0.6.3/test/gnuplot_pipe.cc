// $Id: gnuplot_pipe.cc 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Gnuplot.h"

#include <vector>

int main(const int argc,const char* argv[])
{  
	using namespace theplu;

	bool ok = false; // false indicates success, i.e., no errors detected.

	std::vector<double> x,y;
	for (int i=-10; i<=10; ++i) {
		x.push_back(i);
		y.push_back(i*i);
	}

	svndigest::Gnuplot gnuplot1;
	ok|=gnuplot1.command("set output 'test1.png'; set term png ; set title 'sine'");
	ok|=gnuplot1.command("plot sin(x) title 'sine of x' with lines");

	svndigest::Gnuplot gnuplot2;
	ok|=gnuplot2.command("set output 'test2.png'; set term png");
	ok|=gnuplot2.plot(x);

	svndigest::Gnuplot gnuplot3;
	ok|=gnuplot3.command("set output 'test3.png'; set term png");
	gnuplot3.linetitle("parabola");
	ok|=gnuplot3.plot(x,y);
	ok|=gnuplot3.command("set output 'test4.png'"); 
	ok|=gnuplot3.command("set title 'ddddd'");
	//gnuplot3.linestyle("linespoints");
	gnuplot3.linetitle("bajs");
	ok|=gnuplot3.plot(x);

	/*
	// Jari, if we want to avoid the above usage of plot_x and plot_xy,
	// we need to do something like this. Of course 'filename.data'
	// needs to be created by the user (this is hidden for the user in
	// plot_x and plot_xy). Note, the below example compiles, but will
	// fail at run time if 'filename.data' does not, miraculously, exist.
	svndigest::Gnuplot gnuplot4;
	gnuplot4.command("set output 'test5.png'\nset term png");
	gnuplot4.command("plot 'filename.data' using 1:2 with linespoints");
	*/

	return ok;
}
