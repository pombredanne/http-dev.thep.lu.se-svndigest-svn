#ifndef _theplu_svndigest_svnproperty_
#define _theplu_svndigest_svnproperty_

// $Id: SVNproperty.h 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <map>
#include <string>

namespace theplu {
namespace svndigest {

	class SVN;

	///
	/// The SVNproperty class is a utility class for taking care of 'svn
	/// property' information. An 'svn property' is non-recursively
	/// performed on an item.
	///
	class SVNproperty {
	public:

		///
		/// @brief The contructor.
		///
		/// The constructor performs an 'svn property' on \a path.
		///
		explicit SVNproperty(const std::string& path);

		/**
			 @brief Check whether item used to create this object is binary.

			 @return True if item is binary.
		*/
		inline bool binary(void) const { return binary_; }

		/**
			 @brief Check if item used to create this object has been
			 assigned property svndigest:ignore.

			 Currently files with property svndigest:ignore are to be
			 ignored by svndigest. It is the responsibility of the
			 statistics implementer to obey the ignore state.

			 @return True if item property svndigest:digest was set.
		*/
		inline bool svndigest_ignore(void) const { return svndigest_ignore_; }

		/**
			 @brief Get the list of properties for item used to creat this
			 SVNproperty object.
		*/
		inline const std::map<std::string, std::string>&
		properties(void) const { return property_; }

	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNproperty(const SVNproperty&);

		bool binary_;
		std::map<std::string,std::string> property_;
		bool svndigest_ignore_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
