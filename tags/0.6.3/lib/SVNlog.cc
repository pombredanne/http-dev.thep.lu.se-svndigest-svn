// $Id: SVNlog.cc 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVNlog.h"

#include "Commitment.h"
#include "SVN.h"

#include <cassert>
#include <string>
#include <vector>

namespace theplu {
namespace svndigest {


	SVNlog::SVNlog(const std::string& path)
	{
		SVN::instance()->client_log(path, log_message_receiver,
																static_cast<void*>(&lb_));
		assert(date().size()==author().size());
		assert(date().size()==revision().size());
		assert(date().size()==message().size());
	}


	SVNlog::~SVNlog(void)
	{
	}

	
	bool SVNlog::exist(std::string name) const
	{
		std::vector<std::string>::const_reverse_iterator iter = 
			find(author().rbegin(), author().rend(), name);
		return iter!=author().rend();
	}


	Commitment SVNlog::latest_commit(void) const
	{
		return Commitment(author().back(), Date(date().back()), 
											message().back(), revision().back());
																						 
	}


	Commitment SVNlog::latest_commit(std::string name) const
	{
		std::vector<std::string>::const_reverse_iterator iter = 
			find(author().rbegin(), author().rend(), name);
		size_t dist(std::distance(iter, author().rend()));
		if (!dist) {
			Commitment c;
			assert(false);
			return c;
		}
		return Commitment(author()[dist-1], Date(date()[dist-1]), 
											message()[dist-1], revision()[dist-1]);
																						 
	}


	svn_error_t* 
	SVNlog::log_message_receiver(void *baton, apr_hash_t *changed_paths,
															 svn_revnum_t rev, const char *author,
															 const char *date, const char *msg,
															 apr_pool_t *pool)
	{
		struct log_receiver_baton *lb=static_cast<struct log_receiver_baton*>(baton);
		if (date && date[0])
			lb->commit_dates.push_back(date);
		else
			throw SVNException("No date defined for revision: " + rev); 
		if (author && author[0])
			lb->authors.push_back(author);
		else
			lb->authors.push_back("");
		lb->rev.push_back(rev);
		if (msg)
			lb->msg.push_back(std::string(msg));
		else
			lb->msg.push_back(std::string(""));
    return SVN_NO_ERROR;
	}

}} // end of namespace svndigest and namespace theplu
