// $Id: svndigest.cc 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"

#include "Configuration.h"
#include "css.h"
#include "Directory.h"
#include "first_page.h"
#include "GnuplotFE.h"
#include "html_utility.h"
#include "rmdirhier.h"
#include "SVN.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "utility.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

int main(const int argc,const char* argv[])
{
	using namespace theplu::svndigest;

	// Reading commandline options
	Parameter* option=NULL;
	try {
		option = new Parameter(argc,argv);
		if (option->verbose())
			std::cout << "Done parsing parameters" << std::endl;
	}
	catch (std::runtime_error e) {
		std::cerr << e.what() << std::endl;
		exit(-1);
	}
	assert(option);

	// Reading configuration file
	Configuration& config = Configuration::instance();
	if (node_exist(option->config_file())) {
		std::ifstream is(option->config_file().c_str());
		if (!is.good()){
			is.close();
			std::cerr << "\nsvndigest: Cannot open config file " 
								<< option->config_file() << std::endl; 
			exit(-1);
		}
		try { 
			config.load(is);
		}
		catch (std::runtime_error e) {
			std::cerr << e.what() << std::endl;
			exit(-1);
		}
		is.close();
	}
	else
		config.load();
	
	// write configuration
	if (option->generate_config()) {
		std::cout << config;
		exit(0);
	}

	SVN* svn=NULL;
	try {
		if (option->verbose())
			std::cout << "Initializing SVN singleton." << std::endl;
		svn=SVN::instance(option->root());
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what()
							<< "\nsvndigest: failed to initilize subversion access for "
							<< option->root() << std::endl;
		exit(-1);
	}

	// check if target already exists and behave appropriately
	if (option->verbose())
		std::cout << "Checking target directory" << std::endl;
	std::string target_path=option->targetdir() + '/' + file_name(option->root());
	bool need_to_erase_target = node_exist(target_path);
	if (need_to_erase_target && !option->force())
		throw std::runtime_error(std::string("svndigest: directory (") +
														 target_path + ") already exists");

	// Extract repository location
	std::string repo;
	try {
		if (option->verbose())
			std::cout << "Acquiring repository information" << std::endl;
		repo=SVNinfo(option->root()).repos_root_url();
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what()
							<< "\nsvndigest: Failed to determine repository for "
							<< option->root() << '\n' << std::endl;
		exit(-1);
	}

	// build directory tree already here ... if WC is not upto date with
	// repo an exception is thrown. This avoids several costly
	// statements below and will not remove a digest tree below if a
	// tree already exists.
	if (option->verbose())
		std::cout << "Building directory tree" << std::endl;
	Directory* tree=NULL;
	try {
		tree = new Directory(0,option->root(),"");
	}
	catch (NodeException e) {
		std::cerr << "svndigest: " << e.what() << std::endl;
		exit(-1);
	}
	assert(tree);

	if (option->verbose())
		std::cout << "Parsing directory tree" << std::endl;
	Stats stats(option->root());
	stats+=tree->parse(option->verbose());

	// remove target if needed
	if (need_to_erase_target) {
			if (option->verbose())
				std::cout << "Removing old target tree: " << target_path << "\n";
			rmdirhier(target_path);
	}

	if (option->verbose())
		std::cout << "Generating output" << std::endl;
	if (!option->revisions())
		GnuplotFE::instance()->set_dates(SVNlog(repo).date());
	chdir(option->targetdir().c_str());
	mkdir(tree->name());
	chdir(tree->name().c_str());
	GnuplotFE::instance()->command(std::string("cd '")+option->targetdir()+"/"
																 +tree->name()+"'");
	print_css("svndigest.css");
	SVNlog local_log(option->root());
	print_main_page(tree->name(), local_log, stats, 
									tree->url());
	mkdir("all");
	mkdir("images");
	touch("all/index.html");
	touch("images/index.html");
	for (std::set<std::string>::const_iterator i = stats.authors().begin();
			 i!=stats.authors().end(); ++i) {
		mkdir(*i);
		touch(std::string(*i+"/index.html"));
	}
	try {
		tree->print(option->verbose());
	}
	catch (const std::runtime_error& x) {
		std::cerr << "svndigest: " << x.what() << std::endl;
		exit(-1);
	}

	if (option->copyright()){
		try {
			if (option->verbose())
				std::cout << "Updating copyright statements" << std::endl;
			std::map<std::string, Alias> alias(config.copyright_alias());
			tree->print_copyright(alias);
		}
		catch (const std::runtime_error& x) {
			std::cerr << "svndigest: " << x.what() << std::endl;
			exit(-1);
		}
	}

	if (option->verbose())
		std::cout << "Finalizing" << std::endl;

	delete tree;
	if (option->verbose())
		std::cout << "Done!" << std::endl;
	delete option;
	exit(0);				// normal exit
}
