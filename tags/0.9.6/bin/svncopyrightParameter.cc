// $Id: svncopyrightParameter.cc 1119 2010-07-04 18:34:07Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "svncopyrightParameter.h"

#include "lib/OptionVersion.h"

#include <cassert>
#include <string>

namespace theplu {
namespace svndigest {

	svncopyrightParameter::svncopyrightParameter(void)
		: Parameter()
	{
	}


	void svncopyrightParameter::analyse2(void)
	{
	}


	void svncopyrightParameter::init2(void)
	{
		help_.synopsis() = 
			"Update copyright statement in files under subversion control\n";
		std::stringstream ss;
		ss <<  "svncopyright (" << PACKAGE_NAME << ")";
		version_.program_name(ss.str());
	}


	void svncopyrightParameter::set_default2(void)
	{
	}


}} // of namespace svndigest and namespace theplu
