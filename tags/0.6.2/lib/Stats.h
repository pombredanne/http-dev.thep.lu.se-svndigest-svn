#ifndef _theplu_svndigest_stats_
#define _theplu_svndigest_stats_

// $Id: Stats.h 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parser.h"

#include <subversion-1/svn_types.h>

#include <map>
#include <set>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

  ///
  /// Class taking care of statistics from svn.
  ///
  class Stats
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		explicit Stats(const std::string& path);

		///
		/// @return set of authors
		///
		inline const std::set<std::string>& authors(void) const { return authors_; }

		///
		/// 
		///
    inline u_int code(void) const { return accumulated(code_).back(); }

		///
		/// 
		///
    inline u_int code(const std::string& user) const 
		{ return accumulated(code_, user).back(); }

		///
		/// 
		///
    inline u_int comments(void) const { return accumulated(comments_).back(); }

		///
		/// 
		///
    inline u_int comments(const std::string& user) const 
		{ return accumulated(comments_, user).back(); }

		///
		/// 
		///
    inline u_int empty(void) const { return accumulated(empty_).back(); }

		///
		/// 
		///
    inline u_int empty(const std::string& user) const 
		{ return accumulated(empty_, user).back(); }

		///
		///
		///
		inline u_int last_changed_rev(void) const { return last_changed_rev_; }

		///
		/// 
		///
    inline u_int lines(void) const { return accumulated(total_).back(); }

		///
		/// 
		///
    inline u_int lines(const std::string& user) const 
		{ return accumulated(total_, user).back(); }

		void parse(const std::string&);

		///
		/// Create statistics graph.
		///
		std::string plot(const std::string&, const std::string&) const;

		///
		/// Create statistics graph.
		///
		void plot_init(const std::string& output) const;

		///
		/// Summary plot for the first page
		///
		void plot_summary(const std::string& output) const;

		///
		/// @brief Clear all statistics
		///
    inline void reset(void) 
		{ 
			code_.clear(); comments_.clear(); empty_.clear(); total_.clear(); 
			authors_.clear();
		}

		///
		/// \return latest revision for whole project
		///
		inline u_int revision(void) const { return revision_; }

		///
		/// 
		///
    inline std::vector<u_int> total(const std::string& user) const 
		{ return accumulated(total_, user); }

    ///
    /// @return resulting Stats
    ///
    Stats& operator+=(const Stats&);

  private:
    // Peter, if the vector is sparse make it a map
    typedef std::map<std::string, std::vector<u_int> > Map_;
    typedef Map_::iterator MapIter_;
    typedef Map_::const_iterator MapConstIter_;

		///
		/// Copy constructor (not implemented)
		///
    Stats(const Stats& other);

    ///
    /// @return accumulated vector of total
    ///
    std::vector<u_int> accumulated(const Map_&) const;

    ///
    /// @return accumulated vector of stats_[user]
    ///
    std::vector<u_int> accumulated(const Map_&, 
																	 const std::string& user) const;

    ///
    /// @brief adding a line to user from revision to the stats
    ///
    void add(const std::string& user, const u_int& revision,
						 const Parser::line_type&); 


		svn_revnum_t revision_; // Should be the latest revision for whole project
		svn_revnum_t last_changed_rev_; // Should be the latest revision for file

		std::set<std::string> authors_;
    Map_ code_;
    Map_ comments_;
    Map_ empty_;
    Map_ total_;
  };
}} // end of namespace svndigest end of namespace theplu

#endif 
