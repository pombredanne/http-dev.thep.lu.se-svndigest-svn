# $Id: check_repo_status.sh.in 430 2007-07-06 23:33:11Z peter $

# Copyright (C) 2007 Jari H�kkinen
#
# This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

# assume test repository and svndigest repository are synchronized
repository_status=1

# retrieve last revision in test repository
rev=`cat repo/db/current`
rev=${rev%% *}

# check if the last test revision is a part of the svndigest repository
status_return=`svn status -v repo/db/revs/$rev`
# logic: (file does not exist) || (file exists but not in revision control)
if [[ ${#status_return} -eq 0 || ${status_return:0:1} = "?" ]]; then
		repository_status=0
		echo "ERROR: test repository and svndigest repository are not synchronized:"
		echo "ERROR: test repository has entries not added to svndigest repository"
		echo "ERROR:"
		echo "ERROR: Please resolve issues before committing to svndigest repository"
		echo "ERROR: test revision(s) needs to be added the svndigest repository."
		echo "ERROR:"
		echo "ERROR: Issue these commands:"
fi

# loop until a test repository revision that is a part of svndigest
# repository is found, or break the test repository revision 0 is
# reached.
while [[ ${#status_return} -eq 0 || ${status_return:0:1} = "?" ]] && [ $rev -ge 0 ]; do
		echo "ERROR:    svn add test/repo/db/revs/$rev test/repo/db/revprops/$rev"
		let rev--
		status_return=`svn status -v repo/db/revs/$rev`
done

if [ $repository_status -eq 0 ]; then
		exit -1;		# test failed.
fi
