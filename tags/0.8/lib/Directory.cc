// $Id: Directory.cc 1024 2010-01-10 23:33:34Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Directory.h"

#include "Alias.h"
#include "Configuration.h"
#include "File.h"
#include "html_utility.h"
#include "Node.h"
#include "SVN.h"
#include "SVNlog.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <sstream>

#include <cerrno>	// Needed to check error state below.
#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	Directory::Directory(const unsigned int level, const std::string& path, 
											 const std::string& output)
		: Node(level,path,output)
	{
		output_dir_=local_path();
		if (!output_dir_.empty())
			output_dir_+='/';

		using namespace std;
		DIR* directory=opendir(path.c_str());    // C API from dirent.h
		if (!directory)
			throw NodeException("ERROR: opendir() failed; " + path +
													" is not a directory");
		list<string> entries;
		struct dirent* entry;
		errno=0;	// Global variable used by C to track errors, from errno.h
		while ((entry=readdir(directory)))       // C API from dirent.h
			entries.push_back(string(entry->d_name));
		if (errno)
			throw NodeException("ERROR: readdir() failed on " + path);
		closedir(directory);

		SVN* svn=SVN::instance();
		for (list<string>::iterator i=entries.begin(); i!=entries.end(); ++i)
			if ((*i)!=string(".") && (*i)!=string("..") && (*i)!=string(".svn")) {
				string fullpath(path_+'/'+(*i));
				switch (svn->version_controlled(fullpath)) {
				case SVN::uptodate:
					struct stat nodestat;                // C api from sys/stat.h
					lstat(fullpath.c_str(),&nodestat);   // C api from sys/stat.h
					if (S_ISDIR(nodestat.st_mode))       // C api from sys/stat.h
						daughters_.push_back(new Directory(level_+1,fullpath,local_path()));
					else
						daughters_.push_back(new File(level_,fullpath,local_path()));
					break;
				case SVN::unresolved:
					throw NodeException(fullpath+" is not up to date");
				case SVN::unversioned: ; // do nothing
				}
			}
		std::sort(daughters_.begin(), daughters_.end(), NodePtrLess());
	}


	Directory::~Directory(void)
	{
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			delete *i;
	}

	bool Directory::dir(void) const
	{
		return true;
	}

	std::string Directory::href(void) const
	{ 
		return name() + "/index.html";
	}


	svn_revnum_t Directory::last_changed_rev(void) const
	{
		svn_revnum_t res = svn_info().last_changed_rev();
		for (NodeConstIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			res = std::max(res, (*i)->last_changed_rev());
		return res;
	}


	void Directory::log_core(SVNlog& log) const
	{
		for (NodeConstIterator i(daughters_.begin()); i != daughters_.end(); ++i)
			log += (*i)->log();
	}

	std::string Directory::node_type(void) const
	{
		return std::string("directory");
	}


	std::string Directory::output_path(void) const
	{
		return output_dir()+"index.html";
	}

	const StatsCollection& Directory::parse(bool verbose, bool ignore)
	{
		stats_.reset();
		// Directories themselved give no contribution to statistics.
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			if (!(*i)->ignore())
				stats_ += (*i)->parse(verbose, ignore);
		return stats_;
	}


	void Directory::print_core(const bool verbose) const
	{
		mkdir("blame_output/" + local_path());
		// print daughter nodes
		for (NodeConstIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			(*i)->print(verbose);
	}


	void Directory::print_core(const std::string& stats_type,
														 const std::string& user, 
														 const std::string& line_type,
														 const SVNlog& log) const
	{

		const Stats& stats = stats_[stats_type];
		std::string imagedir = stats_type+"/"+"images/"+line_type;
		std::string outdir   = stats_type+"/"+user+"/" +line_type;
		if (local_path()!="") {
			imagedir += "/"+local_path();
			outdir   += "/"+local_path();
		}
		mkdir(outdir);
		if (user=="all")
			mkdir(imagedir);
		std::string html_name = outdir+"/index.html";
		std::ofstream os(html_name.c_str());
		assert(os.good());
		if (local_path().empty())
			print_header(os, name(), level_+3, user, line_type, "index.html", 
									 stats_type);
		else
			print_header(os, name(), level_+3, user, line_type, 
									 local_path()+"/index.html", stats_type);
		path_anchor(os);

		std::stringstream ss;
		for (size_t i=0; i<level_; ++i)
			ss << "../";
		ss << "../../../";
		if (user=="all")
			ss << stats.plot(imagedir+"/index", line_type);
		else
			ss << imagedir << "/index";
		os << "<p class=\"plot\">\n"; 
		os << image(ss.str());
		os << "</p>\n";

		os << "<h3>File Summary";
		if (user!="all")
			os << " for " << user;
		os << "</h3>";			
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Node</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Author</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (level_){
			os << "<tr class=\"light\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("../index.html", "../");
			os << "</td>\n</tr>\n";
			color = "dark";
		}

		// print html links to daughter nodes
		for (NodeConstIterator d = daughters_.begin(); d!=daughters_.end(); ++d) {
			(*d)->html_tablerow(os,stats_type, color, user);
			if (color=="dark")
				color = "light";
			else
				color = "dark";
		}
		os << "<tr class=\"" << color << "\">\n";
		os << "<td>Total</td>\n";
		if (user=="all"){
			os << "<td>" << stats.lines() << "</td>\n";
			os << "<td>" << stats.code() << "</td>\n";
			os << "<td>" << stats.comments() << "</td>\n";
			os << "<td>" << stats.empty() << "</td>\n";
		}
		else {
			os << "<td>" << stats.lines(user); 
			if (stats.lines(user)) 
				os << " (" << percent(stats.lines(user),stats.lines()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.code(user); 
			if (stats.code(user)) 
				os << " (" << percent(stats.code(user),stats.code()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.comments(user); 
			if (stats.comments(user)) 
				os << " (" << percent(stats.comments(user),stats.comments()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.empty(user); 
			if (stats.empty(user)) 
				os << " (" << percent(stats.empty(user),stats.empty()) << "%)"; 
			os << "</td>\n";
		}
		os << "<td>" << trac_revision(last_changed_rev()) << "</td>\n";
		os << "<td>" << author() << "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
		print_author_summary(os, stats, line_type, log);
		os << "\n";
		print_footer(os);
		os.close();	
	}


	void Directory::print_copyright(std::map<std::string, Alias>& alias, 
																	bool verbose,
																	const std::map<int,svn_revnum_t>& y2r) const 
	{
		if (!ignore()){
			// print daughter nodes, i.e, this function is recursive
			for (NodeConstIterator i = daughters_.begin(); i!=daughters_.end(); ++i)
				(*i)->print_copyright(alias, verbose, y2r);
		}
	}

}} // end of namespace svndigest and namespace theplu
