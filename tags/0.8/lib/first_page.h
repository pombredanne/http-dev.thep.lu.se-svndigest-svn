#ifndef _theplu_svndigest_first_page_
#define _theplu_svndigest_first_page_

// $Id: first_page.h 978 2009-12-12 20:09:41Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iosfwd>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	class Commitment;
	class Stats;
	class StatsCollection;
	class SVNlog;

	void print_authors(std::ostream& os, 
										 const std::vector<Commitment>& latest_commit,
										 const Stats& stats);

	///
	/// called by print_main_page
	///
	void print_general_information(std::ostream&, const SVNlog&, size_t, 
																 std::string url);

	///
	/// @brief print main page
	///
	void print_main_page(const std::string&, const SVNlog&, 
											 const StatsCollection&, std::string url);
		
	void print_recent_logs(std::ostream&, const SVNlog& log,
												 const StatsCollection&);
		
	void print_summary_plot(std::ostream&, const Stats& stats);
		
}} // end of namespace svndigest end of namespace theplu

#endif 
