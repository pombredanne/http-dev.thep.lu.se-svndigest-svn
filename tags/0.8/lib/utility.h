#ifndef _theplu_svndigest_utility_
#define _theplu_svndigest_utility_

// $Id: utility.h 1098 2010-06-13 16:46:29Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <functional>
#include <iosfwd>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	///
	/// @brief Check if access permissions match \a mode. \a mode must
	/// be given as r, w, x, or combinations of these letters.
	///
	/// @return On success (all requested permissions granted), zero
	/// is returned. On error (at least one bit in mode asked for a
	/// permission that is denied, or some other error occurred), -1
	/// is returned, and errno is set appropriately.
	///
	/// @throw An std::runtime_error is thrown when checking for write
	/// permissions for a file/direcotry that does not exist.
	///
	/// @see access(2)
	///
	int access_rights(const std::string& path,const std::string& bits);

	/**
		 wrapper around GNU C Library function chdir

		 \throw if underlying chdir call does not return 0
	 */
	void chdir(const std::string& dir);

	/**
		 \return dir+base if dir ends with '/', else dir+'/'+base
	 */
	std::string concatenate_path(std::string dir, std::string base);

	/**
		 @brief Copy file \a source to \a target.

		 @throw std::runtime_error when read error of \a source or write
		 error for \a target is encountered.
	*/
	void copy_file(const std::string& source, const std::string& target);

	/**
		 Parsing out the directory of path that is everything prior the
		 last '/' with the eception if \a path ends with '/' in which case
		 everything prior the second last '/' is returned.

		 \return directory of path.
	 */
	std::string directory_name(std::string path);

	///
	/// @return everything after last '/'
	///
	std::string file_name(const std::string&);

	/**
		 \return true if \a str matches \a pattern

		 \see fnmatch(3)
	 */
	bool fnmatch(const std::string& pattern, const std::string& str);

	///
	/// @brief environment variable @a var
	///
	std::string getenv(const std::string& var); 

	///
	/// If @a width is set, size is forced to length @a width. This
	/// implies, e.g., that hex(15,2) and hex(17,1) return "0F" and "1",
	/// respectively.
	///
	/// @return x in hexadecimal base
	///
	std::string hex(int x, unsigned int width=0);

	///
	/// @brief remove trailing whitespaces
	///
	std::string htrim(std::string str);

	///
	/// @brief remove leading whitespaces
	///
	std::string ltrim(std::string str);

	inline bool match_begin(std::string::const_iterator first, 
													std::string::const_iterator last, 
													const std::string& str)
	{ return (std::distance(first, last)>=static_cast<int>(str.size()) && 
						std::equal(str.begin(), str.end(), first)); 
	}

	inline bool match_end(std::string::const_reverse_iterator first, 
												std::string::const_reverse_iterator last, 
												const std::string& str)
	{ return (std::distance(first,last)>=static_cast<int>(str.size()) && 
						std::equal(str.rbegin(), str.rend(), first)); 
	}

	///
	/// Create directory \a dir. The call can fail in many ways, cf. 'man
	/// mkdir'.
	///
	void mkdir(const std::string& dir);

	///
	/// Create directory \a dir and parents directories if needed. No
	/// error if \a dir already exists.
	///
	void mkdir_p(const std::string& dir);

	///
	/// @brief Check whether \a path already exists or not.
	///
	/// @return True if \a path exists, false otherwise.
	///
	bool node_exist(const std::string& path);

	/**
		 @return 0 if \a b = 0 otherwise \f$ \frac{100*a}{b} \f$ 
	*/
	int percent(int a, int b);

	///
	/// @return the current working directory.
	///
	std::string pwd(void);

	/**
		 \return true if \a str matches \a pattern

		 \a pattern may contain wildcards '*', '?' and \a vec will contain
		 the matching string in \a str. If it's not a match, \a vec is
		 undefined. The algorithm is greedy, i.e., wildcards '*' will
		 consume as many charcters as possible.

		 \note \a vec is supposed to be empty
	 */
	bool regexp(const std::string& pattern,	const std::string& str,
							std::vector<std::string>& vec);

	/**
		 In \a full_str  replace every sub-string \a old_str with
		 \a new_str;
	 */
	void replace(std::string& full_str, std::string old_str, std::string new_str);

	///
	/// Search finds a subsecuence in [first, last) being identical to @ str
	///
	/// @return iterator pointing to first character in found subsequence
	///
	/// @see std::search
	///
	inline std::string::iterator search(std::string::iterator first, 
																			std::string::iterator last, 
																			std::string str)
	{ return std::search(first, last, str.begin(), str.end()); }

	///
	///
	///
	time_t str2time(const std::string&);

	///
	/// If file does not exist create empty file.
	///
	void touch(std::string);

	///
	/// remove leading and trailing whitespaces
	///
	inline std::string trim(std::string str) { return htrim(ltrim(str)); }


	template <typename T>
	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										const T& func)
	{
		std::string res;
		for (;first!=last && func(first); ++first) 
			res.append(1,*first);
		return res;
	}


	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										std::string);

}} // end of namespace svndigest end of namespace theplu

#endif 
