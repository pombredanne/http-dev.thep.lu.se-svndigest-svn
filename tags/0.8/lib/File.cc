// $Id: File.cc 1097 2010-06-13 03:23:43Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "File.h"

#include "Alias.h"
#include "Colors.h"
#include "Configuration.h"
#include "Date.h"
#include "Graph.h"
#include "html_utility.h"
#include "HtmlStream.h"
#include "Stats.h"
#include "SVNblame.h"
#include "SVNlog.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	File::File(const unsigned int level, const std::string& path, 
						 const std::string& output) 
		: Node(level,path,output) 
	{
		output_dir_=output;
		if (!output_dir_.empty())
			output_dir_+='/';
	}


	std::string File::blame_output_file_name(void) const
	{
		return "blame_output/" + local_path() + ".html";
	}


	std::map<int, std::set<Alias> >
	File::copyright_map(std::map<std::string, Alias>& alias,
											const std::map<int, svn_revnum_t>& year2rev) const
	{
		using namespace std;
		map<int, set<Alias> > year_authors;
		const Stats& stats = stats_["add"];

		// loop over all years
		for (std::map<int, svn_revnum_t>::const_iterator rev_iter=year2rev.begin();
				 rev_iter!=year2rev.end(); ++rev_iter) {

			svn_revnum_t last_rev_this_year = rev_iter->second;
			svn_revnum_t last_rev_last_year = 0;
			if (rev_iter != year2rev.begin()) {
				last_rev_last_year = (--rev_iter)->second;
				++rev_iter;
			}
			// do not go beyond BASE rev of file
			last_rev_this_year = std::min(last_rev_this_year, last_changed_rev());
			last_rev_last_year = std::min(last_rev_last_year, last_changed_rev());
			// loop over authors
			for (std::set<std::string>::const_iterator a_iter=stats.authors().begin();
					 a_iter!=stats.authors().end(); ++a_iter) {

				// check if anything has been added since last year
				if ( (stats(LineTypeParser::code, *a_iter, last_rev_this_year) >
							stats(LineTypeParser::code, *a_iter, last_rev_last_year)) || 
						 (stats(LineTypeParser::comment, *a_iter, last_rev_this_year) >
							stats(LineTypeParser::comment, *a_iter, last_rev_last_year)) ) {
				
				
					// find username in map of aliases
					std::map<string,Alias>::iterator name(alias.lower_bound(*a_iter));
					
					// if alias exist insert alias
					if (name != alias.end() && name->first==*a_iter)
						year_authors[rev_iter->first].insert(name->second);
					else {
						// else insert user name
						Alias a(*a_iter,alias.size());
						year_authors[rev_iter->first].insert(a);
						std::cerr << "svncopyright: warning: no copyright alias found for `" 
											<< *a_iter << "'\n";
						// insert alias to avoid multiple warnings.
						alias.insert(name, std::make_pair(*a_iter, a));
					}
				}
			}
		}
		return year_authors;
	}


	std::string 
	File::copyright_block(const std::map<int, std::set<Alias> >& year_authors,
												const std::string& prefix) const
	{
		using namespace std;
		stringstream ss;
		for (map<int, set<Alias> >::const_iterator i(year_authors.begin());
				 i!=year_authors.end();) {
			ss << prefix << "Copyright (C) "
					<< 1900+i->first;
			map<int, set<Alias> >::const_iterator j = i;
			assert(i!=year_authors.end());
			while (++j!=year_authors.end() && 
						 i->second == j->second){
				ss << ", " << 1900+(j->first);
			}
			// printing authors
			std::vector<Alias> vec_alias;
			back_insert_iterator<std::vector<Alias> > ii(vec_alias);
			std::copy(i->second.begin(), i->second.end(), ii);
			// sort with respect to id
			std::sort(vec_alias.begin(), vec_alias.end(), IdCompare());
			for (std::vector<Alias>::iterator a=vec_alias.begin();
					 a!=vec_alias.end(); ++a){
				if (a!=vec_alias.begin())
					ss << ",";
				ss << " " << a->name();
			}
			ss << "\n";
			i = j;
		}
		return ss.str();
	}

	bool File::detect_copyright(std::string& block, size_t& start_at_line,
															size_t& end_at_line, std::string& prefix) const
	{
		using namespace std;
		LineTypeParser parser(path());
		std::ifstream is(path().c_str());
		std::string line;
		while (std::getline(is, line)) 
			parser.parse(line);
		if (!parser.copyright_found())
			return false;
		block = parser.block();
		start_at_line = parser.start_line();
		end_at_line = parser.end_line();
		prefix = parser.prefix();
		return true;
	}


	std::string File::href(void) const
	{ 
		return name()+".html"; 
	}


	svn_revnum_t File::last_changed_rev(void) const
	{
		return svn_info().last_changed_rev();
	}


	void File::log_core(SVNlog&) const
	{
	}


	std::string File::node_type(void) const
	{
		return std::string("file");
	}


	std::string File::output_path(void) const
	{
		return output_dir()+name()+".html";
	}


	const StatsCollection& File::parse(bool verbose, bool ignore)
	{
		if (verbose)
			std::cout << "Parsing " << path_ << std::endl; 
		stats_.reset();
		std::string cache_dir = directory_name(path()) + std::string(".svndigest/"); 
		std::string cache_file = cache_dir + name()+std::string(".svndigest-cache");
		if (!ignore && node_exist(cache_file)){
			std::ifstream is(cache_file.c_str());
			if (stats_.load_cache(is)) {
				is.close();
				return stats_;
			}
			is.close();
		}
		else 
			stats_.parse(path_);
		if (!node_exist(cache_dir))
			mkdir(cache_dir);
		std::string tmp_cache_file(cache_file+"~");
		std::ofstream os(tmp_cache_file.c_str());
		stats_.print(os);
		os.close();
		rename(tmp_cache_file.c_str(), cache_file.c_str());
		return stats_;
	}


	void File::print_blame(std::ofstream& os) const
	{
		os << "<br /><h3>" << local_path() << "</h3>";
		os << "<div class=\"blame_legend\">\n";
		os << "<dl>\n";
		os << "<dt class=\"code\"></dt><dd>Code</dd>\n";
		os << "<dt class=\"comment\"></dt><dd>Comments</dd>\n";
		os << "<dt class=\"other\"></dt><dd>Other</dd>\n";
		os << "</dl>\n</div>\n";
		os << "<table class=\"blame\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th class=\"rev\">Rev</th>\n";
		os << "<th class=\"date\">Date</th>\n";
		os << "<th class=\"author\">Author</th>\n";
		os << "<th class=\"line\">Line</th>\n";
		os << "<th></th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";
		HtmlStream hs(os);
		SVNblame blame(path_);
		LineTypeParser parser(path_);
		while (blame.valid()) {
			parser.parse(blame.line());
			blame.next_line();
		}
		blame.reset();

		std::vector<LineTypeParser::line_type>::const_iterator 
			line_type(parser.type().begin());
		int last=0;
		int first=0;
		bool using_dates=true;
		if (!Graph::date_xticks()) {
			using_dates=false;
			last = stats_["classic"].revision();
		}
		else {
			last = Date(Graph::xticks().back()).seconds();
			// earliest date corresponds either to revision 0 or revision 1 
			first = std::min(Date(Graph::xticks()[0]).seconds(),
											 Date(Graph::xticks()[1]).seconds());
		}
		// color is calculated linearly on time, c = kt + m
		// brightest color (for oldest rev in log) is set to 192.
		double k = 192.0/(first-last);
		double m = -last*k; 
		while (blame.valid()) {
			std::string color;
			if (using_dates)
				color = hex(static_cast<int>(k*Date(blame.date()).seconds()+m),2);
			else
				color = hex(static_cast<int>(k*blame.revision()+m),2);
			os << "<tr>\n<td class=\"rev\">";
			std::stringstream color_ss;
			color_ss << "#" << color << color << color; 
			os << "<font color=\"" << color_ss.str() << "\">"
				 << trac_revision(blame.revision(), color_ss.str())
				 << "</font></td>\n<td class=\"date\"><font color=\"#" << color 
				 << color << color << "\">" ;
			hs << Date(blame.date())("%d %b %y");
			os << "</font></td>\n<td class=\"author\">";
			const std::string& author_color = 
				Colors::instance().color_str(blame.author());
			assert(!author_color.empty());
			os << "<font color=\"#" << author_color << "\">";
			hs << blame.author();
			os << "</td>\n<td class=\"";
			assert(line_type!=parser.type().end());
			if (*line_type==LineTypeParser::other)
				os << "line-other";
			else if (*line_type==LineTypeParser::comment || 
							 *line_type==LineTypeParser::copyright)				
				os << "line-comment";
			else if (*line_type==LineTypeParser::code)
				os << "line-code";
			else {
				std::string msg="File::print_blame(): unexpected line type found";
				throw std::runtime_error(msg);
			}
			os << "\">" << blame.line_no()+1
				 << "</td>\n<td>";
			hs << blame.line();
			os << "</td>\n</tr>\n";
			blame.next_line();
			++line_type;
		}
		os << "</tbody>\n";
		os << "</table>\n";
	}


	void File::print_copyright(std::map<std::string, Alias>& alias, 
														 bool verbose,
														 const std::map<int,svn_revnum_t>& y2rev) const 
	{
		if (ignore())
			return;

		std::string old_block;
		size_t start_line=0;
		size_t end_line=0;
		std::string prefix;
		if (!detect_copyright(old_block, start_line, end_line, prefix)){
			if (Configuration::instance().missing_copyright_warning())
				std::cerr << "svncopyright: warning: no copyright statement found in `" 
									<< path_ << "'\n";
			return;
		}
		std::map<int, std::set<Alias> > map=copyright_map(alias, y2rev);
		std::string new_block = copyright_block(map, prefix);
		if (old_block==new_block)
			return;
		if (verbose)
			std::cout << "Updating copyright in " << path_ << std::endl; 
		update_copyright(new_block, start_line, end_line);
	}


	void File::print_core(const bool verbose) const 
	{
		std::ofstream os(blame_output_file_name().c_str());
		assert(os.good());
		print_html_start(os, "svndigest", level_+1);
		print_blame(os);
		print_footer(os);
		os.close();
	}


	void File::print_core(const std::string& stats_type, 
												const std::string& user, const std::string& line_type,
												const SVNlog& log) const 
	{
		std::string lpath = local_path();
		if (lpath.empty())
			lpath = "index";
		std::string outpath = stats_type+"/"+user+"/"+line_type+"/"+lpath;
		std::string imagefile = stats_type+"/"+"images/"+line_type+"/"+lpath;
		std::string html_name(outpath + ".html");
		std::ofstream os(html_name.c_str());
		print_header(os, name(), level_+3, user, line_type, lpath+".html",
								 stats_type);
		path_anchor(os);

		std::stringstream ss;
		for (size_t i=0; i<level_; ++i)
			ss << "../";
		ss << "../../../";
		if (user=="all")
			ss << stats_[stats_type].plot(imagefile,line_type);
		else
			ss << imagefile;
		os << "<p class=\"plot\">\n"; 
		os << image(ss.str());
		os << "</p>\n";

		print_author_summary(os, stats_[stats_type], line_type, log);
		os << "\n";
		os << "<h3>"
			 << anchor(blame_output_file_name(), 
								 "Blame Information", level_+3) 
			 << "</h3>\n";

		print_footer(os);
		os.close();	
	}

	void File::update_copyright(const std::string& new_block,
															size_t start_at_line, size_t end_at_line) const
	{
		// Code copied from Gnuplot -r70
		char tmpname[]="/tmp/svndigestXXXXXX";
		int fd=mkstemp(tmpname);	// mkstemp return a file descriptor
		if (fd == -1)
			throw std::runtime_error(std::string("Failed to get unique filename: ") +
															 tmpname);
		// Jari would like to do something like 'std::ofstream tmp(fd);'
		// but has to settle for (which is stupid since the file is
		// already open for writing.
		std::ofstream tmp(tmpname);

		using namespace std;
		ifstream is(path().c_str());
		assert(is.good());
		string line;
		// Copy lines before block
		for (size_t i=1; i<start_at_line; ++i){
			assert(is.good());
			getline(is, line);
			tmp << line << "\n";
		}
		// Printing copyright statement
		tmp << new_block;
		// Ignore old block lines
		for (size_t i=start_at_line; i<end_at_line; ++i){
			assert(is.good());
			getline(is, line);
		}
		// Copy lines after block
		while(is.good()) {
			char ch=is.get();
			if (is.good())
				tmp.put(ch);
		}

		is.close();
		tmp.close();
		close(fd);
		// get file permission
		struct stat nodestat;
		stat(path().c_str(),&nodestat);
		
		// finally copy temporary file to replace original file, and
		// remove the temporary file
		try {
			copy_file(tmpname, path());
		}
		catch (std::runtime_error e) {
			// catch exception, cleanup, and rethrow
			std::cerr << "svncopyright: File::print_copyright: Exception caught, "
								<< "removing temporary file " << tmpname << std::endl;
			if (unlink(tmpname))
				throw runtime_error(std::string("File::print_copyright: ") +
														"failed to unlink temporary file" + tmpname);
			throw;
		}
		if (unlink(tmpname))
			throw runtime_error(std::string("File::print_copyright: ") +
													"failed to unlink temporary file" + tmpname);

		chmod(path().c_str(), nodestat.st_mode);
	}


}} // end of namespace svndigest and namespace theplu
