#ifndef _theplu_svndigest_svndigest_parameter_
#define _theplu_svndigest_svndigest_parameter_

// $Id: svndigestParameter.h 1121 2010-07-05 14:43:13Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Parameter.h"

#include "lib/OptionVersion.h"

#include "yat/CommandLine.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"


#include <string>

namespace theplu {
namespace yat { 
namespace utility {
	class OptionSwitch;
}}
namespace svndigest {

  // class for command line options.
	class svndigestParameter : public Parameter 
	{
	public:
		svndigestParameter(void);
		virtual ~svndigestParameter(void);
		const yat::utility::OptionArg<std::string>& anchor_format(void) const;
		
		bool copyright(void) const;
		bool force(void) const;
		const yat::utility::OptionArg<std::string>& format(void) const;
		bool report(void) const;
		bool revisions(void) const;
		/// @return absolute path to root directory
		std::string targetdir(void) const;

	private:
		void analyse2(void);
		void init2(void);
		void set_default2(void);

		yat::utility::OptionArg<std::string> anchor_format_;
		yat::utility::OptionSwitch copyright_;
		yat::utility::OptionSwitch force_;
		yat::utility::OptionArg<std::string> format_;
		yat::utility::OptionSwitch report_;
		yat::utility::OptionSwitch revisions_;
		yat::utility::OptionArg<std::string> target_;
	};

}} // of namespace svndigest and namespace theplu

#endif
