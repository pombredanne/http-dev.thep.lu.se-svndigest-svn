#ifndef _theplu_svndigest_parameter_
#define _theplu_svndigest_parameter_

// $Id: Parameter.h 1119 2010-07-04 18:34:07Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib/OptionVersion.h"

#include "yat/CommandLine.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"


#include <string>

namespace theplu {
namespace yat { 
namespace utility {
	class OptionHelp;
	class OptionSwitch;
}}
namespace svndigest {

	class OptionVersion;

  // class for command line options.
	class Parameter {
	public:
		Parameter(void);
		virtual ~Parameter(void);
		void parse( int argc, char *argv[]);

		std::string config_file(void) const;
		
		bool generate_config(void) const ;
		bool ignore_cache(void) const;
		/// @return absolute path to root directory
		std::string root(void) const;
		bool verbose(void) const;

	protected:
		yat::utility::CommandLine cmd_;
		yat::utility::OptionHelp help_;

		// throw cmd_error if path doesn't exist
		void check_existence(std::string path) const;
		// throw cmd_error if path is not dir
		void check_is_dir(std::string path) const;
		// throw cmd_error if path is not readable
		void check_readable(std::string path) const;

	private:
		void analyse(void);
		virtual void analyse2(void)=0;
		void init(void);
		// called at end of init(void)
		virtual void init2(void) = 0;
		void set_default(void);
		virtual void set_default2(void)=0;

		yat::utility::OptionArg<std::string> config_file_;
		yat::utility::OptionSwitch generate_config_;
		yat::utility::OptionSwitch ignore_cache_;
		yat::utility::OptionArg<std::string> root_;
		yat::utility::OptionSwitch verbose_;
	protected:
		OptionVersion version_;

	};

}} // of namespace svndigest and namespace theplu

#endif
