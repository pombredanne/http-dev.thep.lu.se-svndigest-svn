// $Id: svndigest.cc 1180 2010-08-24 23:44:20Z peter $

/*
	Copyright (C) 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "svndigestParameter.h"

#include "lib/Configuration.h"
#include "lib/css.h"
#include "lib/Directory.h"
#include "lib/first_page.h"
#include "lib/Graph.h"
#include "lib/html_utility.h"
#include "lib/main_utility.h"
#include "lib/rmdirhier.h"
#include "lib/Stats.h"
#include "lib/StatsCollection.h"
#include "lib/SVN.h"
#include "lib/SVNinfo.h"
#include "lib/SVNlog.h"
#include "lib/utility.h"

#include "yat/Exception.h"
#include "yat/OptionArg.h"

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

using namespace theplu;
using namespace svndigest;
void copy_option2config(const svndigestParameter&, Configuration&);

void create_file_struct(std::string stats_type, 
												const theplu::svndigest::Stats& stats);

void write_report(const svndigestParameter& option, const std::string& repo,
									const Node& tree, const StatsCollection& stats);

int main( int argc, char* argv[])
{
	// Need to create an instance of Graph (plstream) since there is a
	// bug in plplot with dynamic loading of runtime libraries. The
	// issue is reported in the plplot issue tracking system
	// https://sf.net/tracker/?func=detail&aid=3009045&group_id=2915&atid=102915
	// and as http://dev.thep.lu.se/svndigest/ticket/472. Simply remove
	// this comment and the next line of code when plplot developers
	// fixes their bug.
  //
  // Omitting the dummy instance will cause segmentation fault.
	Graph dummy_never_used("/dev/null", "png");

	// Reading commandline options
	svndigestParameter option;
	try {
		option.parse(argc,argv);
		if (option.verbose())
			std::cout << "Done parsing parameters" << std::endl;
	}
	catch (yat::utility::cmd_error& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (std::runtime_error& e) {
		std::cerr << "svndigest: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	try {
		load_config(option.config_file(), option.verbose());
		copy_option2config(option, Configuration::instance());
		if (option.generate_config()) {
			std::cout << Configuration::instance();
			return EXIT_SUCCESS;
		}

		if (option.verbose())
			std::cout << "Initializing SVN singleton." << std::endl;
		SVN::instance(option.root());

		// Extract repository location
		std::string repo=SVNinfo(option.root()).repos_root_url();

		// build directory tree already here ... if WC is not upto date with
		// repo an exception is thrown. This avoids several costly
		// statements below and will not remove a digest tree below if a
		// tree already exists.
		if (option.verbose())
			std::cout << "Building directory tree" << std::endl;
		Directory tree(0,option.root(),"");

		if (option.verbose())
			std::cout << "Parsing directory tree" << std::endl;
		const StatsCollection& stats(tree.parse(option.verbose(), 
																						option.ignore_cache()));

		if (option.report())
			write_report(option, repo, tree, stats);

		if (option.copyright()){
			update_copyright(tree, option.verbose());
		}
	}
	catch (std::runtime_error& e) {
		std::cerr << "svndigest: " << e.what() << "\n";
		return EXIT_FAILURE;
	}

	if (option.verbose())
		std::cout << "Done!" << std::endl;
	return EXIT_SUCCESS;				// normal exit
}


void copy_option2config(const svndigestParameter& option, Configuration& config)
{
	try {
		if (option.format().present()) 
			Configuration::instance().image_format(option.format().value());
	}
	catch (std::runtime_error& e) {
		std::stringstream ss;
		ss << "invalid argument `" 
			 << option.format().value() << "' for `--" 
			 << option.format().long_name() << "'\n"
			 << e.what() << "\n"
			 << "Try `svndigest --help' for more information.\n"; 
		throw std::runtime_error(ss.str());
	}
	try {
		if (option.anchor_format().present()) 
			config.image_anchor_format(option.anchor_format().value());
	}
	catch (std::runtime_error& e) {
		std::stringstream ss;
		ss << "invalid argument `" 
			 << option.anchor_format().value() << "' for `--" 
			 << option.anchor_format().long_name() << "'\n"
			 << e.what() << "\n"
			 << "Try `svndigest --help' for more information.\n"; 
		throw std::runtime_error(ss.str());
	}
}


void create_file_struct(std::string stats_type, 
												const theplu::svndigest::Stats& stats)
{
	using namespace theplu::svndigest;
	mkdir(stats_type);
	touch(stats_type+std::string("index.html"));
	mkdir(stats_type+std::string("all"));
	mkdir(stats_type+std::string("images"));
	touch(stats_type+std::string("all/index.html"));
	touch(stats_type+std::string("images/index.html"));
	for (std::set<std::string>::const_iterator i = stats.authors().begin();
			 i!=stats.authors().end(); ++i) {
		mkdir(stats_type+*i);
		touch(stats_type+*i+std::string("/index.html"));
	}
}

void remove_target(const std::string& target_path, bool verbose)
{
	if (verbose)
		std::cout << "Removing old target tree: " << target_path << "\n";
	rmdirhier(target_path);
	// exit if remove failed
	if (node_exist(target_path))
		throw std::runtime_error("remove failed");
}


void set_dates(const std::string& repo, bool verbose)
{
	if (verbose)
		std::cout << "retrieving dates" << std::endl;
	SVNlog log(repo);
	std::vector<std::string> dates;
	dates.reserve(log.commits().size());
	for (SVNlog::container::const_iterator iter=log.commits().begin();
			 iter!=log.commits().end(); ++iter) {
		assert(static_cast<svn_revnum_t>(dates.size())==iter->revision());
		dates.push_back(iter->date());
	}
	// all plots uses the same dates
	Graph::set_dates(dates);
}

void write_report(const svndigestParameter& option, const std::string& repo,
									const Node& tree, const StatsCollection& stats) 
{
	std::string target_path = concatenate_path(option.targetdir(), 
																						 file_name(option.root()));
	// remove target if needed
	if (node_exist(target_path)) {
		assert(option.force());
		remove_target(target_path, option.verbose());
	}
	
	if (!option.revisions())
		set_dates(repo, option.verbose());

	chdir(option.targetdir());

	if (option.verbose())
		std::cout << "Generating output" << std::endl;
	mkdir(tree.name());
	chdir(tree.name().c_str());
	print_css("svndigest.css");
	print_main_page(tree.name(), tree.log(), stats, tree.url());
	// create structure StatsType/Author/LineType
	for (std::map<std::string, Stats*>::const_iterator i(stats.stats().begin());
			 i!=stats.stats().end(); ++i)
		create_file_struct(i->first+std::string("/"), *i->second);
	tree.print(option.verbose());
}
