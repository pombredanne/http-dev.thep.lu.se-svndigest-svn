#!/bin/sh

# $Id: cmd_format_test.sh 1092 2010-06-12 23:50:18Z peter $
#
# Copyright (C) 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. ./init.sh || exit 1
set -e

SVNDIGEST_run 1 -f --format=banana
test -s stderr || exit_fail
grep 'invalid argument.*banana.*format' stderr || exit_fail

SVNDIGEST_run 0 -g --format=svg --anchor-format=svg
grep 'format = svg' stdout || exit_fail
grep 'anchor_format = svg' stdout || exit_fail

exit_success;
