AS_INIT[]
m4_divert_push([HEADER-COPYRIGHT])dnl
# @configure_input@
# $Id: svndigest-copy-cache.as 957 2009-12-06 15:58:28Z peter $

# Copyright (C) 2009 Peter Johansson

# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

m4_divert_pop([HEADER-COPYRIGHT])dnl back to BODY

dnl FIXME remove ifdef when we assume autoconf 2.64
m4_ifdef([AS_ME_PREPARE], [AS_ME_PREPARE], [as_me=svndigest-copy-cache])

usage=["Usage: $as_me [OPTION]...

Copy svndigest cache from one working copy to another.

Mandatory arguments to long options are mandatory for short options too.
  -h, --help            print this help and exit
  -r  --root=ROOT       svn wc cache is copied from
  -t, --target=TARGET   svn wc cache is copied to
  -v, --verbose         explain what is being done
      --version         print version and exit

Report bugs to @PACKAGE_BUGREPORT@
@PACKAGE_NAME@ home page: <@PACKAGE_URL@>."]

version=["$as_me (@PACKAGE_NAME@) @VERSION@

Copyright (C) @RELEASE_YEAR@ Peter Johansson
This is free software. You may redistribute copies of it under the terms of    
the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.         
There is NO WARRANTY; to the extent permitted by law."]

while test $[#] != 0; do
  optarg=[`expr "x$1" : 'x--[^=]*=\(.*\)' \| "x$1" : 'x-.\(.*\)'`]
  AS_CASE([$1],
          [--help | -h], [AS_ECHO(["$usage"]); AS_EXIT([0])],
          [--version], [AS_ECHO(["$version"]); AS_EXIT([0])],
          [--root | -r], [
            AS_IF([test $# = 1],  
                  [AS_ERROR([option `--root' needs an argument], [1])])
            # restore font-lock `
            rootdir=$2
            shift
          ],
          [--root=*], [
            rootdir=$optarg 
            shift
          ],
          [-t | --target], [
            AS_IF([test $# = 1], 
                  [AS_ERROR([option `--target' needs an argument], [1])])
            # restore font-lock `
            targetdir=$2
            shift
          ],
          [--target=*], [targetdir=$optarg; shift],
          [--verbose | -v], [cp_options=-v;],
          [AS_ERROR([unknown option \`$1' ${as_nl}Try \`$as_me --help'],[1])
          ]
         )
  shift
done

# set default values
AS_VAR_SET_IF([targetdir],[],[targetdir=.])
AS_VAR_SET_IF([rootdir],[],[rootdir=.])

# some simple sanity checking
AS_IF([test -d "$rootdir"],[],
 [AS_ERROR([\`$rootdir' is not a directory], [1])])
# restore font-lock '
    
AS_IF([test -d "$targetdir"],[],
 [AS_ERROR([\`$targetdir' is not a directory], [1])])
# restore font-lock '


AS_IF([test "$rootdir" = "$targetdir"],
 [AS_ERROR([\`$rootdir' and \`$targetdir' are the same directory], [1])])


# Do the actual copying
files=`cd $rootdir && find . -name "*.svndigest-cache"`
for file in $files; do
  mother=`AS_DIRNAME([$file])`
  mother_base=`AS_BASENAME([$mother])`
  # ignore files whose mother is not named .svndigest', which implies
  # we also ignore '$rootdir/foo.svndigest-cache' when $rootdir is
  # '.svndigest' because mother will be euqal to '.' in this
  # case. This is the desired behavior because file 'foo' (which
  # 'foo.svndigest-cache' reflects) resides in '$rootdir/..' which
  # is outside the tree we are expected to copy.
  AS_VAR_IF([mother_base], [.svndigest], [
    grandma=`AS_DIRNAME([$mother])`
    # we only copy files if target's grandma exists
    AS_IF([test -d "$targetdir/$grandma"], [
      AS_MKDIR_P(["$targetdir/$mother"])
      cp $cp_options "$rootdir/$file" "$targetdir/$file" || AS_EXIT([1])
    ], [
      AS_ERROR([$targetdir/$grandma: No such directory], [1])
    ])
  ])
done
