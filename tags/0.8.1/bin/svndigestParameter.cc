// $Id: svndigestParameter.cc 1246 2010-10-30 02:41:51Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "svndigestParameter.h"

#include "lib/OptionVersion.h"
#include "lib/utility.h"

#ifdef HAVE_PLPLOT
#include <plplot/plstream.h>
#endif

#include "yat/ColumnStream.h"
#include "yat/Exception.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	svndigestParameter::svndigestParameter(void)
		: Parameter(),
			anchor_format_(cmd_,"anchor-format", 
										 "override anchor image format defined in config file"),
			copyright_(cmd_, "copyright", "update copyright statement [deprecated]"),
			force_(cmd_, "f,force", 
						 std::string("if sub-directory named <ROOT> exists in target dire")+
						 std::string("ctory, remove sub-directory before writing results")),
			format_(cmd_,"format", "override image format defined in config file"),
			report_(cmd_, "report", "create no HTML report", true),
			revisions_(cmd_, "revisions", 
								 "use revision number as time scale instead of dates [dates]"),
			target_(cmd_, "t,target", "output directory [.]")
	{
	}


	svndigestParameter::~svndigestParameter(void)
	{
	}


	const yat::utility::OptionArg<std::string>& 
	svndigestParameter::anchor_format(void) const
	{
		return anchor_format_;
	}


	void svndigestParameter::analyse2(void)
	{
		std::string save_wd = pwd();

		// check root but not if -g option given
		if (!generate_config()) {
			// check target (only if we write report)
			if (report()) {
				check_existence(target_.value());
				check_readable(target_.value());
				std::string path = concatenate_path(target_.value(),root_basename()); 
				if (access_rights(target_.value().c_str(), "w")) {
					std::stringstream ss;
					ss << "svndigest: cannot create directory `" << path 
						 << "': " << strerror(errno);
					throw yat::utility::cmd_error(ss.str());
				}
				if (node_exist(path) && !force()) {
					std::stringstream ss;
					ss << "svndigest: cannot create directory `" << path << "' "
						 << strerror(EEXIST);
					throw yat::utility::cmd_error(ss.str());
				}
				chdir(target_.value());
				target_.value(pwd());
				chdir(save_wd);
			}
			if (copyright()) {
				std::cerr << "svndigest: WARNING: option `--copyright' is deprecated; "
									<< "use svncopyright instaed.\n";
			}
		}
	}


	bool svndigestParameter::copyright(void) const
	{
		return copyright_.present();
	}


	bool svndigestParameter::force(void) const
	{
		return force_.present();
	}


	const yat::utility::OptionArg<std::string>& 
	svndigestParameter::format(void) const
	{
		return format_;
	}


	void svndigestParameter::init2(void)
	{
		anchor_format_.print_arg("=FMT");
		format_.print_arg("=FMT");
		target_.print_arg("=TARGET");
		help_.synopsis() = 
			"Generate statistical report for a subversion repository\n";

		std::stringstream ss;
#ifdef HAVE_PLPLOT
		ss << "using libplplot ";
		plstream pls;
		char* plplot_version = new char[32];
		pls.gver(plplot_version);
		ss << plplot_version << "\n";
		delete [] plplot_version;
#endif
		version_.extra(ss.str());
		version_.program_name("svndigest");
	}


	bool svndigestParameter::report(void) const
	{
		return report_.value();
	}


	bool svndigestParameter::revisions(void) const
	{
		return revisions_.present();
	}


	void svndigestParameter::set_default2(void)
	{
		if (!target_.present())
			target_.value(".");
	}


	std::string svndigestParameter::targetdir(void) const
	{
		return target_.value();
	}

}} // of namespace svndigest and namespace theplu
