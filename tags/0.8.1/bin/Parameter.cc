// $Id: Parameter.cc 1246 2010-10-30 02:41:51Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "Parameter.h"

#include "lib/OptionVersion.h"
#include "lib/utility.h" // to avoid inclusion of yat file

#include "yat/ColumnStream.h"
#include "yat/Exception.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	Parameter::Parameter(void)
		: cmd_( std::string("Mandatory arguments to long options ") + 
						std::string("are mandatory for short options too.")), 
			config_file_(cmd_, "config-file", 
									 "configuration file [<ROOT>/.svndigest/config]"),
			generate_config_(cmd_, "g,generate-config", 
											 "write configuration to standard output"),
			help_(cmd_),
			ignore_cache_(cmd_, "ignore-cache", 
										std::string("ignore cache files and analyze ") +
										std::string("everything from repository")),
			root_(cmd_, "r,root", 
						"svn controlled directory to perform statistics on [.]"),
			verbose_(cmd_, "v,verbose", "explain what is being done"),
			version_(cmd_, "version", "print version information and exit", &verbose_)
	{
	}

	void Parameter::parse(int argc, char* argv[])
	{
		init();
		try {
			cmd_.parse(argc, argv);
		}
		catch (yat::utility::cmd_error& e) {
			std::cerr << e.what() << std::endl;
			exit (-1);
		}

		set_default();

		// analyse arguments
		analyse();
	}


	Parameter::~Parameter(void)
	{
	}


	void Parameter::analyse(void)
	{
		std::string save_wd = pwd();

		// check root but not if -g option given
		if (!generate_config()) {
			check_existence(root_.value());
			check_readable(root_.value());

			// check that root is directory (or link pointing to directory)
			struct stat stats;
			stat(root_.value().c_str(), &stats);
			if (!S_ISDIR(stats.st_mode)) {
				std::stringstream ss;
				ss << cmd_.program_name() << ": '" << root_.value() 
					 << "': Not a directory"; 
				throw yat::utility::cmd_error(ss.str());
			}
			chdir(root_.value());
			root_full_ = pwd();
			chdir(save_wd);

			// take care of when root is a symlink (see ticket #477)
			lstat(root_.value().c_str(), &stats);
			if (S_ISLNK(stats.st_mode))
				root_basename_ = file_name(root_.value());
			else
				root_basename_ = file_name(root_full_);
		}

		// check config file
		struct stat nodestat;
		// true also if there is a broken symlink named...
		bool config_exists = !lstat(config_file_.value().c_str(), &nodestat);
		// the latter case in order to catch broken symlink
		if (config_file_.present() || config_exists)
			// throws if file does not exists
			check_existence(config_file_.value());
		if (config_exists) {
			// throws if file is not readable
			check_readable(config_file_.value());
			stat(config_file_.value().c_str(), &nodestat);
			if (S_ISDIR(nodestat.st_mode)) {
				std::stringstream ss;
				ss << cmd_.program_name() << ": `" << config_file_.value() 
					 << "' is a directory";
				throw yat::utility::cmd_error(ss.str());
			}
		}
		analyse2();
	}


	void Parameter::check_existence(std::string path) const
	{
		if (node_exist(path))
			return;
		std::stringstream ss;
		ss << cmd_.program_name() << ": cannot stat `" << path << "': " 
			 << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}

	
	void Parameter::check_readable(std::string path) const
	{
		if (!access_rights(path, "r"))
			return;
		std::stringstream ss;
		ss << cmd_.program_name() << ": cannot open `" << path << "': " 
			 << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}
	

	std::string Parameter::config_file(void) const
	{
		return config_file_.value();
	}


	bool Parameter::generate_config(void) const
	{
		return generate_config_.present();
	}


	bool Parameter::ignore_cache(void) const
	{
		return ignore_cache_.present();
	}


	void Parameter::init(void)
	{
		// we like the options sorted alphabetically
		cmd_.sort();
		config_file_.print_arg("=FILE");
		root_.print_arg("=ROOT");
		std::stringstream ss;
		ss << "Report bugs to " << PACKAGE_BUGREPORT << ".\n"
			 << PACKAGE << " home page: <" << PACKAGE_URL << ">.\n";
		help_.post_arguments() = ss.str();
		init2();
	}


	std::string Parameter::root(void) const
	{
		return root_full_;
	}


	const std::string& Parameter::root_basename(void) const
	{
		assert(root_basename_.size());
		return root_basename_;
	}


	void Parameter::set_default(void)
	{
		if (!root_.present())
			root_.value(".");

		if (!config_file_.present())
			config_file_.value(concatenate_path(root_.value(),".svndigest/config"));

		set_default2();
	}


	bool Parameter::verbose(void) const
	{
		return verbose_.present();
	}


}} // of namespace svndigest and namespace theplu
