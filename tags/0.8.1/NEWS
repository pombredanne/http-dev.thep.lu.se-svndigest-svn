$Id: NEWS 1262 2010-11-02 00:12:08Z peter $

See the end of this file for copyrights and conditions.

svndigest 0.8.x series from
	  http://dev.thep.lu.se/svndigest/svn/branches/0.8-stable

Version 0.8.1 (released 2 November 2010)
  - fix bug when ROOT is symlink (ticket #477)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.8.1

Version 0.8 (released 25 August 2010)
  - cache file now contains config information (ticket #433)
  - Gnuplot replaced with plplot (ticket:97)
  - option --copyright deprecated; use new program snvcopyright (ticket #307)
  - new program svndigest-copy-cache (ticket #380)
  - new option --format (ticket:438)
  - new option --anchor-format (ticket:279)
  - new section, `[image]`, in config file (ticket:279 and ticket:405)
  - new configure option --with-plplot
  - new default codon for `*.as` in config file
  - new default codon for `*.R` in config file (ticket:432)
  - image format is configurable, refer to Manual (ticket:405)
  - color of different author is configurable (ticket #75 #413)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.8

svndigest 0.7.x series from
	  http://dev.thep.lu.se/svndigest/svn/branches/0.7-stable

Version 0.7.5 (released 3 July 2010)
  - now handles the case when parts of repository is not readable (bug #458)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7.5

Version 0.7.4 (released 8 June 2010)
  - fixed memory bug in StatsCollection (r1075)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7.4

Version 0.7.3 (released 15 February 2010)
  - Fixed bug that authors were excluded in BlameStats plot (bug #441)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7.3

Version 0.7.2 (released 2 January 2010)
  - Author included in plot unless count is 0 for all revisions (bug #434)
  - Default codons in config file is now corrected. Old cache files
    are obsolete and are ignored by svndigest 0.7.2 (bug #431)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7.2

Version 0.7.1 (released 3 December 2009)
  - File name in parsing rules are matched against the file name and
    not the entire path. This has no influence on most files because
    the name starts with a wild card (*) but for files not doing so
    (default bootstrap and Makefile) the correct parsing rule will be
    used. It is recommended to remove the cache of such files before
    running svndigest (ticket:417).
  - copyright update works on mixed revision wc (ticket:415)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7.1

Version 0.7 (released 22 November 2009)
  - Upgraded to GLPv3 (ticket:339)
  - configure option --enable-staticbin is removed (ticket:340)
  - configure option --enable-wctests is removed (ticket:295)
  - configure option --enable-svn-support is removed
  - parsing rules can be set in config file (ticket:283)
  - Statistics is now cached (ticket:5)
  - More statistics: StatsType Blame (ticket:24) and StatsType Add (ticket:82)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.7

version 0.6.8 (released 12 November 2009)
  - configure now handles --disable correctly (issue #383) 

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.6.8

version 0.6.7 (released 1 February 2009)
  - Improved TracLinks (issue #353)
  - Daylight saving time is now handled correctly (issue #356)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.6.7

version 0.6.6 (released 4 August 2008)
  - Improved removal of sub-directory in --target dir (ticket:242 ticket:243)
  - References were changed to http://dev.thep.lu.se/svndigest

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.6.6

version 0.6.5 (released 18 March 2008)
  - Fixed color bug in blame output (ticket:278)
  - Improved error handling when --root parameter is incorrect.

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=svndigest+0.6.5

version 0.6.4 (released 14 October 2007)
  - fixed bug stats were double counted when root was a file (ticket:272)
  - fixed out of range problem in VectorPlus (ticket:267)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.6.4

version 0.6.3 (released 31 August 2007)
  - Copyright update is no longer adding newline to file (ticket:264)
  - Commandline parsing now accepts '=' syntax (ticket:259)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.6.3

Version 0.6.2 (released 21 August 2007)
  - Fixed issue with moving files across different file systems (ticket:251)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.6.2

Version 0.6.1 (released 9 July 2007)
  - changed refs to svndigest site to http://dev.thep.lu.se/svndigest 
  - fixed lineover bug in blame output (ticket:235)

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.6.1

Version 0.6 (released 29 June 2007)
  - svn blame output
  - reports for individual authors
  - differentiating several line types
  - support for TracLinks
  - config file, see options `--config-file` and `--generate-config`
  - support for automatic update of copyright statement
  - Removed option flag '-rev'
  - More file extensions supported for parsing.

  A complete list of closed tickets can be found here [[br]]
  http://dev.thep.lu.se/svndigest/query?status=closed&milestone=0.6

Version 0.5 (released 7 September 2006)
  - Added support for svndigest:ignore property.
  - Creation of static svndigest binary is now supported, use
    --enable-staticbin.
  - Dates and time now refers to UTC.
  - Statistics now differentiates between code, comments, and empty
    lines. Improvements to this parsing is still needed.

Version 0.4 (released 12 August 2006)
  - Project name changed from svnstat to svndigest.

Version 0.3 (released 10 August 2006)
  - Improved the generated output.
  - svnstat handles unexpected situations more gracefully (like
    running svnstat on a tree that is not under subversion control,
    trees with items that are not up to date).
  - Proper version information is displayed using --version option.

Version 0.2 (released 12 March 2006)
  - Improved web page presentation.
  - Added option to plot changes against time or revision.

Version 0.1 (released 4 March 2006)
  - First release.

-------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari Häkkinen
Copyright (C) 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
Copyright (C) 2010 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
USA.
}}}
----------------------------------------------------------------------

