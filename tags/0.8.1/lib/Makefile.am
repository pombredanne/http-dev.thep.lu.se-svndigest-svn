## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 1159 2010-08-10 13:20:04Z peter $

# Copyright (C) 2005 Jari Häkkinen
# Copyright (C) 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
# Copyright (C) 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

MOVE_IF_CHANGE = $(SHELL) $(top_srcdir)/build_support/move-if-change
SVN_REVISION = `$(SVNVERSION) $(top_srcdir)`

AM_CPPFLAGS = -I$(top_srcdir) $(SVNDIGEST_CPPFLAGS)
AM_CXXFLAGS = $(SVNDIGEST_CXXFLAGS)

noinst_LIBRARIES = libsvndigest.a

noinst_HEADERS = AddStats.h Alias.h BlameStats.h ClassicStats.h \
	Colors.h Commitment.h Configuration.h css.h \
	Date.h Directory.h File.h first_page.h Functor.h \
	Graph.h \
	HtmlBuf.h HtmlStream.h html_utility.h LineTypeParser.h \
	main_utility.h Node.h \
	OptionVersion.h rmdirhier.h \
	Stats.h StatsCollection.h subversion_info.h SVN.h SVNblame.h	\
	SVNinfo.h SVNlog.h SVNproperty.h Trac.h utility.h

libsvndigest_a_SOURCES = AddStats.cc Alias.cc BlameStats.cc \
	ClassicStats.cc Colors.cc \
	Commitment.cc Configuration.cc \
	css.cc Date.cc Directory.cc File.cc first_page.cc\
	Functor.cc Graph.cc HtmlBuf.cc HtmlStream.cc \
	html_utility.cc LineTypeParser.cc main_utility.cc Node.cc \
	OptionVersion.cc \
	rmdirhier.cc Stats.cc StatsCollection.cc subversion_info.cc SVN.cc \
	SVNblame.cc SVNinfo.cc SVNlog.cc SVNproperty.cc Trac.cc utility.cc




clean-local: 
	rm -rf *~

all-local:

if HAVE_SVN_WC
$(srcdir)/subversion_info.cc: subversion_info.cc.tmp
	@$(MOVE_IF_CHANGE) subversion_info.cc.tmp $@

subversion_info.cc.tmp: FORCE
	@echo '// subversion_info.cc generated from subversion_info.cc.in.' > $@ ;\
	revision=$(SVN_REVISION);\
	$(SED) -e 's/sub_2_svn_revision/r'$$revision'/g' \
	$(srcdir)/subversion_info.cc.in >> $@ ;

else
# this is needed in 'svn export' build
$(srcdir)/subversion_info.cc:
	$(SED) -e 's/sub_2_svn_revision//g' \
	$(srcdir)/subversion_info.cc.in >> $@ ;

endif

FORCE:
