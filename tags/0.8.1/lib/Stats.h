#ifndef _theplu_svndigest_stats_
#define _theplu_svndigest_stats_

// $Id: Stats.h 1124 2010-07-07 05:35:17Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineTypeParser.h"

#include <subversion-1/svn_types.h>

#include <map>
#include <istream>
#include <set>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

  ///
  /// Class taking care of statistics from svn.
  ///
  class Stats
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		explicit Stats(const std::string& path);

		/**
			 \brief Destructor
		*/
		virtual ~Stats(void);

    ///
    /// @brief adding \a n line(s) to \a user from \a revision to the stats
    ///
    void add(const std::string& user, const unsigned int& revision,
						 const LineTypeParser::line_type&, unsigned int n=1); 

		///
		/// @return set of authors
		///
		const std::set<std::string>& authors(void) const;

		///
		/// \return number of code lines for \a user for latest revision. 
		///
    unsigned int code(const std::string& user="all") const; 


		///
		/// \return number of comments lines for \a user for latest revision. 
		///
    unsigned int comments(const std::string& user="all") const; 

		///
		/// \return number of empty lines for \a user for latest revision. 
		///
    unsigned int empty(const std::string& user="all") const; 

		///
		/// \return revison node was modified
		///
		svn_revnum_t last_changed_rev(void) const;

		///
		/// \return number of lines for \a user for latest revision
		///
    unsigned int lines(const std::string& user="all") const; 

		/**
			 Load object from a stream.

			 \a latest_ver is set to true if cache_file is latest version;
			 otherwise it is set to false.

			 \return revision the cache represents - 0 if failed
		 */
		svn_revnum_t load_cache(std::istream&, bool& latest_ver);

		/**
			 Do the parsing for \a path. Revisions from \a rev will be parsed.
		*/
		void parse(const std::string& path, svn_revnum_t rev=0);

		///
		/// Create statistics graph.
		///
		std::string plot(const std::string&, const std::string&) const;

		///
		/// Plotting code, comment, other, and total in same plot (for
		/// 'all' not individual authors).
		///
		void plot_summary(const std::string& output) const;

		/**
			 Send Stats to a stream.
		 */
		void print(std::ostream&) const;

		///
		/// @brief Clear all statistics
		///
    void reset(void); 

		///
		/// \return latest revision for whole project
		///
		svn_revnum_t revision(void) const { return revision_; }

		/**
			 \return resulting Stats
		*/
    Stats& operator+=(const Stats&);

		/**
			 \return number of lines for \a author and \a linetype from
			 revision \a rev.

			 \throw if \a author does not exist
		 */
		size_t operator()(int linetype, std::string author, svn_revnum_t rev) const;

	protected:
		typedef std::map<std::string, std::vector<unsigned int> > Author2Vector;
		typedef Author2Vector::iterator A2VIter;
		typedef Author2Vector::const_iterator A2VConstIter;

		/**
			 Calculate accumalated statistics for fundamental statistics,
			 i.e., code, comment, empty, and copyright for each author.
			 \see accumulate
		 */
		void accumulate_stats(svn_revnum_t rev=1);
		void add_author(std::string);
		void add_authors(std::set<std::string>::const_iterator, 
										 std::set<std::string>::const_iterator);

		// references to data
		inline Author2Vector& code_stats(void) 
		{ return stats_[LineTypeParser::code]; }
		inline Author2Vector& comment_stats(void) 
		{ return stats_[LineTypeParser::comment]; }
		inline Author2Vector& copyright_stats(void) 
		{ return stats_[LineTypeParser::copyright]; }
		inline Author2Vector& other_stats(void) 
		{ return stats_[LineTypeParser::other]; }
		inline Author2Vector& comment_or_copy_stats(void) 
		{ return stats_[LineTypeParser::comment_or_copy]; }
		inline Author2Vector& total_stats(void) 
		{ return stats_[LineTypeParser::total]; }

		// const references to data
		inline const Author2Vector& code_stats(void) const
		{ return stats_[LineTypeParser::code]; }
		inline const Author2Vector& comment_stats(void) const
		{ return stats_[LineTypeParser::comment]; }
		inline const Author2Vector& copyright_stats(void) const
		{ return stats_[LineTypeParser::copyright]; }
		inline const Author2Vector& other_stats(void) const
		{ return stats_[LineTypeParser::other]; }
		inline const Author2Vector& comment_or_copy_stats(void) const
		{ return stats_[LineTypeParser::comment_or_copy]; }
		inline const Author2Vector& total_stats(void) const
		{ return stats_[LineTypeParser::total]; }

		/**
			 add range [\a first, \a last) to \a map
		 */
		void map_add(Author2Vector::const_iterator first, 
								 Author2Vector::const_iterator last,
								 Author2Vector& map);

		std::set<std::string> authors_;

		const std::vector<unsigned int>& get_vector(const Author2Vector&, 
																				 std::string user) const;
	private:
		/// one liner used in cache file to validate that cache file was
		/// created current configuration
		std::string config_code_;

		/**
			 \a vec is resized to revision().
			 vec is accumulated such that
			 vec[rev] = vec[rev-1] + vec[rev]
			 vec[rev+1] = vec[rev] + vec[rev+1]
			 et cetera
		 */
		void accumulate(std::vector<unsigned int>& vec,
										svn_revnum_t rev=1) const;
		void add(std::vector<unsigned int>& vec, unsigned int rev, bool x,
						 unsigned int n);

		/**
			 Parse statistics for fundamental categories, i.e., code,
			 comment, empty, and copyright for each author. Ignore revisions
			 earlier than \a first_rev.
		 */
		virtual void do_parse(const std::string&, svn_revnum_t first_rev)=0;
		
		/// load cache file version 7
		svn_revnum_t load_cache7(std::istream&);
		/// load cache file version 8
		svn_revnum_t load_cache8(std::istream&);

		/**
			 called from plot(2)
		 */
		void plot(const std::string& basename, const std::string& linetype,
							const std::string& format) const;

		/**
			 called from plot_summary(1)
		 */
		void plot_summary(const std::string& basename, 
											const std::string& format) const;

		// Change this string if cache format is changed in such a way
		// that all old cache files are obsolete.
		inline std::string cache_check_str(void) const 
		{return "CACHE FILE VERSION 7";} 

		void calc_all(void);
		void calc_comment_or_copy(void);
		void calc_total(void);
		unsigned int get_back(const Author2Vector&, std::string user) const;
		void load(std::istream& is, Author2Vector& m);
		/**
			 Finds the largets element by iterating through the entire
			 vector. Inherited classes should implement their own version
			 when it is possible to get the largest element in faster than
			 in linear time.

			 \return the largest largest element in \a v. 
		*/
		virtual unsigned int max_element(const std::vector<unsigned int>& v) const; 

		void print(std::ostream& os, const Author2Vector& m) const;

		svn_revnum_t revision_; // Should be the latest revision for whole project
		svn_revnum_t last_changed_rev_; // Should be the latest revision for file

		std::vector<Author2Vector> stats_; // from linetype to a2v

		// using compiler generated copy constructor
		//Stats(const Stats&);
		// no assignment
		Stats& operator=(const Stats&);

  };
}} // end of namespace svndigest end of namespace theplu

#endif 
