#ifndef _theplu_svndigest_svnproperty_
#define _theplu_svndigest_svnproperty_

// $Id: SVNproperty.h 978 2009-12-12 20:09:41Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <string>

namespace theplu {
namespace svndigest {

	class SVN;

	///
	/// The SVNproperty class is a utility class for taking care of 'svn
	/// property' information. An 'svn property' is non-recursively
	/// performed on an item.
	///
	class SVNproperty {
	public:

		///
		/// @brief The contructor.
		///
		/// The constructor performs an 'svn property' on \a path.
		///
		explicit SVNproperty(const std::string& path);

		/**
			 @brief Check whether item used to create this object is binary.

			 @return True if item is binary.
		*/
		inline bool binary(void) const { return binary_; }

		/**
			 @brief Check if item used to create this object has been
			 assigned property svndigest:ignore.

			 Currently files with property svndigest:ignore are to be
			 ignored by svndigest. It is the responsibility of the
			 statistics implementer to obey the ignore state.

			 @return True if item property svndigest:digest was set.
		*/
		inline bool svndigest_ignore(void) const { return svndigest_ignore_; }

		/**
			 @brief Get the list of properties for item used to creat this
			 SVNproperty object.
		*/
		inline const std::map<std::string, std::string>&
		properties(void) const { return property_; }

	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNproperty(const SVNproperty&);

		bool binary_;
		std::map<std::string,std::string> property_;
		bool svndigest_ignore_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
