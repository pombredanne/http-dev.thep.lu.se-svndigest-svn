#ifndef _theplu_svndigest_rmdirhier_
#define _theplu_svndigest_rmdirhier_

// $Id: rmdirhier.h 489 2007-10-13 23:16:02Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <stdexcept>
#include <string>

namespace theplu {
namespace svndigest {

	struct DirectoryError : public std::runtime_error
	{ inline DirectoryError(const std::string& s) : runtime_error(s) {} };

	struct BadDirectory : public DirectoryError
	{ inline BadDirectory(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryOpenError : public DirectoryError
	{ inline DirectoryOpenError(const std::string& s) : DirectoryError(s) {} };

	struct FileDeleteError : public DirectoryError
	{ FileDeleteError(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryDeleteError : public DirectoryError
	{ DirectoryDeleteError(const std::string& s) : DirectoryError(s) {} };

	void rmdirhier(const std::string& path);

}} // of namespace svndigest and namespace theplu

#endif
