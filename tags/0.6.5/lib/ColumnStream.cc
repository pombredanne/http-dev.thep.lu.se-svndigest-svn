// $Id: ColumnStream.cc 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "ColumnStream.h"

#include <cassert>
#include <iostream>

namespace theplu{
namespace svndigest{

	ColumnStream::ColumnStream(std::ostream& os, size_t columns)
		: activated_(0),os_(os)
	{
		margins_=std::vector<size_t>(columns); 
		buffer_.reserve(columns);
		while (buffer_.size()<columns)
			buffer_.push_back(new std::stringstream);
		width_=std::vector<size_t>(columns, 8); 
	}


	ColumnStream::~ColumnStream(void)
	{
		for (size_t i=0; i<buffer_.size(); ++i)
			delete buffer_[i];
	}


	void ColumnStream::fill(size_t column, size_t count)
	{
		while(count<width_[column]){
			os_ << ' ';
			++count;
		}
	}


	void ColumnStream::flush(void)
	{
		bool empty=false;
		while(!empty) {
			empty=true;
			for (size_t i=0; i<columns(); ++i){
				if (writeline(i))
					empty=false;
			}
			os_ << '\n';
		}
		for (size_t i=0; i<columns(); ++i)
			buffer_[i]->clear(std::ios::goodbit);
	}

	void ColumnStream::next_column(void)
	{
		++activated_;
		if (activated_>=columns()) {
			flush();
		}
	}

	void ColumnStream::print(std::stringstream& ss)
	{
		assert(buffer_[activated_]);
		assert(activated_<buffer_.size());
		char c;
		ss.get(c);
		while(ss.good()) {
			if (c=='\t'){
				//*(buffer_[activated_]) << ' ';
				next_column();
			}
			else if (c=='\n'){
				//*(buffer_[activated_]) << ' ';
				flush();
				activated_=0;
			}
			else 
				*(buffer_[activated_]) << c;
		ss.get(c);
		}
	}


	bool ColumnStream::writeline(size_t column)
	{
		assert(column<columns());
		for (size_t i=0; i<margins_[column]; ++i)
			os_ << ' ';
		size_t count=0;
		std::string word;
		char c;
		while (buffer_[column]->good()) {
			buffer_[column]->get(c);
			assert(c!='\t');
			assert(c!='\n');
			if (buffer_[column]->good())
				word += c;
			if (c==' ' || !buffer_[column]->good()) {
				if (count+word.size()<=width_[column]) {
					os_ << word;
					count += word.size();
					word = "";
				}				
				else {
					if (!buffer_[column]->good())
						buffer_[column]->clear(std::ios::goodbit);
					
					// if line is empty and word is longer than column width, we
					// have to split the word
					if (!count) {
						os_ << word.substr(0,width_[column]);
						for (std::string::reverse_iterator i=word.rbegin();
								 i!=word.rbegin()+(word.size()-width_[column]); ++i)
							buffer_[column]->putback(*i);
					}
					else {
						for (std::string::reverse_iterator i=word.rbegin();
								 i!=word.rend(); ++i)
							buffer_[column]->putback(*i);
						fill(column, count);
					}
					return true;
				}
			}

		} 
		fill(column, count);
		return false;
	}


}} // end of namespace svndigest and namespace theplu
