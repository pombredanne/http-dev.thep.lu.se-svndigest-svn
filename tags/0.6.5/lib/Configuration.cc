// $Id: Configuration.cc 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Configuration.h"

#include "utility.h"

#include <cassert>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <stdexcept>
#include <utility>

namespace theplu{
namespace svndigest{

	Configuration* Configuration::instance_=NULL;


	Configuration::Configuration(void)
	{
	}


	const std::map<std::string,Alias>& Configuration::copyright_alias(void) const
	{
		return copyright_alias_;
	}


	void Configuration::load(void)
	{
		set_default();
	}


	void Configuration::load(std::istream& is)
	{
		assert(is.good());
		set_default();

		std::string line;
		std::string section;
		std::string tmp;
		while (getline(is, line)) {
			line = ltrim(line);
			if (line.empty() || line[0]=='#')
				continue;
			std::stringstream ss(line);
			if (line[0] == '[') {
				getline(ss, tmp, '[');
				getline(ss, section, ']');
			}
			else if (section == "copyright-alias"){
				getline(ss, tmp, '=');
				std::string key = trim(tmp);
				getline(ss, tmp);
				std::string name = trim(tmp);
				std::map<std::string,Alias>::iterator iter = 
					copyright_alias_.lower_bound(key);
				if (iter!=copyright_alias_.end() && iter->first==key){
					std::stringstream mess;
					mess << "svndigest: invalid config file: "
							 << "in copright-alias section " << key + " defined twice.";
					throw std::runtime_error(mess.str());
				}
				
				// insert alias
				copyright_alias_.insert(iter,std::make_pair(key, Alias(name,copyright_alias_.size())));
			}
			else if (section == "trac"){
				getline(ss, tmp, '=');
				std::string key = trim(tmp);
				getline(ss, tmp);
				std::string value = trim(tmp);
				if (key=="trac-root")
					trac_root_=value;
				else {
					std::stringstream mess;
					mess << "svndigest: invalid config file: "
							 << "in trac section" << key + " is invalid option.";
					throw std::runtime_error(mess.str());
				}
			}
		}
	}


	Configuration& Configuration::instance(void)
	{
		if (!instance_)
			instance_ = new Configuration;
		return *instance_;
	}


	void Configuration::set_default(void)
	{
		copyright_alias_.clear();
		trac_root_ = "";
	}


	std::string Configuration::trac_root(void) const
	{
		return trac_root_;
	}


	std::ostream& operator<<(std::ostream& os, const Configuration& conf)
	{
		os << "### This file configures various behaviors for svndigest\n"
			 << "### The commented-out below are intended to demonstrate how to use\n" 
			 << "### this file.\n"
			 << "\n"
			 << "### Section for setting aliases used in copyright update\n"
			 << "[copyright-alias]\n"
			 << "# jdoe = John Doe\n";

		typedef std::vector<std::pair<std::string, Alias> > vector;
		vector vec;
		std::back_insert_iterator<vector> back_insert_iterator(vec);
		vec.reserve(conf.copyright_alias().size());
		std::copy(conf.copyright_alias().begin(), conf.copyright_alias().end(),
							back_insert_iterator);
		// sort with respect to Alias.id
		IdCompare id;
		PairSecondCompare<const std::string, Alias, IdCompare> comp(id);
		std::sort(vec.begin(),vec.end(), comp);
							

		for (vector::const_iterator i(vec.begin()); i!=vec.end(); ++i) {
			os << i->first << " = " << i->second.name() << " \n";
		}

		os << "\n"
			 << "### Section for setting trac environment\n"
			 << "[trac]\n"
			 << "# If trac-root is set, svndigest will create anchors to "
			 << "the Trac page.\n"
			 << "# trac-root = http://trac.thep.lu.se/trac/svndigest/\n";
		if (!conf.trac_root().empty())
			os << "trac-root = " << conf.trac_root() << "\n";

		return os;
	}


}} // end of namespace svndigest and namespace theplu
