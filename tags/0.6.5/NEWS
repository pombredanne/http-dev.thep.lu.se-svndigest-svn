$Id: NEWS 575 2008-03-18 13:27:27Z peter $

See the end of this file for copyrights and conditions.

version 0.6.5 (released 18 March 2008)
  - Fixed color bug in blame output (ticket:278)
  - Improved error handling when --root parameter is incorrect.

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6.5

version 0.6.4 (released 14 October 2007)
  - fixed bug stats were double counted when root was a file (ticket:272)
  - fixed out of range problem in VectorPlus (ticket:267)

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6.4

version 0.6.3 (released 31 August 2007)
  - Copyright update is no longer adding newline to file (ticket:264)
  - Commandline parsing now accepts '=' syntax (ticket:259)

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6.3

Version 0.6.2 (released 21 August 2007)
  - Fixed issue with moving files across different file systems (ticket:251)

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6.2

Version 0.6.1 (released 9 July 2007)
  - changed refs to svndigest site to http://trac.thep.lu.se/trac/svndigest 
  - fixed lineover bug in blame output (ticket:235)

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6.1

Version 0.6 (released 29 June 2007)
  - svn blame output
  - reports for individual authors
  - differentiating several line types
  - support for TracLinks
  - config file, see options `--config-file` and `--generate-config`
  - support for automatic update of copyright statement
  - Removed option flag '-rev'
  - More file extensions supported for parsing.

  A complete list of closed tickets can be found here [[br]]
  http://trac.thep.lu.se/trac/svndigest/query?status=closed&milestone=0.6

Version 0.5 (released 7 September 2006)
  - Added support for svndigest:ignore property.
  - Creation of static svndigest binary is now supported, use
    --enable-staticbin.
  - Dates and time now refers to UTC.
  - Statistics now differentiates between code, comments, and empty
    lines. Improvements to this parsing is still needed.

Version 0.4 (released 12 August 2006)
  - Project name changed from svnstat to svndigest.

Version 0.3 (released 10 August 2006)
  - Improved the generated output.
  - svnstat handles unexpected situations more gracefully (like
    running svnstat on a tree that is not under subversion control,
    trees with items that are not up to date).
  - Proper version information is displayed using --version option.

Version 0.2 (released 12 March 2006)
  - Improved web page presentation.
  - Added option to plot changes against time or revision.

Version 0.1 (released 4 March 2006)
  - First release.

-------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari H�kkinen
Copyright (C) 2007 Jari H�kkinen, Peter Johansson
Copyright (C) 2008 Peter Johansson

This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.
}}}
----------------------------------------------------------------------

