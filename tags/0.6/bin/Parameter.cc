// $Id: Parameter.cc 374 2007-06-19 23:05:14Z peter $

/*
	Copyright (C) 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"

#include "ColumnStream.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	Parameter::Parameter(const int argc,const char *argv[])
	{
		defaults();
		for (int i=1; i<argc; ++i) {
			bool ok=false;
			std::string myargv(argv[i]);
			if (myargv=="--config-file"){
				if (++i<argc){
					config_file_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="--copyright"){
					copyright_=true;
					ok=true;
			}
			else if (myargv=="-f" || myargv=="--force"){
					force_=true;
					ok=true;
			}
			else if (myargv=="-g" || myargv=="--generate-config"){
					generate_config_=true;
					ok=true;
			}
			else if (myargv=="-h" || myargv=="--help"){
				help();
				exit(0);			// always exit after printing help
			}
			else if (myargv=="-r" || myargv=="--root"){
				if (++i<argc){
					root_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="--revisions") {
					revisions_=true;
					ok=true;
			}
			else if (myargv=="-t" || myargv=="--target"){
				if (++i<argc){
					targetdir_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-v" || myargv=="--verbose"){
					verbose_=true;
					ok=true;
			}
			else if (myargv=="--version"){
					version();
					exit(0);
			}
			else if (myargv=="-vf" || myargv=="-fv"){
					verbose_=true;
					force_=true;
					ok=true;
			}

			if (!ok)
				throw std::runtime_error("svndigest: invalid option: " + myargv +
																 "\nType `svndigest --help' for usage.");
		}

		analyse();
	}


	void Parameter::analyse(void)
	{
		using namespace std;

		string workdir(pwd()); // remember current working directory (cwd).

		// Checking that root_ exists and retrieve the absolute path to root_
		if (chdir(root_.c_str()))
			throw runtime_error(string("svndigest: Root directory (") + root_ +
													") access failed.");
		root_ = pwd();

		// need to get back to cwd if relative paths are used.
		if (chdir(workdir.c_str()))
			runtime_error(string("svndigest: Failed to access cwd: ") + workdir);

		// Checking that targetdir_ exists and retrieve the absolute path
		// to targetdir_
		if (chdir(targetdir_.c_str()))
			throw runtime_error(string("svndigest: Target directory (") + targetdir_ +
													") access failed.");
		targetdir_ = pwd();
		// Checking write permissions for targetdir_
		if (access_rights(targetdir_,"w"))
			throw runtime_error(string("svndigest: No write permission on target ") +
																 "directory (" + targetdir_ +").");

		// return back to cwd
		if (chdir(workdir.c_str()))
			throw runtime_error(string("svndigest: Failed to access cwd: ") + workdir);

	}


	std::string Parameter::config_file(void) const
	{
		// not default
		if (!config_file_.empty())
			return config_file_;
		
		// default behaviour
		return root()+"/.svndigest/config";
	}


	void Parameter::defaults(void)
	{
		config_file_ = "";
		copyright_=false;
		force_=false;
		generate_config_=false;
		revisions_=false;
		root_=".";
		targetdir_=".";
		verbose_=false;
	}


	void Parameter::help(void)
	{
		defaults();

		ColumnStream cs(std::cout, 1);
		cs.width(0)=79;
		cs.margin(0)=0;
		ColumnStream cs2(std::cout, 2);
		cs2.width(0)=24;
		cs2.width(1)=52;
		cs2.margin(0)=2;

		std::cout << "Usage: svndigest [OPTION]...\n"
							<< "\n";

		cs << "Generate statistical report for a subversion repository.\n";

		cs << "\nMandatory arguments to long options are mandatory for "
			 << "short options too.\n";

		cs2	 << "    --copyright\tupdate copyright statement\n" 
				 << "    --config-file=ARG\tconfiguration file " 
				 << "[<ROOT>/.svndigest/config]\n"
				 << "-f, --force\tif sub-directory named <ROOT> exists in "
				 << "target directory, remove sub-directory before writing results.\n"
				 << "-g, --generate-config\twrite configuration file " 
				 << "to standard output and exit\n"
				 << "-h, --help\tdisplay this help and exit\n"
				 << "-r, --root=ROOT\tsvn controlled directory to perform "
				 << "statistics calculation on [" << root_ << "]\n"
				 << "    --revisions\tUse revision numbers as time scale "
				 << "instead of dates [dates].\n"
				 << "-t, --target=TARGET\toutput directory [" << targetdir_ << "]\n"
				 << "-v, --verbose\texplain what is being done\n"
				 << "    --version\tprint version information and exit\n";

		std::cout	<< "\nReport bugs to <" << PACKAGE_BUGREPORT << ">." 
							<< std::endl;
	}


	void Parameter::version(void) const
	{
		ColumnStream cs(std::cout, 1);
		cs.width(0)=79;
		cs << PACKAGE_STRING
			 << "\nCopyright (C) 2007 Jari H�kkinen and Peter Johansson.\n\n"
			 << "This is free software; see the source for copying conditions. "
			 << "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR "
			 << "A PARTICULAR PURPOSE.\n";
	}

}} // of namespace wenni and namespace theplu
