#ifndef _theplu_svndigest_parameter_
#define _theplu_svndigest_parameter_

// $Id: Parameter.h 240 2007-04-28 01:43:54Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <map>
#include <string>

namespace theplu {
namespace svndigest {

  // class for command line options.
	class Parameter {
	public:
		Parameter(const int argc,const char *argv[]);
		std::string config_file(void) const;
		inline bool copyright(void) const { return copyright_; }
		/// @todo
		inline bool force(void) const { return force_; }
		inline bool generate_config(void) const { return generate_config_; }
		inline bool revisions(void) const { return revisions_; }
		/// @return absolute path to root directory
		inline const std::string& root(void) const { return root_; }
		/// @return absolute path to target directory
		inline const std::string& targetdir(void) const { return targetdir_; }
		inline bool verbose(void) const { return verbose_; }

	private:
		void analyse(void);
		void defaults(void);
		void help(void);
		void version(void) const;

		std::string config_file_;
		bool copyright_;
		bool force_;
		bool generate_config_;
		bool revisions_;
		std::string root_;
		std::string targetdir_;
		bool verbose_;
	};

}} // of namespace svndigest and namespace theplu

#endif
