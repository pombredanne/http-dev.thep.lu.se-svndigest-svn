#ifndef _theplu_svndigest_svnblame_
#define _theplu_svndigest_svnblame_

// $Id: SVNblame.h 408 2007-06-29 10:08:19Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <string>
#include <vector>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	/**
		 The SVNblame class is a utility class for taking care of 'svn
		 blame' information. An 'svn blame' is performed on an item, the
		 blame information for each line is traversed with
		 SVNblame.next_line() and SVNblame.valid() calls giving access to
		 the blame information.
	*/
	class SVNblame {
	public:

		/**
			 @brief The contructor.

			 The constructor performs an 'svn blame' on \a path and
			 initializes the SVNblame object for statistics traversal using
			 SVNblame::next_line() and SVNblame::valid().
		*/
		explicit SVNblame(const std::string& path);

		/**
			 @brief The destructor.
		*/
		~SVNblame(void);

		/**
			 @brief Retrieve the author for the current line.

			 If current line is outside blame entries the behaviour is
			 undefined.

			 @return The author.
		*/
		std::string author(void);

		/**
			 Returns true if item is binary false otherwise

			 Binary files are invalid for 'svn blame'.
		*/
		bool binary(void);

		/**
			 @brief Retrieve the blame date for the current line.

			 If current line is outside blame entries the behaviour is
			 undefined.

			 @return The date.
		*/
		std::string date(void);

		/**
			 @brief Retrieve the content of the current line.

			 If current line is outside blame entries the behaviour is
			 undefined.

			 @return The line content.
		*/
		std::string line(void);

		/**
			 @brief Retrieve the line number of the current line.

			 If current line is outside blame entries the behaviour is
			 undefined.

			 @return The line number.
		*/
		apr_int64_t line_no(void);

		/**
			 @brief Skip to the next line.

			 @return False if no more blame information is available, true
			 otherwise.
		*/
		bool next_line(void);

		/**
			 @brief Retrieve the blame revision of the current line.

			 If current line is outside blame entries the behaviour is
			 undefined.

			 @return The blame revision.
		*/
		svn_revnum_t revision(void);

		/**
			 @brief Check if more blame information is available.

			 @return True if valid information exists, false otherwise.
		*/
		bool valid(void);

	private:

		/**
			 @brief Copy Constructor, not implemented.
		*/
		SVNblame(const SVNblame&);

		/**
			 @brief Information return by subversion (blame) API

			 @see Subversion API for blame usage.
		*/
		struct blame_information {
			apr_int64_t line_no;
			svn_revnum_t revision;
			std::string author;
			std::string date;
			std::string line;
		};

		// binary_ is true if item in any revision has been binary.
		bool binary_;
		// blame_info_iterator_ is used in statistics analysis to traverse
		// blame_receiver_baton.blame_info vector through calls to next().
		std::vector<blame_information*>::iterator blame_info_iterator_;
		SVN* instance_;

		struct blame_receiver_baton_ {
			std::vector<blame_information*> blame_info;
		} blame_receiver_baton_ ;

		static svn_error_t *
		blame_receiver(void *baton, apr_int64_t line_no, svn_revnum_t revision,
									 const char *author, const char *date, const char *line,
									 apr_pool_t *pool);
	};

}} // end of namespace svndigest and namespace theplu

#endif
