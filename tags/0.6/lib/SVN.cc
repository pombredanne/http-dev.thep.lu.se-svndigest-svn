// $Id: SVN.cc 408 2007-06-29 10:08:19Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVN.h"

#include <map>
#include <string>
#include <vector>

#include <iostream>

#include <apr_allocator.h>
#include <apr_hash.h>
#include <subversion-1/svn_client.h>
#include <subversion-1/svn_cmdline.h>
#include <subversion-1/svn_path.h>
#include <subversion-1/svn_pools.h>
#include <subversion-1/svn_wc.h>
#include <subversion-1/svn_subst.h>

namespace theplu {
namespace svndigest {


	SVN* SVN::instance_=NULL;


	SVN::SVN(const std::string& path)
		: adm_access_(NULL), allocator_(NULL), context_(NULL), pool_(NULL)
	{
		svn_error_t* err=NULL;

		// initialize something (APR subsystem and more). The APR
		// subsystem is automatically destroyed at program exit. In case
		// of several calls to svn_cmdline_init (ie. several exceptions
		// thrown and caught with subsequent reinitializatios is safe
		// memorywise but what about APR internal counters?)
		if (svn_cmdline_init("svndigest",stderr) != EXIT_SUCCESS)
			throw SVNException("SVN: svn_cmdline_init failed");

		/// create top-level pool
		if (apr_allocator_create(&allocator_))
			throw SVNException("SVN(_allocator_create failed");
		apr_allocator_max_free_set(allocator_,SVN_ALLOCATOR_RECOMMENDED_MAX_FREE);
		pool_ = svn_pool_create_ex(NULL, allocator_);
		apr_allocator_owner_set(allocator_, pool_);

		// initialize the repository access library
		if ((err=svn_ra_initialize(pool_)))
			cleanup_failed_init(err, "SVN: svn_ra_initialize failed");

		// Check that users .subversion exist. Should this really be done?
		// If this check is to be done, we might be forced to support a
		// command line option to change the location of the .subversion
		// stuff (compare with the svn binary).
		if ((err=svn_config_ensure(NULL, pool_)))
			cleanup_failed_init(err, "SVN: svn_config_ensure failed");

		// create a client context object
		if ((err=svn_client_create_context(&context_, pool_)))
			cleanup_failed_init(err, "SVN: svn_client_create_context failed");

		if ((err=svn_config_get_config(&(context_->config), NULL, pool_)))
			cleanup_failed_init(err, "SVN: svn_config_get_config failed");

		// set up authentication stuff
		if ((err=svn_cmdline_setup_auth_baton(&(context_->auth_baton), false, NULL,
																					NULL, NULL, false,
												static_cast<svn_config_t*>(apr_hash_get(context_->config,
																										SVN_CONFIG_CATEGORY_CONFIG,
																										APR_HASH_KEY_STRING)),
																					context_->cancel_func,
																					context_->cancel_baton, pool_)))
			cleanup_failed_init(err, "SVN: svn_cmdline_setup_auth_baton failed");

		// Set up svn administration area access. The whole structure is
		// setup, maybe this is unnecessary?
		const char* canonical_path=svn_path_internal_style(path.c_str(), pool_);
		if (svn_error_t *err=svn_wc_adm_open3(&adm_access_, NULL, canonical_path,
																					false, -1, context_->cancel_func,
																					context_->cancel_baton, pool_))
			cleanup_failed_init(err, "SVN: svn_wc_adm_open3 failed");

		// get a session to the repository
		struct root_url_receiver_baton rurb;
		client_info(path, root_url_receiver, static_cast<void*>(&rurb));
		if (svn_error_t *err=svn_client_open_ra_session(&ra_session_,
																										rurb.path.c_str(),
																										context_, pool_))
			cleanup_failed_init(err, "SVN: svn_client_open_ra_session failed");
	}


	SVN::~SVN(void)
	{
		if (adm_access_)
			svn_error_clear(svn_wc_adm_close(adm_access_));
		svn_pool_destroy(pool_);
		apr_allocator_destroy(allocator_);
		// other apr resources acquired in svn_cmdline_init are destroyed
		// at program exit, ok since SVN is a singleton
	}


	void SVN::cleanup(svn_error_t *err,apr_pool_t *pool,
										const std::string& message)
	{
		svn_handle_error2(err,stderr,false,"svndigest: ");
		svn_error_clear(err);
		if (pool)
			svn_pool_destroy(pool);
		if (message.length()>0)
			throw SVNException(message);
	}


	void SVN::cleanup_failed_init(svn_error_t *err, const std::string& message)
	{
		cleanup(err,pool_);
		apr_allocator_destroy(allocator_);
		throw SVNException(message);
	}


	svn_error_t* SVN::client_blame(const std::string& path,
																 svn_client_blame_receiver_t receiver,
																 void *baton)
	{
		// Setup to use all revisions
		svn_opt_revision_t peg, start, head;
		peg.kind=svn_opt_revision_unspecified;
		start.kind=svn_opt_revision_number;
		start.value.number=0;
		head.kind = ( svn_path_is_url(path.c_str()) ?
									svn_opt_revision_head : svn_opt_revision_base );
		apr_pool_t *subpool = svn_pool_create(pool_);
		svn_error_t* err=svn_client_blame3(path.c_str(), &peg, &start, &head,
																			 svn_diff_file_options_create(subpool),
																			 false, receiver, baton, context_,
																			 subpool);
		if (err && err->apr_err!=SVN_ERR_CLIENT_IS_BINARY_FILE)
			// cleanup will throw an exception
			cleanup(err, subpool, "SVN::client_blame: svn_client_blame3 failed");
		svn_pool_destroy(subpool);
		return err;
	}


	void SVN::client_info(const std::string& path, svn_info_receiver_t receiver,
												void *baton)
	{
		apr_pool_t *subpool = svn_pool_create(pool_);
		if (svn_error_t *err=svn_client_info(path.c_str(), NULL, NULL, receiver,
																				 baton, false, context_, subpool))
			// cleanup will throw an exception
			cleanup(err, subpool, "repository: svn_client_info failed");
		svn_pool_destroy(subpool);
	}


	void SVN::client_log(const std::string& path,
											 svn_log_message_receiver_t receiver, void *baton)
	{
		// Allocate space in subpool to pool_ for apr_path (here a string).
		apr_pool_t *subpool = svn_pool_create(pool_);
		apr_array_header_t* apr_path=apr_array_make(subpool,1,4);
		// Copy path to apr_path.
		(*((const char **) apr_array_push(apr_path))) =
			apr_pstrdup(subpool, svn_path_internal_style(path.c_str(),subpool));

		// Setup to retrieve all commit logs.
		svn_opt_revision_t peg, start, head;
		peg.kind=svn_opt_revision_unspecified;
		start.kind=svn_opt_revision_number;
		start.value.number=0;
		head.kind = ( svn_path_is_url(path.c_str()) ?
									svn_opt_revision_head : svn_opt_revision_base );
		svn_error_t* err=NULL;
		if ((err=svn_client_log3(apr_path, &peg, &start, &head, 0, false, false,
														 receiver, baton, context_, subpool)))
			// cleanupp will throw an exception
			cleanup(err, subpool, "commit_dates: svn_client_log3 failed");
		svn_pool_destroy(subpool);
	}


	void SVN::client_proplist(const std::string& path,
														std::map<std::string, std::string>& property)
	{
		svn_opt_revision_t peg, revision;
		peg.kind=svn_opt_revision_unspecified;
		revision.kind = ( svn_path_is_url(path.c_str()) ?
											svn_opt_revision_head : svn_opt_revision_base );
		apr_pool_t *subpool = svn_pool_create(pool_);
		apr_array_header_t * properties;
		svn_error_t *err=svn_client_proplist2(&properties, path.c_str(), &peg,
																					&revision, false, context_, subpool);
		if (err)
			cleanup(err, subpool, "repository: svn_client_proplist2 failed");

    for (int j = 0; j < properties->nelts; ++j) {
      svn_client_proplist_item_t *item = 
        ((svn_client_proplist_item_t **)properties->elts)[j];
      for (apr_hash_index_t *hi = apr_hash_first(subpool, item->prop_hash); hi; 
           hi = apr_hash_next (hi)) {
        const void *key;
        void *val;
        apr_hash_this (hi, &key, NULL, &val);
				svn_string_t *value;
				err=svn_subst_detranslate_string(&value,
																				 static_cast<const svn_string_t*>(val),
																				 false, subpool);
				if (err)
					cleanup(err, subpool,
									path +
									" property: svn_subst_detranslate_string failed on key " +
									static_cast<const char*>(key));
				property[static_cast<const char*>(key)]=value->data;
      }
		}
		svn_pool_destroy(subpool);
	}


	SVN* SVN::instance(void)
	{
		if (!instance_)
			throw SVNException("SVN::instance(void): SVN singleton not initialized");
		return instance_;
	}


	SVN* SVN::instance(const std::string& url)
	{
		if (!instance_) {
			instance_=new SVN(url);
		}
		return instance_;
	}


	svn_error_t* SVN::root_url_receiver(void *baton, const char *,
																			const svn_info_t *info, apr_pool_t*)
	{
		if (!info)
			throw SVNException(std::string("SVN::url_receriver: ") +
												 "Failed to acquire an svn info object");

		root_url_receiver_baton* rurb=
			static_cast<struct root_url_receiver_baton*>(baton);
		if (info->repos_root_URL)
			rurb->path=info->repos_root_URL;

		return SVN_NO_ERROR;
	}


	SVN::vc_status SVN::version_controlled(const std::string& path)
	{
		svn_wc_status2_t* status=NULL;
		apr_pool_t *subpool = svn_pool_create(pool_);
		if (svn_error_t *err=
				svn_wc_status2(&status,svn_path_internal_style(path.c_str(), subpool),
											 adm_access_, subpool))
			// cleanup will throw an exception
			cleanup(err, subpool,"version_controlled(): svn_config_get_config failed");
		svn_pool_destroy(subpool);

		if ((status->text_status==svn_wc_status_none) ||
				(status->text_status==svn_wc_status_unversioned))
			return unversioned;
		else if (status->text_status==svn_wc_status_normal)
			return uptodate;

		return unresolved;
	}

}} // end of namespace svndigest and namespace theplu
