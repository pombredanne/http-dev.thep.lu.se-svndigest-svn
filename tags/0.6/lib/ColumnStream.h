#ifndef _theplu_svndigest_column_stream_
#define _theplu_svndigest_column_stream_

// $Id: ColumnStream.h 234 2007-04-09 12:08:26Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/
#include <fstream>
#include <sstream>
#include <vector>

namespace theplu{
namespace svndigest{

	///
	/// ostream for sending to multiple columns 
	///
	class ColumnStream
	{
	public:

		///
		/// @brief Constructor
		///
		ColumnStream(std::ostream& os, size_t columns);

		///
		/// @brief Destructor
		///
		~ColumnStream(void);

		///
		/// flush to ostream, goes to newline andactivates first column
		///
		void flush(void);

		inline size_t& margin(size_t c) { return margins_[c]; }

		void next_column(void);

		///
		/// \brief print to active column
		///
		void print(std::stringstream&);

		///
		/// \brief select which column is active
		///
		void set_column(size_t);

		inline size_t& width(size_t c) { return width_[c]; }

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		ColumnStream(const ColumnStream&);

		void fill(size_t, size_t);
		bool writeline(size_t i);
		inline size_t columns(void) const { return buffer_.size(); }
		

		size_t activated_;
		std::vector<size_t> margins_;
		std::ostream& os_;
		std::vector<std::stringstream*> buffer_;
		std::vector<size_t> width_;
	};


	template <typename T>
	ColumnStream& operator<<(ColumnStream& s, const T& rhs)
	{
		std::stringstream ss;
		ss << rhs;
		s.print(ss);
		return s;
	}

}} // end of namespace svndigest and namespace theplu

#endif
