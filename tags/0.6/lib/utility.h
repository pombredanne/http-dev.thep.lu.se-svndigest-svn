#ifndef _theplu_svndigest_utility_
#define _theplu_svndigest_utility_

// $Id: utility.h 373 2007-06-19 21:53:16Z peter $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <algorithm>
#include <functional>
#include <iosfwd>
#include <string>
#include <utility>
#include <vector>

#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	///
	/// @brief Check if access permissions match \a mode. \a mode must
	/// be given as r, w, x, or combinations of these letters.
	///
	/// @return On success (all requested permissions granted), zero
	/// is returned. On error (at least one bit in mode asked for a
	/// permission that is denied, or some other error occurred), -1
	/// is returned, and errno is set appropriately.
	///
	/// @throw An std::runtime_error is thrown when checking for write
	/// permissions for a file/direcotry that does not exist.
	///
	/// @see access(2)
	///
	int access_rights(const std::string& path,const std::string& bits);

	///
	/// @return everything after last '/'
	///
	std::string file_name(const std::string&);

	///
	/// @brief environment variable @a var
	///
	std::string getenv(const std::string& var); 

	///
	/// If @a width is set, size is forced to length @a width. This
	/// implies, e.g., that hex(15,2) and hex(17,1) return "0F" and "1",
	/// respectively.
	///
	/// @return x in hexadecimal base
	///
	std::string hex(int x, u_int width=0);

	///
	/// @brief remove trailing whitespaces
	///
	std::string htrim(std::string str);

	///
	/// @return true if \a str is an integer
	///
	bool is_int(std::string str);

	///
	/// @brief remove leading whitespaces
	///
	std::string ltrim(std::string str);

	inline bool match_begin(std::string::const_iterator first, 
													std::string::const_iterator last, 
													const std::string& str)
	{ return (std::distance(first, last)>=static_cast<int>(str.size()) && 
						std::equal(str.begin(), str.end(), first)); 
	}

	inline bool match_end(std::string::const_reverse_iterator first, 
												std::string::const_reverse_iterator last, 
												const std::string& str)
	{ return (std::distance(first,last)>=static_cast<int>(str.size()) && 
						std::equal(str.rbegin(), str.rend(), first)); 
	}

	///
	/// Create directory \a dir. The call can fail in many ways, cf. 'man
	/// mkdir'.
	///
	inline int mkdir(const std::string& dir) { return ::mkdir(dir.c_str(),0777); }

	///
	/// @brief Check whether \a path already exists or not.
	///
	/// @return True if \a path exists, false otherwise.
	///
	bool node_exist(const std::string& path);

	/**
		 @return 0 if \a b = 0 otherwise \f$ \frac{100*a}{b} \f$ 
	*/
	int percent(int a, int b);

	///
	/// @return the current working directory.
	///
	std::string pwd(void);

	///
	/// Search finds a subsecuence in [first, last) being identical to @ str
	///
	/// @return iterator pointing to first character in found subsequence
	///
	/// @see std::search
	///
	inline std::string::iterator search(std::string::iterator first, 
																			std::string::iterator last, 
																			std::string str)
	{ return std::search(first, last, str.begin(), str.end()); }

	///
	///
	///
	time_t str2time(const std::string&);

	///
	/// If file does not exist create empty file.
	///
	void touch(std::string);

	///
	/// remove leading and trailing whitespaces
	///
	inline std::string trim(std::string str) { return htrim(ltrim(str)); }


	template <typename T>
	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										const T& func)
	{
		std::string res;
		for (;first!=last && func(first); ++first) 
			res.append(1,*first);
		return res;
	}


	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										std::string);

	struct AlNum
	{
		inline bool operator()(std::string::const_iterator i) const 
		{ return isalnum(*i); }
	};


	struct Digit
	{
		inline bool operator()(std::string::const_iterator i) const 
		{ return isdigit(*i); }
	};


	class notChar
	{
	public:
		notChar(char);
		inline bool operator()(std::string::const_iterator i) const 
		{ return *i!=char_; }
	private:
		char char_;
	};


	class not2Char
	{
	public:
		not2Char(char, char);
		inline bool operator()(std::string::const_iterator i) const 
		{ return *i!=char1_ && *i!=char2_; }
	private:
		const char char1_;
		const char char2_;
	};

	class not2Str
	{
	public:
		not2Str(std::string, std::string);
		inline bool operator()(std::string::const_iterator i) const 
		{ 
			return !(std::equal(str1_.begin(), str1_.end(), i) ||
							std::equal(str2_.begin(), str2_.end(), i));
		} 

	private:
		const std::string str1_;
		const std::string str2_;
	};

	///
	/// Functor to be used on contaioners and works as standard less,
	/// but on the reversed container.
	///
	/// Requirements on T is that has rend and rbegin. T::value_type
	/// must be comparable (i.e. have operator<)
	/// 
	template <typename T>
	struct LessReversed
	{
		///
		/// using std::lexicographical_compare on the reversed container
		///
		inline bool operator()(const T& x, const T& y) const
		{ return std::lexicographical_compare(x.rbegin(),x.rend(),
																					y.rbegin(),y.rend()); }
	};


	///
	/// @brief Functor comparing pairs using second.
	///
	/// STL provides operator< for the pair.first element, but none for
	/// pair.second. This template provides this and can be used as the
	/// comparison object in generic functions such as the STL sort.
	///
	template <typename T1, typename T2>
	struct pair_value_compare
	{
		///
		/// @return true if x.second<y.second or (x.second==y.second and
		/// x.first<y.first)
		///
		inline bool operator()(const std::pair<T1,T2>& x,
													 const std::pair<T1,T2>& y) {
			return ((x.second<y.second) ||
							(!(y.second<x.second) && (x.first<y.first))); 
		}
	};

	
	///
	/// Functor working on pair.second, using a user passed functor.
	///
	template <typename T1, typename T2, typename T3>
	struct PairSecondCompare
	{

		///
		/// @brief Constructor
		///
		explicit PairSecondCompare(const T3& comp)
			: compare_(comp) {}

		///
		/// @return compare(x.second, y.second) where compare is a
		/// internal comparison functor.
		///
		inline bool operator()(const std::pair<T1,T2>& x,
													 const std::pair<T1,T2>& y) const
		{ return compare_(x.second,y.second); }

	private:
		T3 compare_;

	};
	
	///
	/// Calculating sum of two vectors.
	///
	/// @return resulting vector
	///
	template <typename T >
	struct VectorPlus : 
		public std::binary_function<std::vector<T>,std::vector<T>,std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& u,
															const std::vector<T>& v) const 
		{
			if ( u.size() > v.size() ){
				std::vector<T> res(u.size());
				transform(u.begin(), u.end(), v.begin(), res.begin(), std::plus<T>());
				copy(u.begin()+v.size(), u.end(), res.begin()+v.size());
				return res;
			}
	
			std::vector<T> res(v.size());
			transform(v.begin(), v.end(), u.begin(), res.begin(), std::plus<T>());
			if ( v.size() > u.size() )
				copy(v.begin()+u.size(), v.end(), res.begin()+u.size());
			return res;
		}

	};

	///
	/// @return resulting vector
	///
	template <typename Key, typename T>
	struct PairValuePlus :
		public std::binary_function<std::vector<T>,
																std::pair<const Key, std::vector<T> >, 
																std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& sum, 
															const std::pair<const Key,std::vector<T> >& p)
		{
			return VectorPlus<T>()(sum, p.second);
		}
	};

}} // end of namespace svndigest end of namespace theplu

#endif 
