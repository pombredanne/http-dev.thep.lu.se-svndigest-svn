// $Id: Parser.cc 408 2007-06-29 10:08:19Z jari $

/*
	Copyright (C) 2006, 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parser.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

namespace theplu{
namespace svndigest{


	Parser::Parser(std::string path)
	{
		std::ifstream is(path.c_str());
		assert(is.good());
		std::vector<std::pair<std::string, std::string> > codon;
		// Ignore trailing '.in' in file names
		if (match_end(path.rbegin(), path.rend(), ".in"))
			path = path.substr(0, path.size()-3);
		if (match_end(path.rbegin(), path.rend(), ".ac") ||
				match_end(path.rbegin(), path.rend(), ".am") ||
				match_end(path.rbegin(), path.rend(), ".m4")) {
			codon.reserve(2);
			codon.push_back(std::make_pair("#", "\n"));
			codon.push_back(std::make_pair("dnl", "\n"));
			parse(is, codon);
		}
		else if (match_end(path.rbegin(), path.rend(), ".c") ||
						 match_end(path.rbegin(), path.rend(), ".cc") ||
						 match_end(path.rbegin(), path.rend(), ".cpp") ||
						 match_end(path.rbegin(), path.rend(), ".cxx") ||
						 match_end(path.rbegin(), path.rend(), ".h") ||
						 match_end(path.rbegin(), path.rend(), ".hh") ||
						 match_end(path.rbegin(), path.rend(), ".hpp") ||
						 match_end(path.rbegin(), path.rend(), ".java")) {
			codon.reserve(2);
			codon.push_back(std::make_pair("//", "\n"));
			codon.push_back(std::make_pair("/*", "*/"));
			parse(is, codon);
		}
		else if (match_end(path.rbegin(), path.rend(), ".pl") ||
						 match_end(path.rbegin(), path.rend(), ".pm") ||
						 match_end(path.rbegin(), path.rend(), ".sh") ||
						 match_end(path.rbegin(), path.rend(), "config") ||
						 file_name(path)=="bootstrap" ||
						 file_name(path)=="Makefile") {
			codon.push_back(std::make_pair("#", "\n"));
			parse(is,codon);
		}
		else if (match_end(path.rbegin(), path.rend(), ".tex") || 
						 match_end(path.rbegin(), path.rend(), ".m")) {
			codon.push_back(std::make_pair("%", "\n"));
			parse(is,codon);
		}
		else if (match_end(path.rbegin(), path.rend(), ".jsp")) {
			codon.reserve(2);
			codon.push_back(std::make_pair("<!--", "-->"));
			codon.push_back(std::make_pair("<%--", "--%>"));
			parse(is,codon);
		}
		else if (match_end(path.rbegin(), path.rend(), ".html") || 
						 match_end(path.rbegin(), path.rend(), ".xml") ||
						 match_end(path.rbegin(), path.rend(), ".xsl") ||
						 match_end(path.rbegin(), path.rend(), ".xsd") ||
						 match_end(path.rbegin(), path.rend(), ".xhtml") ||
						 match_end(path.rbegin(), path.rend(), ".shtml") ||
						 match_end(path.rbegin(), path.rend(), ".xml") ||
						 match_end(path.rbegin(), path.rend(), ".css") ||
						 match_end(path.rbegin(), path.rend(), ".rss") ||
						 match_end(path.rbegin(), path.rend(), ".sgml") ){
			codon.push_back(std::make_pair("<!--", "-->"));
			parse(is,codon);
		}
		else
			text_mode(is);
		is.close();
	}


	void Parser::parse(std::istream& is,
										 std::vector<std::pair<std::string, std::string> >& codon)
	{
		// mode zero means we are currently not in a comment
		// if mode!=0 comment is closed by codon[mode-1].second -> mode=0
		// if codon[x-1].start is found and x >= mode -> mode=x 
		size_t mode = 0;
		std::string str;
		while(getline(is,str)) {
			line_type lt=empty;
			for (std::string::iterator iter=str.begin(); iter!=str.end(); ++iter){
				for (size_t i=mode; i<codon.size(); ++i) {
					if (match_begin(iter, str.end(), codon[i].first)) {
						mode = i+1;
						break;
					}
				}
				assert(mode==0 || mode-1<codon.size());
				if (mode && match_begin(iter,str.end(), codon[mode-1].second)){
					mode=0;
					continue;
				}
				// A line of code or comment must contain at least one
				// alphanumerical character.
				if (isalnum(*iter)) {
					if (!mode) {
						lt=code;
					}
					else if (lt!=code) {
						lt=comment;
					}
				}
			}
			if (mode && codon[mode-1].second==std::string("\n"))
				mode=0;
			type_.push_back(lt);
		}
	}


	void Parser::text_mode(std::istream& is)
	{
		std::string str;
		while(getline(is,str)) {
			line_type lt=empty;
			for (std::string::iterator iter=str.begin(); iter!=str.end(); ++iter){
				if (lt==empty && isalnum(*iter))
					lt = comment;
			}
			type_.push_back(lt);
		}
	}


}} // end of namespace svndigest and namespace theplu
