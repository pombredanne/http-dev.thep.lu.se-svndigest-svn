// $Id: SVNinfo.cc 408 2007-06-29 10:08:19Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVNinfo.h"
#include "SVN.h"

#include <string>

namespace theplu {
namespace svndigest {


	SVNinfo::SVNinfo(const std::string& path)
	{
    SVN::instance()->client_info(path, info_receiver,
																 static_cast<void*>(&info_receiver_baton_));
	}


	svn_error_t* SVNinfo::info_receiver(void *baton, const char *,
																			const svn_info_t *info, apr_pool_t*)
	{
		if (!info)
			throw SVNException(std::string("SVNinfo::info_receriver: ") +
												 "Failed to acquire an svn info object");

		info_receiver_baton* irb=static_cast<struct info_receiver_baton*>(baton);
		if (info->URL)
			irb->url_=info->URL;
		if (info->repos_root_URL)
			irb->repos_root_url_=info->repos_root_URL;
		if (info->last_changed_author)
			irb->last_changed_author_=info->last_changed_author;
		if (info->last_changed_rev)
			irb->last_changed_rev_=info->last_changed_rev;
		if (info->rev)
			irb->rev_=info->rev;

		return SVN_NO_ERROR;
	}

}} // end of namespace svndigest and namespace theplu
