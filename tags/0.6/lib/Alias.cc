// $Id: Alias.cc 303 2007-05-11 20:13:00Z peter $

/*
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Alias.h"

#include <string>

namespace theplu{
namespace svndigest{

	Alias::Alias(void)
		: name_(std::string()), id_(0)
	{}


	Alias::Alias(std::string name, size_t id)
		: name_(name), id_(id)
	{}

	
	std::string Alias::name(void) const
	{
		return name_;
	}


	size_t Alias::id(void) const
	{
		return id_;
	}

	bool operator<(const Alias& lhs, const Alias& rhs)
	{ 
		return lhs.name() < rhs.name(); 
	}


	bool operator==(const Alias& lhs, const Alias& rhs)
	{ 
		return lhs.name() == rhs.name(); 
	}

}} // end of namespace svndigest and namespace theplu
