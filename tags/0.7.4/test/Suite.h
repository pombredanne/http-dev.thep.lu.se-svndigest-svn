#ifndef _theplu_svndigest_test_suite_
#define _theplu_svndigest_test_suite_

// $Id: Suite.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "environment.h"

#include <string>

namespace theplu {
namespace svndigest {
	class Stats;
	class StatsCollection;

namespace test {

	class Suite
	{
	public:
		Suite(int argc, char* argv[], bool need_test_repo=false);
		~Suite(void);

		/**
			 If b is false, set ok to false

			 \return b
		*/
		bool add(bool b);

		/**
			 \return true if all tests are OK
		 */
		bool ok(void) const;

		std::ostream& out(void) const;

		/**
			 \return true if we are running in verbose mode
		 */
		bool verbose(void) const;

	private:
		std::ofstream* dev_null_;
		bool ok_;
		bool verbose_;

		void checkout_test_wc(void) const;
		void update_test_wc(void) const;
	};

	bool check_all(const Stats&, test::Suite&);
	bool check_total(const Stats&, test::Suite&);
	bool check_comment_or_copy(const Stats&, test::Suite&);

	bool consistent(const StatsCollection&, test::Suite&);
	bool consistent(const Stats&, test::Suite&);

	bool equal(const StatsCollection& a, const StatsCollection& b, 
						 test::Suite& suite);

	bool equal(const Stats& a, const Stats& b, test::Suite& suite);

	/**
		 \return absolute path to file
		 \param local_path path relative to builddir
	 */
	std::string filename(const std::string& local_path);

	/**
		 \return absolute path to file
		 \param path path relative to srcdir
	 */
	std::string src_filename(const std::string& path);
}}}

#endif
