// $Id: OptionVersion.cc 795 2009-06-30 03:57:27Z peter $

/*
	Copyright (C) 2008, 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	svndigest is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "copyright_year.h"
#include "OptionVersion.h"
#include "subversion_info.h"
#include "utility.h"

#include "yat/ColumnStream.h"
#include "yat/CommandLine.h"

#include <subversion-1/svn_version.h>

#include <apr_version.h>

#include <sstream>
#include <string>

namespace theplu {
namespace svndigest {

	OptionVersion::OptionVersion(yat::utility::CommandLine& cmd, std::string flag, 
															 std::string desc, OptionSwitch* const verbose)
		: yat::utility::OptionSwitch(cmd, flag, desc, false), verbose_(verbose)
	{
	}


	void OptionVersion::do_parse2(std::vector<std::string>::iterator first, 
																std::vector<std::string>::iterator last)
	{
		yat::utility::ColumnStream cs(std::cout, 1);
		cs.width(0)=79;
		cs << PACKAGE_NAME << " " << PACKAGE_VERSION;
		bool no_verbose = verbose_ && verbose_->present() && !verbose_->value();
		if (!no_verbose) {
			cs << " (";
			if (DEV_BUILD)
				cs << svn_revision() << " ";
			cs << "compiled " << compilation_time() << ", " << compilation_date() 
				 << ")";
		} 
		cs << "\n\nCopyright (C) " << svn_year() 
			 << " Jari H\u00E4kkinen and Peter Johansson.\n"
			 << "This is free software. You may redistribute copies of it under "
			 << "the terms of the GNU General Public License "
			 << "<http://www.gnu.org/licenses/gpl.html>.\n"
			 << "There is NO WARRANTY; to the extent permitted by law.\n";
		if (!no_verbose) {
			cs << "\n"
				 << "Compiled with libsvn_subr " << SVN_VER_NUM 
				 << "; using libsvn_subr ";
			const svn_version_t* svn_ver = svn_subr_version();
			cs << svn_ver->major << '.' << svn_ver->minor << '.' << svn_ver->patch;
			cs << "\n"
				 << "Compiled with libapr " << APR_VERSION_STRING 
				 << "; using libapr ";
			cs << apr_version_string();
			cs << "\n";
		}
		exit(0);
	}

}} // of namespace svndigest and theplu
