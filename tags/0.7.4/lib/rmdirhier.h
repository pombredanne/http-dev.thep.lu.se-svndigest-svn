#ifndef _theplu_svndigest_rmdirhier_
#define _theplu_svndigest_rmdirhier_

// $Id: rmdirhier.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdexcept>
#include <string>

namespace theplu {
namespace svndigest {

	struct DirectoryError : public std::runtime_error
	{ inline DirectoryError(const std::string& s) : runtime_error(s) {} };

	struct BadDirectory : public DirectoryError
	{ inline BadDirectory(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryOpenError : public DirectoryError
	{ inline DirectoryOpenError(const std::string& s) : DirectoryError(s) {} };

	struct FileDeleteError : public DirectoryError
	{ FileDeleteError(const std::string& s) : DirectoryError(s) {} };

	struct DirectoryDeleteError : public DirectoryError
	{ DirectoryDeleteError(const std::string& s) : DirectoryError(s) {} };

	void rmdirhier(const std::string& path);

}} // of namespace svndigest and namespace theplu

#endif
