// $Id: BlameStats.cc 1031 2010-02-15 04:45:41Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "BlameStats.h"

#include "Functor.h"
#include "SVNblame.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	BlameStats::BlameStats(const std::string& path)
		: Stats(path)
	{
	}


	BlameStats::BlameStats(const BlameStats& other)
	: Stats(other)
	{
	}


	void BlameStats::fill_in(Author2Vector& a2v, svn_revnum_t rev)
	{
		if (rev==0)
			return;
		for (std::set<std::string>::const_iterator iter(authors().begin());
				 iter!=authors().end(); ++iter) {
			std::vector<unsigned int>& vec = a2v[*iter];
			vec.resize(revision()+1);
			assert(rev<static_cast<svn_revnum_t>(vec.size()));
			vec[rev]=vec[rev-1];
		}
	}


	void BlameStats::do_parse(const std::string& path, svn_revnum_t first_rev)
	{
		SVNlog log(path);
		typedef std::set<svn_revnum_t> RevSet;
		RevSet revs;
		std::transform(log.commits().begin(), log.commits().end(),
									 std::inserter(revs, revs.begin()), 
									 std::mem_fun_ref(&Commitment::revision));
		for (RevSet::reverse_iterator rev_iter=revs.rbegin();
				 rev_iter!=revs.rend() && *rev_iter>=first_rev; ++rev_iter){
			SVNblame svn_blame(path, *rev_iter);
			LineTypeParser parser(path);
			while (svn_blame.valid()) {
				add(svn_blame.author(), *rev_iter, parser.parse(svn_blame.line()));
				// I dont trust blame and log behave consistent (stop-on-copy).
				revs.insert(svn_blame.revision());
				svn_blame.next_line();
			}
		}
		
		// filling in pristine revisions, i.e., revs that are larger than
		// first_rev and not occuring in revs.
		RevSet::iterator rev_iter=revs.begin();
		svn_revnum_t rev = first_rev;
		while (rev<=revision()) {
			if (rev_iter!=revs.end() && rev>*rev_iter)
				++rev_iter;
			else if (rev_iter!=revs.end() && rev==*rev_iter) {
				++rev_iter;
				++rev;
			}
			else {
				fill_in(code_stats(),rev);
				fill_in(comment_stats(),rev);
				fill_in(other_stats(),rev);
				fill_in(copyright_stats(),rev);
				++rev;
			}
		}
	}


}} // end of namespace svndigest and namespace theplu
