#ifndef _theplu_yat__utility_column_stream_
#define _theplu_yat__utility_column_stream_

// $Id: ColumnStream.h 1887 2009-03-31 17:38:16Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	///
	/// ostream for sending to multiple columns 
	///
	class ColumnStream
	{
	public:

		///
		/// @brief Constructor
		///
		ColumnStream(std::ostream& os, size_t columns);

		///
		/// @brief Destructor
		///
		~ColumnStream(void);

		///
		/// flush to ostream, goes to newline andactivates first column
		///
		void flush(void);

		/**
			 \return reference to margin of column \a c
		 */
		size_t& margin(size_t c);

		/**
			 \brief jump to next column
		 */
		void next_column(void);

		///
		/// \brief print to active column
		///
		void print(std::stringstream&);

		///
		/// \brief select which column is active
		///
		void set_column(size_t);

		/**
			 \return reference to width of column \a c
		 */
		size_t& width(size_t c);

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		ColumnStream(const ColumnStream&);

		void fill(size_t, size_t);
		bool writeline(size_t i);
		inline size_t columns(void) const { return buffer_.size(); }
		

		size_t activated_;
		std::vector<size_t> margins_;
		std::ostream& os_;
		std::vector<std::stringstream*> buffer_;
		std::vector<size_t> width_;
	};


	/**
		 \brief ColumnStream output operator

		 Requirement: T should have operator
		 operator<<(ostream&, const T&)

		 \relates ColumnStream
	 */
	template <typename T>
	ColumnStream& operator<<(ColumnStream& s, const T& rhs)
	{
		std::stringstream ss;
		ss << rhs;
		s.print(ss);
		return s;
	}

}}} // end of namespace utility, yat, theplu

#endif
