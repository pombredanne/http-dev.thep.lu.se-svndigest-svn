$Id: readme.txt 1368 2011-06-10 02:27:10Z peter $
{{{
Copyright (C) 2005 Jari Häkkinen
Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
Copyright (C) 2009, 2010, 2011 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
}}}

= Content =

This manual describes how to use the three programs included in the
svndigest package:

  * svndigest
  * svncopyright
  * svndigest-copy-cache

= About svndigest =

Svndigest traverses a directory structure (controlled by subversion)
and calculates developer statistics for all subversion controlled
entries. The result is written to a sub-directory of a user
specifiable target directory (default is the current working
directory).

=== Statistics ===

To understand what statistics is calculated by svndigest this
definition is needed: The developer who made the latest change to a
line still in use in the latest revision, is considered as the
contributor of that line regardless of who actually originally created
that line.

For each developer svndigest calculates the number of contributed
lines in the latest (checked out) revision. Svndigest calculates for
each revision, by checking when each line was last changed, how many
lines each developer had contributed at that specific revision and
still are in use. 

see also: http://dev.thep.lu.se/svndigest/wiki/StatsType

=== Different linetypes ===

Svndigest parses each file to decide whether a line is ''code'',
''comment'', or ''other''. Depending on the file name different
parsing modes are used, which means different sets of rules what is
''code'' or ''comment'' are employed. Common for all modes is that
comment blocks are identified by a start code (e.g. '/*' in a C file)
and a stop code (e.g. '*/' in a C file). If a line contains
visible characters being outside comment blocks, the line is
considered to be ''code''. Otherwise, if the line contains
alphanumeric characters inside a comment block, the line is considered
to be a line of ''comment''. Otherwise the line is considered to be
''other''. 

From svndigest 0.7, it is possible to configure which rules are used
for different files. For more on how to configure svndigest see
below. If no configuration file is given or no rules are given, a set
of default rules are used. For files named `*.h`, for example, rules
allowing to detect comments as either `// ... \n` or `/*
... */`. These rules can be set in the configuration file using a
one-liner like:

`*.h = "//":"\n"  ;  "/*":"*/"`

The first string (`*.h`) is a file-name-pattern describing which files
this rule will be used on. The second block, `"//":"\n"`, is the first
rule saying one way to mark comments is to start with a ''start
code'', `//`, and end with an ''end code'', `\n`. The start and end
codes are surrounded by `""` and separated by `:`. Similarily, the
second block describes that comments also could come as `/* ... */`.

Sometimes it might be useful if a file could be parsed as though it
was going under a different name. It could, for example, be useful if
files named `Makefile.in` could be parsed as `Makefile` or files named
`test_repo.sh.in` could be parsed as though it was named
`test_repo.sh`. More generally, it would be useful if files named
`<file-name-pattern>.in` could be parsed as though it was named
`<file-name-pattern>`. For this reason it is possible to set rules on
how a filename should be interpreted (see section
[file-name-dictionary] in configuration file). Default behaviour is
that there is only one such a rule in use, namely the one described
above that trailing `.in` in file names are discarded.


=== Caching Statistics ===

To avoid retrieving the same data repeatedly from the repository,
statistics is cached in files located in `.svndigest`. Subsequent
svndigest runs read the cache files and retrieve information from the
repository only for the revisions that have been committed since the
cache file was created. Obviously, it is important that the cache
files reflect the history of the current working copy. If that is not
the case, for example, if you have switched working copy using `svn
switch`, you can make svndigest ignore the cache through option
`--ignore-cache`. From svndigest 0.8 the cache file contains
information on the configuration (e.g. codons) used to create the
statistics.  If that configuration is different from the current one,
the cache file is ignored and statistics is retrieved from repository.


=== Different file types ===

There are many different types of files and for many file types it
does not make sense to define lines. Source code, documentation, and
other human readable files can be treated in single line basis whereas
symbolic links and binary files cannot. svndigest treats binary files
and symbolic links as zero-line files. There is a possibility to
exclude files from the statistics, the use of the property
svndigest:ignore. 

Sometimes large test files and XML files are added to the repository
that should not really be counted in the statistics. This is solved
with the svndigest:ignore property. Files with this property are
excluded from statistics. Setting the svndigest:ignore property to a
directory will exclude all siblings to that directory from svndigest
treatment.

To set the property on file `FILE`, issue `svn propset
svndigest:ignore "" FILE`. For more detailed information refer to
http://svnbook.red-bean.com/.

=== Configuration ===

The configuration file may be used to define various behaviors for
svndigest. By default svndigest looks for a config file named `config`
in directory `<root-dir>/.svndigest` (<root-dir> is directory set by
option `--root`). If no such file is found, svndigest behaves as
default. Which file to be used as config file may also be set with
option `--config-file`. This option can also be used to avoid picking
up a configuration file in `<root-dir>/.svndigest`; issue
`--config-file=/dev/null` and the default configuration will be
used. To update an old config file you can use the option
`--generate-config`. With this option set the used configuration 
will be written to standard output. If new variables has
been added to configuration of svndigest, these variables will be
written with default values. Note: if you have added a comment (e.g. a
subversion keyword) to your old config file, this will not be
inherited to the new config file. To preserve such a comment you must
merge the new config file with the old config file manually.

The color used for each author in plots and blame output can be
configured in section [author-color] in config file. The format of the
color string is a 6-digit hexadecimal number in which each color (red,
green and blue) is decribed by 2 digits, i.e. each color is described
by a value between 0 and 255 in hexadecimal format. It is also allowed
to set the color with a 3-digit hexadecimal number in which case, for
example, 'a5b' would be equivalent to 'aa55bb'.

The format of images in the report can be configured in section
[image]. Allowed formats are `png`, `svg`, and `none` where the latter
implies that images are not created.

In section [output] you can adjust the size of the report. Default a
Blame Information page is created for every file, which can be turned
of using the 'blame-information' option. To decrease the size even
more, option 'file' can be set to 'no' which means output will only be
generated for directories.

Section [svn-props] allows you to set properties on files (or
directories). It is, for example, possible to set property
'svndigest:ignore' on all files named `COPYING` using the following line

COPYING = svndigest:ignore

The format of this section is the same as in section [auto-props] in a
subversion config file.

=== TracLinks ===

From svndigest 0.6 there is support for TracLinks. The root trac
location may be set in the configuration, in which case various links
to the trac site will be included in the resulting report. This
includes links from revision number to the corresponding revision at
the trac site, and various kinds of links are detected in the messages
in `Recent Log`.

=== Prerequisites ===

Svndigest runs against a working copy (WC), i.e., svndigest will not
run directly against a repository. Svndigest requires that the WC is
pristine before running the analysis, i.e., no files are allowed to be
modified. We also recommend that the WC is in synch with the
repository. Issue `svn update` before running svndigest.

= About svncopyright =

svncopyright updates the copyright statement in subversion controlled
files. The program detects the copyright statement and replaces it
with a copyright statement calculated from repository statistics. 

The copyright statement is detected as the first line containing the
string given in configuation variable `copyright-string` (default
'`Copyright (C)`'). The copyright statement ends with the first
following line containing no alphanumerical characters excluding the
''prefix'' string that preceeds '`copyright-string`'. This copyright
statement block is replaced with a new copyright statement generated
from analyzing `svn blame output`. An author is considered to have
copyright of the file if (s)he has added a line of code or comment
(excluding copyright statements). For an example of the format of the
generated copyright statement, please have a look at the top of this
file. 

By default the `svn user name` of the author is printed in the
copyright statement. This may be overridden by setting a
'`copyright-alias`' in the config file. In svndigest, for example,
user name `jari` is set to copyright-alias ''Jari Häkkinen'' and user
name `peter` is set to copyright-alias ''Peter Johansson''. If two (or
several) authors are given the same copyright-alias they are
considered as one person in the copyright statement (i.e. their alias
is not repeated). This may be useful if you want to give copyright to
an organization rather than to individual developers.

Files and directories that have `svndigest:ignore` or
`svncopyright:ignore` set are excluded from the copyright update.

= About svndigest-copy-cache =

As explained above, statistics are cached during a run and svndigest
only communicates with the server to retrieve statistics of the
activity since last run. Subsequent runs are much faster why it is
prefereble to run svndigest in a wc with recent cache available.

Sometimes it is useful to copy the cache from one wc to another, and
an easy way to do this is to use the tool `svndigest-copy-cache`,
which is shipped with svndigest. A common use case is when creating,
say, a release branch from the trunk, in which case you likely will
have two working copies to follow the two branches. If you want to run
svndigest on both of them and you want to avoid the first expensive
run (without cache) you can copy the cache from the trunk wc to the
release-branch wc.

As explained in the caching section, it is important that the cache
reflects a revision that belong to the history of the wc. So, for
example, when creating a branch it is crucial to copy the cache from
the trunk wc to the new branch wc before running svndigest on trunk
wc. Otherwise the cache will contain information about changesets that
were committed after the new branch were forked out. This will not
only be untrue as these changesets are not in the line of history of
the branch, but it will also cause problems for svndigest when
calculating the statistics of the most recent revisons and result is
undefined due to inconsistency between the cache and the log. For an
example of how `svndigest-copy-cache` may be used please refer to the
ReleaseProcedure of svndigest.

`svndigest-copy-cache` copies the cache file from `ROOT` tree to the
corresponding directory in `TARGET` tree. The tree structure of the
two working copies must be identical because `svndigest-copy-cache`
will not create any directories in `TARGET` tree except that
`.svndigest` directories are created when necessary.

Also, `svndigest-copy-cache` is ignorant about svn wc and will parse
through `ROOT` directory and all its sub-directories, looking for
svndigest cache files, regardless whether they belong to the same svn
working copy or not. Therefore, having other svn wc in subdirectories
of `ROOT` might give an unexpected result.
