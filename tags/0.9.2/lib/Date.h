#ifndef _theplu_svndigest_date_
#define _theplu_svndigest_date_

// $Id: Date.h 978 2009-12-12 20:09:41Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <ctime>
#include <vector>
#include <string>

namespace theplu{
namespace svndigest{

	///
	/// Class taking care of time and date handling. 
	///
	class Date 
	{
	public:
		///
		/// @brief Constructor
		///
		Date(void);

		///
		/// @brief Copy Constructor
		///
		Date(const Date&);

		///
		/// @brief Constructor from svn date string
		///
		/// @see svntime(std::string)
		///
		Date(std::string);

		///
		/// @return string describing distance between two dates
		///
		std::string difftime(const Date& other) const;

		///
		/// @brief sets time to gmtime
		///
		void gmtime(void);

		///
		/// @brief sets time to local time
		///
		void localtime(void);

		///
		/// \brief sets time from a svn time string
		///
		/// \parameter str string in format 2006-09-09T10:55:52.132733Z
		///
		void svntime(std::string str);

		///
		/// \return seconds since 00:00:00 Jan 1 1970
		///
		inline time_t seconds(void) const { return time_; }

		///
		/// \a format sets the format of the returned string. Supported
		/// parameters are:
		/// %a - short string of weekday e.g. Wed 
		/// %A - long string of weekday e.g. Wednesday
		/// %b - short string of month e.g. Apr 
		/// %B - long string of weekday e.g. April
		/// %d - day of month integer e.g. 9
		/// %e - day of month integer e.g. 09
		/// %H - hours [0,23]
		/// %I - hours [1,12]
		/// %j - day of the year [1,366]
		/// %m - month integer e.g. 04
		/// %M - minutes after the hour [0,59]
		/// %P - AM or PM
		/// %S - seconds efter the minute [0,59]
		/// %w - day of week [1,7] Sunday is 7
		/// %y - year in two digits
		/// %Y - year in four digits
		/// %Z - timezone abbrevation
		///
		/// \return date and time 
		///
		std::string operator()(std::string format) const;

		///
		/// @brief assignment operator
		///
		/// @return const reference to assigned Date
		///
		const Date& operator=(const Date&);

		///
		/// @return true if lhs is earlier than \a rhs
		///
		inline bool operator<(const Date& rhs) const 
		{ return time_<rhs.time_; }

	private:
		time_t time_;
	};

}}
#endif
