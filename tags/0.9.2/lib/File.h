#ifndef _theplu_svndigest_file_
#define _theplu_svndigest_file_

// $Id: File.h 1267 2010-11-02 03:56:19Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

#include <map>
#include <string>

namespace theplu{
namespace svndigest{

	class NodeVisitor;

	class File : public Node
	{
	public:
		/// 
		/// @brief Default Constructor 
		/// 
		File(const unsigned int level, const std::string& path, 
				 const std::string& output=""); 

		/**
			 For example 'File.h.html'

			 @return href to this file
		*/
		std::string href(void) const;

		/**
			 \brief Get the revision number of the latest commit.
		*/
		svn_revnum_t last_changed_rev(void) const;

		/**
			 @return The explicit string "file", nothing else.
		*/
		std::string node_type(void) const;

		/**
			 @return output path for example 'lib/File.h.html' for this file
		 */
		std::string output_path(void) const;

		///
		/// @brief Parsing out information from svn repository
		///
		/// @return Stats object of the file
		///
		const StatsCollection& parse(bool verbose, bool ignore);

		/**
			 Let the visitor perform its mission via visitor(*this)
		 */
		void traverse(NodeVisitor& visitor);

	private:
		std::string blame_output_file_name(void) const;

		/**
			 do nothing
		*/
		void log_core(SVNlog&) const;

		///
		/// @brief Copy Constructor, not implemented
		///
		File(const File&);

		///
		/// @brief Parsing svn blame output
		///
		/// @return true if parsing is succesful
		///
		bool blame(void) const;

		/**
			 @brief Print blame output 
		*/
		void print_blame(std::ofstream& os) const;

		void print_core(bool verbose=false) const;

		///
		/// print page for specific user (or all) and specific line_style
		/// (or total).
		///
		void print_core(const std::string& stats_type, const std::string& user, 
										const std::string& line_type, const SVNlog&) const; 

	};

}} // end of namespace svndigest and namespace theplu

#endif


