// $Id: CopyrightVisitor.cc 1239 2010-10-23 23:57:56Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CopyrightVisitor.h"

#include "Configuration.h"
#include "Directory.h"
#include "File.h"
#include "NodeVisitor.h"

namespace theplu {
namespace svndigest {

	CopyrightVisitor::CopyrightVisitor(std::map<std::string, Alias>& alias, 
																		 bool verbose,
																		 const std::map<int,svn_revnum_t>& year2rev,
																		 bool ignore_cache)
		: NodeVisitor(), alias_(alias), verbose_(verbose), year2rev_(year2rev),
			ignore_cache_(ignore_cache)
	{}


	std::string 
	CopyrightVisitor::copyright_block(const std::map<int, std::set<Alias> >& year_authors,
																		const std::string& prefix) const
	{
		using namespace std;
		stringstream ss;
		for (map<int, set<Alias> >::const_iterator i(year_authors.begin());
				 i!=year_authors.end();) {
			ss << prefix << Configuration::instance().copyright_string() << " "
				 << 1900+i->first;
			map<int, set<Alias> >::const_iterator j = i;
			assert(i!=year_authors.end());
			while (++j!=year_authors.end() && 
						 i->second == j->second){
				ss << ", " << 1900+(j->first);
			}
			// printing authors
			std::vector<Alias> vec_alias;
			back_insert_iterator<std::vector<Alias> > ii(vec_alias);
			std::copy(i->second.begin(), i->second.end(), ii);
			// sort with respect to id
			std::sort(vec_alias.begin(), vec_alias.end(), IdCompare());
			for (std::vector<Alias>::iterator a=vec_alias.begin();
					 a!=vec_alias.end(); ++a){
				if (a!=vec_alias.begin())
					ss << ",";
				ss << " " << a->name();
			}
			ss << "\n";
			i = j;
		}
		return ss.str();
	}


	void CopyrightVisitor::create_year2alias(std::map<int, std::set<Alias> >& m,
																					 const File& file)
	{
		using namespace std;
		const Stats& stats = file.stats()["add"];

		// loop over all years
		for (std::map<int, svn_revnum_t>::const_iterator rev_iter=year2rev_.begin();
				 rev_iter!=year2rev_.end(); ++rev_iter) {

			svn_revnum_t last_rev_this_year = rev_iter->second;
			svn_revnum_t last_rev_last_year = 0;
			if (rev_iter != year2rev_.begin()) {
				last_rev_last_year = (--rev_iter)->second;
				++rev_iter;
			}
			// do not go beyond BASE rev of file
			last_rev_this_year = std::min(last_rev_this_year,file.last_changed_rev());
			last_rev_last_year = std::min(last_rev_last_year,file.last_changed_rev());
			// loop over authors
			for (std::set<std::string>::const_iterator a_iter=stats.authors().begin();
					 a_iter!=stats.authors().end(); ++a_iter) {

				// check if anything has been added since last year
				if ( (stats(LineTypeParser::code, *a_iter, last_rev_this_year) >
							stats(LineTypeParser::code, *a_iter, last_rev_last_year)) || 
						 (stats(LineTypeParser::comment, *a_iter, last_rev_this_year) >
							stats(LineTypeParser::comment, *a_iter, last_rev_last_year)) ) {
				
				
					// find username in map of aliases
					std::map<string,Alias>::iterator name(alias_.lower_bound(*a_iter));
					
					// if alias exist insert alias
					if (name != alias_.end() && name->first==*a_iter)
						m[rev_iter->first].insert(name->second);
					else {
						// else insert user name
						Alias a(*a_iter,alias_.size());
						m[rev_iter->first].insert(a);
						std::cerr << "svncopyright: warning: no copyright alias found for `" 
											<< *a_iter << "'\n";
						// insert alias to avoid multiple warnings.
						alias_.insert(name, std::make_pair(*a_iter, a));
					}
				}
			}
		}
	}


	bool CopyrightVisitor::detect_copyright(const std::string& path,
																					std::string& block, 
																					size_t& start_at_line,
																					size_t& end_at_line, 
																					std::string& prefix) const
	{
		using namespace std;
		LineTypeParser parser(path);
		std::ifstream is(path.c_str());
		std::string line;
		while (std::getline(is, line)) 
			parser.parse(line);
		if (!parser.copyright_found())
			return false;
		block = parser.block();
		start_at_line = parser.start_line();
		end_at_line = parser.end_line();
		prefix = parser.prefix();
		return true;
	}


	bool CopyrightVisitor::enter(Directory& dir) 
	{
		if (dir.ignore() || dir.svncopyright_ignore())
			return false;
		return true;
	}
	

	void CopyrightVisitor::leave(Directory& dir) 
	{
	}
	

	void CopyrightVisitor::update_copyright(const File& file)
	{
		std::string old_block;
		size_t start_line=0;
		size_t end_line=0;
		std::string prefix;
		if (!detect_copyright(file.path(),old_block, start_line, end_line, prefix)){
			if (Configuration::instance().missing_copyright_warning())
				std::cerr << "svncopyright: warning: no copyright statement found in `" 
									<< file.path() << "'\n";
			return;
		}
		std::map<int, std::set<Alias> > map;
		create_year2alias(map, file);
		std::string new_block = copyright_block(map, prefix);
		if (old_block==new_block)
			return;
		if (verbose_)
			std::cout << "Updating copyright in '" << file.path() << "'" << std::endl; 
		update_copyright(file.path(), new_block, start_line, end_line);
	}


	void CopyrightVisitor::update_copyright(const std::string& path,
																					const std::string& new_block,
																					size_t start_at_line, 
																					size_t end_at_line) const
	{
		// embrace filename with brackets #filename#
		std::string tmpname = concatenate_path(directory_name(path),
																					 "#" + file_name(path) + "#");
		std::ofstream tmp(tmpname.c_str());
		assert(tmp);
		using namespace std;
		ifstream is(path.c_str());
		assert(is.good());
		string line;
		// Copy lines before block
		for (size_t i=1; i<start_at_line; ++i){
			assert(is.good());
			getline(is, line);
			tmp << line << "\n";
		}
		// Printing copyright statement
		tmp << new_block;
		// Ignore old block lines
		for (size_t i=start_at_line; i<end_at_line; ++i){
			assert(is.good());
			getline(is, line);
		}
		// Copy lines after block
		while(is.good()) {
			char ch=is.get();
			if (is.good())
				tmp.put(ch);
		}

		is.close();
		tmp.close();
		
		// finally rename file 
		rename(tmpname, path);
	}

	void CopyrightVisitor::visit(File& file)
	{
		if (file.ignore() || file.svncopyright_ignore())
			return;
		file.parse(verbose_, ignore_cache_);
		update_copyright(file);
		file.stats().reset();
	}

}} // end of namespace svndigest and namespace theplu
