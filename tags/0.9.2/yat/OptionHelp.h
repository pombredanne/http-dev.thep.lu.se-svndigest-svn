#ifndef _theplu_yat_utility_option_help_
#define _theplu_yat_utility_option_help_

// $Id: OptionHelp.h 2119 2009-12-12 23:11:43Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "OptionSwitch.h"

#include <string>

namespace theplu {
namespace yat {
namespace utility {

	class CommandLine;
	/**
		 \brief Class for help option

		 When this option is found in parsing of commandline, it displays
		 a help output and exits. A typical output looks like:

		 \verbatim
		 Usage: foo [OPTION]...

		 SYNOPSIS

		 Available options are:
		 -h, --help      display this help and exit
		 -v, --verbose   explain what is being done

		 Some extra information explaining something.
		 \endverbatim

		 The output consist of four blocks. The first block can be
		 modified with usage(void) function. The second is block is
		 typically a one-liner explaining the application and can be
		 modified with the synopsis(void) function. The third block
		 contain the description of the different options, see
		 operator<<(std::ostream&, const CommandLine&) for further
		 details. The last block can be modified with post_arguments(void)
		 function.
	 */
	class OptionHelp : public OptionSwitch
	{
	public:
		/**
			 \brief Constructor 
			 
			 \param cmd Commandline Option is associated with
			 \param name string such as "help" for --help, "h" for -h or
			 "h,help" (default) for having both short and long option name
			 \param desc string used in help display
		*/
		OptionHelp(CommandLine& cmd, std::string name="h,help", 
							 std::string desc="display this help and exit"); 

		/**
			 Text to be displayed after list of arguments.
		 */
		std::string& post_arguments(void);

		/**
			 Text to be displayed in help output after usage()
		*/
		std::string& synopsis(void);

		/**
			 First line to be displayed in help output. Default this string
			 is set to "Usage: <program_name> [OPTION]...\n\n"
		 */
		std::string& usage(void);

	private:
		std::string usage_;
		std::string synopsis_;
		std::string post_cmd_;

		/**
		 */
		void do_parse2(std::vector<std::string>::iterator, 
									 std::vector<std::string>::iterator);

	};

}}} // of namespace utility, yat, and theplu

#endif
