// $Id: Gnuplot.cc 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Gnuplot.h"

#include <cstdio>
#include <string>
#include <unistd.h>

#include <iostream>


namespace theplu {
namespace svndigest {


	Gnuplot::Gnuplot(void)
		: linestyle_("steps"), pipe_(NULL)
	{
		acquire_program_path("gnuplot");
		if (gnuplot_binary_.empty())
			throw GnuplotException("Can't find gnuplot in your PATH");

		pipe_=popen(gnuplot_binary_.c_str(),"w");
		if (!pipe_)
			throw GnuplotException("Could not open connection to gnuplot");
	}


	Gnuplot::~Gnuplot(void)
	{
		if (pclose(pipe_) == -1)
			throw GnuplotException("Problem closing communication to gnuplot");
	}


	void Gnuplot::acquire_program_path(const std::string& progname)
	{
		char* env_path=getenv("PATH");	// is there a need to free this memory?
		if (!env_path)
			throw GnuplotException("Environment variable PATH is not set");

		std::list<std::string> paths;
		tokenizer(env_path,paths);
		for (std::list<std::string>::const_iterator i=paths.begin();
				 i!=paths.end(); ++i) {
			std::string tmp((*i) + '/' + progname);
			if (!access(tmp.c_str(),X_OK)) {
				gnuplot_binary_=tmp;
				break;
			}
		}

		if (gnuplot_binary_.empty())
			throw GnuplotException("Cannot find '" + progname + "' in PATH");
	}

	
	int Gnuplot::command(const std::string& cmdstr)
	{
		if (fputs(cmdstr.c_str(),pipe_)==EOF)
			return EOF;
		if ((*(cmdstr.rbegin())!='\n') && (fputc('\n',pipe_)==EOF))
			return EOF;
		return fflush(pipe_);
	}


	void Gnuplot::tokenizer(const std::string& in,
													std::list<std::string>& tokens,
													const std::string& delimiters)
	{
		std::string::size_type previous_pos=in.find_first_not_of(delimiters,0);
		std::string::size_type position=in.find_first_of(delimiters,previous_pos);
		while ((std::string::npos!=position) || (std::string::npos!=previous_pos)) {
			tokens.push_back(in.substr(previous_pos, position-previous_pos));
			previous_pos=in.find_first_not_of(delimiters,position);
			position=in.find_first_of(delimiters,previous_pos);
		}
	}


}} // end of namespace svndigest and namespace theplu
