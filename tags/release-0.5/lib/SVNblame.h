#ifndef _theplu_svndigest_svnblame_
#define _theplu_svndigest_svnblame_

// $Id: SVNblame.h 165 2006-08-24 07:38:19Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <string>
#include <vector>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	///
	/// The SVNblame class is a utility class for taking care of 'svn
	/// blame' information. An 'svn blame' is performed on an item, the
	/// blame information for each line is traversed with
	/// SVNblame.next() calls giving access to the blame information in
	/// a blame_information struct.
	///
	class SVNblame {
	public:

		///
		/// @brief Information return by subversion (blame) API
		///
		/// @see Subversion API for blame usage.
		///
		struct blame_information {
			apr_int64_t line_no;
			svn_revnum_t revision;
			std::string author;
			std::string date;
			std::string line;
		};

		///
		/// @brief The contructor.
		///
		/// The constructor performs an 'svn blame' on \a path and
		/// initializes the SVNblame object for statistics traversal using
		/// SVNblame::next().
		///
		explicit SVNblame(const std::string& path);

		///
		/// @brief The destructor.
		///
		~SVNblame(void);

		///
		/// @brief Returns true if item is binary false otherwise
		///
		/// Binary files are invalid for 'svn blame'.
		///
		inline bool binary(void) { return binary_; }

		///
		/// Traverse all lines in the item for statistics analysis. There
		/// is currently no way to reset the pointer to the beginning of
		/// item. The pointer is only valid until the SVNblame object is
		/// in scope.
		///
		/// @see struct blame_information.
		///
		const blame_information* next(void);

	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNblame(const SVNblame&);

		// binary_ is true if item in any revision has been binary.
		bool binary_;
		// blame_info_iterator_ is used in statistics analysis to traverse
		// blame_receiver_baton.blame_info vector through calls to next().
		std::vector<blame_information*>::iterator blame_info_iterator_;
		SVN* instance_;

		struct blame_receiver_baton_ {
			std::vector<blame_information*> blame_info;
		} blame_receiver_baton_ ;

		static svn_error_t *
		blame_receiver(void *baton, apr_int64_t line_no, svn_revnum_t revision,
									 const char *author, const char *date, const char *line,
									 apr_pool_t *pool);
	};

}} // end of namespace svndigest and namespace theplu

#endif
