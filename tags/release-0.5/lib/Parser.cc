// $Id: Parser.cc 184 2006-09-05 11:19:53Z peter $

/*
	Copyright (C) 2006 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parser.h"
#include "utility.h"

#include <algorithm>
#include <functional>
#include <fstream>
#include <iostream>
#include <string>

namespace theplu{
namespace svndigest{


	Parser::Parser(const std::string& path)
	{
		std::ifstream is(path.c_str());
		cc_mode(is);
		is.close();
	}


	void Parser::cc_mode(std::istream& is)
	{
		bool block_com=false;
		std::string str;
		while(getline(is,str)) {
			bool line_com=false;
			line_type lt=empty;
			for (std::string::iterator iter=str.begin(); iter!=str.end(); ++iter){
				if (!block_com && match_begin(iter,str.end(), "/*")){
					block_com=true;
					continue;
				}
				if (block_com && match_begin(iter,str.end(), "*/")){
					block_com=false;
					continue;
				}
				if (block_com){
					if (line_com){
						if (lt==empty && isalnum(*iter))
							lt = comment;
					}
					else if (lt==empty) {
						if (match_begin(iter, str.end(), "//"))
							line_com=true;
						else if (isalnum(*iter))
							lt = comment;
					}
				}
				else if (lt==empty){
					if (isalnum(*iter)){
						if (line_com){
							lt = comment;
						}
						else{
							lt = code;
						}
					}
					else if (match_begin(iter, str.end(), "//")){
						line_com=true;
					}
					else if (!line_com && !isspace(*iter)){
						lt = code;
					}

				}
			}
			type_.push_back(lt);
		}
	}


}} // end of namespace svndigest and namespace theplu
