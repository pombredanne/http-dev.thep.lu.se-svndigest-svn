#ifndef _theplu_svndigest_file_
#define _theplu_svndigest_file_

// $Id: File.h 185 2006-09-06 02:39:18Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Node.h"

#include <string>

namespace theplu{
namespace svndigest{

	class File : public Node
	{
	public:
		/// 
		/// @brief Default Constructor 
		/// 
		File(const u_int level, const std::string& path, 
				 const std::string& output="") 
			: Node(level,path,output) {}

		///
		/// @return href to this file
		///
		std::string href(void) const;

		///
		/// @return file
		///
		const std::string node_type(void) const;

		///
		/// @brief Parsing out information from svn repository
		///
		/// @return true if succesful
		///
		const Stats& parse(const bool verbose=false);

		///
		///
		///
		void print(const bool verbose=false) const;

	private:

		///
		/// @brief Parsing svn blame output
		///
		/// @return true if parsing is succesful
		///
		bool blame(void) const;

		///
		/// @brief Copy Constructor, not implemented
		///
		File(const File&);

	};

}} // end of namespace svndigest and namespace theplu

#endif


