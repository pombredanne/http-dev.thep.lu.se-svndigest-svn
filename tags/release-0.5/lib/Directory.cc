// $Id: Directory.cc 185 2006-09-06 02:39:18Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Directory.h"
#include "File.h"
#include "html_utility.h"
#include "Node.h"
#include "SVN.h"
#include "utility.h"

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>

#include <cerrno>	// Needed to check error state below.
#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	Directory::Directory(const u_int level, const std::string& path, 
											 const std::string& output)
		: Node(level,path,output)
	{
		using namespace std;
		DIR* directory=opendir(path.c_str());    // C API from dirent.h
		if (!directory)
			throw NodeException("ERROR: opendir() failed; " + path +
													" is not a directory");
		list<string> entries;
		struct dirent* entry;
		errno=0;	// Global variable used by C to track errors, from errno.h
		while ((entry=readdir(directory)))       // C API from dirent.h
			entries.push_back(string(entry->d_name));
		if (errno)
			throw NodeException("ERROR: readdir() failed on " + path);
		closedir(directory);

		SVN* svn=SVN::instance();
		for (list<string>::iterator i=entries.begin(); i!=entries.end(); ++i)
			if ((*i)!=string(".") && (*i)!=string("..") && (*i)!=string(".svn")) {
				string fullpath(path_+'/'+(*i));
				switch (svn->version_controlled(fullpath)) {
				case SVN::uptodate:
					struct stat nodestat;                // C api from sys/stat.h
					lstat(fullpath.c_str(),&nodestat);   // C api from sys/stat.h
					if (S_ISDIR(nodestat.st_mode))       // C api from sys/stat.h
						daughters_.push_back(new Directory(level_+1,fullpath, 
																							 output_name()+"/"));
					else
						daughters_.push_back(new File(level_,fullpath,output_name()+"/"));
					break;
				case SVN::unresolved:
					throw NodeException(fullpath+" is not up to date");
				case SVN::unversioned: ; // do nothing
				}
			}
		daughters_.sort(NodePtrLess());
	}


	Directory::~Directory(void)
	{
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			delete *i;
	}

	bool Directory::dir(void) const
	{
		return true;
	}

	std::string Directory::href(void) const
	{ 
		return name() + "/index.html";
	}

	const Stats& Directory::parse(const bool verbose)
	{
		stats_.reset();
		// Directories themselved give no contribution to statistics.
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			if (!(*i)->ignore())
				stats_ += (*i)->parse(verbose);
		return stats_;
	}


	const std::string Directory::node_type(void) const
	{
		return std::string("directory");
	}


	void Directory::print(const bool verbose) const
	{
		mkdir(output_name());
		std::string output(output_name() + "/index.html");
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		std::ofstream os(output.c_str());
		print_header(os);
		os << "<p align=center>\n<img src='" 
			 << file_name(stats_.plot(output_name()+"/index.png", output_name()))
			 << "' alt='[plot]' border=0><br>\n";
		os << "<table class=\"listings\">\n";
		os << "<thead>";
		os << "<tr>\n";
		os << "<th>Node</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Author</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>";

		bool dark=false;
		if (level_){
			os << "<tr class=\"light\">\n";
			os << "<td class=\"directory\" colspan=\"6\">";
			anchor(os, "../index.html", "../");
			os << "</td>\n</tr>\n";
			dark=!dark;
		}

		// print html links to daughter nodes
		for (NodeConstIterator d = daughters_.begin(); d!=daughters_.end(); ++d) {
			if (dark)
				(*d)->html_tablerow(os,"dark");
			else
				(*d)->html_tablerow(os,"light");
			dark = !dark;
		}
		if (dark)
			os << "<tr class=\"dark\">\n";
		else
			os << "<tr class=\"light\">\n";
		os << "<td>Total</td>\n";
		os << "<td>" << stats_.lines() << "</td>\n";
		os << "<td>" << stats_.code() << "</td>\n";
		os << "<td>" << stats_.comments() << "</td>\n";
		os << "<td>" << stats_.last_changed_rev() << "</td>\n";
		os << "<td>" << author() << "</td>\n";
		os << "</tr>\n";
		os << "</table>\n";
		os << "</p>\n";
		print_footer(os);
		os.close();	

		// print daughter nodes, i.e, this function is recursive
		std::for_each(daughters_.begin(), daughters_.end(),
									std::bind2nd(std::mem_fun(&Node::print),verbose));
	}

}} // end of namespace svndigest and namespace theplu
