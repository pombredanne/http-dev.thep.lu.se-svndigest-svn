#ifndef _theplu_svndigest_utility_
#define _theplu_svndigest_utility_

// $Id: utility.h 177 2006-09-02 04:30:57Z peter $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <algorithm>
#include <functional>
#include <iosfwd>
#include <string>
#include <utility>
#include <vector>

#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	///
	/// @brief Check if access permissions match \a mode. \a mode must
	/// be given as r, w, x, or combinations of these letters.
	///
	/// @return On success (all requested permissions granted), zero
	/// is returned. On error (at least one bit in mode asked for a
	/// permission that is denied, or some other error occurred), -1
	/// is returned, and errno is set appropriately.
	///
	/// @throw An std::runtime_error is thrown when checking for write
	/// permissions for a file/direcotry that does not exist.
	///
	/// @see access(2)
	///
	int access_rights(const std::string& path,const std::string& bits);

	///
	/// @return everything after last '/'
	///
	std::string file_name(const std::string&);

	inline bool match_begin(std::string::iterator first, 
													std::string::iterator last, 
													const std::string& str)
	{ return (std::distance(first, last)>=static_cast<int>(str.size()) && 
						std::equal(str.begin(), str.end(), first)); 
	}

	inline bool match_end(std::string::iterator first, 
												std::string::iterator last, 
												const std::string& str)
	{ return (std::distance(first,last)>=static_cast<int>(str.size()) && 
						std::equal(str.rbegin(), str.rend(), first)); 
	}

	///
	/// Create directory \a dir. The call can fail in many ways, cf. 'man
	/// mkdir'.
	///
	inline int mkdir(const std::string& dir) { return ::mkdir(dir.c_str(),0777); }

	///
	/// @return the current working directory.
	///
	std::string pwd(void);

	inline std::string::iterator search(std::string::iterator& first, 
																			std::string::iterator& last, 
																			const std::string& str)
	{ return std::search(first, last, str.begin(), str.end()); }

	///
	/// Calculating sum of two vectors.
	///
	/// @return resulting vector
	///
	template <typename T >
	struct VectorPlus : 
		public std::binary_function<std::vector<T>,std::vector<T>,std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& u,
															const std::vector<T>& v) const 
		{
			if ( u.size() > v.size() ){
				std::vector<T> res(u.size());
				transform(u.begin(), u.end(), v.begin(), res.begin(), std::plus<T>());
				copy(u.begin()+v.size(), u.end(), res.begin()+v.size());
				return res;
			}
	
			std::vector<T> res(v.size());
			transform(v.begin(), v.end(), u.begin(), res.begin(), std::plus<T>());
			if ( v.size() > u.size() )
				copy(v.begin()+u.size(), v.end(), res.begin()+u.size());
			return res;
		}

	};

	///
	/// @return resulting vector
	///
	template <typename Key, typename T>
	struct PairValuePlus :
		public std::binary_function<std::vector<T>,
																std::pair<const Key, std::vector<T> >, 
																std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& sum, 
															const std::pair<const Key,std::vector<T> >& p)
		{
			return VectorPlus<T>()(sum, p.second);
		}
	};

}} // end of namespace svndigest end of namespace theplu

#endif 
