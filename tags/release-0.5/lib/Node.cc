// $Id: Node.cc 185 2006-09-06 02:39:18Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Node.h"
#include "html_utility.h"
#include "SVNproperty.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <ctime>
#include <fstream>
#include <sstream>

namespace theplu{
namespace svndigest{

	Node::Node(const u_int level, const std::string& path, 
						 const std::string& output="")
		: level_(level), path_(path), stats_(path), svninfo_(path)
	{ 
		SVNproperty property(path);
		binary_=property.binary();
		svndigest_ignore_=property.svndigest_ignore();
		output_name_ = output+file_name(path_); 
	}


	bool Node::dir(void) const
	{
		return false;
	}


	void Node::path_anchor(std::ostream& os) const
	{
		std::vector<std::string> words;
		words.reserve(level_+1);
		std::string word;
		std::stringstream ss(output_name());
		while(getline(ss,word,'/'))
			words.push_back(word);
		for (size_t i=0; i<words.size()-1; ++i){
			anchor(os,"index.html", words[i], level_-i, "View " + words[i]);
			os << "<span class=\"sep\">/</span>\n";
		}
		anchor(os,href(), words.back(), level_+2-words.size(), 
					 "View " + words.back()); 
	}


	void Node::html_tablerow(std::ostream& os, 
													 const std::string& css_class) const
	{
		os << "<tr class=\"" << css_class << "\">\n"
			 << "<td class=\"" << node_type() << "\">";
		if (svndigest_ignore())
			os << name() << " (<i>svndigest:ignore</i>)";
		else if (binary())
			os << name() << " (<i>binary</i>)";
		else
			anchor(os,href(), name()); 
		os << "</td>\n" 
			 << "<td>" << stats_.lines() << "</td>\n"
			 << "<td>" << stats_.code() << "</td>\n"
			 << "<td>" << stats_.comments() << "</td>\n"
			 << "<td>" << stats_.last_changed_rev() << "</td>\n"
			 << "<td>" << author() << "</td>\n"
			 << "</tr>\n";
	}


	void Node::print_footer(std::ostream& os) const
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo =  gmtime ( &rawtime );
		os << "<p align=center><font size=-2>\nGenerated on "
			 << asctime (timeinfo) << " (UTC) by ";
		anchor(os, "http://lev.thep.lu.se/trac/svndigest/", PACKAGE_STRING);
		os << "</font>\n</p>\n</div>\n</body>\n</html>\n";
	}


	void Node::print_header(std::ostream& os) const
	{
		os << "<!DOCTYPE html\n"
			 << "PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
			 << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			 << "<html xmlns=\"http://www.w3.org/1999/xhtml\""
			 << " xml:lang=\"en\" lang=\"en\"><head>\n"
			 << "<head>\n"
			 << "<title> svndigest " << name() << "</title>\n"
			 << "</head>\n"
			 << "<link rel=\"stylesheet\" "
			 << "href=\"";
		for (u_int i=0; i<level_; ++i)
			os << "../";
		os << "svndigest.css\" type=\"text/css\" />\n"
			 << "<body>\n"
			 << "<div id=\"menu\">"
			 << "<ul><li></li>"
			 << "<li>";
		anchor(os, "index.html", "Home", level_, "Main page");
		os << "</li>"
			 << "</ul></div>"
			 << "<div id=\"main\">\n"
			 << "<h2 class=\"path\">\n";
		path_anchor(os);
		os << "\n</h2>\n";
	}

}} // end of namespace svndigest and namespace theplu
