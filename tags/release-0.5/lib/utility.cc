// $Id: utility.cc 177 2006-09-02 04:30:57Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "utility.h"

#include <fstream>
#include <iostream> // remove this when 'blame' is removed
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/param.h>
#include <unistd.h>

namespace theplu{
namespace svndigest{

	int access_rights(const std::string& path, const std::string& bits)
	{
		if (access(path.c_str(),F_OK)) {
			throw std::runtime_error(std::string("access_rights: ") + path +
															 "' does not exist.");
		}
		int mode=0;
		for (u_int i=0; i<bits.length(); ++i)
			switch (bits[i]) {
					case 'r':
						mode|=R_OK;
						break;
					case 'w':
						mode|=W_OK;
						break;
					case 'x':
						mode|=X_OK;
						break;
			}
		return access(path.c_str(),mode);
	}


	std::string	file_name(const std::string& full_path)
	{
		std::stringstream ss(full_path);
		std::string name;
		while (getline(ss,name,'/')) {}
		return name;
	}


	std::string pwd(void)
	{
		char buffer[MAXPATHLEN];
		if (!getcwd(buffer, MAXPATHLEN))
			throw std::runtime_error("Failed to get current working directory");
		return std::string(buffer);
	}

}} // end of namespace svndigest and namespace theplu
