#ifndef _theplu_svndigest_html_utility_
#define _theplu_svndigest_html_utility_

// $Id$

/*
	Copyright (C) 2006 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <iostream>
#include <fstream>
#include <iosfwd>
#include <string>

namespace theplu{
namespace svndigest{

	///
	/// @brief send anchor to stream @a os
	///
	/// @param address to link to
	/// @param name text visible on page
	/// @param level '../' is added @a level times before @a href
	/// @param title title of anchor
	///
	void anchor(std::ostream& os, const std::string& href, 
							const std::string& name, u_int level=0, 
							const std::string& title="");

	///
	/// Function translating text to html format. Most character are
	/// just passing through (unary operation). 
	///
	void html(std::istream&, std::ostream&, char delim='\n');

	///
	/// @printing cascading style sheet to stream @a s.
	///
	void print_css(std::ostream& s);

}} // end of namespace svndigest end of namespace theplu

#endif 
