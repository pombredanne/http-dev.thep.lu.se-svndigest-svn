#ifndef _theplu_svndigest_node_
#define _theplu_svndigest_node_

// $Id: Node.h 185 2006-09-06 02:39:18Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "html_utility.h"
#include "Stats.h"
#include "SVNinfo.h"
#include "utility.h"

#include <ostream>
#include <stdexcept>
#include <string>

namespace theplu{
namespace svndigest{

	///
	/// If something goes wrong in the use of the Node or its derived
	/// classes, a NodeException is thrown.
	///
	struct NodeException : public std::runtime_error
	{ inline NodeException(const std::string& msg) : runtime_error(msg) {} };

  ///
  /// Abstract Base Class for files.
  ///
  class Node
  {
  public:

    /// 
    /// @brief Constructor 
    /// 
		Node(const u_int, const std::string&, const std::string&);

		///
		/// @brief Destructor
		///
		virtual inline ~Node(void) {};

		///
		/// @brief Get the author of the latest commit.
		///
		inline const std::string& author(void) const
		{ return svninfo_.last_changed_author(); }

		/**
			 @brief Check whether node is binary.

			 @return True if node is binary.
		*/
		inline bool binary(void) const { return binary_; }

		///
		/// @return true if directory
		///
		virtual bool dir(void) const;

		///
		/// @return href to this node
		///
		virtual std::string href(void) const=0;

		void html_tablerow(std::ostream&, const std::string&) const;

		/**
			 @brief Check whether node should be ignored in statistics.

			 If a node is to be ignored the statistics implementer should
			 respect this state. Currently binary files and items with
			 property svndigest:ignore are to be ignored by svndigest. If
			 the node is a directory then the direcotry and its siblings
			 should be ignored by statistics.

			 @return True of node should be ignored by statistics.

			 @see SVNproperty::svndigest_ingorable
		*/
		inline bool ignore(void) const { return binary_ || svndigest_ignore_; }

		///
		/// @brief Get the revision number of the latest commit.
		///
		inline svn_revnum_t last_changed_rev(void) const
		{ return svninfo_.last_changed_rev(); }

		///
		/// @return file or directory
		///
		virtual const std::string node_type(void) const=0;

		///
		/// @return 
		///
		inline const std::string& output_name(void) const { return output_name_; }

		///
		/// @brief parsing file using svn blame.
		///
		virtual const Stats& parse(const bool verbose=false)=0;

		///
		/// @todo doc
		/// 
		inline const std::string& path(void) const { return path_; }

		///
		/// Function printing HTML in current working directory
		///
		virtual void print(const bool verbose=false) const=0;

		/**
			 @brief Check if item used to create this object has been
			 assigned property svndigest:ignore.

			 Currently files with property svndigest:ignore are to be
			 ignored by svndigest. It is the responsibility of the
			 statistics implementer to obey the ignore state.

			 @return True if item property svndigest:digest was set.
		*/
		inline bool svndigest_ignore(void) const { return svndigest_ignore_; }

	protected:

		///
		/// Function returning everything after the last '/'
		///
		/// @return name of node (not full path)
		///
		inline std::string name(void) const { return file_name(path_); }

		///
		/// @brief print html footer of page
		///
		void print_footer(std::ostream&) const;
		
		///
		/// @brief print html header of page
		///
		void print_header(std::ostream&) const;

		u_int level_;
		std::string output_name_; //without suffix
		std::string path_;
		Stats stats_;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		Node(const Node&);

		///
		/// print path in html format (with anchors) to @a os
		///
		void path_anchor(std::ostream& os) const; 

		bool binary_;
		bool svndigest_ignore_;
		SVNinfo svninfo_;
	};

	struct NodePtrLess
	{
		inline bool operator()(const Node* first, const Node* second) const
		{		
			if (first->dir()==second->dir())
				return first->output_name()<second->output_name();
			return first->dir();
		}
	};

}} // end of namespace svndigest and namespace theplu

#endif
