// $Id$

/*
	Copyright (C) 2006 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "utility.h"

#include <fstream>
#include <iostream> // remove this when 'blame' is removed
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/param.h>
#include <unistd.h>

namespace theplu{
namespace svndigest{

	void anchor(std::ostream& os, const std::string& href, 
							const std::string& name, u_int level, 
							const std::string& title)
	{
		os << "<a title=\"" << title << "\" href=\"" ;
		for (size_t i=0; i<level; ++i)
			os << "../";
		os << href << "\">" << name << "</a>";
	}


	
	void html(std::istream& is, std::ostream& os, char delim) 
	{
		char c;
		while (true){
			is.get(c);
			if (c==delim || !is.good()){
				return;
			}
			if (c=='"')
				os << '\"';
			else if (c=='\'')
				os << "\'";
			else if (c=='\n')
				os << "<br/>";
			else if (c=='<')
				os << "&lt;";
			else if (c=='>')
				os << "&gt;";
			else if (c=='&')
				os << "&amp;";
			else if (c=='\t')
				os << "&nbsp;&nbsp;";
			else if (c=='�')
				os << "&aring;";
			else if (c=='�')
				os << "&auml;";
			else if (c=='�')
				os << "&ouml;";
			else if (c=='�')
				os << "&Aring;";
			else if (c=='�')
				os << "&Auml;";
			else if (c=='�')
				os << "&Ouml;";
			else if (c=='�')
				os << "&eacute;";
			else
				os << c;
		}
	}
	

	void print_css(std::ostream& s)
	{
		s << "body {\n";
		s << " background: #fff; \n";
		s << " color: #000; \n";
		s << " margin: 0px; \n";
		s << " padding: 0; \n";
		s << "} \n";
		s << "\n";
		s << "#menu {\n";
		s << " background: #eee;\n";
		s << " width: 100%;\n";
		s << " margin: 0px;\n";
		s << " padding: 0px;\n";
		s << "}\n\n";
		s << "#menu ul\n";
		s << "{ \n";
		s << "padding: 0px;\n";
		s << "margin: 0px;list-style-type: none; text-align: center;"
			<< "border-bottom: 1px solid black;}\n";
		s << "#menu ul li { display: inline; border-right: 1px solid black;}\n";
		s << "#menu ul li a {text-decoration: none; padding-right: 1em;" 
			<< "padding-left: 1em; margin: 0px;}\n";
		s << "#menu ul li a:hover{ color: #000; background: #ddd;}\n";
		s << "\n";
		s << "#main {\n";
		s << " margin: 10px; \n";
		s << "}\n";
		s << "\n";
		s << "body, th, td {\n";
		s << " font: normal 13px verdana,arial,'Bitstream Vera Sans',"
			<< "helvetica,sans-serif;\n";
		s << "}\n";
		s << ":link, :visited {\n";
		s << " text-decoration: none;\n";
		s << " color: #b00;\n";
		s << "}\n";
		s << "\n";
		s << "table.listings {\n";
		s << " clear: both;\n";
		s << " border-bottom: 1px solid #d7d7d7;\n";
		s << " border-collapse: collapse;\n";
		s << " border-spacing: 0;\n";
		s << " margin-top: 1em;\n";
		s << " width: 100%;\n";
		s << "}\n";
		s << "\n";
		s << "table.listings th {\n";
		s << " text-align: left;\n";
		s << " padding: 0 1em .1em 0;\n";
		s << " font-size: 12px\n";
		s << "}\n";
		s << "table.listings thead { background: #f7f7f0 }\n";
		s << "table.listings thead th {\n";
		s << " border: 1px solid #d7d7d7;\n";
		s << " border-bottom-color: #999;\n";
		s << " font-size: 11px;\n";
		s << " font-wheight: bold;\n";
		s << " padding: 2px .5em;\n";
		s << " vertical-align: bottom;\n";
		s << "}\n";
		s << "\n";
		s << "table.listings tbody td a:hover, table.listing tbody th a:hover {\n";
		s << " background-color: transparent;\n";
		s << "}\n";
		s << "\n";
		s << "table.listings tbody td, table.listing tbody th {\n";
		s << " border: 1px dotted #ddd;\n";
		s << " padding: .33em .5em;\n";
		s << " vertical-align: top;\n";
		s << "}\n";
		s << "\n";
		s << "table.listings tbody td a:hover, table.listing tbody th a:hover {\n";
		s << " background-color: transparent;\n";
		s << "}\n";
		s << "table.listings tbody tr { border-top: 1px solid #ddd }\n";
		s << "table.listings tbody tr.light { background-color: #fcfcfc }\n";
		s << "table.listings tbody tr.dark { background-color: #f7f7f7 }\n";
		s << "table.listings tbody tr:hover { background: #eed }\n";
		s << "table.listings tbody td { text-align: left }\n";
		s << "table.listings tbody td.directory a { font-weight: bold }\n";
		s << "\n";
		s << ".sep { color: #666}\n";
		s << "\n";
		s << "\n";
	}


}} // end of namespace svndigest and namespace theplu
