$Id: README 185 2006-09-06 02:39:18Z jari $

======================================================================
Copyright (C) 2005, 2006 Jari H�kkinen

This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.
======================================================================


0  What is svndigest?

svndigest is a tool to extract development information and statistics
from a subversion repository. A slightly more detailed explanation is
available in doc/readme.txt


1  Prerequisites

ia) Subversion development files, i.e., header files and program
	 libraries, version 1.4 or later.

ib) This item should not be an issue if item ia) is fulfilled; the
	 Apache Portable Runtime (APR) should be available if the subversion
	 API was succesfully compiled. Subversion depends on APR and in
	 consequence, the dependency is inherited by svndigest.

ii) Gnuplot.

iii) A standard C++ compliant compiler with one extension: svndigest
   must be compiled with a C++ compiler that supports 'long long'
   types - GCC does this. This is due to the fact that one of the
   underlying libraries, APR, is using 'long long'. APR is written in
   C and the C standard allows 'long long'.

	 Even though the source is compiled with the -pedantic flag (which
   should catch the non C++ standard 'long long') there is another
   flag to suppress 'long long' diagnostics, -Wno-long-long.


2  Compilation and installation

See INSTALL for instructions on how to compile and install svndigest.


3  Running svndigest

To get a listing of available options issue 'svndigest -h'. Simply run
the program with

   svndigest -r /path/to/a/subversion/WC -t /path/whereto/write/output -v

The -v option will produce some output to stdout and can be omitted.


4  Acknowledgements

svndigest uses a C++ interface to Gnuplot inspired by Rajarshi Guha.

Rajarshi Guha's "A C++ Interface to Gnuplot" can be found through
http://jijo.cjb.net/ . The C++ interface is based on "gnuplot
interfaces in ANSI C" written by Nicolas Devillard. The C implemention
is available through http://ndevilla.free.fr/ .

svndigest build system uses an M4 macro (build_support/find_apr.m4)
created within the Apache project (The Apache Software Foundation). No
changes is made to the M4 script. The apache license text is available
as buid_support/apache_LICENSE-2.0.txt.
