#!/bin/bash

# $Id: svndigest.sh 149 2006-08-12 09:11:46Z jari $

# Copyright (C) 2006 Jari H�kkinen
#
# This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

cd /export/home/trac/tracs/svndigest/htdocs/pristine_svndigest
svn diff -rHEAD > tmp.txt
if [ -s "tmp.txt" ]; then
        svn update
	svndigest -f \
		-r /export/home/trac/tracs/svndigest/htdocs/pristine_svndigest \
		-t /export/home/trac/tracs/svndigest/htdocs/svndigest
fi
rm tmp.txt
