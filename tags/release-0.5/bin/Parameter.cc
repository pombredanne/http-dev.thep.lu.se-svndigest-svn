// $Id: Parameter.cc 180 2006-09-03 07:43:56Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	Parameter::Parameter(const int argc,const char *argv[])
	{
		defaults();
		for (int i=1; i<argc; ++i) {
			bool ok=false;
			std::string myargv(argv[i]);
			if (myargv=="-f" || myargv=="--force"){
					force_=true;
					ok=true;
			}
			else if (myargv=="-h" || myargv=="--help"){
				help();
				exit(0);			// always exit after printing help
			}
			else if (myargv=="-r" || myargv=="--root"){
				if (++i<argc){
					root_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-rev" || myargv=="--revisions") {
					revisions_=true;
					ok=true;
			}
			else if (myargv=="-t" || myargv=="--target"){
				if (++i<argc){
					targetdir_= std::string(argv[i]);
					ok=true;
				}
			}
			else if (myargv=="-v" || myargv=="--verbose"){
					verbose_=true;
					ok=true;
			}
			else if (myargv=="--version"){
					version();
					exit(0);
			}
			else if (myargv=="-vf" || myargv=="-fv"){
					verbose_=true;
					force_=true;
					ok=true;
			}

			if (!ok)
				throw std::runtime_error("svndigest: invalid option: " + myargv +
																 "\nType 'svndigest --help' for usage.");
		}

		analyse();
	}


	void Parameter::analyse(void)
	{
		using namespace std;

		string workdir(pwd()); // remember current working directory (cwd).

		// Checking that root_ exists and retrieve the absolute path to root_
		if (chdir(root_.c_str()))
			throw runtime_error(string("svndigest: Root directory (") + root_ +
													") access failed.");
		root_ = pwd();

		// need to get back to cwd if relative paths are used.
		if (chdir(workdir.c_str()))
			runtime_error(string("svndigest: Failed to access cwd: ") + workdir);

		// Checking that targetdir_ exists and retrieve the absolute path
		// to targetdir_
		if (chdir(targetdir_.c_str()))
			throw runtime_error(string("svndigest: Target directory (") + targetdir_ +
													") access failed.");
		targetdir_ = pwd();
		// Checking write permissions for targetdir_
		if (access_rights(targetdir_,"w"))
			throw runtime_error(string("svndigest: No write permission on target ") +
																 "directory (" + targetdir_ +").");

		// return back to cwd
		if (chdir(workdir.c_str()))
			runtime_error(string("svndigest: Failed to access cwd: ") + workdir);
	}


	void Parameter::defaults(void)
	{
		force_=false;
		revisions_=false;
		root_=".";
		targetdir_=".";
		verbose_=false;
	}


	void Parameter::help(void)
	{
		defaults();
		std::cout << "\n"
							<< "usage: svndigest [options]\n"
							<< "\n"
							<< "svndigest traverses a directory structure (controlled by\n"
							<< "subversion) and calculates developer statistics entries.\n"
							<< "The top level directory of the directory structure to\n"
							<< "traverse is set with the -r option. The result is written to\n"
							<< "the current working directory or a directory set with the\n"
							<< "-t option.\n"
							<< "\n"
							<< "Valid options:\n"
							<< "  -f [--force]   : remove target directory/file if it exists\n"
							<< "                   [no force]. NOTE recursive delete.\n"
							<< "  -h [--help]    : display this help and exit\n"
							<< "  -r [--root] arg : svn controlled directory to perform\n"
							<< "                    statistics calculation on [" 
							<< root_ << "]\n"
							<< "  -rev [--revisions]: Use revision numbers as time scale\n"
							<< "                      instead of dates [dates].\n"
							<< "  -t [--target] arg : output directory [" 
							<< targetdir_ << "]\n"
							<< "  -v [--verbose] : explain what is being done\n"
							<< "  --version      : print version information and exit\n"
							<< std::endl;
	}


	void Parameter::version(void) const
	{
		using namespace std;
		cout << PACKAGE_STRING
				 << "\nCopyright (C) 2006 Jari H�kkinen and Peter Johansson.\n\n"
				 << "This is free software; see the source for copying conditions.\n"
				 << "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR\n"
				 << "A PARTICULAR PURPOSE." << endl;
	}

}} // of namespace wenni and namespace theplu
