// $Id: svndigest.cc 180 2006-09-03 07:43:56Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parameter.h"

#include "Directory.h"
#include "GnuplotFE.h"
#include "html_utility.h"
#include "rmdirhier.h"
#include "SVN.h"
#include "SVNinfo.h"

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

///
/// @brief Check whether \a path already exists or not.
///
/// @return True if \a path exists, false otherwise.
///
bool check_target(const std::string& path)
{
	struct stat buf;
	return !stat(path.c_str(),&buf);
}


int main(const int argc,const char* argv[])
{
	using namespace theplu::svndigest;
	Parameter* option=NULL;
	try {
		option = new Parameter(argc,argv);
	}
	catch (std::runtime_error e) {
		std::cerr << e.what() << std::endl;
		exit(-1);
	}

	// Make sure that root directory is under subversion control.
	SVN* svn=SVN::instance();
	try {
		svn->setup_wc_adm_access(option->root());
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what() << "\nsvndigest: " << option->root()
							<< " is not under subversion control\n" << std::endl;
		exit(-1);
	}

	// check if target already exists and behave appropriately
	std::string target_path=option->targetdir() + '/' + file_name(option->root());
	bool need_to_erase_target = check_target(target_path);
	if (need_to_erase_target && !option->force())
		throw std::runtime_error(std::string("svndigest: directory (") +
														 target_path + ") already exists");

	// Extract repository location
	std::string repo;
	try {
		repo=SVNinfo(option->root()).repos_root_url();
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what()
							<< "\nsvndigest: Failed to determine repository for "
							<< option->root() << '\n' << std::endl;
		exit(-1);
	}

	// build directory tree already here ... if WC is upto date with
	// repo an exception is thrown. This avoids several costly
	// statements below and will not remove a digest tree below if a
	// tree already exists.
	Directory tree(0,option->root(),"");
	tree.parse(option->verbose());

	// Retrieve commit dates.
	std::vector<std::string> commit_dates;
	try {
		svn->setup_ra_session(repo);
		commit_dates=svn->commit_dates(repo);
	}
	catch (SVNException e) {
		std::cerr << "\nsvndigest: " << e.what() << std::endl;
		exit(-1);
	}

	// remove target if needed
	if (need_to_erase_target) {
			if (option->verbose())
				std::cout << "rm -rf " << target_path << "\n";
			rmdirhier(target_path);
	}

	if (!option->revisions())
		GnuplotFE::instance()->set_dates(commit_dates);
	GnuplotFE::instance()->command(std::string("cd '")+option->targetdir()+"'");
	chdir(option->targetdir().c_str());
	try {
		tree.print(option->verbose());
	}
	catch (const std::runtime_error& x) {
		std::cerr << "svndigest: " << x.what() << std::endl;
	}
	std::string css(file_name(option->root())+"/svndigest.css");
	std::ofstream os(css.c_str());
	print_css(os);
	os.close();

	delete option;
	exit(0);				// normal exit
}
