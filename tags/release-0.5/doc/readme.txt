$Id: readme.txt 189 2006-09-06 12:41:27Z jari $

======================================================================
Copyright (C) 2005 Jari H�kkinen
Copyright (C) 2006 Jari H�kkinen, Peter Johansson

This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.
======================================================================


svndigest traverses a directory structure (controlled by subversion)
and calculates developer statistics for all subversion controlled
entries. The result is written to a sub-directory of a user
specifiable target directory (default is the current working
directory).

To understand what statistics is calculated by svndigest this
definition is needed: The developer who made the latest change to a
line still in use in the latest revision, is considered as the
contributor of that line regardless of who actually originally created
that line.

For each developer svndigest calculates the number of contributed
lines in the latest (checked out) revision. Svndigest calculates for
each revision, by checking when each line was last changed, how many
lines each developer had contributed at that specific revision and
still are in use. svndigest separates between different types of
lines: line of code, line of comment, and empty lines.

The separation between types of lines is designed to support C++ style
line comments like

// blah blah

with two or more slashes in front of them, and C style block comments like

/*
 * blah blah
 */

with zero or more stars at the beginning of every line. A line is
consider to be a line of code, if it contains non-whitespace
characters being neither inside a C style block nor after a C++ style
line comment start ('//'). In case the line is not a line of code, it
is considered as a line of comment if it contains alphanumeric
characters (see isalnum). Remaining lines are consider empty, in other
words, lines like: '////////////' or '/*' are considered as empty.

There are many different types of files and for many file types it
does not make sense to define lines. Source code, documentation, and
other human readable files can be treated in single line basis whereas
symbolic links and binary files cannot. svndigest treats binary files
as zero-line files, whereas symbolic links are treated as one-line
files. There is a possibility to exclude files from the statistics, the
use of the property svndigest:ignore.

Sometimes large test files and XML files are added to the repository
that should not really be counted in the statistics. This is solved
with the svndigest:ignore property. Files with this property are
excluded from statistics. Setting the svndigest:ignore property to a
directory will exclude all siblings to that directory from svndigest
treatment.

svndigest requires the subversion repository to be checked out before
analysis, i.e., svndigest will not run directly against a repository,
and up to date with the repository.


The current flow of the program is.

i) Check that we are working with a WC in subversion control.

ii) Build the requested directory structure ignoring not subversion
	 controlled items. During the directory structure creation a check
	 is made that the WC is up to date with the repository.

iii) Walk through the directory structure and calculate statistics for
   each entry.

iv) Create the plots and HTML presentation.
