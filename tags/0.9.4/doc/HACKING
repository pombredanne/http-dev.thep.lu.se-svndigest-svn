$Id: HACKING 874 2009-11-22 22:53:21Z peter $

This file describes a couple of decisons we have taken and why we have
taken them.

* Consider using Node::log(0) rather than class SVNlog directly

   Node::log(0) is recursive in that it returns the union of the log
   of *this and its daughters. Naively there should be no difference,
   but one counter-example is when a file was created in a branch and
   merged into trunk. The log of the file will then contain the
   history in the branch while the directory log will only contain the
   changeset when the file was merged into trunk (note this is true
   for subversion 1.4 but may have changed in later versions merges
   are handled differently). Another example is when a file is moved
   to another directory. That file log will reflect the entire history
   of the file, while the new mother directory of the file only
   contain the changeset of the move. As most statistics is recursive
   we want to use the recursive log.


* Do not use features in report that requires a web server

   It should be possible to view the report in your favorite browser
   directly through the file system without a web server. Besides that
   it is useful for users, it makes it easier for developers to
   inspect the report of the test repository in the regression
   test. This decision implies (obviously) that server side includes
   (SSI) and any kind of scripts such as java scripts are not allowed
   in the generated report.

----

Copyright (C) 2009 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
