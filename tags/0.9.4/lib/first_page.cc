// $Id: first_page.cc 1254 2010-10-31 23:20:34Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "first_page.h"

#include "Commitment.h"
#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "NodeCounter.h"
#include "Stats.h"
#include "StatsCollection.h"
#include "SVNlog.h"
#include "Trac.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/param.h>
#include <unistd.h>
#include <vector>

namespace theplu{
namespace svndigest{

	void print_main_page(const std::string& dir, const SVNlog& log,
											 const StatsCollection& stats, std::string url, 
											 const NodeCounter& node_counter)
	{
		std::string filename="index.html";
		std::ofstream os(filename.c_str());
		print_header(os, dir, 0, "all", "main", "index.html", "none");

		using namespace std;
		set<string> authors;
		std::transform(log.commits().begin(), log.commits().end(),
									 std::inserter(authors, authors.begin()), 
									 std::mem_fun_ref(&Commitment::author));
		// erase invalid authors
		authors.erase("");
		authors.erase("no author");

		vector<Commitment> latest_commit;
		latest_commit.reserve(authors.size());
		for (set<string>::const_iterator i(authors.begin()); i!=authors.end(); ++i)
			latest_commit.push_back(log.latest_commit(*i));

		print_summary_plot(os, stats["classic"]);
		print_general_information(os, log, authors.size(), url, node_counter);
		sort(latest_commit.begin(), latest_commit.end(), GreaterRevision());
		print_authors(os, latest_commit, stats["classic"]);
		print_recent_logs(os, log, stats);
		os << "<hr width=100% />";
		print_footer(os);
		os.close();
	}

	void print_general_information(std::ostream& os, const SVNlog& log, 
																 size_t nof_authors, std::string url,
																 const NodeCounter& node_counter)
	{ 
		assert(log.commits().size());
		Date begin(log.commits().begin()->date());		
		Date end(log.latest_commit().date());
		std::string timefmt("%a %b %e %H:%M:%S %Y");

		os << "<div class=\"main\">" 
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "General Information"
			 << "</th></tr></thead>\n"
			 << "<tbody>\n"
			 << "<tr><td>URL:</td><td>"; 
		if (url.size()>=4 && url.substr(0,4)=="http")
			os << anchor(url, url);
		else
			os << url;
		os << "</td></tr>\n"
			 << "<tr><td>First Revision Date:</td><td>" 
			 << begin(timefmt) << "</td></tr>\n"
			 << "<tr><td>Latest Revision Date:</td><td>" 
			 << end(timefmt) << "</td></tr>\n"
			 << "<tr><td>Age:</td><td>"; 
		os << end.difftime(begin);
		os << "</td></tr>\n"
			 << "<tr><td>Smallest Revision:</td><td>" 
			 << log.commits().begin()->revision() 
			 << "</td></tr>\n"
			 << "<tr><td>Largest Revision:</td><td>" << log.latest_commit().revision() 
			 << "</td></tr>\n"
			 << "<tr><td>Revision Count:</td><td>" << log.commits().size() 
			 << "</td></tr>\n"
			 << "<tr><td>Number of Authors:</td><td>" << nof_authors
			 << "</td></tr>\n"
			 << "<tr><td>Number of Directories:</td><td>" 
			 << node_counter.directories()
			 << "</td></tr>\n"
			 << "<tr><td>Number of Files:</td><td>" 
			 << node_counter.files()
			 << "</td></tr>\n"
			 << "</tbody>\n"
			 << "</table></div>\n";
	}


	void print_authors(std::ostream& os, 
										 const std::vector<Commitment>& lc,
										 const Stats& stats)
	{
		HtmlStream hs(os);
		os << "<div class=\"main\">"
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "Authors"
			 << "</th></tr></thead>\n";

		os << "<tr><td>Author</td>" 
			 << "<td>Number of Lines</td>"
			 << "<td>Code Lines</td>"
			 << "<td>Comment Lines</td>"
			 << "<td>Latest Commitment</td>"
			 <<"</tr>";

		std::string timefmt("%Y-%m-%d  %H:%M");
		using namespace std;
		for (vector<Commitment>::const_iterator i=lc.begin(); i!=lc.end(); ++i) {
			os << "<tr><td>"; 
			if (!stats.lines(i->author()))
				os << i->author();
			else
				os << anchor(std::string("classic/"+i->author()+"/total/index.html"),
										 i->author());
			os << "</td><td>" << stats.lines(i->author()) << " (" 
				 << 100*stats.lines(i->author())/(stats.lines()?stats.lines():1)
				 << "%)</td><td>" << stats.code(i->author()) << " (" 
				 << 100*stats.code(i->author())/(stats.code()?stats.code():1)
				 << "%)</td><td>" << stats.comments(i->author()) << " (" 
				 << 100*stats.comments(i->author())/(stats.comments()?stats.comments():1)
				 << "%)</td><td>";
			std::string date = Date(i->date())(timefmt);
			const Configuration& conf = Configuration::instance();
			if (conf.trac_root().empty())
				hs << date;
			else {
				std::stringstream url;
				url << conf.trac_root() << "changeset/" << i->revision();
				os << anchor(url.str(), date);
			}
			os << "</td>" <<"</tr>";
		}
		os << "<tr><td>Total</td>";
		os << "<td>" << stats.lines() << "</td>"
			 << "<td>" << stats.code() << "</td>"
			 << "<td>" << stats.comments() << "</td>"
			 <<"</tr>";

		os << "</table></div>\n";
	}


	void print_recent_logs(std::ostream& os, const SVNlog& log,
												 const StatsCollection& stats)
	{
		os << "<div class=\"main\">\n"
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "Recent Log"
			 << "</th></tr></thead>\n";

		os << "<tr><td>Author</td><td>Date</td><td>Rev</td><td>Added</td>"
			 << "<td>Removed</td><td>Message</td></tr>\n";
		HtmlStream hs(os);
		std::string timefmt("%Y-%m-%d  %H:%M");
		const size_t maxlength = 80;
		const Configuration& conf = Configuration::instance();
		typedef SVNlog::container::const_reverse_iterator iter;
		size_t count = 0;
		for (iter i=log.commits().rbegin(); 
				 i != log.commits().rend() && count<10; ++i) {
			os << "<tr><td>" 
				 << anchor(std::string("classic/")+i->author()+"/total/index.html",
									 i->author()) 
				 << "</td>";
			Date date(i->date());
			os << "<td>";
			hs << date(timefmt); 
			os << "</td>";
			os << "<td>"; 
			os << trac_revision(i->revision());
			os << "</td>";
			os << "<td>";
			int added = stats["add"](LineTypeParser::total, "all", i->revision()) -
				stats["add"](LineTypeParser::total, "all", i->revision() - 1); 
			os << added;
			os << "</td>";
			os << "<td>";
			os << added-(stats["blame"](LineTypeParser::total, "all", i->revision()) -
									 stats["blame"](LineTypeParser::total,"all",i->revision()-1)); 
			os << "</td>";
			os << "<td>";
			std::string mess = i->message();
			// replace newlines with space
			std::replace(mess.begin(), mess.end(), '\n', ' ');
			mess = htrim(mess);

			if (conf.trac_root().empty()) {
				// truncate message if too long
				if (mess.size()>maxlength)
					mess = mess.substr(0,maxlength-3) + "...";
				hs << mess;
			}
			else {// make anchors to trac
				Trac trac(hs);
				trac.print(mess, maxlength);
			}

			os << "</td></tr>\n";
			++count;
		}
		os << "</table></div>\n";
	}


	void print_summary_plot(std::ostream& os, const Stats& stats)
	{
		std::string name("summary_plot");
		stats.plot_summary(name);
		os << "<div class=\"main\">\n";
		os << image(name);
		os << "</div>";


	}

}} // end of namespace svndigest and namespace theplu
