#ifndef _theplu_svndigest_svndiff_
#define _theplu_svndigest_svndiff_

// $Id: SVNdiff.h 1168 2010-08-15 19:30:22Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	// forward declaration
	class SVN;

	/**
	*/
	class SVNdiff {
	public:
		SVNdiff(const std::string& path1, svn_revnum_t revision1,
						const std::string& path2, svn_revnum_t revision2);

	private:
		SVN* instance_;


		// no copy
		SVNdiff(const SVNdiff&);
		SVNdiff& operator=(const SVNdiff&);
	};

}} // end of namespace svndigest and namespace theplu

#endif
