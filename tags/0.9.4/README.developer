$Id: README.developer 1127 2010-07-15 22:01:38Z peter $

= Developer Information =

This file contains developer specific information.


=== Coding style ===

Developers should follow the coding style outlined in the
[http://cbbp.thep.lu.se/~jari/documents/c++_coding_guidelines/index.html C++ coding guidelines].


=== Subversion usage ===

Commits should be minimalistic and the projects should always compile
(`make` and `make check`) when someone makes a clean checkout. There is
a short introduction to subversion and its usage available as
[http://cbbp.thep.lu.se/~jari/documents/subversion_guidelines/index.html Subversion guidelines].
We follow these guidelines.


=== Requirements ===

To build from a subversion checkout, you will need Autotools. More
specifically Automake 1.10 (or later) and Autoconf 2.63 (or later). To
build man page you will need help2man.

=== bootstrap and autoreconf === 

At first checkout from the repository you must run the bootstrapping
utility

  `./bootstrap` 

to setup autoconf files. To push subsequent changes of this file into
the build scripts you run

	 `autoreconf`


=== configure ===

Run the generated configure script with 

  `./configure --enable-debug`

The option flag `--enable-debug` turns on debug options (e.g. enables
assertions). 


=== Testing ===

Test programs should be created and added to the `make check`
execution. Always check that the test runs before committing code to
the repository, i.e., run `make check`. For more details on how to run
and write tests, please refer to file `test/README`.

There is a test repository in source:trunk/test/repo that is a part of
the svndigest repository. The logic behind this is that it is nice to
have a common test repository that can be used off-line. Changes to
the repository are usually made to create new test cases and it is
important that these changes are committed to svndigest repository as
well. All changes to the test repository are not automatically
propagated to the svndigest repository and it is important that some
files created in the test repository are manually added to the
svndigest repository. Assuming that `make check` is run before a
commit, a warning is generated if the test repository is inconsistent
with the svndigest repository. There will be a diagnostic message and
a resolution hint, however the developer must examine and resolve the
problem manually.

The problem is simply that the file source:trunk/test/repo/db/current
is changed compared to the svndigest repository. The `current` file
indicates a specific number of change sets within the test repository
and the directories source:trunk/test/repo/db/revs and
source:trunk/test/repo/db/revprops contains a matching number of
change sets. However, the change sets added to the test repository are
not automatically added to the svndigest repository, therefore the new
files in the `revs` and `revprops` directories must be added manually
to the svndigest repository.

There is also a race condition when developers make changes to the
test repository simultaneously: It is easy to create severe conflicts
in the test repository. Imaging the case when two developers add test
commits to the test repository, and then tries to add these to the
svndigest repository. Since there are different versions of the same
test repository revisions, the second developer committing to the
svndigest will get messages about files being in the way. This is just
one situation with possible conflicts. This conflict is of course
resolved by the second developer having to throw away his changes and
redo them after a new checkout of the test repository.

The take home message is that developers should communicate with each
other when changing the test repository in order to avoid to large
repository conflicts. AND read the output of `make check`.


=== yat library ===

Directory `yat/` contains some files from the yat library. These
files are not owned by svndigest and should not (if possible) be
edited here. For more details please see `yat/README`.

=== Output file structure ===

A directory reflecting the parsed directory is generated for each
combination of StatsType, Author, and LineType, which is located in
`<TARGET>/<ROOT>/StatsType/Author/LineType/`. Author may be, on top of
each author in the repository, a directory ''all'' that is a sum over
all Authors. In same fashion LineType may be any of the three
linetypes but may also ''total'', which a sum over all linetypes. In
each `<TARGET>/<ROOT>/StatsType` there are directories
`images/LineType` that contain a structure reflecting the parsed
directory with images for every combination of File, LineType, and
StatsType. Images for different Authors are identical and therefore
shared between authors.

== Design and Motivation ==

Please refer to source:trunk/doc/HACKING for a list of design
decisions we try to follow.

== Releasing ==

See http://dev.thep.lu.se/svndigest/wiki/ReleaseProcedure


-------------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari Häkkinen
Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
Copyright (C) 2009, 2010 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
}}}
