$Id: README 1182 2010-08-25 01:02:41Z peter $

= About svndigest =

svndigest is a tool to extract development information and statistics
from a subversion repository.  Svndigest is written in C++ and
extracts repository history using the subversion API. The resulting
report is written to a user specifiable directory in HTML format.

See the file NEWS for the user-visible changes from previous releases.
In addition, there have been bugs fixed.

svndigest is free software. See the file COPYING for copying conditions.

Directory `bin` contains main program and is location for svndigest
binary. Directory `m4` contains additional autotool macros
required for creating the build environment. Directory `doc` contains
documentation mainly in file `readme.txt`. Most of the code can be
found in directory `lib`. Directory `test` contains the code for tests
and the test repository (see README.developer).


== Downloading ==

svndigest can be obtained from 

   https://sourceforge.net/projects/svndigest/files/


== Documentation ==

For documentation see the file doc/readme.txt.


== Requirements ==

 * Subversion development files, i.e., header files and program
   libraries, version 1.4 or later.

 * This item should not be an issue if item above is fulfilled; the
   Apache Portable Runtime (APR) should be available if the subversion
   API was successfully compiled. Subversion depends on APR and in
   consequence, the dependency is inherited by svndigest.

 * PLplot, http://plplot.sourceforge.net, version 5.9.6 (or newer) is
   required to generate graphics in the output. You can build
   svndigest without PLplot if you run configure with
   `--without-plplot`. If you output plots in PNG format, it is
   recommended to install plplot with pngcairo or pngqt devices.

 * pkg-config utility, http://pkg-config.freedesktop.org, is used to
   query information about PLplot headers and libs. It is possible to
   configure and build without pkg-config, but we recommend that you
   have pkg-config available because it makes detection of PLplot much
   more automatic and convenient.

 * A standard C++ compliant compiler with one extension: svndigest
   must be compiled with a C++ compiler that supports 'long long'
   types - GCC does this. This requirement arises from the fact that
   one of the underlying libraries, APR, is using 'long long'. APR is
   written in C and the C standard allows 'long long'.
   Even though the source is compiled with the -pedantic flag (which
   should catch the non C++ standard 'long long') there is another
   flag to suppress 'long long' diagnostics, -Wno-long-long.


== Installing svndigest ==

In file INSTALL you'll find the generic FSF install instructions. To
compile and install svndigest you can follow the usual autotools path:

 * `./configure`

 * `make`

 * Optionally, `make check` to run test programs. Some test programs
   are not enabled by default and the disabled tests should only be
   run by developers.

 * `make install`

The `./configure` script accepts a few options of interest for
svndigest. You can provide `./configure` with APR, subversion, and
PLplot API location information with `--with-apr=DIR`,
`--with-svn=DIR`, and `--with-plplot=DIR, respectively.

If you grabbed the source from the subversion repository you need to
run `./bootstrap` to setup autotools files (see README.developer).


== Development ==

The development of svndigest can be monitored through 

  http://dev.thep.lu.se/svndigest

You can find most information concerning the development of svndigest at
this site.


== Bug Reporting ==

You can report svndigest bugs on 

  http://dev.thep.lu.se/svndigest/newticket

Use user `svndigest` and password `svndigest`. For more details please
refer to section 'Reporting failures' in file `test/README`.


== Subversion Access ==

The svndigest source repository is available via anonymous subversion
access, issue:

  `svn checkout http://dev.thep.lu.se/svndigest/svn/trunk svndigest`

See file README.developer for developer specific information. We make no
guarantees about the contents or quality of the latest code in the
subversion repository: it is not unheard of for code that is known to
be broken to be committed to the repository. Use at your own risk.


----------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari Häkkinen
Copyright (C) 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
Copyright (C) 2010 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
}}}
