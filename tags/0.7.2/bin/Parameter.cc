// $Id: Parameter.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Parameter.h"

#include "OptionVersion.h"
#include "../lib/utility.h" // to avoid inclusion of yat file

#include "yat/ColumnStream.h"
#include "yat/Exception.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"

#include <config.h>	// this header file is created by configure

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

// FIXME: remove this when we require autoconf 2.64
// autoconf 2.64 defines PACKAGE_URL in config.h (from AC_INIT)
#ifndef PACKAGE_URL
#define PACKAGE_URL "http://dev.thep.lu.se/svndigest"
#endif

namespace theplu {
namespace svndigest {

	Parameter::Parameter( int argc, char *argv[])
		: cmd_( std::string("Mandatory arguments to long options ") + 
						std::string("are mandatory for short options too.")), 
			config_file_(cmd_, "config-file", 
									 "configuration file [<ROOT>/.svndigest/config]"),
			copyright_(cmd_, "copyright", "update copyright statement"),
			force_(cmd_, "f,force", 
						 std::string("if sub-directory named <ROOT> exists in target dire")+
						 std::string("ctory, remove sub-directory before writing results")),
			generate_config_(cmd_, "g,generate-config", 
											 "write configuration to standard output"),
			help_(cmd_),
			ignore_cache_(cmd_, "ignore-cache", 
										std::string("ignore cache files and analyze ") +
										std::string("everything from repository")),
			report_(cmd_, "report", "create no HTML report", true),
			revisions_(cmd_, "revisions", 
								 "use revision number as time scale instead of dates [dates]"),
			root_(cmd_, "r,root", 
						"svn controlled directory to perform statistics on [.]"),
			target_(cmd_, "t,target", "output directory [.]"),
			verbose_(cmd_, "v,verbose", "explain what is being done"),
			version_(cmd_, "version", "print version information and exit", &verbose_)
	{
		init();
		try {
			cmd_.parse(argc, argv);
		}
		catch (yat::utility::cmd_error& e) {
			std::cerr << e.what() << std::endl;
			exit (-1);
		}

		// set default values
		if (!root_.present())
			root_.value(".");

		if (!target_.present())
			target_.value(".");

		if (!config_file_.present())
			config_file_.value(concatenate_path(root_.value(),".svndigest/config"));

		// analyse arguments
		analyse();
	}


	Parameter::~Parameter(void)
	{
	}


	void Parameter::analyse(void)
	{
		std::string save_wd = pwd();

		// check root but not if -g option given
		if (!generate_config()) {
			check_existence(root_.value());
			check_readable(root_.value());
			if (chdir(root_.value().c_str())) {
				std::stringstream ss;
				ss << "svndigest: cannot read `" << root_.value() << "': " 
					 << strerror(errno);
				throw yat::utility::cmd_error(ss.str());
			}
			root_.value(pwd());
			chdir(save_wd.c_str());

			// check target (only if we write report)
			if (report()) {
				check_existence(target_.value());
				check_readable(target_.value());
				std::string base_root = file_name(root_.value());
				std::string path = concatenate_path(target_.value(),base_root); 
				if (access_rights(target_.value().c_str(), "w")) {
					std::stringstream ss;
					ss << "svndigest: cannot create directory `" << path 
						 << "': " << strerror(errno);
					throw yat::utility::cmd_error(ss.str());
				}
				if (node_exist(path) && !force()) {
					std::stringstream ss;
					ss << "svndigest: cannot create directory `" << path << "' "
						 << strerror(EEXIST);
					throw yat::utility::cmd_error(ss.str());
				}
				if (chdir(target_.value().c_str())) {
					std::stringstream ss;
					ss << "svndigest: cannot read `" << target_.value() << "': " 
						 << strerror(errno);
					throw yat::utility::cmd_error(ss.str());
				}
				target_.value(pwd());
				chdir(save_wd.c_str());
			}
		}

		// check config file
		struct stat nodestat;
		// true also if there is a broken symlink named...
		bool config_exists = !lstat(config_file_.value().c_str(), &nodestat);
		// the latter case in order to catch broken symlink
		if (config_file_.present() || config_exists)
			// throws if file does not exists
			check_existence(config_file_.value());
		if (config_exists) {
			// throws if file is not readable
			check_readable(config_file_.value());
			stat(config_file_.value().c_str(), &nodestat);
			if (!S_ISREG(nodestat.st_mode)) {
				std::stringstream ss;
				ss << "svndigest: `" << config_file_.value() 
					 << "' is not a regular file";
				throw yat::utility::cmd_error(ss.str());
			}
		}
	}


	void Parameter::check_existence(std::string path) const
	{
		if (node_exist(path))
			return;
		std::stringstream ss;
		ss << "svndigest: cannot stat `" << path << "': " << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}

	
	void Parameter::check_readable(std::string path) const
	{
		if (!access_rights(path, "r"))
			return;
		std::stringstream ss;
		ss << "svndigest: cannot open `" << path << "': " << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}
	

	std::string Parameter::config_file(void) const
	{
		return config_file_.value();
	}


	bool Parameter::copyright(void) const
	{
		return copyright_.present();
	}


	bool Parameter::force(void) const
	{
		return force_.present();
	}


	bool Parameter::generate_config(void) const
	{
		return generate_config_.present();
	}


	bool Parameter::ignore_cache(void) const
	{
		return ignore_cache_.present();
	}


	void Parameter::init(void)
	{
		// don't use argv[0] because user may rename the binary
		cmd_.program_name() = PACKAGE_NAME;
		config_file_.print_arg("=FILE");
		root_.print_arg("=ROOT");
		target_.print_arg("=TARGET");
		std::stringstream ss;
		ss << "Report bugs to " << PACKAGE_BUGREPORT << ".\n"
			 << PACKAGE << " home page: <" << PACKAGE_URL << ">.\n";
		help_.post_arguments() = ss.str();
		help_.synopsis() = 
			"Generate statistical report for a subversion repository\n";
	}


	bool Parameter::report(void) const
	{
		return report_.value();
	}


	bool Parameter::revisions(void) const
	{
		return revisions_.present();
	}


	std::string Parameter::root(void) const
	{
		return root_.value();
	}

	
	std::string Parameter::targetdir(void) const
	{
		return target_.value();
	}

	
	bool Parameter::verbose(void) const
	{
		return verbose_.present();
	}


}} // of namespace svndigest and namespace theplu
