#ifndef _theplu_svndigest_configuration_
#define _theplu_svndigest_configuration_

// $Id: Configuration.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Alias.h"

#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace theplu{
namespace svndigest{

	///
	/// Configuration class takes care of all setting defined in the
	/// configuration file.
	///
	class Configuration
	{
	public:
		static Configuration& instance(void);

		/**
			 \return hexadecimal color code (e.g. 5aee4a) that is used in
			 blame output. If no color is configured for \a author, an empty
			 string is returned.
		 */
		std::string author_str_color(const std::string& author) const;

		/**
			 \return vector of parse codons for the given \a file_name
		 */
		const std::vector<std::pair<std::string, std::string> >* 
		codon(std::string file_name) const;

		///
		/// @brief Aliases for Copyright
		///
		const std::map<std::string, Alias>&	copyright_alias(void) const;

		/// 
		/// throw if stream is not a valid config
		///
		/// @brief load configuration from stream
		/// 
		void load(std::istream&);

		///
		/// @return true if we should warn about missing copyright statement
		///
		bool	missing_copyright_warning(void) const;

		///
		/// @return root for the trac envrionment, e.g.,
		/// http://dev.thep.lu.se/svndigest/
		///
		std::string trac_root(void) const;

	private:
		/// 
		/// Creates a Config object with default settings.
		///
		/// @brief Default Constructor 
		/// 
		Configuration(void);
		// Copy Constructor not implemented
		Configuration(const Configuration&);
		// assignment not implemented because assignment is always self-assignment
		Configuration& operator=(const Configuration&);
		// destructor not implemented
		~Configuration(void);

		friend std::ostream& operator<<(std::ostream&, const Configuration&);

		void add_codon(std::string, std::string, std::string);

		void clear(void);
		const std::pair<std::string,std::string>* dictionary(std::string lhs) const;
		bool equal_false(const std::string&) const;
		bool equal_true(const std::string&) const;
		/// 
		/// @brief load deafult configuration
		/// 
		void load(void);

		void set_default(void);
		/**
			 Translate string \a str using dictionary \a dict

			 \note \a str must be equal to d.first (see function equal), 
			 or behavior is unspecified.

			 \throw if a '$' character is not followed by a positive integer
			 that is not larger than number of wildcards in dictionary \a d.
		 */
		std::string translate(const std::string& str,
													const std::pair<std::string, std::string>& d) const;
		
		void validate_dictionary(void) const;

		static Configuration* instance_;

		std::map<std::string, std::string> author_color_;
		std::map<std::string, Alias> copyright_alias_;

		bool missing_copyright_warning_;
		
		typedef std::vector<std::pair<std::string, std::string> > VectorPair;
		typedef std::vector<std::pair<std::string, VectorPair> > String2Codons; 
		String2Codons string2codons_;

		VectorPair dictionary_;

		std::string trac_root_;
	};

	///
	/// @brief Output operator
	///
	std::ostream& operator<<(std::ostream&, const Configuration&);

	/**
		 If first character is '\n' replace it with "<NEWLINE>"
	 */
	std::string trans_end_code(std::string);

	/**
		 If last character is '\n' replace it with "<NEWLINE>"
	 */
	std::string trans_beg_code(std::string);

	/**
		 Trim \a c from beginning and end of string \a str;
		 
		 \return resulting string

		 \throw if first or last character of \a str is NOT character \a c
	*/
	std::string trim(std::string str, char c);

	/**
		 \brief Class for errors when reading config file.
	 */
	class Config_error : public std::runtime_error
	{
	public:
		Config_error(const std::string& line, const std::string& message);
	};

}} // end of namespace svndigest and namespace theplu

#endif


