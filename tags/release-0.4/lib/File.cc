// $Id: File.cc 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "File.h"
#include "Stats.h"

#include <fstream>
#include <iostream>
#include <string>

namespace theplu{
namespace svndigest{

	std::string File::html_link(void) const
	{ 
		return "<a href=\"" + name() + ".html\">" + name() + "</a>"; 
	}


  const Stats& File::parse(const bool verbose)
  {
		if (verbose)
			std::cout << "Parsing " << path_ << std::endl; 
    stats_.reset();
		binary_ = stats_.parse(path_);
		return stats_;
  }

	void File::print(const bool verbose) const 
	{
		std::string output(output_name() + ".html");
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		std::ofstream os(output.c_str());
		print_header(os);
		os << "<p align=center>\n<img src='" 
			 << file_name(stats_.plot(output_name()+".png",output_name()))
			 << "' alt='[plot]' border=0>\n</p>";

		os << "<table class=\"listings\">\n";
		os << "<thead>";
		os << "<tr>\n";
		os << "<th>Author</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>";

		bool dark=false;
		os << "<tr class=\"light\">\n";
		os << "<td colspan=\"5\"><a href=\"index.html\">../</a></td>\n";
		os << "</tr>\n";
		dark=!dark;
		

		// print authors
		for (std::set<std::string>::const_iterator i=stats_.authors().begin();
				 i!=stats_.authors().end(); ++i){
			if (dark)
				os << "<tr class=\"dark\"><td>" << *i 
					 << "</td><td>" << stats_.lines(*i)
					 << "</td><td>" << stats_.code(*i)
					 << "</td><td>" << stats_.comments(*i)
					 << "</td></tr>\n";
			else
				os << "<tr class=\"light\"><td>" << *i 
					 << "</td><td>" << stats_.lines(*i)
					 << "</td><td>" << stats_.code(*i)
					 << "</td><td>" << stats_.comments(*i)
					 << "</td></tr>\n";
			dark=!dark;
		}
		if (dark)
			os << "<tr class=\"dark\">\n";
		else
			os << "<tr class=\"light\">\n";
		os << "<td>Total</td>\n";
		os << "<td>" << stats_.lines() << "</td>\n";
		os << "<td>" << stats_.code() << "</td>\n";
		os << "<td>" << stats_.comments() << "</td>\n";
		os << "</tr>\n";
		os << "</table>\n";
		os << "</p>\n";

		print_footer(os);
		os.close();	
	}

}} // end of namespace svndigest and namespace theplu
