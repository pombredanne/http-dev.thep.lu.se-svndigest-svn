// $Id: Directory.h 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svndigest_directory_
#define _theplu_svndigest_directory_

#include "Node.h"

#include <list>
#include <string>

namespace theplu{
namespace svndigest{

	///
	/// Class taking care of directories.
	///
	class Directory : public Node
	{
	public:

		///
		/// @brief Constructor
		///
		/// Recursively create a directory tree starting from \a path. All
		/// entries except explicit directories are treated as File nodes,
		/// i.e. symbolic links to directories are treated as File
		/// nodes. This will ensure that the directory structure is a tree
		/// and double counting of branches is avoided.
		///
		/// @note Nodes named '.', '..', and '.svn' are ignored and not
		/// traversed.
		///
		Directory(const u_int level, const std::string& path, 
							const std::string& output="");

		///
		/// @brief Destructor
		///
		~Directory(void);

		///
		/// @return true
		///
		bool dir(void) const;

		///
		/// @return A properly formatted html link to this directory.
		///
		std::string html_link(void) const;

		const Stats& parse(const bool verbose=false);

		void print(const bool verbose=false) const;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		Directory(const Directory&);

		typedef std::list<Node*> NodeContainer;
		typedef NodeContainer::iterator NodeIterator;
		typedef NodeContainer::const_iterator NodeConstIterator;
		NodeContainer daughters_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
