// $Id: Stats.cc 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Stats.h"

#include "GnuplotFE.h"
#include "SVNblame.h"
#include "SVNinfo.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	Stats::Stats(const std::string& path)
	{
		// Make sure latest revision is set properly
		SVNinfo svn_info(path);
		revision_=svn_info.rev();
		last_changed_rev_=svn_info.last_changed_rev();
	}


  std::vector<u_int> Stats::accumulated(const Map_& map) const
  {
		// sum of all users
		std::vector<u_int> sum(revision_+1);
		sum=std::accumulate(map.begin(), map.end(), sum,
												PairValuePlus<std::string,u_int>());

    // calculate accumulated sum
    std::vector<u_int> accum(sum.size());
		std::partial_sum(sum.begin(),sum.end(),accum.begin());
		assert(sum.size()==accum.size());
    return accum;
  }

  std::vector<u_int> Stats::accumulated(const Map_& map, 
																				const std::string& user) const
  {
    if (!map.count(user))
      return std::vector<u_int>();
    std::vector<u_int> vec=(map.find(user))->second;

		// static_cast to remove annoying compiler warning
		if (vec.size() < static_cast<size_t>(revision_+1))
			vec.insert(vec.end(), revision_+1-vec.size(), 0);

    std::vector<u_int> accum(vec.size());
    std::partial_sum(vec.begin(),vec.end(),accum.begin());
    return accum;
  }

  void Stats::add(const std::string& user, const u_int& rev, 
									const Parser::line_type& lt)
  {
		authors_.insert(user);

    std::vector<u_int>* total = &(total_[user]);
    if (total->size() < rev+1){
			total->reserve(revision_ + 1);
			total->insert(total->end(), rev - total->size(), 0);
			total->push_back(1);
    }
    else
      (*total)[rev]++;

		std::vector<u_int>* code = &(code_[user]);
    if (code->size() < rev+1){
			code->reserve(revision_ + 1);
			code->insert(code->end(), rev - code->size(), 0);
			if (lt == Parser::code)
				code->push_back(1);
			else 
				code->push_back(0);
    }
    else if (lt == Parser::code)
			(*code)[rev]++;

		std::vector<u_int>* comments = &(comments_[user]);
    if (comments->size() < rev+1){
			comments->reserve(revision_ + 1);
			comments->insert(comments->end(), rev - comments->size(), 0);
			if (lt == Parser::comment)
				comments->push_back(1);
			else 
				comments->push_back(0);
    }
    else if (lt == Parser::comment)
			(*comments)[rev]++;
  }


	bool Stats::parse(const std::string& path)
	{
		SVNblame svn_blame(path);
		if (svn_blame.binary())
			return true;

		Parser parser(path);
		std::vector<Parser::line_type>::const_iterator count=parser.type().begin();

		while (const SVNblame::blame_information * bi=svn_blame.next()) {
			if (!bi->line.size()) { // skip empty line
				++count; // keep Parser information in sync with Stats parsing
				continue;
			}
			// to handle symbolic links
			if (count==parser.type().end())
				add(bi->author, bi->revision, Parser::empty);
			else 
				add(bi->author, bi->revision, *count);
			count++;
		}
		
		return false;
	}


	std::string Stats::plot(const std::string& filename,
													const std::string& title) const
	{
		GnuplotFE* gp=GnuplotFE::instance();
		gp->command("set term png transparent");
		gp->command("set output '"+filename+"'");
		gp->command("set title '"+title+"'");
		gp->command("set xtics nomirror");
		gp->command("set ytics nomirror");
		gp->command("set key default");
		gp->command("set key left Left reverse");
		gp->command("set multiplot");
		std::vector<u_int> total=accumulated(total_);		
		double yrange_max=1.03*total.back()+1;
		gp->yrange(yrange_max);
		size_t plotno=1;
		std::stringstream ss;
		for (MapConstIter_ i= total_.begin(); i != total_.end(); i++) {
			ss.str("");
			ss << "set key height " << 2*plotno;
			gp->command(ss.str());
			std::vector<u_int> x=accumulated(total_, i->first);
			ss.str("");
			ss << x.back() << " " << i->first;
			gp->yrange(yrange_max);
			gp->linetitle(ss.str());
			ss.str("");
			ss << "steps " << ++plotno;
			gp->linestyle(ss.str());
 			gp->plot(x);
		}
		ss.str("");
		ss << total.back() << " total";
		gp->command("set key height 0");
		gp->linetitle(ss.str());
		gp->linestyle("steps 1");
		gp->plot(total);

		gp->command("unset multiplot");
		gp->yrange();

		return filename;
	}


  Stats& Stats::operator+=(const Stats& other)
  {
    for (MapConstIter_ o_i= other.code_.begin(); 
				 o_i != other.code_.end(); ++o_i)
    {
      std::pair<MapIter_,bool> result = code_.insert(*o_i);
      if (!result.second)
				code_[(*(result.first)).first] = 
					VectorPlus<u_int>()( (*(result.first)).second, (*o_i).second );
	
    }
 
    for (MapConstIter_ o_i= other.comments_.begin(); 
				 o_i != other.comments_.end(); ++o_i)
    {
      std::pair<MapIter_,bool> result = comments_.insert(*o_i);
      if (!result.second)
				comments_[(*(result.first)).first] = 
					VectorPlus<u_int>()( (*(result.first)).second, (*o_i).second );
	
    }
		
    for (MapConstIter_ o_i= other.total_.begin(); 
				 o_i != other.total_.end(); ++o_i)
    {
      std::pair<MapIter_,bool> result = total_.insert(*o_i);
      if (!result.second)
				total_[(*(result.first)).first] = 
					VectorPlus<u_int>()( (*(result.first)).second, (*o_i).second );
	
    }
		
		if (!other.authors().empty())
			authors_.insert(other.authors().begin(), other.authors().end());
    return *this;
  }

}} // end of namespace svndigest and namespace theplu
