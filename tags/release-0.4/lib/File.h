// $Id: File.h 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2005, 2006 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#ifndef _theplu_svndigest_file_
#define _theplu_svndigest_file_

#include "Node.h"

#include <string>

namespace theplu{
namespace svndigest{

	class File : public Node
	{
	public:
		/// 
		/// @brief Default Constructor 
		/// 
		File(const u_int level, const std::string& path, 
				 const std::string& output="") 
			: Node(level,path,output), binary_(false), ignore_(false) {}

		///
		/// @return A properly formatted html link to this node.
		///
		std::string html_link(void) const;

		///
		/// @brief Parsing out information from svn repository
		///
		/// @return true if succesful
		///
		const Stats& parse(const bool verbose=false);

		///
		///
		///
		void print(const bool verbose=false) const;

	private:
		///
		/// @brief Parsing svn blame output
		///
		/// @return true if parsing is succesful
		///
		bool blame(void) const;

		///
		/// @brief Copy Constructor, not implemented
		///
		File(const File&);

		bool binary_;
		bool ignore_;
	};

}} // end of namespace svndigest and namespace theplu

#endif


