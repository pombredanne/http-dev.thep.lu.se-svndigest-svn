// $Id: SVNblame.cc 149 2006-08-12 09:11:46Z jari $

/*
	Copyright (C) 2006 Jari H�kkinen

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "SVNblame.h"
#include "SVN.h"

#include <string>

namespace theplu {
namespace svndigest {


	SVNblame::SVNblame(const std::string& path)
		: binary_(false), instance_(SVN::instance())
	{
		if (svn_error_t* err=
				instance_->client_blame(path.c_str(), blame_receiver,
																static_cast<void*>(&blame_receiver_baton_))) {
			// SVN_ERR_CLIENT_IS_BINARY_FILE is the only error allowed to
			// escape the client_blame call
			svn_error_clear(err);
			binary_=true;
		}
		blame_info_iterator_ = blame_receiver_baton_.blame_info.begin();
	}


	SVNblame::~SVNblame(void)
	{
		std::vector<blame_information*>::iterator i=
			blame_receiver_baton_.blame_info.begin();
		while (i!=blame_receiver_baton_.blame_info.end()) {
			delete *i;
			++i;
		}
	}


	const SVNblame::blame_information* SVNblame::next(void)
	{
		blame_information *bi=NULL;
		if (blame_info_iterator_!=blame_receiver_baton_.blame_info.end()) {
			bi=*blame_info_iterator_;
			++blame_info_iterator_;
		}
		return bi;
	}


	svn_error_t *
	SVNblame::blame_receiver(void *baton, apr_int64_t line_no,
													 svn_revnum_t revision, const char *author,
													 const char *date, const char *line, apr_pool_t *pool)
	{
		blame_information *bi=new blame_information;
		bi->line_no=line_no;
		bi->revision=revision;
		bi->author=author;
		bi->date=date;
		bi->line=line;
		static_cast<struct blame_receiver_baton_*>(baton)->blame_info.push_back(bi);
		return SVN_NO_ERROR;
	}


}} // end of namespace svndigest and namespace theplu
