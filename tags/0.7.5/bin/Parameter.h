#ifndef _theplu_svndigest_parameter_
#define _theplu_svndigest_parameter_

// $Id: Parameter.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "OptionVersion.h"

#include "yat/CommandLine.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"


#include <string>

namespace theplu {
namespace yat { 
namespace utility {
	class OptionHelp;
	class OptionSwitch;
}}
namespace svndigest {

	class OptionVersion;

  // class for command line options.
	class Parameter {
	public:
		Parameter( int argc, char *argv[]);
		virtual ~Parameter(void);
		std::string config_file(void) const;
		
		bool copyright(void) const;
		bool force(void) const;
		bool generate_config(void) const ;
		bool ignore_cache(void) const;
		bool report(void) const;
		bool revisions(void) const;
		/// @return absolute path to root directory
		std::string root(void) const;
		/// @return absolute path to target directory
		std::string targetdir(void) const;
		bool verbose(void) const;

	private:
		void analyse(void);
		// throw cmd_error if path doesn't exist
		void check_existence(std::string path) const;
		// throw cmd_error if path is not dir
		void check_is_dir(std::string path) const;
		// throw cmd_error if path is not readable
		void check_readable(std::string path) const;

		void init(void);

		yat::utility::CommandLine cmd_;
		yat::utility::OptionArg<std::string> config_file_;
		yat::utility::OptionSwitch copyright_;
		yat::utility::OptionSwitch force_;
		yat::utility::OptionSwitch generate_config_;
		yat::utility::OptionHelp help_;
		yat::utility::OptionSwitch ignore_cache_;
		yat::utility::OptionSwitch report_;
		yat::utility::OptionSwitch revisions_;
		yat::utility::OptionArg<std::string> root_;
		yat::utility::OptionArg<std::string> target_;
		yat::utility::OptionSwitch verbose_;
		OptionVersion version_;

	};

}} // of namespace svndigest and namespace theplu

#endif
