#ifndef _theplu_svndigest_option_version_
#define _theplu_svndigest_option_version_

// $Id: OptionVersion.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2008, 2009 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "yat/OptionSwitch.h"

#include <string>

namespace theplu {
namespace yat {
namespace utility {
	class CommandLine;
}}
namespace svndigest {

	/**
		 \brief Class for version option

		 When this option is found in parsing of commandline, it displays
		 version output.
	 */
	class OptionVersion : public yat::utility::OptionSwitch
	{
	public:
		/**
			 \brief Constructor 
			 
			 \param cmd Commandline Option is associated with
			 \param name string such as "help" for --help, "h" for -h or
			 "h,help" (default) for having both short and long option name
			 \param desc string used in help display
		*/
		OptionVersion(yat::utility::CommandLine& cmd, std::string name="version", 
									std::string desc="print version information and exit",
									OptionSwitch* const verbose=NULL); 

	private:
		OptionSwitch* const verbose_;


		/**
		 */
		void do_parse2(std::vector<std::string>::iterator, 
									 std::vector<std::string>::iterator);

	};

}} // of namespace svndigest and theplu

#endif
