// $Id: Date.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Date.h"

#include <algorithm>
#include <cassert>
#include <ctime>
#include <sstream>
#include <stdexcept>
#include <string>

namespace theplu {
namespace svndigest {

	Date::Date(void)
	{
		std::time(&time_);
	}


	Date::Date(const Date& other)
		: time_(other.time_)
	{
	}


	Date::Date(std::string str)
	{
		std::time(&time_);
		svntime(str);
	}


	std::string Date::difftime(const Date& other) const
	{
		std::stringstream ss;
		time_t t0 = std::min(seconds(), other.seconds());
		time_t t1 = std::max(seconds(), other.seconds());
		struct tm* last = std::gmtime(&t1);

		unsigned int year=0;
		while (t0<=t1) {
			++year;
			--last->tm_year;
			t1=mktime(last);
		}
		--year;
		++last->tm_year;
		t1=mktime(last);
		if (year) {
			ss << year << " year";
			if (year>1)
				ss << "s";
			ss << " ";
		}

		unsigned int month=0;
		while (t0<=t1) {
			++month;
			--last->tm_mon;
			t1=mktime(last);
		}
		--month;
		++last->tm_mon;
		t1=mktime(last);
		if (month || year) {
			ss << month << " month";
			if (month>1 || (year && !month))
				ss << "s";
			ss << " and ";
		}
		

		unsigned int day = (t1-t0)/24/60/60;
		ss << day << " day";
		if (day!=1)
			ss << "s";
		ss << " ";

		return ss.str();
	}


	void Date::svntime(std::string str)
	{
		std::stringstream sstream(str);
		struct tm* timeinfo = std::localtime(&time_);
		time_t timezone_correction = timeinfo->tm_gmtoff;

		unsigned int year, month, day, hour, minute, second;
		std::string tmp;
		std::getline(sstream,tmp,'-');
		year=atoi(tmp.c_str());
		timeinfo->tm_year = year - 1900;

		std::getline(sstream,tmp,'-');
		month=atoi(tmp.c_str());
		timeinfo->tm_mon = month - 1;

		std::getline(sstream,tmp,'T');
		day=atoi(tmp.c_str());
		timeinfo->tm_mday = day;

		std::getline(sstream,tmp,':');
		hour=atoi(tmp.c_str());
		timeinfo->tm_hour = hour;

		std::getline(sstream,tmp,':');
		minute=atoi(tmp.c_str());
		timeinfo->tm_min = minute;

		std::getline(sstream,tmp,'.');
		second=atoi(tmp.c_str());
		timeinfo->tm_sec = second;

		time_ = mktime(timeinfo);
		time_ += timezone_correction;
	}


	std::string Date::operator()(std::string format) const
	{
		std::stringstream ss;
		struct tm* timeinfo = std::gmtime(&time_);
		char buffer[80]; 
		size_t res = std::strftime(buffer, 80, format.c_str(), timeinfo);
		if (!res) {
			throw std::runtime_error("svndigest::Date::operator() failed");
		}
		return buffer;
	}


	const Date& Date::operator=(const Date& rhs) 
	{
		time_ = rhs.time_;
		return *this;
	}


}}
