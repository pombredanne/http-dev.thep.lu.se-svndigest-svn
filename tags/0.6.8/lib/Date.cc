// $Id: Date.cc 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Date.h"

#include <algorithm>
#include <sstream>
#include <string>

namespace theplu {
namespace svndigest {

	static const char* wdays_[7] = {
		"Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", 
		"Saturday" 
	};

		
	static const char* month_[12] = {
		"January", "February", "March", "April", "May", "June", "July", "August", 
		"September", "October", "November", "December"
	}; 


	Date::Date(void)
	{
		std::time(&time_);
	}


	Date::Date(const Date& other)
		: time_(other.time_)
	{
	}


	Date::Date(std::string str)
	{
		std::time(&time_);
		svntime(str);
	}


	std::string Date::difftime(const Date& other) const
	{
		std::stringstream ss;
		time_t t0 = std::min(seconds(), other.seconds());
		time_t t1 = std::max(seconds(), other.seconds());
		struct tm* last = std::gmtime(&t1);

		unsigned int year=0;
		while (t0<=t1) {
			++year;
			--last->tm_year;
			t1=mktime(last);
		}
		--year;
		++last->tm_year;
		t1=mktime(last);
		if (year) {
			ss << year << " year";
			if (year>1)
				ss << "s";
			ss << " ";
		}

		unsigned int month=0;
		while (t0<=t1) {
			++month;
			--last->tm_mon;
			t1=mktime(last);
		}
		--month;
		++last->tm_mon;
		t1=mktime(last);
		if (month || year) {
			ss << month << " month";
			if (month>1 || (year && !month))
				ss << "s";
			ss << " and ";
		}
		

		unsigned int day = (t1-t0)/24/60/60;
		ss << day << " day";
		if (day!=1)
			ss << "s";
		ss << " ";

		return ss.str();
	}


	void Date::svntime(std::string str)
	{
		std::stringstream sstream(str);
		struct tm* timeinfo = std::localtime(&time_);
		time_t timezone_correction = timeinfo->tm_gmtoff;

		unsigned int year, month, day, hour, minute, second;
		std::string tmp;
		std::getline(sstream,tmp,'-');
		year=atoi(tmp.c_str());
		timeinfo->tm_year = year - 1900;

		std::getline(sstream,tmp,'-');
		month=atoi(tmp.c_str());
		timeinfo->tm_mon = month - 1;

		std::getline(sstream,tmp,'T');
		day=atoi(tmp.c_str());
		timeinfo->tm_mday = day;

		std::getline(sstream,tmp,':');
		hour=atoi(tmp.c_str());
		timeinfo->tm_hour = hour;

		std::getline(sstream,tmp,':');
		minute=atoi(tmp.c_str());
		timeinfo->tm_min = minute;

		std::getline(sstream,tmp,'.');
		second=atoi(tmp.c_str());
		timeinfo->tm_sec = second;

		time_ = mktime(timeinfo);
		time_ += timezone_correction;
	}


	std::string Date::operator()(std::string format) const
	{
		std::stringstream ss;
		struct tm* timeinfo = std::gmtime(&time_);
		for (std::string::iterator i=format.begin(); i!=format.end(); ++i) {
			if (*i == '%' && ++i !=format.end()) {
				if (*i == 'a')
					ss << std::string(wdays_[timeinfo->tm_wday]).substr(0,3);
				else if (*i == 'A')
					ss << wdays_[timeinfo->tm_wday];
				else if (*i == 'b')
					ss << std::string(month_[timeinfo->tm_mon]).substr(0,3);
				else if (*i == 'B')
					ss << month_[timeinfo->tm_mon];
				else if (*i == 'd')
					ss << timeinfo->tm_mday;
				else if (*i == 'e') {
					if (timeinfo->tm_mday<10)
						ss << "0";
					ss << timeinfo->tm_mday;
				}
				else if (*i == 'H') {
					if (timeinfo->tm_hour<10)
						ss << "0";
					ss << timeinfo->tm_hour;
				}
				else if (*i == 'I') {
					int tmp = (timeinfo->tm_hour + 11) % 12 + 1;
					if (tmp<10)
						ss << "0";
					ss << tmp;
				}
				else if (*i == 'j')
					ss << timeinfo->tm_yday+1;
				else if (*i == 'm') {
					if (timeinfo->tm_mon<9)
						ss << "0";
					ss << timeinfo->tm_mon+1;
				}
				else if (*i == 'M') {
					if (timeinfo->tm_min<10)
						ss << "0";
					ss << timeinfo->tm_min;
				}
				else if (*i == 'P')
					if (timeinfo->tm_hour<12)
						ss << "AM";
					else
						ss << "PM";
				else if (*i == 'S') {
					if (timeinfo->tm_sec<10)
						ss << "0";
					ss << timeinfo->tm_sec;
				}
				else if (*i == 'w')
					if (timeinfo->tm_wday==0)
						ss << "7";
					else
						ss << timeinfo->tm_wday;
				else if (*i == 'y') {
					int y(timeinfo->tm_year % 100);
					if (y<10)
						ss << "0";
					ss << y;
				}
				else if (*i == 'Y')
					ss << timeinfo->tm_year+1900;
				else if (*i == 'Z')
					ss << timeinfo->tm_zone;
				else {
					ss << '%';
					--i;
				}
			}
			else
				ss << *i;
		}

		return ss.str();
	}


	const Date& Date::operator=(const Date& rhs) 
	{
		time_ = rhs.time_;
		return *this;
	}


}}
