#ifndef _theplu_svndigest_commitment_
#define _theplu_svndigest_commitment_

// $Id: Commitment.h 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2007, 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Date.h"

#include <string>

namespace theplu {
namespace svndigest {

	/**
		 The Commitment is an aggregrate containing the information from
		 one commitment in the log.

		 \see SVNlog
	*/
	class Commitment {
	public:

		/**
			 \brief Default contructor.
		*/
		Commitment(void);

		/**
			 \brief The contructor.
		*/
		Commitment(std::string author, const Date& date, std::string msg, 
							 size_t rev);

		/**
			 \return Author
		*/
		inline std::string author(void) const { return author_; }

		/**
			 \return Date
		*/
		inline const Date& date(void) const { return date_; }

		/**
			 \return Message
		*/
		inline std::string message(void) const { return msg_; }

		/**
			 \return Revision
		*/
		inline size_t revision(void) const { return rev_; }

	private:
		// Using compiler-generated Copy Constructor.
		// Commitment(const Commitment&);
		//
		// Using compiler-generated Copy assignment.
		// Commitment& operator=(const Commitment&);

		std::string author_;
		Date date_;
		std::string msg_;
		size_t rev_;

	};

	
	struct GreaterRevision
	{
		inline bool operator()(const Commitment& lhs, const Commitment& rhs)
		{ return lhs.revision()>rhs.revision(); }
	};

}} // end of namespace svndigest and namespace theplu

#endif
