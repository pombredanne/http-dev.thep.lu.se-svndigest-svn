#ifndef _theplu_svndigest_first_page_
#define _theplu_svndigest_first_page_

// $Id: first_page.h 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <iosfwd>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	class Commitment;
	class Stats;
	class SVNlog;

	void print_authors(std::ostream& os, 
										 const std::vector<Commitment>& latest_commit,
										 const Stats& stats);

	///
	/// called by print_main_page
	///
	void print_general_information(std::ostream&, const SVNlog&, size_t, 
																 std::string url);

	///
	/// @brief print main page
	///
	void print_main_page(const std::string&, const SVNlog&, const Stats&,
											 std::string url);
		
	void print_recent_logs(std::ostream&, const SVNlog& log);
		
	void print_summary_plot(std::ostream&, const Stats& stats);
		
}} // end of namespace svndigest end of namespace theplu

#endif 
