#ifndef _theplu_svndigest_trac_
#define _theplu_svndigest_trac_

// $Id: Trac.h 764 2009-01-31 19:41:08Z peter $

/*
	Copyright (C) 2007, 2008, 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <string>

namespace theplu{
namespace svndigest{

	class HtmlStream;

	///
	/// Class responsible for filtering log messages and detect
	/// trac:links and create appropriate anchors.
	///
	class Trac
	{
	public:
		///
		/// \Constructor
		///
		Trac(HtmlStream& html);
		
		///
		/// 
		///
		void print(std::string str, size_t width);

	private:
		/**
		 */
		std::string anchor_text(std::string::const_iterator first,
														std::string::const_iterator last,
														std::string::const_iterator last_trunc);
		///
		/// @see changeset1 changeset2 changeset3
		///
		/// @return true if any of changesetX returns true
		///
		bool changeset(const std::string::const_iterator& first, 
									 std::string::const_iterator& iter,
									 const std::string::const_iterator& last,
									 const std::string::const_iterator& last_trunc);

		///
		/// \brief search for 'r123'
		///
		/// Search in range [\a first, \a last) for expression
		/// /r(\d+)/. In addition character before cannot be
		/// alpha-numeric, and character after expression cannot be
		/// alpha-numeric or ':' (beginning/end of string is allowed). If
		/// expression is found an anchor to trac-root/changeset/123,
		/// displaying expression, and first is pointing to character
		/// after expression.
		///
		/// @return true if expression is found
		///
		bool changeset1(const std::string::const_iterator& first, 
										std::string::const_iterator& iter,
										const std::string::const_iterator& last,
										const std::string::const_iterator& last_trunc);

		///
		/// \brief search for '[123]'
		///
		/// Search in range [\a first, \a last) for expression
		/// /\[(\d+)\]/. If expression is found an anchor to
		/// trac-root/changeset/123, displaying expression, and first is
		/// pointing to character after expression.
		///
		/// @return true if expression is found
		///
		bool changeset2(std::string::const_iterator& first, 
										const std::string::const_iterator& last,
										const std::string::const_iterator& last_trunc);

		///
		/// \brief search for changeset:123
		///
		/// Search in range [\a first, \a last) for expression
		/// /changeset:(\d+)/. If expression is found an anchor to
		/// trac-root/changeset/123, displaying expression, and first is
		/// pointing to character after expression.
		///
		/// @return true if expression is found
		///
		bool changeset3(std::string::const_iterator& first, 
										const std::string::const_iterator& last,
										const std::string::const_iterator& last_trunc);

		///
		/// \brief search for /comment:ticket:123:1/
		///
		/// Search in range [\a first, \a last) for expression
		/// /comment:ticket:(\w+):(\d+)/. If expression is found an anchor
		/// to trac-root/ticket/123#comment:1, displaying expression, and
		/// first is pointing to character after expression.
		///
		/// @return true if expression is found
		///
		bool comment(std::string::const_iterator& first, 
								 const std::string::const_iterator& last,
								 const std::string::const_iterator& last_trunc);

		///
		/// @see diff1 diff2 diff3
		///
		/// @return true if any of diffX returns true
		///
		bool diff(std::string::const_iterator& first, 
							const std::string::const_iterator& last,
							const std::string::const_iterator& last_trunc);

		///
		/// \brief search for diff:trunk@12:123
		///
		/// Search in range [\a first, \a last) for expression
		/// /diff:(.*)@(\w+):(\w+)/. If expression is found an anchor is
		/// created, displaying the expression, and first is pointing to
		/// character after expression.
		///
		/// If $1 (trunk) is empty anchor goes to
		/// trac-root/changeset?new=123&old=12 otherwise it goes to
		/// trac-root/changeset?new=123&newpath=trunk&old=12&oldpath=trunk
		///
		/// @return true if expression is found
		///
		bool diff1(std::string::const_iterator& first, 
							 const std::string::const_iterator& last,
							 const std::string::const_iterator& last_trunc);

		///
		/// @brief search for diff:tags/1.0 or diff:tags/1.0//tags/1.0.1
		///
		/// Search in range [\a first, \a last) for expression
		/// /diff:(^\s+)/ or /diff:(^\s+)\/\/(^\s+)/. If expression is found
		/// an anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to
		/// trac-root/changeset?new_path=tags/1.0&old_path=tags/1.0 or
		/// trac-root/changeset?new_path=tags/1.0&old_path=tags/1.0.1
		/// respectively.
		///
		/// @return true if expression is found
		///
		bool diff2(std::string::const_iterator& first, 
							 const std::string::const_iterator& last,
							 const std::string::const_iterator& last_trunc);

		///
		/// @brief search for diff:tags/1.0@123//trunk@236
		///
		/// Search in range [\a first, \a last) for expression
		/// /diff:(^\s+)@(\w+)\/\/(^\s+)@(\w+)/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to
		/// trac-root/changeset?new=236&new_path=trunk&old=123&old_path=tags/1.0
		///
		/// @return true if expression is found
		///
		bool diff3(std::string::const_iterator& first, 
							 const std::string::const_iterator& last,
							 const std::string::const_iterator& last_trunc);


		///
		/// @see log1 log2 log3
		///
		/// @return true if any of logX returns true
		///
		bool log(const std::string::const_iterator& first, 
						 std::string::const_iterator& iter,
						 const std::string::const_iterator& last,
						 const std::string::const_iterator& last_trunc);

		///
		/// @brief search for r123:236
		///
		/// Search in range [\a first, \a last) for expression
		/// /r(\d+):(\d+)/. In addition character before and after
		/// expression cannot be alpha-numeric (beginning/end of string is
		/// allowed). If expression is found an anchor is created,
		/// displaying the expression, and iter is pointing to character
		/// after expression.
		///
		/// The created anchor goes to trac-root/log/?rev=236&stop_rev=123
		///
		/// @return true if expression is found
		///
		bool log1(const std::string::const_iterator& first,
							std::string::const_iterator& iter, 
							const std::string::const_iterator& last,
							const std::string::const_iterator& last_trunc);

		///
		/// @brief search for [123:236]
		///
		/// Search in range [\a first, \a last) for expression
		/// /\[(\w+):(\w+)\]/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to trac-root/log/?rev=236&stop_rev=123
		///
		/// @return true if expression is found
		///
		bool log2(std::string::const_iterator& first, 
							const std::string::const_iterator& last,
							const std::string::const_iterator& last_trunc);

		///
		/// @brief search for log:trunk@123:236 or log:trunk#123:236
		///
		/// Search in range [\a first, \a last) for expression
		/// /log:(^\s*)(@|#)(\w+):(\w+)/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to trac-root/log/trunk?rev=236&stop_rev=123
		///
		/// @return true if expression is found
		///
		bool log3(std::string::const_iterator& first, 
							const std::string::const_iterator& last,
							const std::string::const_iterator& last_trunc);

		///
		/// @brief search for milestone:1.0
		///
		/// Search in range [\a first, \a last) for expression
		/// /milestone:(^s\*)\w/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to trac-root/milestone/1.0
		///
		/// @return true if expression is found
		///
		bool milestone(std::string::const_iterator& first, 
									 const std::string::const_iterator& last,
									 const std::string::const_iterator& last_trunc);

		///
		/// @brief search for source:trunk or source:trunk@123 or
		/// source:trunk@123#L3 
		///
		/// Search in range [\a first, \a last) for expression
		/// /source:(^s\*)/, /source:(^s\*)@(\w+)/ or
		/// /source:(^s\*)@(\w+)#L(\d+)/. If expression is found an anchor
		/// is created, displaying the expression, and first is pointing
		/// to character after expression.
		///
		/// The created anchor goes to trac-root/browser/trunk or
		/// trac-root/browser/trunk?rev=123 or
		/// trac-root/browser/trunk?rev=123#L3
		///
		/// @return true if expression is found
		///
		bool source(std::string::const_iterator& first, 
								const std::string::const_iterator& last,
								const std::string::const_iterator& last_trunc);

		///
		/// @see ticket1 ticket2
		///
		/// @return true ticket1 or ticket2 returns true
		///
		bool ticket(std::string::const_iterator& first, 
								const std::string::const_iterator& last,
								const std::string::const_iterator& last_trunc);

		///
		/// @brief search for #65
		///
		/// Search in range [\a first, \a last) for expression
		/// /#(\d+)/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to trac-root/ticket/65
		///
		/// @return true if expression is found
		///
		bool ticket1(std::string::const_iterator& first, 
								 const std::string::const_iterator& last,
								 const std::string::const_iterator& last_trunc);

		///
		/// @brief search for ticket:65
		///
		/// Search in range [\a first, \a last) for expression
		/// /ticket:(\d+)/. If expression is found an
		/// anchor is created, displaying the expression, and first is
		/// pointing to character after expression.
		///
		/// The created anchor goes to trac-root/ticket/65
		///
		/// @return true if expression is found
		///
		bool ticket2(std::string::const_iterator& first, 
								 const std::string::const_iterator& last,
								 const std::string::const_iterator& last_trunc);



		HtmlStream& hs_;
	};

}} // end of namespace svndigest end of namespace theplu

#endif 
