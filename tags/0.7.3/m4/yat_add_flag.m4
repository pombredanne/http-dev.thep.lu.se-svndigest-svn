## $Id: yat_add_flag.m4 1780 2009-02-06 18:13:22Z peter $

# SYNOPSIS
#
#   YAT_CPP_ADD_FLAG([FLAGS], [FLAG])
#
#   YAT_CXX_ADD_FLAG([FLAGS], [FLAG])
#
#   YAT_LD_ADD_FLAG([FLAGS], [FLAG])
#
# DESCRIPTION
#
#   Check that FLAG is not already included in FLAGS and test that
#   FLAG is supported by C++ compiler. If true FLAG is appended to
#   FLAGS.
#
# LAST MODIFICATION
#
#   $Date: 2009-02-06 13:13:22 -0500 (Fri, 06 Feb 2009) $
#
# COPYLEFT
#
#   Copyright (C) 2008, 2009 Peter Johansson
#
#   This file is part of the yat library, http://dev.thep.lu.se/yat
#
#   The yat library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 3 of the
#   License, or (at your option) any later version.
#
#   The yat library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with yat. If not, see <http://www.gnu.org/licenses/>.
#

#
# serial 2  
#
# see http://www.gnu.org/software/automake/manual/automake.html#Serials

AC_DEFUN([YAT_CPP_ADD_FLAG],
[
  for i in $2; do
    YAT_FIND_STR([$1], [$i], , [AX_CXXCPP_CHECK_FLAG([$i], ,, [$1="$$1 $i"])])
  done
]) # YAT_CPP_ADD_FLAG


AC_DEFUN([YAT_CXX_ADD_FLAG],
[
  for i in $2; do
    YAT_FIND_STR([$1], [$i], , [AX_CXX_CHECK_FLAG([$i], ,, [$1="$$1 $i"])])
  done
]) # YAT_CXX_ADD_FLAG

AC_DEFUN([YAT_LD_ADD_FLAG],
[
  for i in $2; do
 	  YAT_FIND_STR([$1], [$i], , [AX_LD_CHECK_FLAG([$i], ,, [$1="$$1 $i"])])
  done
]) # YAT_LD_ADD_FLAG

AC_DEFUN([YAT_FIND_STR],
[
  # YAT_FIND_STR
  found=no;				
  for a in $$1; do 
    AS_IF([test "x$a" = "x$2"],[found=yes])
  done

  # ACTION
  AS_IF([test "$found" = "yes"], [m4_default([$3], [:])],
        [m4_default([$4], [:])])

]) # YAT_FIND_STR

