#ifndef _theplu_yat_utility_option_
#define _theplu_yat_utility_option_

// $Id: Option.h 2046 2009-09-02 21:42:32Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	class CommandLine;
	///
	/// @brief Container of variables for an option. @see CommandLine
	///
	class Option
	{
	public:
		/**
			 @brief Constructor 
			 
			 \param cmd Commandline to be hooked up with.
			 @param name string such as "help" for --help, "h" for -h or
			 "h,help" for having both short and long option name
			 @param desc string used in help display
		*/
		Option(CommandLine& cmd, std::string name, std::string desc); 

		/**
			 @brief destructor
		*/
		virtual ~Option(void);
		
		/**
			 @return description
		*/
		std::string description(void) const;

		/**
			 \return long name e.g. 'help' for --help option.
		*/
		std::string long_name(void) const;

		/**
			 \brief parsing the commandline
		 */
		void parse(std::vector<std::string>::iterator&, 
							 const std::vector<std::string>::iterator&);

		/**
			 @brief Get if option was found in cmd.
			 
			 @return true if option has been detected in parsing
		*/
		bool present(void) const;

		/**
			 \brief print help output

			 This function calls the four virtual private functions print1,
			 print2, print3, and print4. This allows an inherited class to
			 implement one (or several) of these functions and keep the
			 default output of the others. The default behavior is that:

			 - print1 prints the short name, '-h', as short_name(void) const
			 - print2 prints the long name, '--help', as long_name(void) const 
			 - print3 is empty 
			 - print4 prints the description as description(void) const.
		 */
		std::string print(void);

		/**
			 \brief sets present to false
		*/
		void reset(void);

		/**
			 \return short name e.g. 'h' for -h option.
		*/
		char short_name(void) const;

		/**
			 \brief Validate the Option

			 This function is called by CommandLine::parse() after all
			 options have been detected and parsed (see Option::parse()).
		 */
		void validate(void);

	protected:
		/**
			 \return const reference to CommandLine Option belongs to. 
		 */
		const CommandLine& cmd(void) const;

	private:
		virtual void do_parse(std::vector<std::string>::iterator&, 
													const std::vector<std::string>::iterator&)=0;

		/**
		 */
		virtual std::string print1(void) const;

		/**
		 */
		virtual std::string print2(void) const;

		/**
		 */
		virtual std::string print3(void) const;

		/**
		 */
		virtual std::string print4(void) const;

		/**
		 */
		virtual void do_validate(void) const=0;


		const CommandLine& cmd_;
		std::string description_;
		std::string long_name_;
		bool present_;
		char short_name_;

		// copy not allowed
		Option(const Option&);
		Option& operator=(const Option&);
	};

}}} // of namespace utility, yat, and theplu

#endif
