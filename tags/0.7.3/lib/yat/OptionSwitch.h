#ifndef _theplu_yat_utility_option_switch_
#define _theplu_yat_utility_option_switch_

// $Id: OptionSwitch.h 1797 2009-02-12 18:07:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Option.h"

#include <string>

namespace theplu {
namespace yat {
namespace utility {

	class CommandLine;
	/**
		 \brief Class for switch option
	 */
	class OptionSwitch : public Option
	{
	public:
		/**
			 \brief Constructor 
			 
			 \param cmd Commandline Option is associated with
			 \param name string such as "help" for --help, "h" for -h or
			 "h,help" for having both short and long option name
			 \param desc string used in help display
			 \param def default value for switch
		*/
		OptionSwitch(CommandLine& cmd, std::string name, 
								 std::string desc, bool def=false); 


		/**
			 \brief return value
		 */
		bool value(void) const;

	private:
		/**
		 */
		void do_parse(std::vector<std::string>::iterator&, 
									const std::vector<std::string>::iterator&);

		/**
			 called inside do_parse and allows inherited classes to add
			 stuff to do_parse.
		 */
		virtual void do_parse2(std::vector<std::string>::iterator, 
													 std::vector<std::string>::iterator);

		/**
		 */
		std::string print2(void) const;

		/**
		 */
		void do_validate(void) const;

	private:
		bool def_;
		bool switch_;
	};

}}} // of namespace utility, yat, and theplu

#endif
