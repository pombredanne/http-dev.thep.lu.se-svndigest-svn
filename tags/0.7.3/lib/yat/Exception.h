#ifndef _theplu_yat_utility_exception_
#define _theplu_yat_utility_exception_

// $Id: Exception.h 1797 2009-02-12 18:07:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdexcept>
#include <string>

namespace theplu {
namespace yat {
namespace utility {

	/**
		 \brief Class used for error reported from Commandline or Option.
	 */
	class cmd_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor

			 \param message message to be displayed using function what().
		 */
		inline cmd_error(std::string message)
			: std::runtime_error(message) {}
	};


	/**
		 \brief Class to report errors associated with IO operations.

		 IO_error is used in the same way as C++ standard library
		 exceptions.
	*/
	class IO_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor to create an exception with a message.
		*/
		inline IO_error(std::string message) throw()
			: std::runtime_error("IO_error: " + message) {}
	};

}}} // of namespace utility, yat, and theplu

#endif
