#ifndef _theplu_svndigest_html_utility_
#define _theplu_svndigest_html_utility_

// $Id: html_utility.h 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <iosfwd>
#include <string>
#include <vector>

#include <subversion-1/svn_types.h>

namespace theplu{
namespace svndigest{

	///
	/// @brief create anchor
	///
	/// @param url address to link to
	/// @param name text visible on page
	/// @param level '../' is added @a level times before @a href
	/// @param title title of anchor
	///
	std::string anchor(const std::string& url,
										 const std::string& name, unsigned int level=0, 
										 const std::string& title="",
										 const std::string& color="");

	///
	/// @Brief print html footer of page
	///
	void print_footer(std::ostream&);
		
	///
	/// @brief print html header of page
	///
	/// \param os stream to print to
	/// \param name
	/// \param level
	/// \param user
	/// \param item total, code, comment, or other
	/// \param path current path
	/// \param stats which stats are we in
	void print_header(std::ostream& os, std::string name, unsigned int level, 
										std::string user, std::string item, std::string path,
										const std::string& stats);


	/**
		 \brief print html start

		 Just like print_header, this function outputs start of html page,
		 but in contrast to print_header there are no menus in the otput
		 of this function.
	 */
	void print_html_start(std::ostream& os, const std::string& title, 
												unsigned int level);


	///
	/// @return if trac-revision is set in config file anchor to trac is
	/// given otherwise just a string corresponding to passed parameter.
	///
	std::string trac_revision(svn_revnum_t, std::string color="");

}} // end of namespace svndigest end of namespace theplu

#endif 
