// $Id: GnuplotFE.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "GnuplotFE.h"
#include "Gnuplot.h"

#include <cassert>
#include <string>
#include <sstream>


namespace theplu {
namespace svndigest {


	GnuplotFE* GnuplotFE::instance_=NULL;


	void GnuplotFE::plot(std::vector<unsigned int>& y, const std::string& format)
	{
		if (!date_.empty()) {
			assert(date_.size()>=y.size());
			if (date_.size()!=y.size()) {
				y.reserve(date_.size());
				for (size_t i=y.size(); i<date_.size(); ++i)
					y.push_back(*y.rbegin());
			}
			command(std::string("set xdata time"));
			command("set timefmt '" + date_input_format_ + "'");
			command("set format x '" + format + "'");
			Gnuplot::plot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::plot(y);
	}


	void GnuplotFE::replot(std::vector<unsigned int>& y)
	{
		if (!date_.empty()) {
			assert(date_.size()>=y.size());
			if (date_.size()!=y.size()) {
				y.reserve(date_.size());
				for (size_t i=y.size(); i<date_.size(); ++i)
					y.push_back(*y.rbegin());
			}
			command(std::string("set xdata time"));
			Gnuplot::replot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::replot(y);
	}


	double GnuplotFE::yrange(double ymax)
	{
		if (ymax<0)
			ymax=0;
		std::ostringstream cmd;
		cmd << "set yrang[0:";
		if (ymax)
			cmd << ymax;
		cmd << "]";

		command(cmd.str());
		return ymax;
	}


}} // end of namespace svndigest and namespace theplu
