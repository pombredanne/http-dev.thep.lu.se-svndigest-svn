// $Id: Functor.cc 847 2009-11-17 01:38:52Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Functor.h"

#include <string>

namespace theplu{
namespace svndigest{

	notChar::notChar(char c)
		: char_(c)
	{}


	not2Char::not2Char(char c1, char c2)
		: char1_(c1), char2_(c2)
	{}
		

	not2Str::not2Str(std::string s1, std::string s2)
		: str1_(s1), str2_(s2)
	{}
		
}} // end of namespace svndigest and namespace theplu
