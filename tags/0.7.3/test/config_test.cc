// $Id: config_test.cc 925 2009-12-02 01:24:41Z peter $

/*
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "Configuration.h"

namespace theplu{
namespace svndigest{
	void test_codon(test::Suite&);
}} // end of namespace svndigest and theplu


int main( int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv, false);

	test_codon(suite);
                                                                                
	if (suite.ok()) {
		suite.out() << "Test is Ok!" << std::endl;
		return 0;
	}
	suite.out() << "Test failed." << std::endl;
	return 1;
}


namespace theplu{
namespace svndigest{

	void test_codon(test::Suite& suite)
	{
		const Configuration& c(Configuration::instance());
		if (!c.codon("foo.h")){
			suite.out() << "No codon for foo.h\n";
			suite.add(false);
		}
		if (!c.codon("../dir/test.cc")){
			suite.out() << "No codon for test.cc\n";
			suite.add(false);
		}
		if (!c.codon("bootstrap")){
			suite.out() << "No codon for bootstrap\n";
			suite.add(false);
		}
	}
	
}} // end of namespace svndigest and theplu
