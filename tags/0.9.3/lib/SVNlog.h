#ifndef _theplu_svndigest_svnlog_
#define _theplu_svndigest_svnlog_

// $Id: SVNlog.h 978 2009-12-12 20:09:41Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVN.h"

#include "Commitment.h"

#include <string>
#include <set>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	/**
		 The SVNlog class is a utility class for taking care of 'svn
		 log' information. An 'svn log' is performed on an item, the
		 log information for each revision is a vector<Commitment>.
	*/
	class SVNlog {
	public:
		/**
			 container used to store log
		 */
		typedef std::set<Commitment, LessRevision> container;

		/**
			 Default constructor. Creates empty log.
		 */
		SVNlog(void);

		/**
			 \brief The contructor.
			 
			 The constructor performs an 'svn log' on \a path and
			 stores the information for later access.
		*/
		explicit SVNlog(const std::string& path);

		/**
			 \brief The destructor.
		*/
		~SVNlog(void);

		/**
			 \return Commitments
		*/
		inline const container& commits(void) const
		{ return lb_.commits; }

		/**
			 \return Commitments
		*/
		inline container& commits(void)
		{ return lb_.commits; }

		/**
			 \return true if \a author appears in log.
		*/
		bool exist(std::string author) const;

		/**
			 \return Latest commit
		*/
		const Commitment& latest_commit(void) const;

		/**
			 \return Latest commit \a author did. If no author is found an
			 empty Commitment (default constructor) is returned.
		*/
		const Commitment& latest_commit(std::string author) const;

		/**
		 */
		void swap(SVNlog&);
		
	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		// using compiler generated copy constructor
		//SVNlog(const SVNlog&);

		/**
			 log information is stored in the log_receiver_baton. The
			 information is retrieved with the info_* set of member
			 functions. The struct is filled in the info_receiver function.

			 \see info_receiver
		*/
		struct log_receiver_baton {
			container commits;
		} lb_;

		/**
			 info_receiver is the function passed to the underlying
			 subversion API. This function is called by the subversion API
			 for every item matched by the conditions of the API call.

			 \see Subversion API documentation
		*/
		static svn_error_t*
		log_message_receiver(void *baton, apr_hash_t *changed_paths,
												 svn_revnum_t rev, const char *author, const char *date,
												 const char *msg, apr_pool_t *pool);
	};

	/**
	 */
	SVNlog& operator+=(SVNlog&, const SVNlog&);

}} // end of namespace svndigest and namespace theplu

#endif
