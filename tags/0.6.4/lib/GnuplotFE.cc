// $Id: GnuplotFE.cc 489 2007-10-13 23:16:02Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "GnuplotFE.h"
#include "Gnuplot.h"

#include <cassert>
#include <string>
#include <sstream>


namespace theplu {
namespace svndigest {


	GnuplotFE* GnuplotFE::instance_=NULL;


	void GnuplotFE::plot(std::vector<u_int>& y, const std::string& format)
	{
		if (!date_.empty()) {
			assert(date_.size()>=y.size());
			if (date_.size()!=y.size()) {
				y.reserve(date_.size());
				for (size_t i=y.size(); i<date_.size(); ++i)
					y.push_back(*y.rbegin());
			}
			command(std::string("set xdata time"));
			command("set timefmt '" + date_input_format_ + "'");
			command("set format x '" + format + "'");
			Gnuplot::plot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::plot(y);
	}


	void GnuplotFE::replot(std::vector<u_int>& y)
	{
		if (!date_.empty()) {
			assert(date_.size()>=y.size());
			if (date_.size()!=y.size()) {
				y.reserve(date_.size());
				for (size_t i=y.size(); i<date_.size(); ++i)
					y.push_back(*y.rbegin());
			}
			command(std::string("set xdata time"));
			Gnuplot::replot(y,date_);
			command(std::string("set xdata"));
		}
		else
			Gnuplot::replot(y);
	}


	double GnuplotFE::yrange(double ymax)
	{
		if (ymax<0)
			ymax=0;
		std::ostringstream cmd;
		cmd << "set yrang[0:";
		if (ymax)
			cmd << ymax;
		cmd << "]";

		command(cmd.str());
		return ymax;
	}


}} // end of namespace svndigest and namespace theplu
