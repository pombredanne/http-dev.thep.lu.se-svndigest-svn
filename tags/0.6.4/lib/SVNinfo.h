#ifndef _theplu_svndigest_svninfo_
#define _theplu_svndigest_svninfo_

// $Id: SVNinfo.h 430 2007-07-06 23:33:11Z peter $

/*
	Copyright (C) 2006 Jari H�kkinen
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <string>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	///
	/// The SVNinfo class is a utility class for taking care of 'svn
	/// info'.
	///
	class SVNinfo {
	public:

		///
		/// Retrieve meta information about the item with \a path.
		/// 
		/// @note The recursivness of the underlying subversion API is not
		/// allowed (nor supported).
		///
		explicit SVNinfo(const std::string& path);

		///
		/// @brief Get the repository root URL.
		///
		inline const std::string& repos_root_url(void) const
		{ return info_receiver_baton_.repos_root_url_; }

		///
		/// @brief Get the URL.
		///
		inline std::string url(void) const
		{ return info_receiver_baton_.url_; }

		///
		/// @brief Get the author of the latest commit.
		///
		inline const std::string& last_changed_author(void) const
		{ return info_receiver_baton_.last_changed_author_; }

		///
		/// @brief Get the revision of the latest commit.
		///
		inline svn_revnum_t last_changed_rev(void) const
		{ return info_receiver_baton_.last_changed_rev_; }

		///
		/// @brief Get the current revision of the item.
		///
		inline svn_revnum_t rev(void) const { return info_receiver_baton_.rev_; }


	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNinfo(const SVNinfo&);

		///
		/// svn info is stored in the info_receiver_baton_. The
		/// information is retrieved with the info_* set of member
		/// functions. The struct is filled in the info_receiver function.
		///
		/// @see info_receiver
		///
		struct info_receiver_baton {
			// more info is available but we only use these
			std::string url_;
			std::string repos_root_url_;
			std::string last_changed_author_;
			svn_revnum_t last_changed_rev_;
			svn_revnum_t rev_;
		} info_receiver_baton_ ;

		///
		/// info_receiver is the function passed to the underlying
		/// subversion API. This function is called by the subversion API
		/// for every item matched by the conditions of the API call.
		///
		/// @see Subversion API documentation
		///
		// The return type should be svn_log_message_receiver_t but I
		// cannot get it to compile (see comment for log_message_receiver
		// in SVNlog.h)
		static svn_error_t * info_receiver(void *baton, const char *path,
																			 const svn_info_t *info, apr_pool_t *pool);
	};

}} // end of namespace svndigest and namespace theplu

#endif
