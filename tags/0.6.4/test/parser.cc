// $Id: parser.cc 489 2007-10-13 23:16:02Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://trac.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Parser.h"

#include <iostream>
#include <fstream>

bool test(const std::string&, std::ostream&);

int main(const int argc,const char* argv[])
{
	bool ok=true;
	std::ofstream os("parser.tmp");
	ok = ok && test("parser.cc",os);
	ok = ok && test("Makefile.am",os);
	ok = ok && test("../INSTALL",os);
  return 0;
}

bool test(const std::string& file, std::ostream& os)
{
	using namespace theplu::svndigest;
	os << "Testing: " << file << std::endl;
	Parser parser(file);
	std::ifstream is(file.c_str());
	for (size_t i=0; i<parser.type().size(); ++i){
		if (parser.type()[i]==Parser::empty)
			os << "empty:   ";
		else if (parser.type()[i]==Parser::comment)
			os << "comment: ";
		else
			os << "code:    ";
		std::string str;
		getline(is, str);
		os << str << "\n";
	}
	return true;
}
