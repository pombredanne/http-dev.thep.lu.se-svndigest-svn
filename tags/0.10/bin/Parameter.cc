// $Id: Parameter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "Parameter.h"

#include "lib/utility.h"

#include "yat/ColumnStream.h"
#include "yat/Exception.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"

#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	Parameter::Parameter(void)
		: config_file_(cmd_, "config-file",
									 "configuration file [<ROOT>/.svndigest/config]"),
			generate_config_(cmd_, "g,generate-config",
											 "write configuration to standard output"),
			ignore_cache_(cmd_, "ignore-cache",
										std::string("ignore cache files and analyze ") +
										std::string("everything from repository"))
	{
	}


	Parameter::~Parameter(void)
	{
	}


	void Parameter::analyse1(void)
	{
		std::string save_wd = pwd();

		// check root but not if -g option given
		if (!generate_config()) {
			analyse_root(root_.value());
		}

		// check config file
		struct stat nodestat;
		// true also if there is a broken symlink named...
		bool config_exists = !lstat(config_file_.value().c_str(), &nodestat);
		// the latter case in order to catch broken symlink
		if (config_file_.present() || config_exists)
			// throws if file does not exists
			check_existence(config_file_.value());
		if (config_exists) {
			// throws if file is not readable
			check_readable(config_file_.value());
			stat(config_file_.value().c_str(), &nodestat);
			if (S_ISDIR(nodestat.st_mode)) {
				std::stringstream ss;
				ss << cmd_.program_name() << ": '" << config_file_.value()
					 << "' is a directory";
				throw yat::utility::cmd_error(ss.str());
			}
		}
		analyse2();
	}


	std::string Parameter::config_file(void) const
	{
		return config_file_.value();
	}


	bool Parameter::generate_config(void) const
	{
		return generate_config_.present();
	}


	bool Parameter::ignore_cache(void) const
	{
		return ignore_cache_.present();
	}


	void Parameter::init1(void)
	{
		config_file_.print_arg("=FILE");
		init2();
	}


	void Parameter::set_default1(void)
	{
		if (!config_file_.present())
			config_file_.value(concatenate_path(root_.value(),".svndigest/config"));

		set_default2();
	}


}} // of namespace svndigest and namespace theplu
