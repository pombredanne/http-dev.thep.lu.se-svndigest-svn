#ifndef _theplu_svndigest_abstract_parameter_
#define _theplu_svndigest_abstract_parameter_

// $Id: AbstractParameter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib/OptionVersion.h"

#include "yat/CommandLine.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"


#include <string>

namespace theplu {
namespace yat {
namespace utility {
	class OptionHelp;
	class OptionSwitch;
}}
namespace svndigest {

	class OptionVersion;

  // class for command line options.
	class AbstractParameter {
	public:
		AbstractParameter(void);
		virtual ~AbstractParameter(void);
		void parse( int argc, char *argv[]);

		/**
			 @return Dereferenced absolute path to root directory
		*/
		std::string root(void) const;

		/**
			 If --root argument is a link we return the file_name(argument).
			 Otherwise we return file_name(root()). See root(void).

			 \return basename of --root argument.
		 */
		const std::string& root_basename(void) const;
		bool verbose(void) const;

	protected:
		yat::utility::CommandLine cmd_;
		yat::utility::OptionHelp help_;
		yat::utility::OptionArg<std::string> root_;

		// check that root is OK, i.e., an existing, readable and
		// accessible directory. throw cmd_error if not.
		void analyse_root(const std::string root);

		// throw cmd_error if path doesn't exist
		void check_existence(std::string path) const;
		// throw cmd_error if path is not dir
		void check_is_dir(std::string path) const;
		// throw cmd_error if path is not readable
		void check_readable(std::string path) const;

	private:
		void analyse(void);
		virtual void analyse1(void)=0;
		void init(void);
		// called at end of init(void)
		virtual void init1(void) = 0;
		void set_default(void);
		virtual void set_default1(void)=0;

		std::string root_basename_;
		std::string root_full_;
		yat::utility::OptionSwitch verbose_;
	protected:
		OptionVersion version_;

	};

}} // of namespace svndigest and namespace theplu

#endif
