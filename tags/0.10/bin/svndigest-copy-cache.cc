// $Id: svndigest-copy-cache.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "svndigest_copy_cacheParameter.h"

#include "lib/CacheCopyer.h"
#include "lib/Directory.h"
#include "lib/utility.h"

#include "yat/Exception.h"
#include "yat/OptionArg.h"

#include <iostream>

using namespace theplu;
using namespace svndigest;
int main( int argc, char* argv[])
{
	// Reading commandline options
	svndigest_copy_cacheParameter option;
	try {
		option.parse(argc,argv);
		if (option.verbose())
			std::cout << "Done parsing parameters" << std::endl;
	}
	catch (yat::utility::cmd_error& e) {
		std::cerr << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (std::runtime_error& e) {
		std::cerr << "svndigest-copy-cache: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	try {
		if (option.verbose())
			std::cout << "Initializing SVN singleton." << std::endl;
		SVN::instance(option.root());

		Directory tree(0, option.root(), "", option.root_basename());

		chdir(option.root());
		CacheCopyer copyer(option.target(), option.verbose());
		tree.traverse(copyer);
	}
	catch (std::runtime_error& e) {
		std::cerr << "svndigest-copy-cache: " << e.what() << "\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;				// normal exit
}
