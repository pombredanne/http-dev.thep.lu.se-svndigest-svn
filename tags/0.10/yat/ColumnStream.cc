// $Id: ColumnStream.cc 2526 2011-07-25 02:03:35Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011 Peter Johansson

	This file is part of yat, http://dev.thep.lu.se/yat

	yat is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	yat is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ColumnStream.h"

#include <cassert>
#include <ostream>

namespace theplu{
namespace yat{
namespace utility{

	ColumnStream::ColumnStream(std::ostream& os, size_t columns)
		: activated_(0), margins_(columns), os_(os), width_(columns, 8)
	{
		buffer_.reserve(columns);
		while (buffer_.size()<columns)
			buffer_.push_back(new std::stringstream);
	}


	ColumnStream::~ColumnStream(void)
	{
		for (size_t i=0; i<buffer_.size(); ++i)
			delete buffer_[i];
	}


	size_t ColumnStream::columns(void) const
	{
		return buffer_.size();
	}


	void ColumnStream::fill(size_t column, size_t count)
	{
		while(count<width_[column]){
			os_ << ' ';
			++count;
		}
	}


	void ColumnStream::flush(void)
	{
		bool empty=false;
		while(!empty) {
			empty=true;
			for (size_t i=0; i<columns(); ++i){
				if (writeline(i))
					empty=false;
			}
			os_ << '\n';
		}
		for (size_t i=0; i<columns(); ++i)
			buffer_[i]->clear(std::ios::goodbit);
	}


	size_t& ColumnStream::margin(size_t c) 
	{ 
		return margins_[c]; 
	}


	void ColumnStream::next_column(void)
	{
		++activated_;
		if (activated_>=columns()) {
			flush();
		}
	}

	void ColumnStream::print(std::stringstream& ss)
	{
		assert(buffer_[activated_]);
		assert(activated_<buffer_.size());
		char c;
		ss.get(c);
		while(ss.good()) {
			if (c=='\t'){
				//*(buffer_[activated_]) << ' ';
				next_column();
			}
			else if (c=='\n'){
				//*(buffer_[activated_]) << ' ';
				flush();
				activated_=0;
			}
			else 
				*(buffer_[activated_]) << c;
		ss.get(c);
		}
	}


	size_t& ColumnStream::width(size_t c) 
	{ 
		return width_[c]; 
	}


	bool ColumnStream::writeline(size_t column)
	{
		assert(column<columns());
		for (size_t i=0; i<margins_[column]; ++i)
			os_ << ' ';
		size_t count=0;
		std::string word;
		char c;
		while (buffer_[column]->good()) {
			buffer_[column]->get(c);
			assert(c!='\t');
			assert(c!='\n');
			if (buffer_[column]->good())
				word += c;
			if (c==' ' || !buffer_[column]->good()) {
				if (count+word.size()<=width_[column]) {
					os_ << word;
					count += word.size();
					word = "";
				}				
				else {
					if (!buffer_[column]->good())
						buffer_[column]->clear(std::ios::goodbit);
					
					// if line is empty and word is longer than column width, we
					// have to split the word
					if (!count) {
						os_ << word.substr(0,width_[column]);
						for (std::string::reverse_iterator i=word.rbegin();
								 i!=word.rbegin()+(word.size()-width_[column]); ++i)
							buffer_[column]->putback(*i);
					}
					else {
						for (std::string::reverse_iterator i=word.rbegin();
								 i!=word.rend(); ++i)
							buffer_[column]->putback(*i);
						fill(column, count);
					}
					return true;
				}
			}

		} 
		fill(column, count);
		return false;
	}


}}} // end of namespace utility, yat, and theplu
