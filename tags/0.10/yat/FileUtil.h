#ifndef _theplu_yat_utility_fileutil_
#define _theplu_yat_utility_fileutil_ 

// $Id: FileUtil.h 2125 2009-12-22 20:19:41Z peter $

/*
	Copyright (C) 2004 Jari Häkkinen
	Copyright (C) 2005 Jari Häkkinen, Peter Johansson
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <sys/stat.h>
#include <unistd.h>

namespace theplu {
namespace yat {
namespace utility {

	///
	///
	/// @brief Checking file/directory existence and access permissions.
	///
	/// FileUtil is a wrapper to access(2) and stat(2).
	///
	class FileUtil {
	public:

		///
		/// \brief Constructor, copies \a path only
		///
		explicit FileUtil(const std::string& path);

		/**
			 \brief Copies the path

			 \since New in yat 0.6
		*/
		FileUtil(const FileUtil&);

		///
		/// \brief Check file/directory permissions
		///
		/// Check if access permissions match \a mode. \a mode must be
		/// given as r, w, x, d, or combinations of these letters. The check
		/// is performed at call time.
		///
		/// \note Checking permissions on a non-existent file will match
		/// the permissions against the parent directory, but only the
		/// parent directory. This means that when the parent directory
		/// does not exist then fail is returned.
		///
		/// \return On success (all requested permissions granted), zero
		/// is returned. On error (at least one bit in mode asked for a
		/// permission that is denied, or some other error occurred), -1
		/// is returned, and errno is set appropriately.
		///
		/// \exception IO_error if exists() throws.
		/// \exception std::invalid_argument if \a bits contain other
		/// character than r, w, x, or d.
		///
		int permissions(const std::string& bits) const;

		///
		/// \brief Check whether \a file exists.
		///
		/// The check for the file is done when calling this function.
		///
		/// \return True if \a file exists, false otherwise.
		///
		/// \exception IO_error if underlying stat(2) call fails in other
		/// ways than that file does not exist.
		///
		/// \see stat(2)
		///
		bool exists(void) const;

		/**
			 Same as exists(void) but uses \a lstat rather than \a
			 stat. That implies that the function will return true also if
			 \a path is a broken link.

			 \return true if file exists

			 \see lstat(2)
			 
			 \since New in yat 0.5
		 */
		bool lexists(void) const;

		///
		/// \brief Get path associated with the object.
		///
		const std::string& path(void) const;

		/**
			 \brief assignment operator

			 \since New in yat 0.6
		 */
		FileUtil& operator=(const FileUtil&);

	private:
		bool exists_common(bool) const;

		std::string path_;
	};

}}} // of namespace utility, yat, and theplu

#endif
