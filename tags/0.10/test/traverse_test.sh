#!/bin/sh

# $Id: traverse_test.sh 1495 2012-08-27 04:06:43Z peter $

# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# Test that svncopyright (and indirectly svndigest) does not traverse
# into another WC. See ticket #512

required="repo"

rm -rf testSubdir/traverse_test.sh.dir

. ./init.sh || exit 99

daughter=.svndigest/branches_wc

# check out
rm -rf $rootdir/$daughter
$SVN checkout file://$repo/branches $rootdir/$daughter || exit_fail

#$SVN revert -R $rootdir/$daughter
#$SVN update $rootdir/$daughter
# modify file
echo >> $rootdir/$daughter/NEWS

$SVN revert -R $rootdir
$SVN update $rootdir

$SVN log $rootdir/$daughter
SVNCOPYRIGHT_run 0 -v -r $rootdir
$GREP "^Parsing.*$rootdir/bin/" stdout || exit_fail
$GREP "^Parsing.*$rootdir/$daughter" stdout && exit_fail

exit_success;
