// $Id: config.cc 1373 2011-06-10 23:31:35Z peter $

/*
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Configuration.h"

#include <sstream>

namespace theplu{
namespace svndigest{
	void test_codon(test::Suite&);
	void test_read_write(test::Suite&);
	void test_props(test::Suite&);
}} // end of namespace svndigest and theplu


int main( int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv, false);

	test_codon(suite);
	test_read_write(suite);
	test_props(suite);
                                                                                
	return suite.exit_status();
}


namespace theplu{
namespace svndigest{

	void test_codon(test::Suite& suite)
	{
		suite.out() << "test codons" << std::endl;
		const Configuration& c(Configuration::instance());
		if (!c.codon("foo.h")){
			suite.out() << "No codon for foo.h\n";
			suite.add(false);
		}
		if (!c.codon("/dir/test.cc")){
			suite.out() << "No codon for test.cc\n";
			suite.add(false);
		}
		if (!c.codon("bootstrap")){
			suite.out() << "No codon for bootstrap\n";
			suite.add(false);
		}
	}
	
	void test_props(test::Suite& suite)
	{
		suite.out() << "test props" << std::endl;
		Configuration& conf(Configuration::instance());
		std::stringstream ss;
		ss << "[svn-props]\n"
			 << "foo* = svndigest:ignore\n"
			 << "bar* = svn:eol-style=native;svncopyright:ignore\n";
		conf.load(ss);
		const std::map<std::string, std::string>& props =
			conf.svn_properties("foo_bla_bla");
		if (props.find("svndigest:ignore")==props.end()) {
				suite.out() << "property svndigest:ignore not set for foo_bla_bla\n";
				suite.add(false);
		}
		const std::map<std::string, std::string>& props2 =
			conf.svn_properties("bar_bla_bla");
		std::map<std::string, std::string>::const_iterator it =
			props2.find("svn:eol-style");
		if (!suite.add(it != props2.end()))
			suite.out() <<"expected property 'svn:eol-style' set for bar_bla_bla\n";
		else if (!suite.add(it->second == "native"))
			suite.out() << "expected 'svn:eol-style' set to 'native' "
									<< "for bar_bla_bla\n";
		// checking that we return empty map for files not mentioned in config
		suite.out() << "props3" << std::endl;
		const std::map<std::string, std::string>& props3 =
			conf.svn_properties("nothing-nothing");
		suite.add(props3.empty());
	}

	void test_read_write(test::Suite& suite)
	{
		Configuration& config = Configuration::instance();
		std::stringstream ss;
		ss << config;
		config.load(ss);
		std::stringstream ss2;
		ss2 << config;
		if (ss2.str() != ss.str()) {
			suite.out() << "error: configuration output not equal\n";
			suite.out() << "defalt output:\n" << ss.str() << "\n";
			suite.out() << "second output:\n" << ss2.str() << "\n";
			suite.add(false);
		}
	}

}} // end of namespace svndigest and theplu
