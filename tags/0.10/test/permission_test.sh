#!/bin/sh
# $Id: permission_test.sh 1392 2011-07-12 02:56:29Z peter $
#
# Copyright (C) 2011 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. ./init.sh || exit 1
set -e

$SVN revert toy_project -R

echo create config
cat > config <<EOF

EOF

ls -l toy_project/bootstrap
ls -l toy_project/bootstrap | cut -f 1 -d ' ' > pre_permission.txt
SVNCOPYRIGHT_run 0 -r toy_project --config-file=config -v

ls -l toy_project/bootstrap
ls -l toy_project/bootstrap | cut -f 1 -d ' ' > post_permission.txt
diff pre_permission.txt post_permission.txt || echo exit_fail
test -x toy_project/bootstrap || exit_fail

exit_success
