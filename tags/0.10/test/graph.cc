// $Id: graph.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Graph.h"
#include "lib/Vector.h"

#include <cstdlib>
#include <ostream>

using namespace theplu::svndigest;

void test_graph(const std::string format, test::Suite& suite);
void test_graph__(const std::string format, test::Suite& suite);

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv);

#ifndef HAVE_PLPLOT
	suite.out() << "no plplot: skip test\n";
	exit(EXIT_SKIP);
#endif

	test_graph("svg", suite);
	// Need to create an instance of Graph (plstream) since there is a
	// bug in plplot with dynamic loading of runtime libraries. The
	// issue is reported in the plplot issue tracking system
	// https://sf.net/tracker/?func=detail&aid=3009045&group_id=2915&atid=102915
	// and as http://dev.thep.lu.se/svndigest/ticket/472. Simply remove
	// this comment and the next line of code when plplot developers
	// fixes their bug.
  //
  // Omitting the dummy instance will cause segmentation fault.
	Graph dummy_never_used("/dev/null", "png");
	test_graph("png", suite);
	test_graph("pdf", suite);

	return suite.exit_status();
}


void test_graph(const std::string format, test::Suite& suite)
{
	suite.out() << "testing " << format << std::endl;
	for (size_t i=0; i<20; ++i)
		test_graph__(format, suite);
}


void test_graph__(const std::string format, test::Suite& suite)
{
	std::string filename("plot.");
	filename+=format;
	Graph graph(filename, format);
	graph.ymax(10);
	graph.current_color(255, 0, 0);
	SumVector data;
	graph.rev_max(100);
	data.set(99,1);
	graph.plot(data, "label", 1);
}
