#!@SHELL@
# @configure_input@

# $Id: init.sh.in 1515 2012-09-26 00:35:10Z peter $

# Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009, 2010, 2011 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

exit_fail () 
{ 
  set +x
  echo "$me: failed test: $@"; 
  exit 1
}

exit_skip () 
{ 
  set +x
  echo "$me: skipped test: $@" 
  exit 77
}

exit_success () 
{ 
  set +x
  echo "$me: successsful test: $@"; 
  exit 0
}

# primary variables - set by config.status
abs_top_builddir=@abs_top_builddir@
abs_top_srcdir=@abs_top_srcdir@
abs_builddir=@abs_builddir@
mkdir_p="@MKDIR_P@"
repo="@abs_test_repo@"
shell=@SHELL@
test_repo_found=@test_repo_found@
have_svn_wc="@have_svn_wc@"

test -z "$SVNDIGEST" && SVNDIGEST="${abs_top_builddir}/bin/svndigest";
test -z "$SVNDIGEST_COPY_CACHE" \
&& SVNDIGEST_COPY_CACHE="${abs_top_builddir}/bin/svndigest-copy-cache";
test -z "$SVNCOPYRIGHT" && SVNCOPYRIGHT="${abs_top_builddir}/bin/svncopyright";
test -z "$srcdir" && srcdir="@srcdir@";
test -z "$SVN" && SVN="svn --non-interactive";
test -z "$GREP" && GREP=@GREP@;

# some helpful derived variables
rootdir=toy_project
repo_url="file://$repo"
svn_update=${abs_builddir}/svn_update.sh

me=$0

test_dir=`echo $me.dir | sed -e 's,.*[\\/],,'`
test_dir="testSubDir/$test_dir"
$mkdir_p --verbose $test_dir
cd $test_dir

abs_wcdir=${abs_builddir}/${test_dir}/toy_project

for r in : $required; do
  case $r in
    repo*)
      if test x$test_repo_found = xyes; then
        $shell ${svn_update} || exit_fail;
      else
        exit_skip "no test repository available";
      fi;;
    wc)
      if test x$have_svn_wc = xno; then
        exit_skip "not subversion working copy";
      fi;;
    *)
    :
  esac
done


foo_run ()
{
		expected_exitcode=$1
		shift
		exitcode=0
		$foo_prog ${1+"$@"} > stdout 2>stderr || exitcode=$?
		cat stderr >&2
		cat stdout
		test $exitcode = $expected_exitcode || exit_fail
}

SVNDIGEST_run ()
{
		foo_prog="$SVNDIGEST --verbose"
		foo_run $@
}

SVNDIGEST_COPY_CACHE_run ()
{
		foo_prog="$SVNDIGEST_COPY_CACHE --verbose"
		foo_run $@
}

SVNCOPYRIGHT_run ()
{
		foo_prog="$SVNCOPYRIGHT"
		foo_run $@
}


echo "=== running $me ==="

# turn on trace
set -x;
