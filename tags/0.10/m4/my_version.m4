## $Id: my_version.m4 1511 2012-09-16 23:23:58Z peter $
#
# Copyright (C) 2012 Peter Johansson
#
# This file is part of the yat library, http://dev.thep.lu.se/yat
#
# The yat library is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# The yat library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with yat. If not, see <http://www.gnu.org/licenses/>.
#

# serial 0

# How to use this file
# ====================
# In 'configure.ac', include this file and call 'MY_VERSION_early'
# before 'AC_INIT'.  After AC_INIT you can call MY_VERSION in order to
# AC_SUBST and AC_DEFINE variables MAJOR_VERSION, MINOR_VERSION etc.
#   m4_include([m4/version.m4])
#   MY_VERSION_early([3],[1],[4],[true])
#   AC_INIT([foo], [my_VERSION])
#   MY_VERSION


# MY_VERSION_early(major, minor, patch, dev)
# ==========
m4_define([MY_VERSION_early],
[
m4_define([my_MAJOR_VERSION], [$1])
m4_define([my_MINOR_VERSION], [$2])
m4_define([my_PATCH_VERSION], [$3])
m4_define([my_DEV_BUILD], [$4])
m4_define([my_VERSION],[$1.$2m4_if([$3],[0],,[.$3])m4_if([$4],[true],[pre])])
]) # my_version


# MY_VERSION
# ==========
# AC_SUBST and AC_DEFINE variables set in MY_VERSION_early
m4_define([MY_VERSION],
[
m4_ifndef([my_VERSION],
          [m4_fatal([MY_VERSION_early must be expanded before MY_VERSION])])
AC_SUBST(m4_toupper(AC_PACKAGE_NAME)[_MAJOR_VERSION], [my_MAJOR_VERSION])
AC_SUBST(m4_toupper(AC_PACKAGE_NAME)[_MINOR_VERSION], [my_MINOR_VERSION])
AC_SUBST(m4_toupper(AC_PACKAGE_NAME)[_PATCH_VERSION], [my_PATCH_VERSION])

AC_DEFINE([DEV_BUILD],[my_DEV_BUILD],
          [true if dev build (version ends with pre)])
AC_DEFINE(m4_toupper(AC_PACKAGE_NAME)[_MAJOR_VERSION], [my_MAJOR_VERSION],
									  [Define to major version])
AC_DEFINE(m4_toupper(AC_PACKAGE_NAME)[_MINOR_VERSION], [my_MINOR_VERSION],
									  [Define to minor version])
AC_DEFINE(m4_toupper(AC_PACKAGE_NAME)[_PATCH_VERSION], [my_PATCH_VERSION],
									  [Define to patch version])
]) # MY_VERSION
