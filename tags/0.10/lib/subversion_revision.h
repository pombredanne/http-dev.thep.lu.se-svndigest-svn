#ifndef _theplu_svndigest_subversion_info_
#define _theplu_svndigest_subversion_info_

// $Id: subversion_revision.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

namespace theplu{
namespace svndigest{

	// These functions are (mis)placed here to reflect compilation time
	// of subversion_info.cc. Previously, __TIME__ macro was placed in
	// `bin/Parameter.cc' and since the Parameter is almost independent
	// of other parts of the package, that compilation time does not
	// very accurately reflect the building time of the svndigest
	// binary. subversion_info.cc is at least modified when updating
	// against repository and therefore these functions will return a
	// time no earlier than last `svn update'
	std::string compilation_date(void);
	std::string compilation_time(void);

	std::string svn_revision(void);

}} // end of namespace svndigest and namespace theplu

#endif
