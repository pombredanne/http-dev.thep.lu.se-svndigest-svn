#ifndef _theplu_svndigest_graph_
#define _theplu_svndigest_graph_

// $Id: Graph.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "Vector.h"

#include <subversion-1/svn_types.h>

#include <ctime>
#include <string>
#include <vector>

#ifdef HAVE_PLPLOT
#include <plplot/plstream.h>
#else
typedef int PLINT;
typedef double PLFLT;
typedef double plstream;
#endif

namespace theplu {
namespace svndigest {

	/**
		 Graph is used to generate plots in svndigest and is a specialized
		 wrapper to the plplot plot library, http://plplot.sourceforge.net/

		 Graph allows some axis label manipulation, setting pen colour,
		 and plotting lines.

		 Graph supports three graphics formats - portable network graphics
		 (png), scalable vector graphics (svg), and protable document
		 format (pdf) - if the underlying plplot library was built with
		 the appropriate modules. If not, rebuild and reinstall plplot.
	*/
	class Graph
	{
	public:

		/**
			 \brief Constructor

			 \a filename to be defined depending on whether we'll support
			 more output formats than SVG.

			 \note The plot legend is created in the destructioe, i.e., when
			 the graph object is destroyed.
		*/
		Graph(const std::string& filename, const std::string& format);

		/**
			 \brief Destructor
		*/
		~Graph(void);

		/**
			 \brief Set the pen colour to use in next drawing call
		*/
		void current_color(unsigned char r, unsigned char g, unsigned char b);

		/**
		*/
		static const std::vector<time_t>& dates(void);

		static bool date_xticks(void);

		/**
			 \brief Plot \a data and use \a lines and \a label to compose
			 the legend label.

			 The legend will be a coloured line followed by \a lines
			 followed by \a label.

			 The label order in the legend is reverse to the plot order,
			 i.e., the last plotted line will get the top entry in the
			 legend, the second to last plotted line will be the second
			 legend entry, and so on.
		*/
		void plot(const SumVector& data, const std::string& label,
							unsigned int lines);

		/**
			 Sets the right end of xrange to correspond to revision \a rev.
		 */
		static void rev_max(svn_revnum_t rev);

		/**
			 Gets the right end of xrange to correspond to revision \a rev.
		 */
		static svn_revnum_t rev_max(void);

		/**
			 Sets the left end of xrange to correspond to revision \a rev.
		 */
		static void rev_min(svn_revnum_t rev);

		/**
			 Gets the left end of xrange to correspond to revision \a rev.
		 */
		static svn_revnum_t rev_min(void);

		/**
			 \brief Function setting the dates.

			 The date is given in seconds (since 1/1 1970)
		*/
		static void set_dates(const std::vector<time_t>& date);

		/**
			 \brief Set max y value in the plot

			 The plot area is (xstart,0) to (xend,\a ymax) where xend is
			 either the length of vector to plot (corresponds to latest
			 revision number) or the date of the last revision commit,
			 xstart is 0 or the date of the first commit.
		*/
		double ymax(double ymax);

	private:

		// Copy constructor not implemented
		Graph(const Graph&);

		struct legend_data {
			std::string label;
			unsigned int lines;
			PLINT r,g,b;
		};

		/**
			 \brief Set the pen colour to use in next drawing call
		*/
		void current_color(const legend_data&);
		void print_legend(void);
		void staircase(svn_revnum_t rev0, PLFLT y0, svn_revnum_t rev1, PLFLT y1);

		std::vector<legend_data> legend_;
		unsigned int plots_; // keep track of number of plots drawn
		plstream pls_;
		static svn_revnum_t rev_min_;
		static svn_revnum_t rev_max_;
		std::string title_;
		static std::vector<time_t> dates_;
		PLFLT xmin_, xmax_, xrange_, ymin_, ymax_, yrange_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
