// $Id: LineTypeParser.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineTypeParser.h"
#include "Configuration.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

namespace theplu{
namespace svndigest{


	LineTypeParser::LineTypeParser(std::string path)
		: mode_(0), post_copyright_(false), copyright_found_(false)
	{
		codon_ = Configuration::instance().codon(file_name(path));
	}


	LineTypeParser::line_type LineTypeParser::parse(std::string line)
	{
		if (!post_copyright_) {
			if (copyright_found_) {
				// check if line is end of copyright statement, i.e. contains
				// no alphanumerical character (except in copyright_prefix).
				post_copyright_ = true;
				for (size_t i=0; i<line.size()&&post_copyright_; ++i)
					if (isalnum(line[i]) &&
							!(i<copyright_prefix_.size() && copyright_prefix_[i]==line[i])){
						post_copyright_ = false;
						block_ += line + "\n";
					}
				if (post_copyright_)
					end_line_ = type_.size()+1;
			}
			else {
				// check whether copyright starts on this line
				std::string::iterator i =
					search(line.begin(), line.end(),
								 Configuration::instance().copyright_string());
				if (i!=line.end()) {
					start_line_ = type_.size()+1;
					copyright_prefix_ = line.substr(0, distance(line.begin(), i));
					copyright_found_ = true;
					block_ = line+"\n";
				}
			}
		}
		// we are in copyright block
		if (copyright_found_ && !post_copyright_)
			type_.push_back(LineTypeParser::copyright);

		else if (codon_)
			type_.push_back(code_mode(line));
		else
			type_.push_back(text_mode(line));
		return type_.back();

	}


	LineTypeParser::line_type LineTypeParser::code_mode(const std::string& str)
	{
		// mode zero means we are currently not in a comment
		// if mode!=0 comment is closed by codon[mode-1].second -> mode=0
		// if codon[x-1].start is found and x >= mode -> mode=x
		line_type lt=other;
		{
			for (std::string::const_iterator iter=str.begin();iter!=str.end();++iter){
				for (size_t i=mode_; i<codon_->size(); ++i) {
					if ( iter==str.begin() && (*codon_)[i].first[0] == '\n' &&
							 match_begin(iter, str.end(), (*codon_)[i].first.substr(1)) ) {
						iter += (*codon_)[i].first.size()-1;
						mode_ = i+1;
						break;
					}
					if (match_begin(iter, str.end(), (*codon_)[i].first)) {
						iter += (*codon_)[i].first.size();
						mode_ = i+1;
						break;
					}
				}
				if (iter==str.end())
					break;
				assert(mode_==0 || mode_-1<(*codon_).size());
				if (mode_ && match_begin(iter,str.end(), (*codon_)[mode_-1].second)){
					iter += (*codon_)[mode_-1].second.size();
					mode_=0;
					if (iter==str.end())
						break;
				}
				else if (!mode_ && isgraph(*iter))
					lt=code;
				else if (mode_ && lt!=code && isalnum(*iter))
					lt=comment;
			}
			if (mode_ && (*codon_)[mode_-1].second==std::string("\n"))
				mode_=0;
		}
		return lt;
	}


	LineTypeParser::line_type LineTypeParser::text_mode(const std::string& str)
	{
		for (std::string::const_iterator iter=str.begin(); iter!=str.end(); ++iter)
			if (isalnum(*iter))
				return comment;
		return other;
	}


}} // end of namespace svndigest and namespace theplu
