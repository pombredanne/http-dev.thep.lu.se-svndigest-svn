// $Id: CopyrightVisitor.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CopyrightVisitor.h"

#include "Configuration.h"
#include "CopyrightStats.h"
#include "Directory.h"
#include "File.h"
#include "NodeVisitor.h"
#include "utility.h"

#include <cassert>
#include <fstream>

namespace theplu {
namespace svndigest {

	CopyrightVisitor::CopyrightVisitor(std::map<std::string, Alias>& alias,
																		 bool verbose,
																		 const std::map<int,svn_revnum_t>& year2rev,
																		 bool ignore_cache)
		: NodeVisitor(), alias_(alias), verbose_(verbose), year2rev_(year2rev),
			ignore_cache_(ignore_cache)
	{}


	void CopyrightVisitor::add(RevisionSet& a, const RevisionSet& b) const
	{
		for (RevisionSet::const_iterator i=b.begin(); i!=b.end(); ++i)
			a.insert_merge(*i);
	}


	std::string
	CopyrightVisitor::copyright_block(const std::map<int, std::set<Alias> >& year_authors,
																		const std::string& prefix) const
	{
		using namespace std;
		stringstream ss;
		for (map<int, set<Alias> >::const_iterator i(year_authors.begin());
				 i!=year_authors.end();) {
			ss << prefix << Configuration::instance().copyright_string() << " "
				 << 1900+i->first;
			map<int, set<Alias> >::const_iterator j = i;
			assert(i!=year_authors.end());
			while (++j!=year_authors.end() &&
						 i->second == j->second){
				ss << ", " << 1900+(j->first);
			}
			// printing authors
			std::vector<Alias> vec_alias;
			back_insert_iterator<std::vector<Alias> > ii(vec_alias);
			std::copy(i->second.begin(), i->second.end(), ii);
			// sort with respect to id
			std::sort(vec_alias.begin(), vec_alias.end(), IdCompare());
			for (std::vector<Alias>::iterator a=vec_alias.begin();
					 a!=vec_alias.end(); ++a){
				if (a!=vec_alias.begin())
					ss << ",";
				ss << " " << a->name();
			}
			ss << "\n";
			i = j;
		}
		return ss.str();
	}


	bool CopyrightVisitor::detect_copyright(const std::string& path,
																					std::string& block,
																					size_t& start_at_line,
																					size_t& end_at_line,
																					std::string& prefix) const
	{
		using namespace std;
		LineTypeParser parser(path);
		std::ifstream is(path.c_str());
		std::string line;
		while (std::getline(is, line))
			parser.parse(line);
		if (!parser.copyright_found())
			return false;
		block = parser.block();
		start_at_line = parser.start_line();
		end_at_line = parser.end_line();
		prefix = parser.prefix();
		return true;
	}


	bool CopyrightVisitor::enter(Directory& dir)
	{
		if (dir.svncopyright_ignore())
			return false;

		RevisionSet ignore = dir.property().svncopyright_ignore_rev();

		typedef std::map<std::string, RevisionSet> Map;
		Map::const_iterator mother = path2ignore_.find(directory_name(dir.path()));
		if (mother!=path2ignore_.end())
			add(ignore, mother->second);

		if (!ignore.empty())
			path2ignore_[dir.path()] = ignore;

		return true;
	}


	void CopyrightVisitor::leave(Directory& dir)
	{
	}


	void CopyrightVisitor::update_copyright(const File& file)
	{
		std::string old_block;
		size_t start_line=0;
		size_t end_line=0;
		std::string prefix;
		if (!detect_copyright(file.path(),old_block, start_line, end_line, prefix)){
			if (Configuration::instance().missing_copyright_warning())
				std::cerr << "svncopyright: warning: no copyright statement found in '"
									<< file.path() << "'\n";
			return;
		}
		if (verbose_)
			std::cout << "Parsing '" << file.path() << "'\n";

		RevisionSet ignore_revs = file.property().svncopyright_ignore_rev();

		typedef std::map<std::string, RevisionSet> Map;
		Map::const_iterator mother = path2ignore_.find(directory_name(file.path()));
		if (mother!=path2ignore_.end())
			add(ignore_revs, mother->second);

		CopyrightStats stats(file.path(), ignore_cache_, year2rev_, ignore_revs);
		if (stats.map().empty())
			return;
		const std::map<int, std::set<std::string> >& year2users = stats.map();
		assert(!year2users.empty());
		std::map<int, std::set<Alias> > year2alias;
		translate(year2users, year2alias);
		std::string new_block = copyright_block(year2alias, prefix);
		if (old_block==new_block)
			return;
		if (verbose_)
			std::cout << "Updating copyright in '" << file.path() << "'" << std::endl;
		update_copyright(file.path(), new_block, start_line, end_line);
	}


	void CopyrightVisitor::update_copyright(const std::string& path,
																					const std::string& new_block,
																					size_t start_at_line,
																					size_t end_at_line) const
	{
		// embrace filename with brackets #filename#
		std::string tmpname = concatenate_path(directory_name(path),
																					 "#" + file_name(path) + "#");
		std::ofstream tmp(tmpname.c_str());
		assert(tmp);
		using namespace std;
		ifstream is(path.c_str());
		assert(is.good());
		string line;
		// Copy lines before block
		for (size_t i=1; i<start_at_line; ++i){
			assert(is.good());
			getline(is, line);
			tmp << line << "\n";
		}
		// Printing copyright statement
		tmp << new_block;
		// Ignore old block lines
		for (size_t i=start_at_line; i<end_at_line; ++i){
			assert(is.good());
			getline(is, line);
		}
		// Copy lines after block
		while(is.good()) {
			char ch=is.get();
			if (is.good())
				tmp.put(ch);
		}

		is.close();
		tmp.close();

		// finally rename file
		struct stat nodestat;
		stat(path.c_str(), &nodestat);
		rename(tmpname, path);
		chmod(path, nodestat.st_mode);
	}


	void CopyrightVisitor::visit(File& file)
	{
		if (file.svncopyright_ignore())
			return;
		update_copyright(file);
	}


	void CopyrightVisitor::translate(const std::set<std::string>& users,
																	 std::set<Alias>& aliases)
	{
		for (std::set<std::string>::const_iterator user=users.begin();
				 user!=users.end(); ++user) {
			std::map<std::string, Alias>::const_iterator i = alias_.find(*user);
			// if alias not found for author
			if (i==alias_.end()) {
				std::cerr << "svncopyright: warning: no copyright alias found for '"
									<< *user << "'\n";
				// insert alias to avoid multiple warnings.
				Alias a(*user, alias_.size());
				alias_[*user] = a;
			}
			else {
				// FIXME: perhaps use hint
				aliases.insert(i->second);
			}
		}
	}


	void
	CopyrightVisitor::translate(const std::map<int, std::set<std::string> >& y2u,
															std::map<int, std::set<Alias> >& y2a)
	{
		using std::map;
		using std::set;
		using std::string;
		for (map<int, set<string> >::const_iterator yu=y2u.begin();
				 yu!=y2u.end();++yu) {
			set<Alias>& alias = y2a[yu->first];
			translate(yu->second, alias);
		}
	}

}} // end of namespace svndigest and namespace theplu
