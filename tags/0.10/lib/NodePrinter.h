#ifndef _theplu_svndigest_node_printer_
#define _theplu_svndigest_node_printer_

// $Id: NodePrinter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson
	Copyright (C) 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineTypeParser.h"
#include "StatsCollection.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "TinyStats.h"

#include <map>
#include <ostream>
#include <stdexcept>
#include <string>

namespace theplu{
namespace svndigest{

	class Node;

  ///
  /// Base class for FilePrinter and DirectoryPrinter
  ///
  class NodePrinter
  {
  public:

    ///
    /// @brief Constructor
    ///
		NodePrinter(void);

		///
		/// @brief Destructor
		///
		virtual ~NodePrinter(void);

		/**
			 Note that returned string always end with '/' with the
			 exception when an empty string is returned.

			 @return output dir for example 'lib/' for this file
		 */
		std::string output_dir(void) const;

		/**
			 @return output path for example 'lib/NodePrinter.h.html' for this file
		 */
		virtual std::string output_path(void) const=0;

		///
		/// Function printing HTML in current working directory
		///
		void print(const bool verbose=false) const;

		void print_author_summary(std::ostream&,
															const Stats& stats,
															const std::string& line_type,
															const SVNlog&) const;

	protected:
		///
		/// print path in html format (with anchors) to @a os
		///
		void path_anchor(std::ostream& os) const;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		NodePrinter(const NodePrinter&);

		virtual const Node& node(void) const=0;

		virtual void print_core(bool verbose=false) const=0;

		///
		/// print page for specific user (or all) and specific line_style
		/// (or total).
		///
		virtual void print_core(const std::string& stats_type,
														const std::string& user,
														const std::string& line_type,
														const SVNlog&) const=0;

	};




}} // end of namespace svndigest and namespace theplu

#endif
