#ifndef _theplu_svndigest_node_counter_
#define _theplu_svndigest_node_counter_

// $Id: NodeCounter.h 1513 2012-09-23 04:09:08Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodeVisitor.h"

namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
		 Class to calculate number files and directories in a tree
	*/
	class NodeCounter : public NodeVisitor
  {
  public:
		/**
			 Default constructor
		 */
		NodeCounter(void);

		/**
			 \return number of directories
		*/
		unsigned int directories(void) const;

		/**
			 This function is called from Directory::traverse

			 Add 1 to number of directories

			 \return true if we should traverse daughter nodes
		 */
		bool enter(Directory& dir);

		/**
			 \return number of files
		*/
		unsigned int files(void) const;

		/**
			 Does nothing
		 */
		void leave(Directory& dir);

		/**
			 add 1 to number of files
		 */
		void visit(File& file);
	private:
		unsigned int directories_;
		unsigned int files_;
	};


}} // end of namespace svndigest and namespace theplu

#endif
