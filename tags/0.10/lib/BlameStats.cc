// $Id: BlameStats.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "BlameStats.h"

#include "SVNblame.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "Vector.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	BlameStats::BlameStats(const std::string& path)
		: Stats(path)
	{
	}


	BlameStats::BlameStats(const BlameStats& other)
	: Stats(other)
	{
	}


	void BlameStats::do_parse(const std::string& path, svn_revnum_t first_rev)
	{
		first_rev = std::max(first_rev, ignore_rev()+1);
		// FIXME: using log from File object
		SVNlog log(path);
		typedef std::set<svn_revnum_t> RevSet;
		RevSet revs;
		std::transform(log.commits().begin(), log.commits().end(),
									 std::inserter(revs, revs.begin()),
									 std::mem_fun_ref(&Commitment::revision));
		// we use a ParseVector here to be able to detect when people are
		// absent in a revision.
		std::vector<std::map<std::string, SparseVector> > data(4);
		for (RevSet::reverse_iterator rev_iter=revs.rbegin();
				 rev_iter!=revs.rend() && *rev_iter>=first_rev; ++rev_iter){
			SVNblame svn_blame(path, *rev_iter);
			LineTypeParser parser(path);
			while (svn_blame.valid()) {
				int lt = parser.parse(svn_blame.line());
				assert(static_cast<size_t>(lt)<data.size());
				SparseVector& vec = data[lt][svn_blame.author()];
				if (svn_blame.revision()>ignore_rev()) {
					vec.set(*rev_iter, vec[*rev_iter] + 1);
					add_author(svn_blame.author());
					// I dont trust blame and log behave consistently (stop-on-copy).
					revs.insert(svn_blame.revision());
				}
				svn_blame.next_line();
			}
		}

		// for count=zero we have not created an entry in SumVector and
		// value will be "value for previous rev". Therefore we need to
		// set the value to zero explicitely.
		if (stats_.size()<data.size())
			stats_.resize(data.size());
		for (size_t lt =0; lt<data.size(); ++lt) {
			// just to avoid long long lines
			typedef std::map<std::string,SparseVector>::const_iterator const_iterator;
			for (const_iterator av = data[lt].begin(); av!=data[lt].end(); ++av) {
				for (RevSet::const_reverse_iterator rev_iter=revs.rbegin();
						 rev_iter!=revs.rend() && *rev_iter >= first_rev; ++rev_iter) {
					stats_[lt][av->first].set(*rev_iter, data[lt][av->first][*rev_iter]);
				}
			}
		}
	}

}} // end of namespace svndigest and namespace theplu
