#ifndef _theplu_svndigest_option_version_
#define _theplu_svndigest_option_version_

// $Id: OptionVersion.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2008, 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "yat/OptionSwitch.h"

#include <string>

namespace theplu {
namespace yat {
namespace utility {
	class CommandLine;
}}
namespace svndigest {

	/**
		 \brief Class for version option

		 When this option is found in parsing of commandline, it displays
		 version output.
	 */
	class OptionVersion : public yat::utility::OptionSwitch
	{
	public:
		/**
			 \brief Constructor

			 \param cmd Commandline Option is associated with
			 \param name string such as "help" for --help, "h" for -h or
			 "h,help" (default) for having both short and long option name
			 \param desc string used in help display
		*/
		OptionVersion(yat::utility::CommandLine& cmd, std::string name="version",
									std::string desc="print version information and exit",
									OptionSwitch* const verbose=NULL);

		/**
			 add extra string appended to output
		 */
		void extra(const std::string&);

		/**
			 set program name
		 */
		void program_name(const std::string&);

		/**
			 \brief set version string
		*/
		void version(const std::string&);

	private:
		OptionSwitch* const verbose_;
		std::string extra_;
		std::string prog_name_;

		/**
		 */
		void do_parse2(std::vector<std::string>::iterator,
									 std::vector<std::string>::iterator);

	};

}} // of namespace svndigest and theplu

#endif
