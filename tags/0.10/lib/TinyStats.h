#ifndef _theplu_svndigest_tiny_stats_
#define _theplu_svndigest_tiny_stats_

// $Id: TinyStats.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineTypeParser.h"

#include <map>
#include <string>

namespace theplu{
namespace svndigest{

	class StatsCollection;

	/**
	 */
  class TinyStats
  {
  public:
		unsigned int operator()(const std::string& stats_type,
														const std::string& user,
														LineTypeParser::line_type lt) const;

		void init(const StatsCollection&);

	private:
		typedef std::vector<unsigned int> Vector;
		typedef std::map<std::string, Vector> Map;
		std::map<std::string, Map> data_;
  };
}} // end of namespace svndigest end of namespace theplu

#endif
