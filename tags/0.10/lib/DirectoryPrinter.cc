// $Id: DirectoryPrinter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "DirectoryPrinter.h"

#include "Alias.h"
#include "Configuration.h"
#include "Directory.h"
#include "File.h"
#include "html_utility.h"
#include "Node.h"
#include "NodeVisitor.h"
#include "StatsPlotter.h"
#include "SVN.h"
#include "SVNlog.h"
#include "TinyStats.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <sstream>

#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	DirectoryPrinter::DirectoryPrinter(const Directory& dir)
		: NodePrinter(), directory_(dir)
	{
	}


	DirectoryPrinter::~DirectoryPrinter(void)
	{
	}


	const Node& DirectoryPrinter::node(void) const
	{
		return directory_;
	}


	std::string DirectoryPrinter::output_path(void) const
	{
		return output_dir()+"index.html";
	}


	void DirectoryPrinter::print_core(const bool verbose) const
	{
	}


	void DirectoryPrinter::print_core(const std::string& stats_type,
														 const std::string& user,
														 const std::string& line_type,
														 const SVNlog& log) const
	{

		const Stats& stats = directory_.stats_[stats_type];
		std::string imagedir = stats_type+"/"+"images/"+line_type;
		std::string outdir   = stats_type+"/"+user+"/" +line_type;
		if (node().local_path()!="") {
			imagedir += "/"+node().local_path();
			outdir   += "/"+node().local_path();
		}
		mkdir_p(outdir);
		if (user=="all")
			mkdir_p(imagedir);
		std::string html_name = outdir+"/index.html";
		std::ofstream os(html_name.c_str());
		assert(os.good());
		if (node().local_path().empty())
			print_header(os, node().name(), directory_.level_+3, user, line_type, "index.html",
									 stats_type);
		else
			print_header(os, node().name(), directory_.level_+3, user, line_type,
									 node().local_path()+"/index.html", stats_type);
		path_anchor(os);

		std::stringstream ss;
		for (size_t i=0; i<directory_.level_; ++i)
			ss << "../";
		ss << "../../../";
		if (user=="all")
			ss << StatsPlotter(stats).plot(imagedir+"/index", line_type);
		else
			ss << imagedir << "/index";
		os << "<p class=\"plot\">\n";
		os << image(ss.str());
		os << "</p>\n";

		os << "<h3>File Summary";
		if (user!="all")
			os << " for " << user;
		os << "</h3>";
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Node</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Author</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (directory_.level_){
			os << "<tr class=\"light\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("../index.html", "../");
			os << "</td>\n</tr>\n";
			color = "dark";
		}

		// print html links to daughter nodes
		for (NodeConstIterator d = directory_.daughters_.begin();
				 d!=directory_.daughters_.end(); ++d) {

			(*d)->html_tablerow(os, stats_type, color, user);
			if (color=="dark")
				color = "light";
			else
				color = "dark";
		}
		os << "<tr class=\"" << color << "\">\n";
		os << "<td>Total</td>\n";
		if (user=="all"){
			os << "<td>" << stats.lines() << "</td>\n";
			os << "<td>" << stats.code() << "</td>\n";
			os << "<td>" << stats.comments() << "</td>\n";
			os << "<td>" << stats.empty() << "</td>\n";
		}
		else {
			os << "<td>" << stats.lines(user);
			if (stats.lines(user))
				os << " (" << percent(stats.lines(user),stats.lines()) << "%)";
			os << "</td>\n";
			os << "<td>" << stats.code(user);
			if (stats.code(user))
				os << " (" << percent(stats.code(user),stats.code()) << "%)";
			os << "</td>\n";
			os << "<td>" << stats.comments(user);
			if (stats.comments(user))
				os << " (" << percent(stats.comments(user),stats.comments()) << "%)";
			os << "</td>\n";
			os << "<td>" << stats.empty(user);
			if (stats.empty(user))
				os << " (" << percent(stats.empty(user),stats.empty()) << "%)";
			os << "</td>\n";
		}
		os << "<td>" << trac_revision(node().last_changed_rev()) << "</td>\n";
		os << "<td>" << node().author() << "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
		print_author_summary(os, stats, line_type, log);
		os << "\n";
		print_footer(os);
		os.close();
	}


}} // end of namespace svndigest and namespace theplu
