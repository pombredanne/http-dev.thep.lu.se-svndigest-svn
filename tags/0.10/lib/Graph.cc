// $Id: Graph.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2009, 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graph.h"

#include "Date.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <sstream>

namespace theplu {
namespace svndigest {

	svn_revnum_t Graph::rev_max_=0;
	svn_revnum_t Graph::rev_min_=0;
	std::vector<time_t> Graph::dates_;

	Graph::Graph(const std::string& filename, const std::string& format)
#ifdef HAVE_PLPLOT
		: plots_(0), pls_(1,1,format.c_str(),filename.c_str()),
			title_(filename), xmin_(0.0), xmax_(0.0), ymin_(0.0), ymax_(0.0)
	{
		// should match the maximum number of authors plotted, change this
		// when the maximum number of authors becomes configurable
		legend_.reserve(10);
		// we use color map 0 position 0 for background color
		pls_.scolbga(255,255,255,0);
		pls_.setopt("geometry", "600x500");
		pls_.init();
		pls_.adv(0);
		pls_.vsta();
		pls_.syax(6,0);
	}
#else
{}
#endif


	Graph::~Graph(void)
	{
		print_legend();
	}


	bool Graph::date_xticks(void)
	{
		return dates_.size() != 0;
	}


	void Graph::current_color(const legend_data& legend)
	{
		// we use color map 0 position 1 for current color
#ifdef HAVE_PLPLOT
		pls_.scol0a(1,legend.r,legend.g,legend.b,1.0);
#endif
	}


	void Graph::current_color(unsigned char r, unsigned char g, unsigned char b)
	{
		// we use color map 0 position 1 for current color
#ifdef HAVE_PLPLOT
		pls_.scol0a(1,r,g,b,1.0);
#endif
	}


	void Graph::plot(const SumVector& y, const std::string& label,
									 unsigned int lines)
	{
#ifdef HAVE_PLPLOT
		if (!plots_) {
			assert(!date_xticks() || static_cast<size_t>(rev_min_)<dates_.size());
			assert(!date_xticks() || dates_[rev_min_]);
			assert(static_cast<size_t>(rev_min_)<dates_.size() || !date_xticks());
			xmin_= date_xticks() ? dates_[rev_min_] : rev_min_;
			assert(static_cast<size_t>(rev_max_)<dates_.size() || !date_xticks());
			xmax_= date_xticks() ? dates_[rev_max_] : rev_max_;
			xrange_=xmax_-xmin_;
			yrange_=ymax_-ymin_;
			pls_.wind(xmin_, xmax_, ymin_, ymax_);

			// draw plot frame, x and y ticks only for the first plot
			pls_.scol0a(2,0,0,0,1.0);
			pls_.col0(2);

			std::string xopt("bcnstv");
			if (date_xticks()) {
				pls_.timefmt("%Y-%b");
				xopt="bcnstd";
			}

			pls_.box(xopt.c_str(), 0, 1, "bcnstv", 0, 2);
			pls_.lab("Date", "Number of lines", title_.c_str());
		}
		++plots_;

		pls_.col0(1);

		SumVector::const_iterator iter = y.begin();
		svn_revnum_t x0=rev_min_;
		PLFLT y0=0;
		for (; iter!=y.end(); ++iter) {
			staircase(x0, y0, iter->first, iter->second);
			x0 = iter->first;
			y0 = iter->second;
		}
		staircase(x0, y0, rev_max_, y0);

		legend_data legend;
		legend.label=label;
		legend.lines=lines;
		pls_.gcol0(1,legend.r,legend.g,legend.b);
		legend_.push_back(legend);
#endif
	}


	void Graph::print_legend(void)
	{
#ifdef HAVE_PLPLOT
		PLFLT line_length=0.05*xrange_;
		PLFLT x=xmin_+1.7*line_length;
		unsigned char characteristic=log10(ymax_);
		PLFLT legend_lines_length=0.016*xrange_*(characteristic+1);
		PLFLT dx=0.005*xrange_;
		PLFLT dy=0.003*yrange_;
		unsigned int row=0;
		std::vector<legend_data>::const_reverse_iterator end = legend_.rend();
		for (std::vector<legend_data>::const_reverse_iterator i=legend_.rbegin();
				 i!=end; ++i, ++row) {
			PLFLT y=(0.95-0.04*row)*yrange_;
			current_color(*i);
			pls_.col0(1);
			pls_.join(x-line_length, y-dy, x, y-dy);
			std::stringstream ss;
			ss << i->lines;
			pls_.col0(2);
			pls_.ptex(x+legend_lines_length+dx*2, y, 0, 0, 0, i->label.c_str());
			pls_.ptex(x+legend_lines_length+dx  , y, 0, 0, 1, ss.str().c_str());
		}
#endif
	}


	void Graph::rev_max(svn_revnum_t rev)
	{
		rev_max_ = rev;
	}


	svn_revnum_t Graph::rev_max(void)
	{
		return rev_max_;
	}


	void Graph::rev_min(svn_revnum_t rev)
	{
		rev_min_ = rev;
	}


	svn_revnum_t Graph::rev_min(void)
	{
		return rev_min_;
	}


	void Graph::set_dates(const std::vector<time_t>& date)
	{
		dates_=date;
	}


	void Graph::staircase(svn_revnum_t rev0, PLFLT y0,
												svn_revnum_t rev1, PLFLT y1)
	{
		PLFLT x0 = rev0;
		PLFLT x1 = rev1;
		if (date_xticks()) {
			assert(static_cast<size_t>(rev0)<dates_.size());
			assert(dates_[rev0]);
			x0 = dates_[rev0];
			assert(static_cast<size_t>(rev1)<dates_.size());
			assert(dates_[rev1]);
			x1 = dates_[rev1];
		}
#ifdef HAVE_PLPLOT
		// join {x0,y0} with {x1,y1} via {x1,y0}
		pls_.join(x0,y0,x1,y0);
		pls_.join(x1,y0,x1,y1);
#endif
	}


	const std::vector<time_t>& Graph::dates(void)
	{
		return dates_;
	}


	double Graph::ymax(double ymax)
	{
		return ymax_=ymax;
	}

}} // end of namespace svndigest and namespace theplu
