// $Id: SVNdiff.cc 1168 2010-08-15 19:30:22Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNdiff.h"

#include "SVN.h"

namespace theplu {
namespace svndigest {

	SVNdiff::SVNdiff(const std::string& path1, svn_revnum_t revision1,
									 const std::string& path2, svn_revnum_t revision2)
		: instance_(SVN::instance())
	{
	}

}} // end of namespace svndigest and namespace theplu
