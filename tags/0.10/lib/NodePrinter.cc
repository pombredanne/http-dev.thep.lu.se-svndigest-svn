// $Id: NodePrinter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodePrinter.h"

#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "LineTypeParser.h"
#include "Node.h"
#include "SVNlog.h"
#include "utility.h"

#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	NodePrinter::NodePrinter(void)
	{
	}

	NodePrinter::~NodePrinter(void)
	{
	}


	std::string NodePrinter::output_dir(void) const
	{
		return node().output_dir_;
	}


	void NodePrinter::path_anchor(std::ostream& os) const
	{
		os << "<h2 class=\"path\">\n";
		std::vector<std::string> words;
		words.reserve(node().level_+1);
		std::string word;
		words.push_back(Node::project_);
		std::stringstream ss(node().local_path());
		while(getline(ss,word,'/'))
			if (!word.empty()) // ignore double slash in path
				words.push_back(word);
		if (words.size()==1)
			os << anchor("index.html", Node::project_,0, "View " + Node::project_);
		else {
			for (size_t i=0; i<words.size()-1; ++i){
				os << anchor("index.html", words[i], node().level_-i, "View " + words[i]);
				os << "<span class=\"sep\">/</span>";
			}
			os << anchor(node().href(), words.back(), node().level_+2-words.size(),
						 "View " + words.back());
		}
		os << "\n</h2>\n";
	}


	void NodePrinter::print(const bool verbose) const
	{
		if (node().svndigest_ignore())
			return;
		if (!Configuration::instance().output_file() && !node().dir())
			return;
		if (verbose)
			std::cout << "Printing output for '" << node().path_ << "'" << std::endl;
		const SVNlog& log = node().log();
		typedef std::map<std::string, Stats*>::const_iterator iter;

		const iter end(node().stats_.stats().end());
		for (iter i=node().stats_.stats().begin();i!=end; ++i){
			print_core(i->first, "all", "total", log);
			print_core(i->first, "all", "code", log);
			print_core(i->first, "all", "comments", log);
			print_core(i->first, "all", "empty", log);
			for (std::set<std::string>::const_iterator j=i->second->authors().begin();
				 j!=i->second->authors().end(); ++j) {
				print_core(i->first, *j, "total", log);
				print_core(i->first, *j, "code", log);
				print_core(i->first, *j, "comments", log);
				print_core(i->first, *j, "empty", log);
			}
		}
		print_core(verbose);
	}


	void NodePrinter::print_author_summary(std::ostream& os,
																	const Stats& stats,
																	const std::string& line_type,
																	const SVNlog& log) const
	{
		HtmlStream hs(os);
		os << "<h3>Author Summary</h3>";
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Author</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Date</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (!node().dir()) {
			os << "<tr class=\"" << color << "\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("index.html", "../");
			os << "</td>\n</tr>\n";
		}

		// print authors
		const std::string timefmt("%Y-%m-%d  %H:%M");
		for (std::set<std::string>::const_iterator i=stats.authors().begin();
				 i!=stats.authors().end(); ++i){
			if (color=="dark")
				color="light";
			else
				color="dark";
			os << "<tr class=\"" << color << "\"><td>";
			os << anchor(*i+"/"+line_type+"/"+output_path()
									 ,*i, node().level_+2, "View statistics for "+*i);
			os << "</td><td>" << stats.lines(*i)
				 << "</td><td>" << stats.code(*i)
				 << "</td><td>" << stats.comments(*i)
				 << "</td><td>" << stats.empty(*i);
			if (log.exist(*i)) {
				const Commitment& lc(log.latest_commit(*i));
				os << "</td>" << "<td>" << trac_revision(lc.revision())
					 << "</td>" << "<td>";
				hs << Date(lc.date())(timefmt);
			}
			else {
				os << "</td>" << "<td>N/A"
					 << "</td>" << "<td>N/A";
			}
			os << "</td></tr>\n";
		}

		os << "<tr class=\"" << color << "\">\n";
		os << "<td>";
		if (node().dir())
			if (node().local_path().empty())
				os << anchor("all/"+line_type+"/index.html"
										 ,"Total", node().level_+2, "View statistics for all");
			else
				os << anchor("all/"+line_type+"/"+node().local_path()+"/index.html"
										 ,"Total", node().level_+2, "View statistics for all");
		else
			os << anchor("all/"+line_type+"/"+node().local_path()+".html"
									 ,"Total", node().level_+2, "View statistics for all");
		os << "</td>\n";
		os << "<td>" << stats.lines() << "</td>\n";
		os << "<td>" << stats.code() << "</td>\n";
		os << "<td>" << stats.comments() << "</td>\n";
		os << "<td>" << stats.empty() << "</td>\n";
		const Commitment& lc(log.latest_commit());
		os << "<td>" << trac_revision(lc.revision()) << "</td>\n";
		os << "<td>";
		hs << Date(lc.date())(timefmt);
		os << "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
	}


}} // end of namespace svndigest and namespace theplu
