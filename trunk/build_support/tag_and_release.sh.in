#!/bin/sh
#$Id: tag_and_release.sh.in 1495 2012-08-27 04:06:43Z peter $
#@configure_input@

# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# default config values
# you can override these values in a file config.txt
sf_user=peter31042

# read config.txt if it exists
test -f config.txt && . config.txt

distdir=@PACKAGE@-@VERSION@
dist_archive="${distdir}.tar.gz"


# Create text for release announcement text
write_announce_mail ()
{

cat <<EOF
I'm happy to announce the release of @PACKAGE_STRING@.

<INSERT TEXT HERE>

You can find the new release here:

http://sourceforge.net/projects/svndigest/files/${dist_archive}/download

Here is the checksums:

EOF
printf "MD5:    "
cat ${dist_archive}.MD5    | sed 's/ .*//'
cat <<EOF

Please report bugs by mail to @PACKAGE_BUGREPORT@

This release was bootstrapped with the following tools:
EOF
@AUTOCONF@ --version | head -n 1 | sed -e 's/.*(//' -e 's/)//'
@AUTOMAKE@ --version | head -n 1 | sed -e 's/.*(//' -e 's/)//'
cat <<EOF

You can find the list of significant changes between @VERSION@ and
earlier versions at

  http://dev.thep.lu.se/svndigest/browser/tags/@VERSION@/NEWS

EOF
}

set -e

make release
make svn-tag
svn update
make release
echo "write announcement.txt"
write_announce_mail > announcement.txt

echo "upload to sourceforge"
scp ${dist_archive} ${dist_archive}.MD5 $sf_user,svndigest@frs.sourceforge.net:/home/frs/project/s/sv/svndigest/.
