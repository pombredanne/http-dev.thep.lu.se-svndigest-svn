## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 1555 2012-11-05 09:37:08Z peter $

# Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

check_SCRIPTS = test/svn_update.sh test/init.sh

## we use suffix .cc for all test source
AM_DEFAULT_SOURCE_EXT = .cc

EXTRA_PROGRAMS = test/cache.test test/cache_partial.test test/color.test \
	test/config.test test/copyright.test test/date.test \
	test/graph.test test/htmlstream.test test/ignore_rev.test \
	test/parser.test test/option.test test/stats.test \
	test/svn_cat.test test/svn_cat2.test \
	test/svn_log.test test/trac.test test/utility.test \
	test/vector.test

CLEANFILES += $(EXTRA_PROGRAMS)

test_option_test_SOURCES = test/option.cc bin/Parameter.cc \
	bin/AbstractParameter.cc bin/svndigestParameter.cc

distributed_TESTS =
distributed_TESTS += test/cmd_format_test.sh
distributed_TESTS += test/config2_test.sh
distributed_TESTS += test/config3_test.sh
distributed_TESTS += test/config_props_test.sh
distributed_TESTS += test/copyright2_test.sh
distributed_TESTS += test/copyright3_test.sh
distributed_TESTS += test/copyright_cache_test.sh
distributed_TESTS += test/error_test.sh
distributed_TESTS += test/ignore_revs_test.sh
distributed_TESTS += test/link_root_test.sh
distributed_TESTS += test/permission_test.sh
distributed_TESTS += test/remove_cache_test.sh
distributed_TESTS += test/repo_status_test.sh
distributed_TESTS += test/repo_test.sh
distributed_TESTS += test/svncopyright_test.sh
distributed_TESTS += test/svndigest_copy_cache_test.sh
distributed_TESTS += test/svndigest_copy_cache_test2.sh
distributed_TESTS += test/traverse_test.sh
distributed_TESTS += test/try_help_test.sh
distributed_TESTS += test/update_test.sh
distributed_TESTS += test/update_test2.sh
distributed_TESTS += test/update_test3.sh
distributed_TESTS += test/update_test4.sh

TESTS = $(EXTRA_PROGRAMS) $(distributed_TESTS)

TEST_EXTENSIONS = .sh .test

EXTRA_DIST += $(distributed_TESTS)

# tests not yet passing are listed here
XFAIL_TESTS = test/copyright3_test.sh test/svn_cat2.test

noinst_HEADERS += test/Suite.h

check_LIBRARIES = test/libsvndigesttest.a

LDADD = test/libsvndigesttest.a \
	lib/libsvndigest_core.a \
	yat/libyat.a \
	$(SVN_LIBS) $(APR_LIBS) $(PLPLOT_LIBS)

## graph test needs to link against Graph class
test_graph_test_LDADD = $(LDADD) lib/libsvndigest.a

test_libsvndigesttest_a_SOURCES = test/Suite.cc

.PHONY: lazycheck

lazycheck:; $(MAKE) $(AM_MAKEFLAGS) check RECHECK_LOGS=

if HAVE_SVN_WC
repo_stamp = $(srcdir)/test/repo/db/current test/svn_update.sh
else
repo_stamp =
endif

# dependencies for lazycheck
test/cmd_format_test.log: test/init.sh bin/svndigest$(EXEEXT) $(repo_stamp)
test/config2_test.log: test/init.sh bin/svndigest$(EXEEXT) $(repo_stamp)
test/config3_test.log: test/init.sh bin/svndigest$(EXEEXT)
test/config_props_test.log: test/init.sh bin/svndigest$(EXEEXT) $(repo_stamp)
test/copyright2_test.log: test/init.sh bin/svncopyright$(EXEEXT) $(repo_stamp)
test/copyright3_test.log: test/init.sh bin/svncopyright$(EXEEXT) $(repo_stamp)
test/copyright_cache_test.log: test/init.sh bin/svncopyright$(EXEEXT) \
	$(repo_stamp)
test/link_root_test.log: test/init.sh bin/svndigest$(EXEEXT) $(repo_stamp)
test/permission_test.log: test/init.sh bin/svncopyright$(EXEEXT) $(repo_stamp)
test/repo_status_test.log: test/init.sh
test/repo_test.log: test/init.sh bin/svndigest$(EXEEXT) $(repo_stamp)
test/svncopyright_test.log: test/init.sh bin/svncopyright$(EXEEXT)
test/svndigest_copy_cache_test.log: test/init.sh bin/svndigest-copy-cache \
	$(repo_stamp)
test/svndigest_copy_cache_test.log: test/init.sh bin/svndigest-copy-cache \
	$(repo_stamp)
test/try_help_test.log: test/init.sh bin/svndigest
test/try_help_test.log: test/init.sh bin/svndigest $(repo_stamp)
test/update_test.log: test/init.sh bin/svndigest $(repo_stamp)
test/update_test2.log: test/init.sh bin/svndigest $(repo_stamp)
test/update_test3.log: test/init.sh bin/svndigest $(repo_stamp)
test/update_test4.log: test/init.sh bin/svndigest $(repo_stamp)

test/cache_partial.log: $(repo_stamp)
test/copyright.log: $(repo_stamp)
test/stats.log: $(repo_stamp)
test/svn_cat.log: $(repo_stamp)