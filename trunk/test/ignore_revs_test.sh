#!/bin/sh
# $Id: ignore_revs_test.sh 1551 2012-11-03 05:03:36Z peter $
#
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. test/init.sh || exit 99
set -e

run_svncopyright_with_ignore ()
{
ignore=$1
$SVN revert -R toy_project
cp toy_project/.svndigest/config config
cat >> config << _EOF
[svn-props]
Node.h = svncopyright:ignore=$ignore
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 0 --verbose --root toy_project --config-file=config
}

# log for AUTHORS
# r4  jari  2006
# r21 jari  2006
# r22 jari  2006
# r23 jari  2006
# r41 peter 2007
# r42 peter 2007 only updated copyright
# r44 peter 2007 only copyright
# r47 peter 2007 only copyright
# r67 peter 2009 only copyright

# no ignore
SVNCOPYRIGHT_run 0 -v --ignore-cache --root toy_project
$GREP "2006 Jari" toy_project/lib/Node.h || exit_fail
$GREP "2007 Peter" toy_project/lib/Node.h || exit_fail

run_svncopyright_with_ignore 21
$GREP "2006 Jari" toy_project/lib/Node.h || exit_fail
$GREP "2007 Peter" toy_project/lib/Node.h || exit_fail

run_svncopyright_with_ignore "-30"
$GREP "2006 Jari" toy_project/lib/Node.h && exit_fail
$GREP "2007 Peter" toy_project/lib/Node.h || exit_fail

run_svncopyright_with_ignore "1"
$GREP "2006 Jari" toy_project/lib/Node.h || exit_fail

run_svncopyright_with_ignore "1-22"
$GREP "2006 Jari" toy_project/lib/Node.h || exit_fail

run_svncopyright_with_ignore "1-23"
$GREP "2006 Jari" toy_project/lib/Node.h && exit_fail

run_svncopyright_with_ignore "41-"
$GREP "2006 Jari" toy_project/lib/Node.h || exit_fail
$GREP "2007 Peter" toy_project/lib/Node.h && exit_fail

# test that nonsense argument fails with grace
$SVN revert toy_project -R
cat > config << _EOF
[svn-props]
Node.h = svncopyright:ignore=nonsense
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 1 -v --root toy_project --config-file=config
$GREP svncopyright: stderr || exit_fail
$GREP yat stderr && exit_fail

# test ignore property on both Node and subNode
$SVN revert toy_project -R
cat > config << _EOF
[copyright-alias]
jari = Jari
peter = Peter
[svn-props]
Node.h = svncopyright:ignore=41
toy_project = svncopyright:ignore=42
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 0 -v --root toy_project --config-file=config
$GREP "Copyright (C) 2006" toy_project/lib/Node.h || exit_fail
$GREP "Copyright (C) 2007" toy_project/lib/Node.h && exit_fail

exit_success
