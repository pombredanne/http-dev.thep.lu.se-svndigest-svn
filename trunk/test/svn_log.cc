// $Id: svn_log.cc 1555 2012-11-05 09:37:08Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/SVN.h"
#include "lib/SVNlog.h"

#include <cassert>
#include <cstdlib>
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>

using namespace theplu::svndigest;

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);
	SVN* svn=SVN::instance("toy_project");
	if (!svn) {
		suite.out() << "error: cannot create SVN instance\n";
		return EXIT_FAILURE;
	}

	SVNlog log("toy_project/late_copy/Node.h");

	const SVNlog::container& commits = log.commits();
	suite.out() << commits.size() << " commits\n";
	typedef SVNlog::container::const_iterator log_iterator;
	if (commits.size()!=10) {
		suite.add(false);
		suite.out() << "expecte size 10\n";
	}

	log_iterator commit = commits.begin();
	if (commit->revision()!=4 || commit->author()!="jari") {
		suite.out() << "expected first commit r4 jari\n";
		suite.add(false);
	}

	for (; commit!=commits.end(); ++commit)
		suite.out() << "r" << commit->revision() << " "
								<< commit->author() << " "
								<< commit->message() << " "
								<< commit->date() << " "
								<< "\n";
	return suite.exit_status();
}

