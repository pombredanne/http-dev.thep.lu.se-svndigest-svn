#ifndef _theplu_svndigest_test_suite_
#define _theplu_svndigest_test_suite_

// $Id: Suite.h 1525 2012-10-01 06:31:07Z peter $

/*
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// used to tell automake that test should be skipped
#define EXIT_SKIP 77

// include from topdir to allow VPATH build
#include "test/environment.h"

#include <string>

namespace theplu {
namespace svndigest {
	class Stats;
	class StatsCollection;

namespace test {

	class Suite
	{
	public:
		Suite(int argc, char* argv[], bool need_test_repo=false);
		~Suite(void);

		/**
			 If b is false, set ok to false

			 \return b
		*/
		bool add(bool b);

		/**
			 \return EXIT_FAILURE or EXIT_SUCCESS depending on ok()
		 */
		int exit_status(void) const;

		/**
			 \return true if all tests are OK
		 */
		bool ok(void) const;

		std::ostream& out(void) const;

		/**
			 Wrapper around std system that calls \a cmd to shell. If return
			 value is different from \a xret exception is thrown
		*/
		void system(const std::string& cmd, int xret=0) const;

		/**
			 \return true if we are running in verbose mode
		 */
		bool verbose(void) const;

	private:
		bool ok_;

		void checkout_test_wc(void) const;
		void update_test_wc(void) const;
	};

	bool check_all(const Stats&, test::Suite&);
	bool check_total(const Stats&, test::Suite&);
	bool check_comment_or_copy(const Stats&, test::Suite&);

	bool consistent(const StatsCollection&, test::Suite&);
	bool consistent(const Stats&, test::Suite&);

	// return true if a and b are different
	// sends differences to cout
	bool diff(const std::string& a, const std::string& b);

	bool equal(const StatsCollection& a, const StatsCollection& b, 
						 test::Suite& suite);

	bool equal(const Stats& a, const Stats& b, test::Suite& suite);

	/**
		 \return absolute path to file
		 \param local_path path relative to builddir
	 */
	std::string filename(const std::string& local_path);

	/**
		 \return absolute path to file
		 \param path path relative to srcdir
	 */
	std::string src_filename(const std::string& path);


}}}

#endif
