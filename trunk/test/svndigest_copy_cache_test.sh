#!/bin/sh
# $Id: svndigest_copy_cache_test.sh 1532 2012-10-05 01:14:42Z peter $
#
# Copyright (C) 2009, 2010, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"
. test/init.sh || exit 99
set -e

for opt in --version --help; do
  SVNDIGEST_COPY_CACHE_run 0 $opt
  test -n "`cat stdout`" || exit_fail
  test -z "`cat stderr`" || exit_fail
done

pristine=fresh_wc
rm -rf $pristine
$SVN co ${repo_url}/trunk $pristine

SVNDIGEST_run 0 -r $rootdir --no-report
SVNDIGEST_COPY_CACHE_run 0 -r $rootdir -t $pristine

find $pristine | $GREP svndigest-cache || exit_fail
rm -rf $pristine
exit_success
