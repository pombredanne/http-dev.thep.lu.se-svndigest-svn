#!/bin/sh
# $Id: remove_cache_test.sh 1532 2012-10-05 01:14:42Z peter $
#
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. test/init.sh || exit 99
set -e

# clean up
rm -rf toy_project/bin/.svndigest/*
rm -rf toy_project/empty/.svndigest/*
rm -rf toy_project/dir_to_be_ignored/.svndigest/*

# Create fake old cache file
$mkdir_p toy_project/bin/.svndigest
echo dummie > toy_project/bin/.svndigest/foo.svndigest-cache
$mkdir_p toy_project/empty/.svndigest
echo dummie > toy_project/empty/.svndigest/foo.svndigest-cache
$mkdir_p toy_project/dir_to_be_ignored/.svndigest
echo dummie > toy_project/dir_to_be_ignored/.svndigest/foo.svndigest-cache
echo dummie > toy_project/dir_to_be_ignored/.svndigest/SVNproperty.cc.svndigest-cache



SVNDIGEST_run 0 --root toy_project --no-report
test -r toy_project/bin/.svndigest/Parameter.cc.svndigest-cache || exit_fail
test -e toy_project/bin/.svndigest/foo.svndigest-cache && exit_fail
test -e toy_project/empty/.svndigest/foo.svndigest-cache && exit_fail
test -e toy_project/dir_to_be_ignored/.svndigest/foo.svndigest-cache \
  && exit_fail
test -e toy_project/dir_to_be_ignored/.svndigest/SVNproperty.cc.svndigest-cache\
  || exit_fail
test -e toy_project/empty/.svndigest && exit_fail
test -e toy_project/dir_to_be_ignored/.svndigest || exit_fail

# === testing svncopyright ===

# Create fake old cache file
$mkdir_p toy_project/bin/.svndigest
echo dummie > toy_project/bin/.svndigest/foo.svncopyright-cache
$mkdir_p toy_project/empty/.svndigest
echo dummie > toy_project/empty/.svndigest/foo.svncopyright-cache
$mkdir_p toy_project/dir_to_be_ignored/.svndigest
echo dummie > toy_project/dir_to_be_ignored/.svndigest/foo.svncopyright-cache

SVNCOPYRIGHT_run 0 --root toy_project
test -r toy_project/bin/.svndigest/svnstat.cc.svncopyright-cache || exit_fail
test -e toy_project/bin/.svndigest/foo.svncopyright-cache && exit_fail
test -e toy_project/empty/.svndigest/foo.svncopyright-cache && exit_fail
test -e toy_project/dir_to_be_ignored/.svndigest/foo.svncopyright-cache \
  && exit_fail
test -e toy_project/empty/.svndigest && exit_fail
test -e toy_project/dir_to_be_ignored/.svndigest || exit_fail

exit_success
