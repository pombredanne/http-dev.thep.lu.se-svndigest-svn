#!/bin/sh
# $Id: config_props_test.sh 1532 2012-10-05 01:14:42Z peter $
#
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# testing that setting svn prop via config file works as expected (see
# ticket #501)

required="repo"

. test/init.sh || exit 99
set -e

# create config file
cat > config <<EOF
[svn-props]
lib = svndigest:ignore
EOF

SVNDIGEST_run 0 -r $rootdir --ignore-cache --no-report --config-file=config

grep "$abs_wcdir/lib" stdout && exit_fail

exit_success
