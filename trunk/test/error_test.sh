#!/bin/sh

# $Id: error_test.sh 1532 2012-10-05 01:14:42Z peter $

# Copyright (C) 2010, 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# testing the output and return value of some expected failures

required=

. test/init.sh || exit 99

# exit if cmd fails
set -e

SVNDIGEST_run 1 --root ${abs_top_srcdir}/README
$GREP 'svndigest:' stderr || exit_fail
$GREP "'${abs_top_srcdir}/README': " stderr || exit_fail

SVNCOPYRIGHT_run 1 --root ${abs_top_srcdir}/README
$GREP 'svncopyright:' stderr || exit_fail
$GREP "'${abs_top_srcdir}/README': " stderr || exit_fail
$GREP "Unknown error" stderr && exit_fail

SVNDIGEST_run 1 --root .
$GREP "svndigest: '.*' is not a working copy" stderr || exit_fail
$GREP "svndigest: '.*' is not a working copy.*is not a working copy" stderr\
 && exit_fail
SVNCOPYRIGHT_run 1 --root .
$GREP "svncopyright: '.*' is not a working copy" stderr || exit_fail
$GREP "svncopyright: '.*' is not a working copy.*is not a working copy" stderr\
 && exit_fail

SVNDIGEST_run 1 --rapakalja
$GREP "svndigest: unrecognized option.*rapakalja" stderr || exit_fail

SVNCOPYRIGHT_run 1 --rapakalja
$GREP "svncopyright: unrecognized option.*rapakalja" stderr || exit_fail

exit_success;