#!/bin/sh
# $Id: svncopyright_test.sh 1532 2012-10-05 01:14:42Z peter $
#
# Copyright (C) 2009, 2010, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required=""

. test/init.sh || exit 99
set -e

for opt in --version --help; do
  $SVNCOPYRIGHT $opt > out.tmp 2> error.tmp
  test -n "`cat out.tmp`" || exit_fail
  test -z "`cat error.tmp`" || exit_fail
  rm -f *.tmp
done

$SVNCOPYRIGHT --version | head -n 1 | $GREP svncopyright || exit_fail

exit_success
