#!/bin/sh

# $Id: copyright3_test.sh 1555 2012-11-05 09:37:08Z peter $

# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# test that svncopyright follows copy

required="repo"
. test/init.sh || exit 99

# exit if cmd fails
set -e

$SVN revert $rootdir -R

SVNCOPYRIGHT_run 0 --root $rootdir/late_copy --ignore-cache \
--config-file $rootdir/.svndigest/config --verbose

$GREP 'Copyright (C)' $rootdir/late_copy/Node.h || exit_fail
$GREP 'Copyright (C) 2006 Jari' $rootdir/late_copy/Node.h || exit_fail
$GREP 'Copyright (C) 2007 Peter' $rootdir/late_copy/Node.h || exit_fail
$GREP 'Copyright (C) 2008' $rootdir/late_copy/Node.h && exit_fail

rm -rf $rootdir
exit_success;
