// $Id: svn_cat2.cc 1555 2012-11-05 09:37:08Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// test that SVNcat follows copy

#include "Suite.h"

#include "lib/SVN.h"
#include "lib/SVNcat.h"

#include <cassert>
#include <ostream>
#include <string>

using namespace theplu::svndigest;

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);
	SVN* svn=SVN::instance("toy_project");
	if (!svn) {
		suite.out() << "error: cannot create SVN instance\n";
		return EXIT_FAILURE;
	}

	SVNcat readme("toy_project/late_copy/Node.h", 5);
	suite.out() << readme.str().size() << " characters\n";
	if (readme.str().empty()) {
		suite.out() << "error expected non empty string\n";
		suite.add(false);
	}
	return suite.exit_status();
}

