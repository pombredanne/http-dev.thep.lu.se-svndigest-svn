#!/bin/sh

# $Id: update_test4.sh 1539 2012-10-10 10:17:54Z peter $

# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# test that when a changeset only removes a directory that this will
# be reflected in mother's output

required="repo"
. test/init.sh || exit 99

targetdir=out
rm -rf out/*
$mkdir_p out

# remove directories that are prone to conflicts
rm -rf $rootdir/design
rm -rf $rootdir/late_copy

# create a mixed revision wc
$SVN update -r 52 $rootdir || exit_fail

SVNDIGEST_run 0 -r $rootdir -t $targetdir --ignore-cache --format=none
test -e stderr || exit_fail
test -s stderr && exit_fail

$SVN update -r 53 $rootdir || exit_fail

SVNDIGEST_run 0 -r $rootdir -t $targetdir --ignore-cache --format=none -u
test -e stderr || exit_fail
test -s stderr && exit_fail

grep '^Printing' stdout || exit_fail

exit_success;
