#!/bin/sh

# $Id: copyright_cache_test.sh 1551 2012-11-03 05:03:36Z peter $

# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. test/init.sh || exit 99

# exit if cmd fails
set -e

file=Credits.txt
cache_dir=$rootdir/alhambra/.svndigest
cache_file=$cache_dir/${file}.svncopyright-cache
rm -f $cache_file

# mini config file
cat > config <<EOF
[copyright-alias]
jari = Jari Häkkinen
peter = Peter Johansson
EOF

$SVN revert -R $rootdir
SVNCOPYRIGHT_run 0 -r $rootdir/alhambra --ignore-cache -v --config-file=config
test -s stderr && exit_fail
test -r $cache_file || exit_fail
cat $cache_file
# check that we cache user name not alias
grep peter $cache_file || exit_fail
cp $cache_file cache.txt

cat > correct.txt <<EOF
Copyright (C) 2006 Jari Häkkinen
Copyright (C) 2009 Peter Johansson
EOF
grep 'Copyright (C)' $rootdir/alhambra/$file > copyright.txt || exit_fail
diff -u correct.txt copyright.txt || exit_fail

# modify the cache file to allow us to detect that it is used
sed 's/peter/winston/' $cache_file > ${cache_file}2
mv ${cache_file}2 ${cache_file}
cat $cache_file
# create a config reflecting this name change
sed 's/peter/winston/' config > config2

# copyright update using cache created above
$SVN revert -R $rootdir
SVNCOPYRIGHT_run 0 -r $rootdir/alhambra -v --config-file=config2
test -s stderr && exit_fail
grep 'Copyright (C)' $rootdir/alhambra/$file > copyright2.txt || exit_fail
diff -u correct.txt copyright.txt || exit_fail

exit_success;
