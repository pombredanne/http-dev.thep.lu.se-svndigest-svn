#!/bin/sh
# $Id: svndigest_copy_cache_test2.sh 1527 2012-10-02 10:41:54Z peter $
#
# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# test that svndigest_copy_cache copies cache for svncopyright

required="repo"
. test/init.sh || exit 99
set -e

SVNCOPYRIGHT_run 0 -r $rootdir
svn revert $rootdir -R

find $rootdir | $GREP svncopyright-cache || exit_fail

pristine=fresh_wc
rm -rf $pristine
$SVN co ${repo_url}/trunk $pristine

SVNDIGEST_COPY_CACHE_run 0 -r $rootdir -t $pristine

find $pristine | $GREP svncopyright-cache || exit_fail
rm -rf $pristine

exit_success
