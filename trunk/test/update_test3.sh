#!/bin/sh

# $Id: update_test3.sh 1539 2012-10-10 10:17:54Z peter $

# Copyright (C) 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"
. test/init.sh || exit 99

targetdir=out
rm -rf out/*
$mkdir_p out

# create a mixed revision wc
$SVN update -r 66 $rootdir || exit_fail
$SVN update -r 30 $rootdir/lib || exit_fail

svnversion $rootdir
SVNDIGEST_run 0 -r $rootdir -t $targetdir --ignore-cache --format=none
test -e stderr || exit_fail
test -s stderr && exit_fail

head $targetdir/toy_project/classic/all/total/bootstrap.html \
| grep 'revision:' || exit_fail

head $targetdir/toy_project/classic/all/total/index.html \
| grep 'revision:' || exit_fail

head $targetdir/toy_project/classic/all/total/bin/index.html \
| grep 'revision:' || exit_fail

# update lib to latest rev
$SVN update -r 71 $rootdir/lib/

svnversion $rootdir
# e expect files in lib to be reprinted as well lib and its mother
SVNDIGEST_run 0 -r $rootdir -t $targetdir --update --format=none
test -e stderr || exit_fail
test -s stderr && exit_fail

grep '^Printing output for ' stdout || exit_fail

grep '^Printing output for .*lib/Node.h' stdout || exit_fail
# lib/Node.cc hasn't been changed and shouldn't be printed again
grep '^Printing output for .*lib/Node.cc' stdout && exit_fail

# directory bin hasn't changed and shouldn't...
grep '^Printing output for .*toy_project/bin' stdout && exit_fail

grep '^Printing output.*toy_project/lib' stdout | grep -v 'lib/' || exit_fail

head $targetdir/toy_project/classic/all/total/index.html \
| grep 'revision:' | grep 66 || exit_fail

head $targetdir/toy_project/classic/all/total/lib/index.html \
| grep 'revision:' | grep 71 || exit_fail

# update to revision 66 (but nothing changes)
$SVN update -r 71 $rootdir

svnversion $rootdir
SVNDIGEST_run 0 -r $rootdir -t $targetdir --update --format=none
test -e stderr || exit_fail
test -s stderr && exit_fail

# lib did not change in latest 'svn update'
grep '^Printing output for .*toy_project/lib' stdout && exit_fail

grep '^Printing output for .*toy_project' stdout || exit_fail

exit_success;
