// $Id: svn_cat.cc 1548 2012-10-20 08:50:26Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/SVN.h"
#include "lib/SVNcat.h"

#include <cassert>
#include <cstdlib>
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>

using namespace theplu::svndigest;

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);
	SVN* svn=SVN::instance("toy_project");
	if (!svn) {
		suite.out() << "error: cannot create SVN instance\n";
		return EXIT_FAILURE;
	}

	suite.system("svn cat -r 5 toy_project/README > README-r5");
	std::ifstream is("README-r5");
	assert(is);
	std::string line;
	// avoid first line
	getline(is, line, '\n');
	// read remaining file
	getline(is, line, '\0');
	std::string str = "$I" "d$\n" + line;
	is.close();

	SVNcat readme("toy_project/README", 5);
	suite.out() << "README rev 5:\n" << readme.str() << "\n";

	suite.out() << "comparing 'svn cat' with SVNcat string:\n";
	if (test::diff(str, readme.str()))
		suite.add(false);

	return suite.exit_status();
}

