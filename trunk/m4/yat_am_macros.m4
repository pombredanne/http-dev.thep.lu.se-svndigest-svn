## $Id: yat_am_macros.m4 2807 2012-08-05 23:39:51Z peter $

# serial 11 (yat 0.9)

m4_define([yat_am_macros_copyright], [
#
#   Copyright (C) 2011, 2012 Peter Johansson
#
#   This file is part of the yat library, http://dev.thep.lu.se/yat
#
#   The yat library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 3 of the
#   License, or (at your option) any later version.
#
#   The yat library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with yat. If not, see <http://www.gnu.org/licenses/>.
#
])

# YAT_HAVE_SVN_WC
#
# Test if we are building from a subversion repository and sets shell
# variable `have_svn_wc' to `yes' or `no'. Macro calls
# AC_SUBST([have_svn_wc]) and defines an Automake Conditional
# HAVE_SVN_WC.
#
AC_DEFUN([YAT_HAVE_SVN_WC],
[
  AC_MSG_CHECKING([if building from subversion wc])
  AS_IF([test -d $srcdir/.svn], [have_svn_wc=yes], [have_svn_wc=no])
  AM_CONDITIONAL([HAVE_SVN_WC], [test "x$have_svn_wc" = "xyes"])
  AC_SUBST([have_svn_wc])
  AC_MSG_RESULT([$have_svn_wc])
]) # YAT_HAVE_SVN_WC


# YAT_PROG_MOVE_IF_CHANGE
#
# Create shell script move-if-change at autoconf time and AC_SUBST
# MOVE_IF_CHANGE
AC_DEFUN([YAT_PROG_MOVE_IF_CHANGE],
[
m4_define([yat_MOVE_IF_CHANGE], m4_default([$1], [move-if-change]))
AC_SUBST([MOVE_IF_CHANGE], ['${SHELL} $(top_srcdir)/yat_MOVE_IF_CHANGE'])
AX_AC_PRINT_TO_FILE(yat_MOVE_IF_CHANGE, [#!/bin/sh])
_YAT_AC_APPEND_TO_FILE(yat_MOVE_IF_CHANGE,[
# Like mv ]$[1 ]$[2, but if the files are the same, just delete ]$[1.
# Status is zero if successful, nonzero otherwise.

usage="[$]0: usage: [$]0 SOURCE DEST"

test [$]# = 2 || { echo $usage >&2 && exit 1; }

if test -r "[$]2" && cmp -s "[$]1" "[$]2"; then
  rm -f "[$]1"
else
  mv -f "[$]1" "[$]2"
fi
])
]) # YAT_PROG_MOVE_IF_CHANGE


# YAT_SVN_REVISION([AM_FILE = svn_revison.am], [REVISION_FILE = .revision])
#
# Create a file, AM_FILE, at Autoconf time that should be included in
# your top Makefile.am. The file creates make rules for creation of a
# file REVISION_FILE that will contain the current revision of the
# subversion wc. We use `svnversion' to generate the revision number,
# so `svnversion' (included in Apache Subversion) must be available
# when building in a subversion wc. When building in the tarball there
# is no need to re-generate `.revision' as the file is included in the
# distribution and the revision number is hopefully constant in a
# tarball release.
#
# You need to declare Automake variable in your Makefile.am
#   'EXTRA_DIST = '
#
AC_DEFUN([YAT_SVN_REVISION],
[
  AC_REQUIRE([YAT_HAVE_SVN_WC])
  AC_REQUIRE([YAT_PROG_MOVE_IF_CHANGE])
  AC_REQUIRE([_YAT_PROG_SVNVERSION])
  m4_pushdef([YAT_AM_FILE], [m4_default([$1], [svn_revision.am])])
  m4_pushdef([yat_svn_revision_FILE], m4_default([$2], [.revision]))
  AS_VAR_IF([have_svn_wc], [yes], [
             AS_VAR_IF([SVNVERSION], [false], [
                        AC_MSG_ERROR([could not find svnversion, which is
                         required when building from a subversion working copy])
                       ])
            ])
dnl create dummy file in 'svn export'
m4_syscmd([test -d .svn || test -r ]yat_svn_revision_FILE[ || \
           echo "exported" > ]yat_svn_revision_FILE[])
dnl write top of am file
_YAT_AM_TOP(YAT_AM_FILE)

dnl write rules to create .revision
_YAT_AC_APPEND_TO_FILE(YAT_AM_FILE,
EXTRA_DIST += $(srcdir)/yat_svn_revision_FILE
if HAVE_SVN_WC
YAT_SVN_REVISION_FORCE:
$(srcdir)/yat_svn_revision_FILE: YAT_SVN_REVISION_FORCE
	$(AM_V_at)$(SVNVERSION) $(srcdir) > [$][@]-t \
	&& $(MOVE_IF_CHANGE) [$][@]-t [$][@]
endif
)
dnl end AX_AC_APPEND_TO_FILE
m4_popdef([YAT_AM_FILE])
]) # YAT_SVN_REVISION


# YAT_REVISION_HEADER([AM_FILE = svn_revision_header.am],
#                     [HEADER = svn_revision.h],
#                     [DEFINE = SVN_REVISION],
#                     [REVISION = .revision])
#
# Create make rules for creation of a C header file, which defines the
# current revision as DEFINE. The make rule is generated in file,
# AM_FILE, that should be included in your Makefile.am. The C header
# file, HEADER, in two steps. First, at Autoconf time HEADER.in is
# created. Then at Make time HEADER is created from files HEADER.in
# and REVISION. The latter file is supposed to contain the current
# revision and a convenient way to generate this file is to the use
# macro YAT_SVN_REVISION.  You need to declare the following variables
# in your Makefile.am:
#   BUILT_SOURCES =
#   DISTCLEANFILES =
#   EXTRA_DIST =
#
AC_DEFUN([YAT_REVISION_HEADER],
[
AC_REQUIRE([YAT_PROG_MOVE_IF_CHANGE])
m4_pushdef([yat_AM_FILE], [m4_default([$1], [svn_revision_header.am])])
m4_define([yat_HEADER_FILE], [m4_default([$2], [svn_revision.h])])
m4_pushdef([yat_REVISION_DEFINE], [m4_default([$3], [SVN_REVISION])])
m4_pushdef([yat_INPUT_FILE], [m4_default([$4], [.revision])])

dnl create svn_revision.h.in
AX_AC_PRINT_TO_FILE([]yat_HEADER_FILE[.in-t],[
/* created by $0 */
#ifndef ]yat_REVISION_DEFINE[
#define ]yat_REVISION_DEFINE[ \"@]yat_REVISION_DEFINE[@\"
#endif
])
m4_syscmd([sh ]yat_MOVE_IF_CHANGE[ ]yat_HEADER_FILE[.in-t ]yat_HEADER_FILE[.in])
dnl write make rule for svn_revision.h
_YAT_AM_TOP(yat_AM_FILE)
_YAT_AC_APPEND_TO_FILE(yat_AM_FILE,
EXTRA_DIST += $(srcdir)/]yat_HEADER_FILE[.in
DISTCLEANFILES += $(builddir)/]yat_HEADER_FILE[
BUILT_SOURCES += $(builddir)/]yat_HEADER_FILE[
$(builddir)/]yat_HEADER_FILE[: $(srcdir)/yat_HEADER_FILE.in $(srcdir)/]yat_INPUT_FILE[
	$(AM_V_at)revision=$$(cat $(srcdir)/]yat_INPUT_FILE[) \
	&& sed "s|@]yat_REVISION_DEFINE[@|$$revision|g" < $(srcdir)/]yat_HEADER_FILE.in[ \
	> [$][@]-t && mv [$][@]-t [$][@]
)
m4_popdef([yat_AM_FILE])
m4_popdef([yat_REVISION_DEFINE])
m4_popdef([yat_INPUT_FILE])
]) # YAT_REVISION_HEADER


# YAT_SVN_RELEASE_YEAR([AM_FILE = svn_release_year.am],[OUTPUT = .release_year])
#
# Create file, AM_FILE, with make rules for creation of a file,
# OUTPUT, that holds the year the package was last modified. The rule
# is only active when building in subversion wc and the wc is modified
# compared to the repository. The value of OUTPUT is AC_SUBSTed and
# AC_DEFINEed and OUTPUT added to CONFIG_STATUS_DEPENDENCIES which
# means ./configure will be re-run when OUTPUT is updated. Typically
# it is a good idea to check in OUTPUT in repository because then
# OUTPUT will get expected value also in 'svn export' case. You need
# to declare the following variables in Makefile.am:
#   CONFIG_STATUS_DEPENDENCIES =
#   EXTRA_DIST =
#
AC_DEFUN([YAT_SVN_RELEASE_YEAR],
[
AC_REQUIRE([YAT_HAVE_SVN_WC])
AC_REQUIRE([_YAT_PROG_SVNVERSION])
AC_REQUIRE([YAT_PROG_MOVE_IF_CHANGE])
m4_pushdef([YAT_am_file], [m4_default([$1], [svn_release_year.am])])
m4_pushdef([YAT_release_year], [m4_default([$2], [.release_year])])
# .release_year is generated during make so in order to avoid bootstrap problem
test -r $srcdir/YAT_release_year || date -u "+%Y" > $srcdir/YAT_release_year
# propagate RELEASE_YEAR from file .release_year
RELEASE_YEAR=`cat "$srcdir/YAT_release_year"`
AC_SUBST([RELEASE_YEAR])
AC_DEFINE_UNQUOTED([RELEASE_YEAR], ["$RELEASE_YEAR"],
                   [Define year package was last modified])
dnl generate make rule for .release_year
_YAT_AM_TOP(YAT_am_file)
_YAT_AC_APPEND_TO_FILE(YAT_am_file,
EXTRA_DIST += $(srcdir)/YAT_release_year
CONFIG_STATUS_DEPENDENCIES += $(srcdir)/YAT_release_year
RELEASE_YEAR_FORCE:
if HAVE_SVN_WC
$(srcdir)/YAT_release_year: RELEASE_YEAR_FORCE
	$(AM_V_at)if $(SVNVERSION) $(srcdir) | grep 'M' > /dev/null; then \
	   date -u "+%%Y" > [$][@]-t \
	   && $(MOVE_IF_CHANGE) [$][@]-t [$][@]; \
	fi
endif
)
m4_popdef([YAT_am_file])
m4_popdef([YAT_release_year])
]) # YAT_SVN_RELEASE_YEAR


### Private Macros ###

# _YAT_AM_TOP(FILE)
#
# Write the header of a Automake snippet with the copyright noticed
# decalared in top of this file.
AC_DEFUN([_YAT_AM_TOP],
[
AC_PREREQ([2.62])
AX_AC_PRINT_TO_FILE([$1],dnl
[# ]$1[ generated automatically by GNU Autoconf
]yat_am_macros_copyright[
# stub rule to work around bug in Automake 1.11
\$(top_srcdir)/]$1[:

])
]) # _YAT_AM_TOP


# wrapper around AX_AC_APPEND_TO_FILE to avoid usage of variables
# defined in AX_FILE_ESCAPES
AC_DEFUN([_YAT_AC_APPEND_TO_FILE],
[
AX_AC_APPEND_TO_FILE([$1],
m4_bpatsubsts([$2], [\$], [${AX_DOLLAR}],
                    [\\], [${AX_BS}],
                    [\"], [${AX_DQ}]))
])


# _YAT_PROG_SVNVERSION
#
# Wrapper around AC_PATH_PROG to allow using it as argument to
# AC_REQUIRE
AC_DEFUN([_YAT_PROG_SVNVERSION],
[
AC_PATH_PROG([SVNVERSION], [svnversion], [false])
])
