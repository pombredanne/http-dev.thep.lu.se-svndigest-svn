## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 1532 2012-10-05 01:14:42Z peter $

# Copyright (C) 2008, 2009, 2010, 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/trac/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

dist_man1_MANS =
dist_man1_MANS += $(srcdir)/man/svncopyright.1
dist_man1_MANS += $(srcdir)/man/svndigest.1
dist_man1_MANS += $(srcdir)/man/svndigest-copy-cache.1

EXTRA_DIST += man/svndigest.x

MAINTAINERCLEANFILES += $(dist_man1_MANS)

# We would like to depend on binaries, but since svndigest.1
# is distributed it is recommended to depend on only distributed files
# (see automake manual 28.5).
# We depend on configure to catch VERSION changes, Makefile.in to
# catch rules changes, svndigest.x is an obvious dependency, and
# .revision changes for every revision.
man_deps=$(top_srcdir)/configure $(srcdir)/Makefile.in \
	$(srcdir)/man/svndigest.x $(top_srcdir)/.revision

svndigest_man_deps = $(man_deps)
svncopyright_man_deps = $(man_deps)
svndigest_copy_cache_man_deps = $(man_deps)

## we add dependencies to corresponding binaries in case when we
## already have man files (at configure time) and/or are building from
## a subversion WC. See comment above why we don't add this
## dependencies all the time
if MAN_TARBALL
else
svndigest_man_deps += bin/svndigest$(EXEEXT)
svncopyright_man_deps += bin/svncopyright$(EXEEXT)
svndigest_copy_cache_man_deps += bin/svndigest-copy-cache$(EXEEXT)
endif

$(srcdir)/man/svndigest.1: $(svndigest_man_deps)
	$(AM_V_at)$(HELP2MAN) --no-info -o $@ \
	--include $(srcdir)/man/svndigest.x \
	--version-option="--no-verbose --version" \
	$(top_builddir)/bin/svndigest$(EXEEXT)

$(srcdir)/man/svncopyright.1: $(svncopyright_man_deps)
	$(AM_V_at)$(HELP2MAN) --no-info -o $@ \
	--include $(srcdir)/man/svndigest.x \
	--version-option="--no-verbose --version" \
	$(top_builddir)/bin/svncopyright$(EXEEXT)

$(srcdir)/man/svndigest-copy-cache.1: $(svndigest_copy_cache_man_deps)
	$(AM_V_at)$(HELP2MAN) --no-info -o $@ \
	--include $(srcdir)/man/svndigest.x \
	--version-option="--no-verbose --version" \
	$(top_builddir)/bin/svndigest-copy-cache$(EXEEXT)

