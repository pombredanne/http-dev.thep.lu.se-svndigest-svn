// $Id: SvndigestVisitor.cc 1538 2012-10-07 09:29:04Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SvndigestVisitor.h"

#include "Directory.h"
#include "DirectoryPrinter.h"
#include "File.h"
#include "FilePrinter.h"
#include "NodeVisitor.h"

#include <algorithm>
#include <cassert>

namespace theplu {
namespace svndigest {

	SvndigestVisitor::SvndigestVisitor(bool verbose, bool ignore_cache,
																		 bool report, bool update)
		: verbose_(verbose), ignore_cache_(ignore_cache), report_(report),
			update_(update)
	{}


	bool SvndigestVisitor::enter(Directory& dir)
	{
		svn_revnum_t r = dir.property().svndigest_ignore_rev();
		if (ignore_rev_.size() && r<ignore_rev_.back())
			ignore_rev_.push_back(ignore_rev_.back());
		ignore_rev_.push_back(r);
		if (dir.svndigest_ignore())
			return false;
		return true;
	}


	void SvndigestVisitor::leave(Directory& dir)
	{
		assert(ignore_rev_.size());
		ignore_rev_.pop_back();
		if (report_) {
			dir.collect_stats();
			dir.init_tiny_stats();
			DirectoryPrinter dp(dir);
			dp.print(verbose_, update_);
		}
	}


	void SvndigestVisitor::visit(File& file)
	{
		if (!file.svndigest_ignore()) {
			svn_revnum_t rev = file.property().svndigest_ignore_rev();
			assert(ignore_rev_.size());
			rev = std::max(rev, ignore_rev_.back());
			file.parse(verbose_, ignore_cache_, rev);
		}
		if (report_) {
			file.init_tiny_stats();
			if (!file.svndigest_ignore()) {
				FilePrinter fp(file);
				fp.print(verbose_, update_);
			}
		}
	}

}} // end of namespace svndigest and namespace theplu
