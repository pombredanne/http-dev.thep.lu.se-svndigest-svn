#ifndef _theplu_svndigest_cache_copyer_
#define _theplu_svndigest_cache_copyer_

// $Id: CacheCopyer.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodeVisitor.h"

#include <string>

namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
		 Visitor for updating copyright in files.
	*/
  class CacheCopyer : public NodeVisitor
  {
  public:
		CacheCopyer(const std::string& target, bool verbose);

		/**
			 \return false if dir.ignore or dir.svncopyright_ignore
		 */
		bool enter(Directory& dir);

		/**
			 Doing nothing
		 */
		void leave(Directory& dir);

		/**
			 Updating copyright in \a file
		 */
		void visit(File& file);

	private:
		void copy(const std::string& src, const std::string& trg_dir) const;

		std::string target_;
		bool verbose_;
	};
}} // end of namespace svndigest and namespace theplu

#endif
