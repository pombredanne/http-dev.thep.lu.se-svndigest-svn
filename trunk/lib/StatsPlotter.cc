// $Id: StatsPlotter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "StatsPlotter.h"

#include "Colors.h"
#include "Configuration.h"
#include "Functor.h"
#include "Graph.h"
#include "Stats.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>

namespace theplu{
namespace svndigest{


	StatsPlotter::StatsPlotter(const Stats& stats)
		:	stats_(stats)
	{
	}


	StatsPlotter::~StatsPlotter(void)
	{
	}


	std::string StatsPlotter::plot(const std::string& filename,
																 const std::string& linetype) const
	{
		const std::string& format = Configuration::instance().image_format();
		if (format=="none")
			return filename;
		plot(filename, linetype, format);
		const std::string& anchor_format =
			Configuration::instance().image_anchor_format();

		if (format!=anchor_format)
			plot(filename, linetype, anchor_format);
		return filename;
	}


	void StatsPlotter::plot(const std::string& filename,
													const std::string& linetype,
													const std::string& format) const
	{
		Graph gp(filename+"."+format, format);
		const Author2Vector* stat=NULL;
		if (linetype=="total")
			stat = &stats_.total_stats();
		else if (linetype=="code")
			stat = &stats_.code_stats();
		else if (linetype=="comments")
			stat = &stats_.comment_or_copy_stats();
		else if (linetype=="empty")
			stat = &stats_.other_stats();
		assert(stat);
		assert(stat->size());
		assert(stat->find("all")!=stat->end());
		const SumVector& total=stats_.get_vector(*stat, "all");
		double yrange_max=1.03 * stats_.max_element(total) +1.0;
		gp.ymax(yrange_max);

		typedef std::vector<std::pair<std::string, SumVector> > vec_type;
		vec_type author_cont;
		author_cont.reserve(stat->size());
		for (std::set<std::string>::const_iterator i=stats_.authors_.begin();
				 i != stats_.authors_.end(); ++i) {
			assert(stat->find(*i)!=stat->end());
			const SumVector& vec = stats_.get_vector(*stat,*i);
			if (stats_.max_element(vec)) {
				author_cont.push_back(std::make_pair(*i,vec));
			}
		}

		LessReversed<SumVector> lr;
		PairSecondCompare<std::string, SumVector, LessReversed<SumVector> >
			compare(lr);
		std::sort(author_cont.begin(), author_cont.end(), compare);

		vec_type::iterator end(author_cont.end());
		vec_type::iterator i(author_cont.begin());
		const vec_type::size_type maxauthors=8;
		int authskip=author_cont.size()-maxauthors;
		if (authskip>1) {
			// only use others if there is more than 1 author to be skipped,
			// there is no reason to add only 1 author to others.
			vec_type::iterator j(i);
			i+=authskip;
			SumVector others;
			sum(j, i, others, PairValuePlusAssign<std::string, SumVector>());
			unsigned char r, g, b;
			std::string label("others");
			Colors::instance().get_color(label, r,g,b);
			gp.current_color(r,g,b);
			gp.plot(others, label, others.size() ? others.back() : 0);
		}
		for ( ; i!=end; ++i) {
			unsigned char r, g, b;
			Colors::instance().get_color(i->first,r,g,b);
			gp.current_color(r,g,b);
			gp.plot(i->second, i->first, stats_.get_back(*stat, i->first));
		}
		gp.current_color(255,0,0);
		gp.plot(total, "total", stats_.get_back(*stat, "all"));
	}


	void StatsPlotter::plot_summary(const std::string& filename) const
	{
		const std::string& format = Configuration::instance().image_format();
		if (format=="none")
			return;
		plot_summary(filename, format);
		const std::string& anchor_format =
			Configuration::instance().image_anchor_format();

		if (format!=anchor_format)
			plot_summary(filename, anchor_format);
	}


	void StatsPlotter::plot_summary(const std::string& filename,
																	const std::string& format) const
	{
		Graph gp(filename+"."+format, format);
		const SumVector& total = stats_.get_vector(stats_.total_stats(), "all");
		double yrange_max=1.03*stats_.max_element(total)+1;
		gp.ymax(yrange_max);

		const SumVector& x(stats_.get_vector(stats_.code_stats(), "all"));
		gp.current_color(255,255,0);
		gp.plot(x, "code", x.back());

		const SumVector& y = stats_.get_vector(stats_.comment_or_copy_stats(),
																					 "all");
		gp.current_color(0,0,255);
		gp.plot(y, "comment", y.back());

		const SumVector& z = stats_.get_vector(stats_.other_stats(), "all");
		gp.current_color(0,255,0);
		gp.plot(z, "other", z.back());

		gp.current_color(255,0,0);
		gp.plot(total, "total", total.back());
	}

}} // end of namespace svndigest and namespace theplu
