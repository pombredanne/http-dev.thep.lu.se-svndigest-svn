#ifndef _theplu_svndigest_node_
#define _theplu_svndigest_node_

// $Id: Node.h 1538 2012-10-07 09:29:04Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson
	Copyright (C) 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "LineTypeParser.h"
#include "StatsCollection.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "SVNproperty.h"
#include "TinyStats.h"

#include <map>
#include <ostream>
#include <stdexcept>
#include <string>

namespace theplu{
namespace svndigest{

	class Alias;
	class NodeVisitor;

	///
	/// If something goes wrong in the use of the Node or its derived
	/// classes, a NodeException is thrown.
	///
	struct NodeException : public std::runtime_error
	{ inline NodeException(const std::string& msg) : runtime_error(msg) {} };

  ///
  /// Abstract Base Class for files.
  ///
  class Node
  {
  public:

    ///
    /// @brief Constructor
    ///
		Node(const unsigned int, const std::string&, const std::string&,
				 const std::string& = "");

		///
		/// @brief Destructor
		///
		virtual ~Node(void);

		///
		/// @brief Get the author of the latest commit.
		///
		std::string author(void) const;


		/**
			 @brief Check whether node is binary.

			 @return True if node is binary.
		*/
		bool binary(void) const;

		///
		/// @return true if directory
		///
		virtual bool dir(void) const;

		///
		/// @return href to this node
		///
		virtual std::string href(void) const=0;

		/**
		 */
		void html_tablerow(std::ostream& os,
											 const std::string& stats_type,
											 const std::string& css_class,
											 const std::string& user) const;

		/**
			 Create the TinyStats based on the stored CollectionStats
		 */
		void init_tiny_stats(void);

		///
		/// @brief Get the revision number of the latest commit.
		///
		virtual svn_revnum_t last_changed_rev(void) const=0;

		/**
			 @return log of this node in a recursive manner.
		*/
		const SVNlog& log(void) const;

		///
		/// @return
		///
		inline const std::string& local_path(void) const { return local_path_; }

		///
		/// Function returning everything after the last '/'
		///
		/// @return name of node (not full path)
		///
		std::string name(void) const;

		/**
			 @return The explicit string identifying the underlying derived
			 class.
		*/
		virtual std::string node_type(void) const=0;

		///
		/// @todo doc
		///
		inline const std::string& path(void) const { return path_; }

		/**
			 \return svnprop for this Node
		 */
		const SVNproperty& property(void) const;

		/**
			 check revision of this node and sub-nodes and return the
			 smallest revision, i.e., all nodes are at least at revision
			 returned.
		 */
		virtual svn_revnum_t revision_min(void) const=0;

		/**
			 \return true if svncopyright::ignore property was set
		 */
		bool svncopyright_ignore(void) const;

		/**
			 @brief Check if Node should be ignored by svndigest.

			 Binary files and links will be ignored as well as nodes with
			 property svndigest:ignore set to "" as that implies that all
			 revs should be ignored.

			 @return True if item property svndigest:ignore was set with
			 empty string or node is binary or link.

			 \see SVNproperty::svndigest_ignore()
		*/
		bool svndigest_ignore(void) const;

		/**
		 */
		const StatsCollection& stats(void) const;

		/**
		 */
		StatsCollection& stats(void);

		/**
			 Access point for a NodeVisitor to start traversing Node and its
			 daughter nodes.
		 */
		virtual void traverse(NodeVisitor& visitor)=0;

		/**
		 */
		inline const SVNinfo& svn_info(void) const { return svninfo_; }

		/**
		 */
		const TinyStats& tiny_stats(void) const { return tiny_stats_; }

	protected:
		unsigned int level_;
		std::string output_dir_;
		std::string path_; // absolute path
		static std::string project_;
		StatsCollection stats_;
		TinyStats tiny_stats_;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		Node(const Node&);

		/**
		 */
		void html_tabletd(std::ostream&,
											const std::string& stats_type,
											const std::string& user,
											LineTypeParser::line_type lt) const;

		virtual void log_core(SVNlog&) const=0;

		bool link_;
		std::string local_path_; // path from root
		mutable SVNlog* log_;
		SVNproperty property_;
		SVNinfo svninfo_;

		friend class FilePrinter;
		friend class DirectoryPrinter;
		friend class NodePrinter;
	};

	/**
		 \brief Functor class to compare pointers of Nodes
	*/
	struct NodePtrLess
	{
		/**
			 @return true if first and second are of same type (Directory or
			 File) and name of first is (alphabetically) less than name of
			 second; or if first is a Directory and second is a File.
		 */
		inline bool operator()(const Node* first, const Node* second) const
		{
			if (first->dir()==second->dir())
				return first->name()<second->name();
			return first->dir();
		}
	};

}} // end of namespace svndigest and namespace theplu

#endif
