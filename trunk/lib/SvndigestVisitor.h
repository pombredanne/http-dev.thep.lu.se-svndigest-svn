#ifndef _theplu_svndigest_svndigest_visitor_
#define _theplu_svndigest_svndigest_visitor_

// $Id: SvndigestVisitor.h 1535 2012-10-06 03:33:13Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodeVisitor.h"

#include <subversion-1/svn_types.h>

#include <vector>

namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
	*/
  class SvndigestVisitor : public NodeVisitor
  {
  public:
		SvndigestVisitor(bool verbose, bool ignore_cache, bool report,bool update);

		/**
		 */
		bool enter(Directory& dir);

		/**
		 */
		void leave(Directory& dir);

		/**
		 */
		void visit(File& dir);

	private:
		bool verbose_;
		bool ignore_cache_;
		bool report_;
		bool update_;
		std::vector<svn_revnum_t> ignore_rev_;
	};
}} // end of namespace svndigest and namespace theplu

#endif
