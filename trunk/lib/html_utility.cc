// $Id: html_utility.cc 1537 2012-10-07 07:37:33Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "html_utility.h"

#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include "subversion_revision.h"

#include <cassert>
#include <sstream>
#include <string>

namespace theplu{
namespace svndigest{

	std::string anchor(const std::string& url,
										 const std::string& name, unsigned int level,
										 const std::string& title,
										 const std::string& color)
	{
		std::stringstream ss;
		HtmlStream hs(ss);
		ss << "<a title=\"";
		hs << title;
		ss << "\" href=\"";
		for (size_t i=0; i<level; ++i)
			ss << "../";
		ss << url;
		ss << "\">";
		if (color.size())
			ss << "<font color=\"" << color << "\">";
		hs << name;
		if (color.size())
			ss << "</font>";
		ss << "</a>";
		return ss.str();
	}


	std::string image(const std::string& name)
	{
		std::ostringstream os;
		const std::string& format = Configuration::instance().image_format();
		const std::string& anchor_format =
			Configuration::instance().image_anchor_format();
		if (format=="svg") {
			os << "<object data='" << name << ".svg' type='image/svg+xml'"
				 << " width='600'>\n"
				 << "<embed src='" << name << ".svg' type='image/svg+xml'"
				 << " width='600' />\n"
				 << "</object>\n";
			if (anchor_format != "svg")
				std::cerr << "svndigest: anchor_format: " << anchor_format
									<< " not supported with format " << format << "\n";
		}
		else if (format=="png") {
			if (anchor_format=="none")
				os << "<img src='" << name << ".png' alt='[plot]'/>";
			else
				os << "<a href=\"" << name << "." << anchor_format << "\">"
					 << "<img src='" << name << ".png' alt='[plot]'/>"
					 << "</a>";
		}
		else if (format=="none")
			os << "<!-- no images -->";
		else {
			assert(false);
			throw std::runtime_error("unknown image format: " + format);
		}
		return os.str();
	}


	void print_footer(std::ostream& os)
	{
		Date date;
		HtmlStream hs(os);
		os << "<p class=\"footer\">\nGenerated on ";
		hs << date("%a %b %e %H:%M:%S %Y") << " (UTC) by ";
		os << anchor(PACKAGE_URL, PACKAGE_STRING, 0, "");
		if (DEV_BUILD)
			os << " (dev build " << svn_revision() << ")";
		os << "\n</p>\n</div>\n</body>\n</html>\n";
	}


	void print_header(std::ostream& os, std::string title, unsigned int level,
										std::string user, std::string item, std::string path,
										const std::string& stats, svn_revnum_t rev)
	{
		print_html_start(os, title, level, rev);
		os << "<div id=\"menu\">"
			 << "<ul>\n<li></li>\n";
		if (item=="main")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor("index.html", "Main", level, "Main page");
		os << "</li>\n";

		std::string stats_local(stats);
		if (stats_local=="none")
			stats_local = "classic";

		if (item=="total")
			os << "<li class=\"highlight\">";
		else
			os << "<li>\n";
		os << anchor(stats_local+"/"+user+"/total/"+path, "Total", level,
					 "View statistics of all lines");
		os << "</li>\n";

		if (item=="code")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(stats_local+"/"+user+"/code/"+path, "Code", level,
								 "View statistics of code lines");
		os << "</li>\n";

		if (item=="comments")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(stats_local+"/"+user+"/comments/"+path, "Comment", level,
								 "View statistics of comment lines");
		os << "</li>\n";


		if (item=="empty")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor(stats_local+"/"+user+"/empty/"+path, "Other", level,
								 "View statistics of other lines");
		os << "</li>\n";
		os << "<li>";
		// Peter, this is ugly! But how to add space?
		for (size_t i=0; i<50; ++i)
			os << "&nbsp;";
		os << "</li>\n";

		std::string item_local(item);
		if (item_local=="none")
			item_local = "total";
		if (item_local=="main")
			item_local = "total";

		if (stats=="classic")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor("classic/"+user+"/"+item_local+"/"+path, "Classic", level,
								 "View classic statistics");
		os << "</li>\n";

		if (stats=="blame")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor("blame/"+user+"/"+item_local+"/"+path, "Blame", level,
								 "View blame statistics");
		os << "</li>\n";

		if (stats=="add")
			os << "<li class=\"highlight\">";
		else
			os << "<li>";
		os << anchor("add/"+user+"/"+item_local+"/"+path, "Add", level,
								 "View add statistics");
		os << "</li>\n";


		os << "</li>\n"
			 << "</ul></div>\n"
			 << "<div id=\"main\">\n";
	}


	void print_html_start(std::ostream& os, const std::string& title,
												unsigned int level, svn_revnum_t rev)
	{
		os << "<!-- Generated by " << PACKAGE_STRING << "-->\n";
		if (rev)
			os << "<!-- revision: " << rev << " -->\n";
		os << "<!DOCTYPE html\n"
			 << "PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
			 << "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
			 << "<html xmlns=\"http://www.w3.org/1999/xhtml\""
			 << " xml:lang=\"en\" lang=\"en\">\n"
			 << "<head>\n"
			 << "<title> " << title << " - svndigest</title>\n"
			 << "<meta http-equiv=\"Content-type\" content=\"text/html; "
			 << "charset=UTF-8\" />\n"
			 << "<link rel=\"stylesheet\" "
			 << "href=\"";
		for (unsigned int i=0; i<level; ++i)
			os << "../";
		os << "svndigest.css\" type=\"text/css\" />\n"
			 << "</head>\n"
			 << "<body>\n";
	}

	std::string trac_revision(svn_revnum_t r, std::string color)
	{
		const Configuration& conf = Configuration::instance();
		std::stringstream ss;
		if (conf.trac_root().empty())
			ss << r;
		else {
			std::stringstream rev;
			rev << r;
			ss << anchor(conf.trac_root()+"changeset/"+rev.str(), rev.str(),
									 0, "View ChangeSet "+rev.str(), color);
		}
		return ss.str();
	}

}} // end of namespace svndigest and namespace theplu
