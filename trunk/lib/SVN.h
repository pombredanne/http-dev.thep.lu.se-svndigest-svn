#ifndef _theplu_svndigest_svn_
#define _theplu_svndigest_svn_

// $Id: SVN.h 1547 2012-10-20 08:31:38Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// Turning off warnings from using deprecated function, i.e., we are
// using subversion 1.4
#ifndef SVN_DEPRECATED
#define SVN_DEPRECATED
#endif


#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include <subversion-1/svn_client.h>
#include <subversion-1/svn_types.h>

namespace theplu {
namespace svndigest {

	struct log_receiver_baton;

	///
	/// If something goes wrong in the use of the different SVN classes,
	/// an SVNException is thrown.
	///
	struct SVNException : public std::runtime_error
	{
		SVNException(const std::string& msg, svn_error_t* error=NULL);

		/**
			 Copy constructor
		 */
		SVNException(const SVNException& other);

		/**
			 Destructor
		 */
		virtual ~SVNException(void) throw();

		/**
			 override base implementation
		 */
		const char* what(void) const throw();

		/**
			 access svn_error_t
		 */
		const svn_error_t* error(void) const;
	private:
		svn_error_t* error_;
		std::string msg_;
		int* ref_count_;

		// assignment not allowed
		SVNException& operator=(const SVNException&);
	};

	/**
		 \brief The SVN class is a front end to the subversion API.

		 SVN provides one single global access point to the underlying
		 subversion API and makes sure that there is only one point of
		 access for the binary.

		 The singleton SVN object should be initialized with
		 SVN::instancs(const std::string& path), rather than
		 SVN::instance(void), before using any other subversion related
		 classes or calls. Best practice is to initilize the singleton
		 object early in the main program. The logic behind this
		 requirement is that all subverison related classes and calls
		 expect that repository and WC access is properly set up at
		 initialization. However, most functionality is available
		 irrespectively which instance call is made.

		 \see Design Patterns (the singleton pattern). Subversion API
		 documents, SVN::instancs(void), SVN::instancs(const
		 std::string&).
	*/
	class SVN {
	public:

		enum vc_status {
			unversioned=0,
			uptodate,
			unresolved
		};

		/**
			 \brief Call the underlying svn_client_blame3 for \a path with
			 \a receiver and \a baton.

			 This function is called from SVNblame to do 'svn blame' on an
			 item. The \a receiver and \a baton is defined in SVNblame and
			 the \a receiver is called by the underlying subversion API for
			 every line in \a path provided it the item is under subversion
			 control. The \a baton is used to communicate anonymous
			 information through the API to the \a receiver. If \a path is a
			 binary object an error is returned, all other errors will
			 generate an SVNException.

			 \a path can be either a URL or an WC target.
		*/
		void client_blame(const std::string& path,
											svn_client_blame_receiver_t receiver,
											void *baton);

		/**
			 \brief Same as function above with the extension that revision
			 \a rev can be set.
		 */
		void client_blame(const std::string& path,
											svn_client_blame_receiver_t receiver,
											void *baton, svn_revnum_t rev);

		/**
			 \brief access the content of file \a path at revision \a rev

			 Content of file \a path at revision \a rev is copied to \a
			 result. svn:keywords are expanded just as in 'svn cat'.

			 \see svn_client_call2(6)
		*/
		void client_cat(const std::string& path, svn_revnum_t rev,
										std::string& result);



		/**
			 \brief Call the underlying svn_client_info for \a path with \a
			 receiver and \a baton.

			 This function is called from SVNinfo to do 'svn info' on an
			 item. The \a receiver and \a baton is defined in SVNinfo and
			 the \a receiver is called by the underlying subversion API if
			 \a path is under subversion control. The \a baton is used to
			 communicate anonymous information through the API to the \a
			 receiver.

			 \a path can be either a URL or an WC target.

			 \see Subversion API documentation, SVNinfo
		*/
		void client_info(const std::string& path, svn_info_receiver_t receiver,
										 void *baton);

		/**
			 \a path can be either a URL or an WC target.

			 \todo doc
		*/
		void client_log(const std::string& path, svn_log_message_receiver_t receiver,
										void *baton);

		/**
			 \brief Get the subversion properties for \a path.

			 The retrieved properties are stored in \a properties. To check
			 whether \a is a binary item use SVNproperty::binary(void).

			 \a path can be either a URL or an WC target.
		*/
		void client_proplist(const std::string& path,
												 std::map<std::string, std::string>& properties);

		/**
			 \brief Get an instance of SVN.

			 The singleton SVN object should be initialized with
			 SVN::instancs(const std::string&) before usage of this
			 function. Best practice is to initilize the singleton object
			 early in the main program. The logic behind this requirement is
			 that subverison related classes and calls may expect that
			 repository and WC access is properly set up at initialization.

			 \throw An SVNException if the singleton SVN onject is not
			 already initilized.

			 \see SVN::instancs(const std::string&)
		*/
		static SVN* instance(void);

		/**
			 \brief Get an instance of SVN setup against repository pointed
			 to by \a path.

			 The singleton SVN object should be initialized with this
			 instance call before any subversion related classes or calls
			 are made. Best practice is to initilize the singleton object
			 early in the main program. The logic behind this requirement is
			 that subverison related classes and calls may expect that
			 repository and WC access is properly set up at initialization.

			 \throw Throws an SVNException if initialization fails in the
			 underlying subversion API calls, or if \a path is a URL.
		*/
		static SVN* instance(const std::string& path);

		/**
			 \brief Set up a repository access session.

			 \throws SVNException if session setup fails, or if a session is
			 already set up (i.e., repository cannot be changed during
			 program lifetime).
		*/
		//		void setup_ra_session(const std::string& path);

		///
		/// @brief Check if entry \a path is under version control
		///
		/// @return True if \a path is under version control, false
		/// otherwise.
		///
		vc_status version_controlled(const std::string& path);

	private:
		/**
			 \brief Constructor

			 The only way to create an object of SVN type is by calling
			 SVN::instance(const std::string&). \a path must be a WC path,
			 i.e., not a URL.
		*/
		SVN(const std::string& path);

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVN(const SVN&);
		SVN& operator=(const SVN&);

		///
		/// @brief The destructor.
		///
		virtual ~SVN(void);

		/**
			 @brief Free resources when failing to reach end of
			 constructor.

			 cleanup_failed_init will free all resource acquired in the
			 constructor and throw an SVNException with \a message as the
			 message.

			 @see SVNException
		*/
		void cleanup_failed_init(svn_error_t *err, const std::string& message);

		static SVN* instance_;

		// Subversion API stuff

		/**
			 the url is fech with svn info. The url is stored in a
			 url_receiver_baton. The struct is filled in the url_receiver
			 function.
		*/
		struct url_receiver_baton {
			std::string root_url;
			std::string url;
		};

		/**
			 url_receiver is the function passed to the underlying
			 subversion API call svn_client_info. This function is called by
			 the subversion API for every item matched by the conditions of
			 the API call.

			 \see Subversion API documentation
		*/
		static svn_error_t*
		url_receiver(void *baton, const char *path,
								 const svn_info_t *info, apr_pool_t *pool);

		void client_blame_call(const std::string& path,
													 svn_client_blame_receiver_t receiver,
													 void *baton, svn_opt_revision_t& head);

		svn_wc_adm_access_t* adm_access_;
		apr_allocator_t* allocator_;
		svn_client_ctx_t* context_;
		apr_pool_t* pool_;
		svn_ra_session_t* ra_session_;
		std::string abs_wc_root_path_;
		std::string relative_url_;

		/**
			 Private class holding an apr_pool_t* and that takes care of
			 deallocation (in destructor) which decreases risk for memory
			 leaks especially in case of errors and when exceptions are
			 thrown.
		 */
		class AprPool
		{
		public:
			/**
				 create a sub-pool of \a pool
			*/
			AprPool(apr_pool_t* pool);

			/**
				 \brief destructor deallocates apr pool
			 */
			~AprPool(void);

			/**
				 Access apr pool
			 */
			apr_pool_t* get(void);
		private:
			apr_pool_t* pool_;
		};

	};

}} // end of namespace svndigest and namespace theplu

#endif
