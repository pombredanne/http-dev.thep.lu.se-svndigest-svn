// $Id: SkipWhiteSpaceIterator.cc 1550 2012-10-24 22:50:22Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SkipWhiteSpaceIterator.h"

#include <string>

namespace theplu {
namespace svndigest {

	using std::string;

	SkipWhiteSpaceIterator::SkipWhiteSpaceIterator(string::const_iterator b,
																								 string::const_iterator e)
		: base_(b), end_(e)
	{
		// if b points to whitespace, increment
		if (base_ != end_ && isspace(*base_))
			++(*this);
	}


	const std::string::const_iterator& SkipWhiteSpaceIterator::base(void) const
	{
		return base_;
	}


	SkipWhiteSpaceIterator::reference SkipWhiteSpaceIterator::operator*(void) const
	{
		return *base_;
	}


	SkipWhiteSpaceIterator::pointer SkipWhiteSpaceIterator::operator->(void) const
	{
		return &(*base_);
	}


	SkipWhiteSpaceIterator& SkipWhiteSpaceIterator::operator++(void)
	{
		++base_;
		// go to next non white space
		while (base_!= end_ && isspace(*base_))
			++base_;
		return *this;
	}


	bool operator==(const SkipWhiteSpaceIterator& lhs,
									const SkipWhiteSpaceIterator& rhs)
	{
		return lhs.base() == rhs.base();
	}


	bool operator!=(const SkipWhiteSpaceIterator& lhs,
									const SkipWhiteSpaceIterator& rhs)
	{
		return lhs.base() != rhs.base();
	}

}} // end of namespace svndigest and namespace theplu
