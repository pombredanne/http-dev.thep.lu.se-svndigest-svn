// $Id: FilePrinter.cc 1538 2012-10-07 09:29:04Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "FilePrinter.h"

#include "File.h"

#include "Alias.h"
#include "Colors.h"
#include "Configuration.h"
#include "Date.h"
#include "Graph.h"
#include "html_utility.h"
#include "HtmlStream.h"
#include "NodeVisitor.h"
#include "Stats.h"
#include "StatsPlotter.h"
#include "SVNblame.h"
#include "SVNlog.h"
#include "TinyStats.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	FilePrinter::FilePrinter(const File& file)
		: NodePrinter(), file_(file)
	{
	}


	std::string FilePrinter::blame_output_file_name(void) const
	{
		return "blame_output/" + node().local_path() + ".html";
	}


	const Node& FilePrinter::node(void) const
	{
		return file_;
	}


	std::string FilePrinter::out_name(const std::string& stats_type,
																		const std::string& user,
																		const std::string& line_type) const
	{
		std::string lpath = node().local_path();
		if (lpath.empty())
			lpath = "index";
		return stats_type + "/" + user + "/" + line_type + "/" + lpath + ".html";
	}


	std::string FilePrinter::output_path(void) const
	{
		return output_dir()+node().name()+".html";
	}


	void FilePrinter::print_blame(std::ofstream& os) const
	{
		os << "<br /><h3>" << node().local_path() << "</h3>";
		os << "<div class=\"blame_legend\">\n";
		os << "<dl>\n";
		os << "<dt class=\"code\"></dt><dd>Code</dd>\n";
		os << "<dt class=\"comment\"></dt><dd>Comments</dd>\n";
		os << "<dt class=\"other\"></dt><dd>Other</dd>\n";
		os << "</dl>\n</div>\n";
		os << "<table class=\"blame\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th class=\"rev\">Rev</th>\n";
		os << "<th class=\"date\">Date</th>\n";
		os << "<th class=\"author\">Author</th>\n";
		os << "<th class=\"line\">Line</th>\n";
		os << "<th></th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";
		HtmlStream hs(os);
		SVNblame blame(node().path_);
		LineTypeParser parser(node().path_);
		while (blame.valid()) {
			parser.parse(blame.line());
			blame.next_line();
		}
		blame.reset();

		std::vector<LineTypeParser::line_type>::const_iterator
			line_type(parser.type().begin());
		int last=0;
		int first=0;
		bool using_dates=true;
		if (!Graph::date_xticks()) {
			using_dates=false;
			first = Graph::rev_min();
			last = Graph::rev_max();
		}
		else {
			first = Graph::dates()[Graph::rev_min()];
			assert(static_cast<size_t>(Graph::rev_max()) < Graph::dates().size());
			last = Graph::dates()[Graph::rev_max()];
		}
		assert(last>first);
		// color is calculated linearly on time, c = kt + m
		// brightest color (for oldest rev in log) is set to 192.
		double k = 192.0/(first-last);
		double m = -last*k;
		while (blame.valid()) {
			std::string color;
			Date date(blame.date());
			if (using_dates)
				color = hex(static_cast<int>(k*date.seconds()+m),2);
			else
				color = hex(static_cast<int>(k*blame.revision()+m),2);
			os << "<tr>\n<td class=\"rev\">";
			std::stringstream color_ss;
			color_ss << "#" << color << color << color;
			os << "<font color=\"" << color_ss.str() << "\">"
				 << trac_revision(blame.revision(), color_ss.str())
				 << "</font></td>\n<td class=\"date\"><font color=\"#" << color
				 << color << color << "\">" ;
			hs << date("%d %b %y");
			os << "</font></td>\n<td class=\"author\">";
			const std::string& author_color =
				Colors::instance().color_str(blame.author());
			assert(!author_color.empty());
			os << "<font color=\"#" << author_color << "\">";
			hs << blame.author();
			os << "</td>\n<td class=\"";
			assert(line_type!=parser.type().end());
			if (*line_type==LineTypeParser::other)
				os << "line-other";
			else if (*line_type==LineTypeParser::comment ||
							 *line_type==LineTypeParser::copyright)
				os << "line-comment";
			else if (*line_type==LineTypeParser::code)
				os << "line-code";
			else {
				std::string msg="FilePrinter::print_blame(): unexpected line type found";
				throw std::runtime_error(msg);
			}
			os << "\">" << blame.line_no()+1
				 << "</td>\n<td>";
			hs << blame.line();
			os << "</td>\n</tr>\n";
			blame.next_line();
			++line_type;
		}
		os << "</tbody>\n";
		os << "</table>\n";
	}


	void FilePrinter::print_core(const bool verbose) const
	{
		if (!Configuration::instance().output_blame_information())
			return;
		mkdir_p(directory_name(blame_output_file_name()));
		std::ofstream os(blame_output_file_name().c_str());
		assert(os.good());
		print_html_start(os, "svndigest", file_.level_+1,file_.revision_min());
		print_blame(os);
		print_footer(os);
		os.close();
	}


	void FilePrinter::print_core(const std::string& stats_type,
															 const std::string& user,
															 const std::string& line_type,
															 const SVNlog& log) const
	{
		std::string lpath = node().local_path();
		if (lpath.empty())
			lpath = "index";
		std::string imagefile = stats_type+"/"+"images/"+line_type+"/"+lpath;
		std::string html_name = out_name(stats_type, user, line_type);
		mkdir_p(directory_name(html_name));
		mkdir_p(directory_name(imagefile));
		std::ofstream os(html_name.c_str());
		assert(os);
		print_header(os, node().name(), file_.level_+3, user, line_type,
								 lpath+".html", stats_type, file_.revision_min());
		path_anchor(os);

		std::stringstream ss;
		for (size_t i=0; i<file_.level_; ++i)
			ss << "../";
		ss << "../../../";
		if (user=="all")
			ss << StatsPlotter(file_.stats_[stats_type]).plot(imagefile,line_type);
		else
			ss << imagefile;
		os << "<p class=\"plot\">\n";
		os << image(ss.str());
		os << "</p>\n";

		print_author_summary(os, file_.stats_[stats_type], line_type, log);
		os << "\n";
		if (Configuration::instance().output_blame_information())
			os << "<h3>"
				 << anchor(blame_output_file_name(), "Blame Information", file_.level_+3)
				 << "</h3>\n";

		print_footer(os);
		os.close();
	}


}} // end of namespace svndigest and namespace theplu
