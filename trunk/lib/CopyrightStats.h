#ifndef _theplu_svndigest_copyright_stats_
#define _theplu_svndigest_copyright_stats_

// $Id: CopyrightStats.h 1551 2012-11-03 05:03:36Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Alias.h"

#include "yat/SegmentSet.h"

#include <subversion-1/svn_types.h>

#include <map>
#include <set>
#include <string>

namespace theplu {
namespace svndigest {

	/**
	 */
  class CopyrightStats
  {
  public:
		/**
			 Constructor
		 */
		CopyrightStats(const std::string& path, bool ignore_cache,
									 const std::map<int, svn_revnum_t>& year2rev,
									 const yat::utility::SegmentSet<svn_revnum_t>& ignore_revs);

		const std::map<int, std::set<std::string> >& map(void) const;
	private:
		/**
			 load data from cache file (if it exists), and then parse from
			 blame remaining data required to build data.
		 */
		void init(bool ignore_cache, const std::map<int, svn_revnum_t>& year2rev);

		/// return 0 if load failed; otherwise return rev cache represents
		svn_revnum_t load_cache(void);

		void parse(svn_revnum_t rev, const std::map<int, svn_revnum_t>& year2rev);
		void remove_copyright_lines(const std::string& src,
																std::string& result) const;
		void reset(void);

		/**
			 sub is subsequence of seq if sub can be created from seq by
			 removing element in seq.

			 \return true if \a sub is subsequence of \a seq
		 */
		bool subseq(const std::string& sub, const std::string& seq) const;

		void write_cache(void);

		std::string cache_file_;
		std::string config_;
		const yat::utility::SegmentSet<svn_revnum_t>& ignore_revs_;
		std::string path_;
		std::map<int, std::set<std::string> > year2user_;
	};
}} // end of namespace svndigest and namespace theplu

#endif
