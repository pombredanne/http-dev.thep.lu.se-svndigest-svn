// $Id: SVNcat.cc 1547 2012-10-20 08:31:38Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNcat.h"

#include "SVN.h"
#include "SVNproperty.h"


#include <iostream>
namespace theplu {
namespace svndigest {

	SVNcat::SVNcat(const std::string& path, svn_revnum_t rev)
	{
		SVN::instance()->client_cat(path, rev, str_);
	}


	const std::string& SVNcat::str(void) const
	{
		return str_;
	}

}} // end of namespace svndigest and namespace theplu
