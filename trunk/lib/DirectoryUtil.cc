// $Id: DirectoryUtil.cc 1423 2011-12-16 03:19:31Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "DirectoryUtil.h"
#include "utility.h"

#include "yat/Exception.h"

#include <cerrno>
#include <dirent.h>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	DirectoryUtil::DirectoryUtil(const std::string& path)
	{
		using namespace std;
		DIR* directory=opendir(path.c_str());    // C API from dirent.h
		if (!directory)
			throw yat::utility::errno_error("opendir() failed: " + path);
		struct dirent* entry;

		using yat::utility::FileUtil;
		errno=0;
		while ((entry=readdir(directory)))
			entries_.push_back(FileUtil(concatenate_path(path, entry->d_name)));
		closedir(directory);
		if (errno)
			throw yat::utility::errno_error("readdir() failed: " + path);
	}


	DirectoryUtil::const_iterator DirectoryUtil::begin(void) const
	{
		return entries_.begin();
	}


	bool DirectoryUtil::empty(void) const
	{
		return entries_.size()==2;
	}


	DirectoryUtil::const_iterator DirectoryUtil::end(void) const
	{
		return entries_.end();
	}

}} // end of namespace svndigest and namespace theplu
