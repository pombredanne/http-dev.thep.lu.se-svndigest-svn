// $Id: File.cc 1538 2012-10-07 09:29:04Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "File.h"

#include "Alias.h"
#include "Colors.h"
#include "Configuration.h"
#include "Date.h"
#include "html_utility.h"
#include "HtmlStream.h"
#include "NodeVisitor.h"
#include "Stats.h"
#include "SVNblame.h"
#include "SVNlog.h"
#include "TinyStats.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	File::File(const unsigned int level, const std::string& path,
						 const std::string& output)
		: Node(level,path,output)
	{
		output_dir_=output;
		if (!output_dir_.empty())
			output_dir_+='/';
	}


	std::string File::cache_name(void) const
	{
		std::string dir = concatenate_path(directory_name(path()), ".svndigest/");
		return dir + name() + ".svndigest-cache";
	}


	std::string File::href(void) const
	{
		return name()+".html";
	}


	svn_revnum_t File::last_changed_rev(void) const
	{
		return svn_info().last_changed_rev();
	}


	void File::log_core(SVNlog&) const
	{
	}


	std::string File::node_type(void) const
	{
		return std::string("file");
	}


	const StatsCollection& File::parse(bool verbose, bool ignore,
																		 svn_revnum_t ignore_rev)
	{
		if (verbose)
			std::cout << "Parsing '" << path_ << "'" << std::endl;
		stats_.reset();
		stats_.ignore_rev(ignore_rev);
		std::string cache_file = cache_name();
		std::string cache_dir = directory_name(cache_file);
		if (!ignore && node_exist(cache_file)){
			std::ifstream is(cache_file.c_str());
			if (stats_.load_cache(is)) {
				is.close();
				return stats_;
			}
			is.close();
		}
		else
			stats_.parse(path_);
		if (!node_exist(cache_dir))
			mkdir(cache_dir);
		std::string tmp_cache_file(cache_file+"~");
		std::ofstream os(tmp_cache_file.c_str());
		assert(os);
		stats_.print(os);
		os.close();
		rename(tmp_cache_file, cache_file);
		return stats_;
	}


	svn_revnum_t File::revision_min(void) const
	{
		return svn_info().rev();
	}


	void File::traverse(NodeVisitor& visitor)
	{
		visitor.visit(*this);
	}

}} // end of namespace svndigest and namespace theplu
