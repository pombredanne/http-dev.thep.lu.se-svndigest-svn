// $Id: Directory.cc 1538 2012-10-07 09:29:04Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson
	Copyright (C) 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Directory.h"

#include "Alias.h"
#include "Configuration.h"
#include "DirectoryUtil.h"
#include "File.h"
#include "html_utility.h"
#include "Node.h"
#include "NodeVisitor.h"
#include "SVN.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "TinyStats.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <vector>

#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	Directory::Directory(const unsigned int level, const std::string& path,
											 const std::string& output, const std::string& project)
		: Node(level,path,output,project)
	{
		output_dir_=local_path();
		if (!output_dir_.empty())
			output_dir_+='/';

		DirectoryUtil dir(path);

		SVN* svn=SVN::instance();
		std::string mother_url = SVNinfo(path).url();
		assert(mother_url.size());
		for (DirectoryUtil::const_iterator i=dir.begin(); i!=dir.end(); ++i) {
			std::string fn = file_name(i->path());
			if (fn!="." && fn!=".." && fn!=".svn") {
				const std::string& fullpath = i->path();
				switch(svn->version_controlled(fullpath)) {
				case SVN::uptodate:
					struct stat nodestat;                // C api from sys/stat.h
					lstat(fullpath,&nodestat);   // C api from sys/stat.h
					if (S_ISDIR(nodestat.st_mode)) {     // C api from sys/stat.h
						// ignore directory if it is not a daughter of 'path' in repo
						std::string url = SVNinfo(fullpath).url();
						if (mother_url != directory_name(url))
							continue;
						daughters_.push_back(new Directory(level_+1,fullpath,local_path()));
					}
					else
						daughters_.push_back(new File(level_,fullpath,local_path()));
					break;
				case SVN::unresolved:
					throw NodeException("'" + fullpath + "' is not up to date");
				case SVN::unversioned: ; // do nothing
				}
			}
		}
		std::sort(daughters_.begin(), daughters_.end(), NodePtrLess());
	}


	Directory::~Directory(void)
	{
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			delete *i;
	}


	void Directory::collect_stats(void)
	{
		stats_.reset();
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			if (!(*i)->svndigest_ignore()) {
				stats_ += (*i)->stats();
				(*i)->stats().reset();
			}
	}


	bool Directory::dir(void) const
	{
		return true;
	}


	std::string Directory::href(void) const
	{
		return name() + "/index.html";
	}


	svn_revnum_t Directory::last_changed_rev(void) const
	{
		svn_revnum_t res = svn_info().last_changed_rev();
		for (NodeConstIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			res = std::max(res, (*i)->last_changed_rev());
		return res;
	}


	void Directory::log_core(SVNlog& log) const
	{
		for (NodeConstIterator i(daughters_.begin()); i != daughters_.end(); ++i)
			log += (*i)->log();
	}

	std::string Directory::node_type(void) const
	{
		return std::string("directory");
	}


	svn_revnum_t Directory::revision_min(void) const
	{
		svn_revnum_t result = svn_info().rev();
		for (NodeConstIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			result = std::min(result, (*i)->revision_min());
		return result;
	}


	void Directory::traverse(NodeVisitor& visitor)
	{
		if (visitor.enter(*this))
			for (NodeIterator first=daughters_.begin(), end=daughters_.end();
					 first!=end; ++first)
				(*first)->traverse(visitor);
		visitor.leave(*this);
	}


}} // end of namespace svndigest and namespace theplu
