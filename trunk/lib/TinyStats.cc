// $Id: TinyStats.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "TinyStats.h"

#include "LineTypeParser.h"
#include "Stats.h"
#include "StatsCollection.h"

#include <cassert>
#include <map>
#include <string>

namespace theplu{
namespace svndigest{

	unsigned int TinyStats::operator()(const std::string& stats_type,
																		 const std::string& user,
																		 LineTypeParser::line_type lt) const
	{
		std::map<std::string, Map>::const_iterator i = data_.find(stats_type);
		assert (i!=data_.end());
		Map::const_iterator j = i->second.find(user);
		if (j==i->second.end())
			return 0;
		assert(static_cast<size_t>(lt) < j->second.size());
		return j->second[lt];
	}


	void TinyStats::init(const StatsCollection& sc)
	{
		size_t max_lt = LineTypeParser::total;
		typedef std::map<std::string, Stats*> StatsMap;
		const StatsMap& stats_map = sc.stats();
		for (StatsMap::const_iterator i = stats_map.begin();i!=stats_map.end();++i){
			const std::string& stats_type = i->first;
			const Stats& stats = *(i->second);
			std::map<std::string, Vector>& m = data_[stats_type];
			// loop over authors
			for (std::set<std::string>::const_iterator author=stats.authors().begin();
					 author != stats.authors().end(); ++author) {
				Vector& v = m[*author];
				v.resize(max_lt+1, 0);
				v[LineTypeParser::total] = stats.lines(*author);
				v[LineTypeParser::code] = stats.code(*author);
				v[LineTypeParser::comment] = stats.comments(*author);
				v[LineTypeParser::other] = stats.empty(*author);
				assert((*this)(stats_type, *author, LineTypeParser::total)==
							 stats.lines(*author));
			}
			Vector& v = m["all"];
			v.resize(max_lt+1, 0);
			v[LineTypeParser::total] = stats.lines();
			v[LineTypeParser::code] = stats.code();
			v[LineTypeParser::comment] = stats.comments();
			v[LineTypeParser::other] = stats.empty();
		}
	}

}} // end of namespace svndigest and namespace theplu
