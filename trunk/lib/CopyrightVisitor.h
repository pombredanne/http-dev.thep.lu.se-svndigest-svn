#ifndef _theplu_svndigest_copyright_visitor_
#define _theplu_svndigest_copyright_visitor_

// $Id: CopyrightVisitor.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Alias.h"
#include "NodeVisitor.h"

#include "yat/SegmentSet.h"

#include <subversion-1/svn_types.h>

#include <map>
#include <set>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
		 Visitor for updating copyright in files.
	*/
  class CopyrightVisitor : public NodeVisitor
  {
  public:
		CopyrightVisitor(std::map<std::string, Alias>&, bool verbose,
										 const std::map<int, svn_revnum_t>& year2rev,
										 bool ignore_cache);

		/**
			 \return false if dir.svncopyright_ignore
		 */
		bool enter(Directory& dir);

		/**
			 Doing nothing
		 */
		void leave(Directory& dir);

		/**
			 Updating copyright in \a file
		 */
		void visit(File& file);

	private:
		std::map<std::string, Alias>& alias_;
		bool verbose_;
		const std::map<int, svn_revnum_t>& year2rev_;
		bool ignore_cache_;
		typedef yat::utility::SegmentSet<svn_revnum_t> RevisionSet;
		std::map<std::string, RevisionSet> path2ignore_;

		/**
			 add set \a b to set \a a
		*/
		void add(RevisionSet& a, const RevisionSet& b) const;

		/**
			 \return copyright block

			 Create a Copyright block from \a year2auth and prefix each line
			 with \a prefix.
		 */
		std::string copyright_block(const std::map<int, std::set<Alias> >& year2auth,
																const std::string& prefix) const;


		/**
			 Look for copyright block in file \a path.

			 \param path file to look for copyright
			 \param block found copyright block
			 \param start_at_line line number of first line in found block
			 \param end_at_line line number of first line after found block

			 \return true if Copyright block is found
		 */
		bool detect_copyright(const std::string& path, std::string& block,
													size_t& start_at_line, size_t& end_at_line,
													std::string& prefix) const;


		/**
			 Update the copyright in \a file.
		 */
		void update_copyright(const File& file);

		/**
			 Doing the actual print of copyright statement

			 \param path to file
			 \param block new copyright block
			 \param start_at_line line number of first line in old block
			 \param end_at_line line number of first line after old block
		 */
		void update_copyright(const std::string& path, const std::string& block,
													size_t start_at_line, size_t end_at_line) const;


		/**
			 Translating a set of users to a set of aliases using mapping in alias_
		*/
		void translate(const std::set<std::string>&, std::set<Alias>&);

		/**
			 Translating each year
		 */
		void translate(const std::map<int, std::set<std::string> >& year2user,
									 std::map<int, std::set<Alias> >& year2alias);


	};
}} // end of namespace svndigest and namespace theplu

#endif
