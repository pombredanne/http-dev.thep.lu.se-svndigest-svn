// $Id: CopyrightStats.cc 1551 2012-11-03 05:03:36Z peter $

/*
	Copyright (C) 2011 Jari Häkkinen, Peter Johansson
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CopyrightStats.h"

#include "Configuration.h"
#include "LineTypeParser.h"
#include "SkipWhiteSpaceIterator.h"
#include "SVNinfo.h"
#include "SVNlog.h"
#include "SVNcat.h"
#include "utility.h"

#include "yat/utility.h"

#include <subversion-1/svn_types.h>

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

namespace theplu {
namespace svndigest {

	using yat::utility::SegmentSet;

	CopyrightStats::CopyrightStats(const std::string& path, bool ignore_cache,
																 const std::map<int, svn_revnum_t>& year2rev,
																 const SegmentSet<svn_revnum_t>& ignore_revs)
		: ignore_revs_(ignore_revs), path_(path)
	{
		cache_file_ = concatenate_path(concatenate_path(directory_name(path),
																										".svndigest"),
																	 file_name(path)+".svncopyright-cache");
		std::ostringstream ss;
		ss << Configuration::instance().code(path_);
		for (SegmentSet<svn_revnum_t>::const_iterator i=ignore_revs.begin();
				 i!=ignore_revs.end(); ++i) {
			if (i->begin()+1 == i->end())
				ss << i->begin();
			else
				ss << i->begin() << "-" << i->end();
			ss << ";";
		}
		config_ = ss.str();
		init(ignore_cache, year2rev);
	}


	void CopyrightStats::init(bool ignore_cache,
														const std::map<int, svn_revnum_t>& year2rev)
	{
		svn_revnum_t cache_rev = 0;
		if (!ignore_cache)
			cache_rev = load_cache();
		SVNinfo info(path_);
		if (cache_rev >= info.last_changed_rev())
			return;

		// reset stats if cache was invalid
		if (cache_rev == 0)
			reset();

		parse(cache_rev+1, year2rev);
		write_cache();
	}


	svn_revnum_t CopyrightStats::load_cache(void)
	{
		std::ifstream is(cache_file_.c_str());

		std::string line;
		getline(is, line);
		if (line!="SVNCOPYRIGHT CACHE VERSION 2")
			return 0;
		getline(is, line);
		if (line!=config_) {
			std::cout << "cache file is for different configuration.\n"
								<< "config code in cache file: '" << line << "'\n"
								<< "config code: '" << config_ << "'\n"
								<< "retrieving statistics from repository.\n";
			return 0;
		}

		svn_revnum_t rev = 0;
		try {
			getline(is, line);
			size_t nof_years = yat::utility::convert<size_t>(line);
			for (size_t i=0; i<nof_years; ++i) {
				getline(is, line);
				int year = yat::utility::convert<size_t>(line);
				std::set<std::string>& users = year2user_[year];
				getline(is, line);
				size_t nof_users = yat::utility::convert<size_t>(line);
				for (size_t i=0; i<nof_users; ++i) {
					getline(is, line);
					users.insert(line);
				}
			}
			getline(is, line);
			rev = yat::utility::convert<svn_revnum_t>(line);
			getline(is, line);
			if (line!="SVNCOPYRIGHT CACHE")
				return 0;
			return rev;
		}
		catch (yat::utility::runtime_error e) {
			return 0;
		}
		return 0;
	}


	const std::map<int, std::set<std::string> >& CopyrightStats::map(void) const
	{
		return year2user_;
	}


	void CopyrightStats::parse(svn_revnum_t first_rev,
														 const std::map<int, svn_revnum_t>& year2rev)
	{
		std::map<int, svn_revnum_t>::const_iterator yearrev=year2rev.begin();
		SVNlog log(path_);
		typedef SVNlog::container::const_iterator log_iterator;
		log_iterator commit = log.commits().begin();
		while (commit->revision() < first_rev)
			++commit;
		assert(commit->revision() >= first_rev);
		log_iterator end = log.commits().end();

		// loop over all commits
		for ( ; commit!=end; ++commit) {
			// skip commit if its revision is in revisions to ignore
			if (ignore_revs_.count(commit->revision()))
				continue;
			// assure yearrev correspond to commit
			while (yearrev->second < commit->revision()) {
				++yearrev;
				assert(yearrev!=year2rev.end());
			}
			assert(yearrev!=year2rev.end());
			assert(yearrev->second >= commit->revision());

			const std::string& name = commit->author();
			// skip if alias already has copyright for this year.
			std::map<int, std::set<std::string> >::const_iterator year_users =
				year2user_.find(yearrev->first);
			if (year_users!=year2user_.end()
					&& year_users->second.count(name))
				continue;

			SVNcat previous(path_, commit->revision()-1);
			std::string previous_content;
			remove_copyright_lines(previous.str(), previous_content);

			SVNcat current(path_, commit->revision());
			std::string current_content;
			remove_copyright_lines(current.str(), current_content);

			// if any character added between previous and current add
			// author to set of authors
			if (!subseq(current_content, previous_content)) {
				year2user_[yearrev->first].insert(name);
			}
		}
	}


	void CopyrightStats::remove_copyright_lines(const std::string& src,
																							std::string& result) const
	{
		LineTypeParser ltp(path_);
		std::istringstream is(src);
		std::string line;
		while (getline(is, line)) {
			if (ltp.parse(line) != LineTypeParser::copyright) {
				result += line;
				result += '\n';
			}
		}
 	}


	void CopyrightStats::reset(void)
	{
		year2user_.clear();
	}


	bool
	CopyrightStats::subseq(const std::string& sub, const std::string& seq) const
	{
		SkipWhiteSpaceIterator iter1(sub.begin(), sub.end());
		SkipWhiteSpaceIterator end1(sub.end(), sub.end());
		SkipWhiteSpaceIterator iter2(seq.begin(), seq.end());
		SkipWhiteSpaceIterator end2(seq.end(), seq.end());

		for ( ; iter1!=end1; ++iter1) {
			iter2 = std::find(iter2, end2, *iter1);
			if (iter2 == end2) {
				return false;
			}
		}
		return true;
	}


	void CopyrightStats::write_cache(void)
	{
		mkdir_p(directory_name(cache_file_));
		std::ofstream os(cache_file_.c_str());
		assert(os.good());

		os << "SVNCOPYRIGHT CACHE VERSION 2\n";
		os << config_ << "\n";
		os << year2user_.size() << "\n";
		using std::map;
		using std::set;
		using std::string;
		for (map<int, set<string> >::const_iterator i=year2user_.begin();
				 i!=year2user_.end(); ++i) {
			os << i->first << "\n";
			os << i->second.size() << "\n";
			for (set<string>::const_iterator j=i->second.begin();
					 j!=i->second.end(); ++j) {
				os << *j << "\n";
			}
		}

		SVNinfo info(path_);
		os << info.last_changed_rev() << "\n";
		os << "SVNCOPYRIGHT CACHE\n";
		os.close();
	}

}}
