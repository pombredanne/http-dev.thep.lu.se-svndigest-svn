#ifndef _theplu_svndigest_stats_plotter_
#define _theplu_svndigest_stats_plotter_

// $Id: StatsPlotter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Vector.h"

#include <string>
#include <map>

namespace theplu {
namespace svndigest {

	class Stats;

  /**
		 Class plotting a Stats object
	*/
  class StatsPlotter
  {
  public:
    /**
				\brief Default Constructor
		*/
		explicit StatsPlotter(const Stats& stats);

		/**
			 \brief Destructor
		*/
		virtual ~StatsPlotter(void);

		/**
			 Create statistics graph.
		*/
		std::string plot(const std::string&, const std::string&) const;

		/**
			 Plotting code, comment, other, and total in same plot (for
			 'all' not individual authors).
		*/
		void plot_summary(const std::string& output) const;

	private:
		typedef std::map<std::string, SumVector> Author2Vector;
		const Stats& stats_;

		/**
			 called from plot(2)
		 */
		void plot(const std::string& basename, const std::string& linetype,
							const std::string& format) const;

		/**
			 called from plot_summary(1)
		 */
		void plot_summary(const std::string& basename,
											const std::string& format) const;

  };
}} // end of namespace svndigest end of namespace theplu

#endif
