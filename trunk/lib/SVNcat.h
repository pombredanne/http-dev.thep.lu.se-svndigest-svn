#ifndef _theplu_svndigest_svndiff_
#define _theplu_svndigest_svndiff_

// $Id: SVNcat.h 1542 2012-10-11 21:59:45Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	/**
		 Class to access content of a file at a specifici revision
	*/
	class SVNcat
	{
	public:
		SVNcat(const std::string& path1, svn_revnum_t revision1);

		/**
			 \brief access string content of file
		 */
		const std::string& str(void) const;

	private:
		std::string str_;

		// user compiler generated copy
		//SVNcat(const SVNcat&);
		//SVNcat& operator=(const SVNcat&);
	};

}} // end of namespace svndigest and namespace theplu

#endif
