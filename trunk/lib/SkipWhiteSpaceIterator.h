#ifndef theplu_svndigest_skip_white_space_iterator
#define theplu_svndigest_skip_white_space_iterator

// $Id: SkipWhiteSpaceIterator.h 1549 2012-10-24 22:40:01Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iterator>
#include <string>

namespace theplu {
namespace svndigest {

	/**
		 \brief A wrapper around std::string::const_iterator.

		 Iterator behaves as its base std::string::const_iterator except
		 when incremented it skips white-spaces. Iterator is non-mutable
		 and only have forward iterator features, i.e., no random
		 access. Iterator could easily be extended to be a bidirectional
		 iterator by adding a 'operator--'.

		 Constructor takes an end-of-range iterator that typically is
		 'std::string::end()'. This is needed because when 'base_'
		 iterates out of range, 'base_' is not dereferencable and it is
		 thus impossible to check if iterator points to a
		 white-space. Instead we stop incrementing 'base_' as soon as it
		 reaches 'end_'.
	*/
	class SkipWhiteSpaceIterator
	{
	public:
		typedef std::forward_iterator_tag iterator_category;
		typedef std::string::const_iterator::value_type value_type;
		typedef std::string::const_iterator::difference_type difference_type;
		typedef std::string::const_iterator::pointer pointer;
		typedef std::string::const_iterator::reference reference;

		/**
			 Create iterator pointing to same item as \a iter.

			 \a end defines end of range and the iterator will not iterate
			 passed this point.
		 */
		SkipWhiteSpaceIterator(std::string::const_iterator iter,
													 std::string::const_iterator end);

		/**
			 return underlying string iterator
		 */
		const std::string::const_iterator& base(void) const;

		/**
			 return const reference to character
		 */
		reference operator*(void) const;

		/**
			 return pointer
		 */
		pointer operator->(void) const;

		/**
			 Increment iterator to next non-white-space character or end
			 position.
		 */
		SkipWhiteSpaceIterator& operator++(void);

	private:
		std::string::const_iterator base_;
		std::string::const_iterator end_;
		// user compiler generated copy
		//SkipWhiteSpaceIterator(const SkipWhiteSpaceIterator&);
		//SkipWhiteSpaceIterator& operator=(const SkipWhiteSpaceIterator&);
	};

	bool operator==(const SkipWhiteSpaceIterator& lhs,
									const SkipWhiteSpaceIterator& rhs);

	bool operator!=(const SkipWhiteSpaceIterator& lhs,
									const SkipWhiteSpaceIterator& rhs);


}} // end of namespace svndigest and namespace theplu
#endif
