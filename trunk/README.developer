$Id: README.developer 1523 2012-09-28 05:48:51Z peter $

= Developer Information =

This file contains developer specific information.


=== Coding style ===

Developers should follow the coding style outlined in the
[http://cbbp.thep.lu.se/~jari/documents/c++_coding_guidelines/index.html C++ coding guidelines].


=== Subversion usage ===

Commits should be minimalistic and the projects should always compile
('make' and 'make check') when someone makes a clean checkout. There is
a short introduction to subversion and its usage available as
[http://cbbp.thep.lu.se/~jari/documents/subversion_guidelines/index.html Subversion guidelines].
We follow these guidelines.


=== Requirements ===

To build from a subversion checkout, you will need Autotools. More
specifically Automake 1.11 (or later) and Autoconf 2.63 (or later). To
build man page you will need help2man.

=== bootstrap and autoreconf ===

At first checkout from the repository you must run the bootstrapping
utility

  #> ./bootstrap


=== configure ===

Run the generated configure script with

  #> ./configure --enable-debug

The option flag '--enable-debug' turns on some compiler options
appropriate for development (e.g. enables assertions).


=== Testing ===

Test programs should be created and added to the 'make check'
execution. Always check that the test runs before committing code to
the repository, i.e., run 'make check'. For more details on how to run
and write tests, please refer to file 'test/README'.

There is a test repository in 'test/repo' that is a part of
the svndigest repository. The logic behind this is that it is nice to
have a common test repository that can be used off-line. Changes to
the repository are usually made to create new test cases and it is
important that these changes are committed to svndigest repository as
well. All changes to the test repository are not automatically
propagated to the svndigest repository and it is important that some
files created in the test repository are manually added to the
svndigest repository. Assuming that 'make check' is run before a
commit, a warning is generated if the test repository is inconsistent
with the svndigest repository. There will be a diagnostic message and
a resolution hint, however the developer must examine and resolve the
problem manually.

The problem is simply that the file 'test/repo/db/current'
is changed compared to the svndigest repository. The 'current' file
indicates a specific number of change sets within the test repository
and the directories 'test/repo/db/revs' and
'test/repo/db/revprops' contains a matching number of
change sets. However, the change sets added to the test repository are
not automatically added to the svndigest repository, therefore the new
files in the 'revs' and 'revprops' directories must be added manually
to the svndigest repository.

There is also a race condition when developers make changes to the
test repository simultaneously: It is easy to create severe conflicts
in the test repository. Imaging the case when two developers add test
commits to the test repository, and then tries to add these to the
svndigest repository. Since there are different versions of the same
test repository revisions, the second developer committing to the
svndigest will get messages about files being in the way. This is just
one situation with possible conflicts. This conflict is of course
resolved by the second developer having to throw away his changes and
redo them after a new checkout of the test repository.

The take home message is that developers should communicate with each
other when changing the test repository in order to avoid to large
repository conflicts. AND read the output of 'make check'.


=== yat library ===

Directory 'yat/' contains some files from the yat library. These
files are not owned by svndigest and should not (if possible) be
edited here. For more details please see 'yat/README'.

=== Output file structure ===

A directory reflecting the parsed directory is generated for each
combination of StatsType, Author, and LineType, which is located in
'<TARGET>/<ROOT>/StatsType/Author/LineType/'. Author may be, on top of
each author in the repository, a directory 'all' that is a sum over
all authors. In same fashion 'LineType' may be any of the three
linetypes but may also be 'total', which is a sum over all
linetypes. In each '<TARGET>/<ROOT>/StatsType' there are directories
'images/LineType' that contain a structure reflecting the parsed
directory with images for every combination of 'File', 'LineType', and
'StatsType'. Images for different authors are identical and therefore
shared between authors.

== Design and Motivation ==

Please refer to 'doc/HACKING' for a list of design decisions we try to
follow.

= Releasing =

The main development is performed in the trunk branch of the
repository. A new release branch is created for each minor release
from the trunk and patch releases are snapshots of the minor release
branch. This implies that patch work is performed in the minor release
branch, and changes made release branch is transferred to the trunk
every time a new patch release is made. Remember, patch work should be
limited to bug fixes and important fixes only leaving development of
new features and designs to the trunk branch.

Releases should only be performed by an appointed member of the team,
the Release Manager.

== Release procedure ==

This section describes what to do when publishing a release.

=== Creating a release branch ===

Once people agree that a new release branch should be made, the
Release Manager creates it with the following procedure (substitute
A.B with the version you are preparing):

1. Mark the new minor series in 'NEWS':

8<---
 See the end for copyrights and conditions.

+svndigest A.B.x series from
+          http://dev.thep/lu.se/svndigest/svn/branches/A.B-stable
+
Version A.B (released NOT YET)
8<---

   commit the changes with

   #> svn ci -m "preparing branch A.B-stable"

2. Update copyright statements with commands

   #> svncopyright -v

   Examine the updates and commit changes with

   #> svn ci -m "updating copyright statements"

3. Create a new minor branch using a one liner like

   #> make svn-stable-branch

4. Prepare the trunk for the next minor release:

   a) Update version number in 'm4/version.m4'.
      MY_VERSION_early([A], [B+1], [0], [true])

   b) Add an entry in 'NEWS'

version A.[B+1] (released NOT YET)

      The date is set when version A.[B+1] is released.

   c) Commit changes to the repository

   #> svn ci -m "Bumping VERSION to A.[B+1]"


=== Rolling a release ===

This section describes how to release a new version. If this is a
minor release (C=0), replace A.B.C with A.B. Before you start, make
sure you have write access to sourceforge page and that you can create
checksum files, i.e., you need 'openssl' and 'md5'/'md5sum'.

1. Create a new milestone for next patch release A.B.[C+1].

2. Update version number in 'm4/version.m4'. Locate and change the below line
---
MY_VERSION_early([A], [B], [C], [false])
---

3. Update file 'THANKS'

4. Make sure that the items in 'NEWS' cover the new features of the
   release. Set the date for the new release:

version A.B.C (released 26 June 2007)

5. Commit changes to the repository

   #> svn ci -m "Preparing release A.B.C"

6. Update copyright statements with commands

   #> make && ./bin/svncopyright -v

   Examine the updates and commit changes with

   #> svn ci -m "updating copyright statements"

7. Issue 'svn update' to make entire wc to be in revision 'HEAD'.
   Needless to say, make sure that the program is in a state to be
   released; make sure that all the tests pass, test the distribution
   package, and perform all other release tests you think is
   appropriate. Run:

  #> make all && ./build_support/tag_and_release.sh


8. Update wiki first page under section 'Latest Release'.

   a) Update version to A.B.C.

   b) Update link to NEWS file to 'source:tags/A.B.C/NEWS'

   c) If this is a minor release (C=0) update links to
      'source:branches/A.B-stable/doc/readme.txt',
      'source:branches/A.B-stable/README'.

9. Update SubversionCheckout

   a) In section 'svndigest latest release' update link to
      [source:tags/A.B.C/NEWS NEWS] and modify the svn commands to

svn checkout http://dev.thep.lu.se/svndigest/svn/tags/A.B.C svndigest-A.B.C

svn switch http://dev.thep.lu.se/svndigest/svn/tags/A.B.C

   b) If this is a minor release (C=0) update link in 'svndigest stable' to
      [source:branch/A.B-stable/NEWS NEWS] and modify the svn commands to

svn co http://dev.thep.lu.se/svndigest/svn/branches/A.B-stable svndigest-A.B

svn switch ^/branches/A.B-stable

10. Add a version A.B.C using trac-admin tool

11. Close milestone A.B.C

12. Merge the release into trunk. Go to a pristine trunk WC and

   #> cd /path/to/svndigest-trunk
   #> svn update
   #> svn diff
   #> svn merge ^/branches/A.B-stable

   Resolve all conflicts. Run tests and perform all other appropriate
   tests to make sure that the merge does not create havoc. Typically
   changes in 'm4/version.m4' are problematic so check this file extra
   carefully.

   Commit changes to the trunk branch.

   #> svn commit -m "Merged release A.B.C to the trunk."

13. Prepare stable branch for future release

   a) Update version number in 'version.m4' in line

      MY_VERSION_early([A], [B], [C+1], [true])

   b) Add an entry in 'NEWS'

8<---
version A.B.[C+1] (released NOT YET)
8<---
      The date is set when version A.B.[C+1] is released.

   c) Commit changes to the repository,

   #> svn ci -m "Bumping version"

14. Send an announcement to svndigest-users@lists.sourceforge.net
    using 'announment.txt' as template.

15. Create a macport ticket, https://trac.macports.org/newticket, with
      summary='svndigest A.B.C',
      type=update,
      keywords="haspatch maintainer",
      and port=svndigest.
    Attach 'Portfile.diff' to newly created ticket.


--------------
Copyright (C) 2005, 2006 Jari Häkkinen
Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
Copyright (C) 2009, 2010, 2012 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
