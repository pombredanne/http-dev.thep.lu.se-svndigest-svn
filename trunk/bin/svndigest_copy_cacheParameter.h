#ifndef _theplu_svndigest_svndigest_copy_cache_parameter_
#define _theplu_svndigest_svndigest_copy_cache_parameter_

// $Id: svndigest_copy_cacheParameter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Parameter.h"

#include "lib/OptionVersion.h"

#include "yat/CommandLine.h"
#include "yat/OptionArg.h"

#include <string>

namespace theplu {
namespace svndigest {

  // class for command line options.
	class svndigest_copy_cacheParameter : public AbstractParameter
	{
	public:
		svndigest_copy_cacheParameter(void);
		virtual ~svndigest_copy_cacheParameter(void);
		/// @return absolute path to target directory
		std::string target(void) const;
	private:
		void analyse1(void);
		void init1(void);
		void set_default1(void);
		yat::utility::OptionArg<std::string> target_;

	};

}} // of namespace svndigest and namespace theplu

#endif
