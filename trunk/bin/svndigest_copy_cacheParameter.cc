// $Id: svndigest_copy_cacheParameter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "svndigest_copy_cacheParameter.h"

#include "lib/utility.h"

#include <string>

namespace theplu {
namespace svndigest {

	svndigest_copy_cacheParameter::svndigest_copy_cacheParameter(void)
		: AbstractParameter(),
			target_(cmd_, "t,target", "svn wc cache is copied to")
	{
	}


	svndigest_copy_cacheParameter::~svndigest_copy_cacheParameter(void)
	{
	}


	void svndigest_copy_cacheParameter::analyse1(void)
	{
		analyse_root(root_.value());
		// check target
		check_existence(target_.value());
		check_readable(target_.value());
		check_is_dir(target_.value());
		std::string save_wd = pwd();
		chdir(target_.value());
		target_.value(pwd());
		chdir(save_wd);
		// FIXME: should check inodeno to handle symlinks
		if (root()==target()) {
			std::stringstream ss;
			ss << cmd_.program_name() << ": '" << root() << "' and '"
				 << target() << "' are identical (no copy)\n";
			throw yat::utility::cmd_error(ss.str());
		}
	}


	void svndigest_copy_cacheParameter::init1(void)
	{
		target_.print_arg("=TARGET");
		root_.description("svn wc cache is copied from");
		help_.synopsis() =
			"Copy svndigest cache from one working copy to another.\n";
		version_.program_name("svndigest-copy-cache");
	}


	void svndigest_copy_cacheParameter::set_default1(void)
	{
		if (!target_.present())
			target_.value(".");
	}

	std::string svndigest_copy_cacheParameter::target(void) const
	{
		return target_.value();
	}

}} // of namespace svndigest and namespace theplu
