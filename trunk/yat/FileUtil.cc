// $Id: FileUtil.cc 2837 2012-09-16 23:07:47Z peter $

/*
	Copyright (C) 2004 Jari Häkkinen
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "FileUtil.h"

#include "Exception.h"

#include <cerrno>
#include <cstddef>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>

#include <sys/stat.h>
#include <unistd.h>


namespace theplu {
namespace yat {
namespace utility {


	FileUtil::FileUtil(const std::string& path)
		: path_(path)
	{
	}


	FileUtil::FileUtil(const FileUtil& other)
		: path_(other.path_)
	{
	}


	int FileUtil::permissions(const std::string& bits) const
	{
		std::string tryme=path_;
		if (!exists()) {
			std::string::size_type pos=path_.find_last_of('/');
			if (pos == std::string::npos)
				tryme = ".";
			else if (pos == 0)
				tryme = "/";
			else
				tryme.resize(pos);
		}

		int mode=0;
		bool ok = true;
		for (size_t i=0; i<bits.length(); i++)
			switch (bits[i]) {
			    case 'r':
						mode|=R_OK;
						break;
					case 'w':
						mode|=W_OK;
						break;
					case 'x':
						mode|=X_OK;
						break;
			    case 'd':
						struct stat nodestat;
						stat(tryme.c_str(),&nodestat);
						if (!S_ISDIR(nodestat.st_mode))
							ok = false;
						break;
			    default:
						throw std::invalid_argument("FileUtil::permission: "+bits);
			}
		if (!ok)
			return -1;
		return access(tryme.c_str(),mode);
	}


	bool FileUtil::exists(void) const
	{
		struct stat statt;
		return exists_common(stat(path_.c_str(),&statt));
	}


	bool FileUtil::exists_common(bool failed) const
	{
		if ( failed && (errno!=ENOENT) ) {
			std::stringstream ss;
			ss << "stat(2) call failed with errno: " << errno << "\n"
				 << strerror(errno);
 			throw IO_error(ss.str());
		}
		return !failed;
	}


	bool FileUtil::lexists(void) const
	{
		struct stat statt;
		return exists_common(lstat(path_.c_str(),&statt));
	}


	const std::string& FileUtil::path(void) const
	{
		return path_;
	}


	FileUtil& FileUtil::operator=(const FileUtil& rhs)
	{
		path_ = rhs.path_;
		return *this;
	}

}}} // of namespace utility, yat, and theplu
