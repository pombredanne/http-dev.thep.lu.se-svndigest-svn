$Id: README 1528 2012-10-02 10:45:50Z peter $

This directory contains files that belong to the yat project. For more
information on the yat project please refer to
http://dev.thep.lu.se/yat.

Developers may update to the latest revision of yat via 'make
fetch'. Be careful doing this and particularly in sensitive branches
such as a stable-branch. All files are left pristine except
`Exception.cc' and `stl_utility', which are modified to avoid
dependency to GSL and Boost, respectively.

How to add a new file from yat? Add the file to 'yat_headers' or
'libyat_a_SOURCES' as appropriate. Check that file only includes
standard header files and yat header files already present here. Issue
'make fetch' and set svndigest:ignore="" and svn:keywords="". The
latter is to clarify that the 'Id' string that appears in the file
contains information related to the yat repository and not svndigest
repository.

---

Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with svndigest. If not, see <http://www.gnu.org/licenses/>.
