#ifndef _theplu_yat_utility_split_
#define _theplu_yat_utility_split_

// $Id: split.h 2898 2012-12-12 05:13:02Z peter $

/*
	Copyright (C) 2010, 2012 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	/**
		 \brief split a string into several substrings

		 Split \a str with respect to \a delim and place result in \a
		 result. If \a str contains N \a delim, the resulting \a result
		 will hold N+1 elements (given that it was empty to start
		 with). If first or last character in \a str is \a delim,
		 corresponding element in \a result will be empty string ("").

		 \since New in yat 0.7
	 */
	void split(std::vector<std::string>& result, const std::string& str,
						 char delim);

	/**
		 \brief split a string into substrings

		 Same as
		 void split(std::vector<std::string>&, const std::string&,char delim);
		 but split if character matches any in delims, i.e.,
		 split(vec,"split,me;please", ",;") will be split into "split",
		 "me", and "please".

		 \since New in yat 0.10
	 */
	void split(std::vector<std::string>& result, const std::string& str,
						 const std::string& delim);

}}} // of namespace utility, yat, and theplu

#endif
