// $Id: OptionHelp.cc 2919 2012-12-19 06:54:23Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "OptionHelp.h"
#include "CommandLine.h"

#include <cstdlib>
#include <iostream>
#include <string>

namespace theplu {
namespace yat {
namespace utility {


	OptionHelp::OptionHelp(CommandLine& cmd, std::string flag, 
												 std::string desc)
		: OptionSwitch(cmd, flag, desc, false), synopsis_("")
	{
	}


	void OptionHelp::do_parse2(std::vector<std::string>::iterator first, 
															 std::vector<std::string>::iterator last)
	{
		if (usage_.empty()) 
			std::cout << "Usage: " << cmd().program_name() << " [OPTION]...\n\n";
		else
			std::cout << usage_ << "\n"; 
		std::cout << synopsis_ << "\n";
		std::cout << cmd() << "\n"; 
		std::cout	<< post_cmd_;
		exit(EXIT_SUCCESS);
	}


	std::string& OptionHelp::post_arguments(void)
	{
		return post_cmd_;
	}


	std::string& OptionHelp::synopsis(void)
	{
		return synopsis_;
	}

	std::string& OptionHelp::usage(void)
	{
		return usage_;
	}

}}} // of namespace utility, yat, and theplu
