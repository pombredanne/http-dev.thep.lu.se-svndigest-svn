// $Id: dummie.cc 1376 2011-06-14 00:02:11Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

/*
	This file contains some empty implementations of functions in
	libyat. The implementations are needed to avoid link errors but the
	full implementations are not needed.
 */

#include "utility.h"

namespace theplu {
namespace yat {
namespace utility {

	bool is_equal(std::string, std::string) { return false; }
	bool is_nan(const std::string&) { return false; }

}}}
