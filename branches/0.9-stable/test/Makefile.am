## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 1468 2012-03-19 05:46:07Z peter $

# Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010 Jari Häkkinen, Peter Johansson
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

check_SCRIPTS = svn_update.sh init.sh

## we use suffix .cc for all source
AM_DEFAULT_SOURCE_EXT = .cc

EXTRA_PROGRAMS = cache_partial.test color.test \
	config.test copyright.test date.test \
	graph.test htmlstream.test \
	parser.test option.test stats.test svn_diff.test trac.test utility.test \
	vector.test

CLEANFILES = $(EXTRA_PROGRAMS)

option_test_SOURCES = option.cc $(top_srcdir)/bin/Parameter.cc \
	$(top_srcdir)/bin/svndigestParameter.cc

distributed_TESTS =
distributed_TESTS += cmd_format_test.sh
distributed_TESTS += config2_test.sh
distributed_TESTS += config3_test.sh
distributed_TESTS += config_props_test.sh
distributed_TESTS += copyright2_test.sh
distributed_TESTS += error_test.sh
distributed_TESTS += link_root_test.sh
distributed_TESTS += permission_test.sh
distributed_TESTS += repo_status_test.sh
distributed_TESTS += repo_test.sh
distributed_TESTS += svncopyright_test.sh
distributed_TESTS += svndigest_copy_cache_test.sh
distributed_TESTS += traverse_test.sh

TESTS = $(EXTRA_PROGRAMS) $(distributed_TESTS)

TEST_EXTENSIONS = .sh .test

EXTRA_DIST = $(distributed_TESTS)

# tests not yet passing are listed here
XFAIL_TESTS =

noinst_HEADERS = Suite.h

check_LIBRARIES = libsvndigesttest.a

LDADD = $(builddir)/libsvndigesttest.a \
	$(top_builddir)/lib/libsvndigest.a \
	$(top_builddir)/yat/libyat.a \
	$(SVN_LIBS) $(APR_LIBS) $(PLPLOT_LIBS)
AM_LDFLAGS = $(SVNDIGEST_LDFLAGS)

AM_CPPFLAGS = -I$(top_srcdir) $(SVNDIGEST_CPPFLAGS)
AM_CXXFLAGS = $(SVNDIGEST_CXXFLAGS)

libsvndigesttest_a_SOURCES = Suite.cc

clean-local: 
	rm -rf generated_output toy_project testSubDir

mostlyclean-local: 
	rm -f *.png *.tmp *~ 

.PHONY: lazycheck

lazycheck:; $(MAKE) $(AM_MAKEFLAGS) check RECHECK_LOGS= 

if HAVE_SVN_WC
repo_stamp = $(srcdir)/repo/db/current
else
repo_stamp =
endif

# dependencies for lazycheck
cmd_format_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT) $(repo_stamp)
config2_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT) $(repo_stamp)
config3_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT)
config_props_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT) $(repo_stamp)
copyright2_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT) $(repo_stamp)
permission_test.log:init.sh $(top_builddir)/bin/svncopyright$(EXEEXT) $(repo_stamp)
repo_status_test.log:init.sh 
repo_test.log:init.sh $(top_builddir)/bin/svndigest$(EXEEXT) $(repo_stamp)
svncopyright_test.log:init.sh $(top_builddir)/bin/svncopyright$(EXEEXT)
svndigest_copy_cache_test.log:init.sh $(top_builddir)/bin/svndigest-copy-cache $(repo_stamp)

cache_partial.log: $(repo_stamp)
copyright.log: $(repo_stamp)
stats.log: $(repo_stamp)

