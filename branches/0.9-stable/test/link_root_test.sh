#!/bin/sh

# $Id: link_root_test.sh 1264 2010-11-02 00:46:21Z peter $

# Copyright (C) 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. ./init.sh || exit 1

targetdir=output
rm -rf $targetdir
$mkdir_p $targetdir
SVNDIGEST_run 0 -r $rootdir/link-dir -t $targetdir --ignore-cache --format=none

ls $targetdir
test -e $targetdir/link-dir || exit_fail

exit_success;
