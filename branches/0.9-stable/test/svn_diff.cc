// $Id: svn_diff.cc 1168 2010-08-15 19:30:22Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/SVNdiff.h"
#include "lib/SVN.h"

#include <cstdlib>
#include <ostream>
#include <string>

using namespace theplu::svndigest;

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);
	SVN* svn=SVN::instance("toy_project");
	if (!svn){
		suite.out() << "error: cannot create SVN instance\n";
		return EXIT_FAILURE;
	}

	SVNdiff diff("toy_project/README", 42, "toy_project/README", 61);

	return suite.exit_status();
}

