// $Id: color.cc 1164 2010-08-13 20:21:40Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Colors.h"

#include <iostream>
#include <sstream>

int main(int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv);

	// The loop adds new authors to the author color map until all
	// colors are use. The color map contains 12 colors and black
	// (total 13). The loop is 14 iterations and therefore the first
	// color is reused for the last author. This test checks that
	// colors are reused. If the color map size changes, then the loop
	// must be adjusted accordingly
	unsigned char r_first, g_first, b_first;
	r_first = g_first = b_first = '\0';
	unsigned char r, g, b;
	for (unsigned int i=0; i<14; ++i) {
		std::stringstream ss;
		ss << i;
		Colors::instance().get_color(ss.str(), r, g, b);
		if (!i) r_first=r, g_first=g, b_first=b;  // remember first color
	}
	// Test first and last color, expected to be the same.
	if (r!=r_first || g!=g_first || b!=b_first) {
		suite.add(false);
		std::cerr << "Expected r,g,b "
							<< static_cast<int>(r_first) << ','
							<< static_cast<int>(g_first) << ','
							<< static_cast<int>(b_first) << std::endl;
		std::cerr << "     got r,g,b "
							<< static_cast<int>(r) << ','
							<< static_cast<int>(g) << ','
							<< static_cast<int>(b) << std::endl;
	}

	
	return suite.exit_status();
}
