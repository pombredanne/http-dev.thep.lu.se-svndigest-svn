// $Id: parser.cc 1164 2010-08-13 20:21:40Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/LineTypeParser.h"

#include <iostream>
#include <fstream>

bool test(const std::string&, std::ostream&);


int main(int argc, char* argv[])
{
	theplu::svndigest::test::Suite suite(argc, argv);
	bool ok=true;
	std::ofstream os("parser.tmp");
	test("parser.cc",os);
	test("Makefile.am",os);
	test("../INSTALL",os);
	return suite.exit_status();
}

bool test(const std::string& file, std::ostream& os)
{
	using namespace theplu::svndigest;
	LineTypeParser parser(file);
	std::ifstream is(file.c_str());
	for (size_t i=0; i<parser.type().size(); ++i){
		if (parser.type()[i]==LineTypeParser::other)
			os << "other:   ";
		else if (parser.type()[i]==LineTypeParser::comment)
			os << "comment: ";
		else
			os << "code:    ";
		std::string str;
		getline(is, str);
		os << str << "\n";
	}
	return true;
}
