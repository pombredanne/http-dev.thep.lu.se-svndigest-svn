#ifndef _theplu_svndigest_stats_collection_
#define _theplu_svndigest_stats_collection_

// $Id: StatsCollection.h 1081 2010-06-08 04:04:08Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Stats.h"

#include <map>
#include <string>

namespace theplu{
namespace svndigest{

  ///
  /// Class taking care of statistics from svn.
  ///
  class StatsCollection
  {
  public:
    /// 
    /// @brief Default Constructor 
		///
		explicit StatsCollection(const std::string& path);

		/**
			 \brief Destructor
		 */
		virtual ~StatsCollection(void);

		/**
			 \return true if cache was up to date
		 */
		bool load_cache(std::istream&);

		/**
			 Do the parsing for different statistics
		 */
		void parse(const std::string& path);

		/**
		 */
		void print(std::ostream&);

		/**
			 reset everything
		 */
		void reset(void);

		const std::map<std::string, Stats*>& stats(void) const;

		/**
		 */
		const StatsCollection& operator+=(const StatsCollection&);

		/**
			 \return const Stats reference associated to \a key

			 \throw if key does not exists
		 */
		const Stats& operator[](const std::string& key) const;

		/**
			 \return Stats reference associated to \a key
		 */
		Stats& operator[](const std::string& key);

  private:
		// copy not allowed
		StatsCollection(const StatsCollection&);
		StatsCollection& operator=(const StatsCollection&);

		const std::string path_;
		typedef std::map<std::string, Stats*> map;
		map stats_;
  };
}} // end of namespace svndigest end of namespace theplu

#endif 
