// $Id: rmdirhier.cc 1213 2010-10-09 14:47:10Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "rmdirhier.h"
#include "utility.h"

#include <cstdio>
#include <cstring>
#include <dirent.h>
#include <cerrno>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>


namespace theplu {
namespace svndigest {

	void rmdirhier(const std::string& path)
	{
		int fd=open(".",O_RDONLY);	// remember "original" cwd
		void rmdirhier__(const std::string&);

		try {
			rmdirhier__(path);
		}
		catch(const BadDirectory& x) {
			std::cerr << "svndigest: " << x.what() << "\n" 
								<< strerror(errno) << std::endl;
		}
		catch(const DirectoryOpenError& x) {
			std::cerr << "svndigest: cannot open directory " << x.what() << "\n" 
								<< strerror(errno) << "\n";
		}
		catch(const FileDeleteError& x) {
			std::cerr << "svndigest: cannot delete file " << x.what() << "\n"
								<< strerror(errno) << "\n";
		}
		catch(const DirectoryDeleteError& x) {
			std::cerr << "svndigest: cannot delete directory " << x.what() << "\n"
								<< strerror(errno) << "\n";
		}

		fchdir(fd);	// return to "original" cwd
	}


	void rmdirhier__(const std::string& dir)
	{
		struct stat buf;
		// using lstat because links should not be treated as dirs
		lstat(dir, &buf);
		// check if dir - if not delete the node
		if (!(buf.st_mode & S_IFDIR)) {
			// Make sure file is removable before removing it
			chmod(dir.c_str(),S_IWRITE);
			if (remove(dir.c_str()))
				throw FileDeleteError(concatenate_path(pwd(),dir));
			return;
		}
		if (::chdir(dir.c_str()))
      throw BadDirectory(concatenate_path(pwd(),dir));

   // Delete any remaining directory entries
		DIR* dp;
		struct dirent *entry;
		if (!(dp=opendir(".")))
			throw DirectoryOpenError(concatenate_path(pwd(),dir));
		while ((entry=readdir(dp)) != NULL) {
      if ((std::string(entry->d_name) == ".") ||
					(std::string(entry->d_name) == ".."))
				continue;
			rmdirhier__(entry->d_name);
		}
		closedir(dp);

		// Remove the directory from its parent
		chdir("..");
		if (remove(dir.c_str()))
      throw DirectoryDeleteError(concatenate_path(pwd(),dir));
	}

}} // of namespace svndigest and namespace theplu
