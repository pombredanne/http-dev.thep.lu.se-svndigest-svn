## $Id: version.m4 1496 2012-08-27 04:09:03Z peter $
#
# Copyright (C) 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.
#

#
# Set version numbers, see http://apr.apache.org/versioning.html  
#
# MAJOR - Modify when incompatible changes are made
m4_define([MAJOR_VERSION], [0])
# MINOR - Modify when new functionality is added
m4_define([MINOR_VERSION], [9])
# PATCH - Modify for every released patch
m4_define([PATCH_VERSION], [7])

# SVNDIGEST_DEV_BUILD - When rolling a tarball we set this to `false'. In
# repository value remains `true'.
m4_define([SVNDIGEST_DEV_BUILD], [true])


###
###  DO NOT EDIT BELOW THIS LINE
###

# Setting SVNDIGEST_VERSION based upon settings above
m4_define([SVNDIGEST_VERSION],MAJOR_VERSION.MINOR_VERSION) 
# disregarding patch number if zero
m4_define([SVNDIGEST_VERSION], m4_if(PATCH_VERSION,[0], SVNDIGEST_VERSION,
                                  SVNDIGEST_VERSION.PATCH_VERSION))
m4_define([SVNDIGEST_VERSION], m4_if(SVNDIGEST_DEV_BUILD,
															      [true], SVNDIGEST_VERSION[pre],
                                    SVNDIGEST_VERSION))

