// $Id: stats_test.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "AddStats.h"
#include "BlameStats.h"
#include "ClassicStats.h"
#include "Configuration.h"
#include "Stats.h"
#include "SVN.h"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <iostream>
#include <numeric>
#include <string>
#include <sstream>
#include <vector>

using namespace theplu::svndigest;

namespace theplu{
namespace svndigest{

	bool check(const Stats& stats, const std::vector<int>& correct,
						 int linetype, const std::string& descr, const std::string& author);
	std::string path(void);
	bool test_add(test::Suite&);
	bool test_blame(test::Suite&);
	bool test_classic(test::Suite&);
	bool test_base_class(const Stats&);
	bool test_cache(const Stats&);
	
}} // end of namespace svndigest and theplu


int main( int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);

	bool verbose=suite.verbose();
	bool ok=true;

	SVN* svn=SVN::instance("toy_project");
	if (!svn){
		std::cerr << "error: cannot create SVN instance\n";
		return 1;
	}

	ok &= test_add(suite);
	ok &= test_blame(suite);
	ok &= test_classic(suite);
                                                                                
	if (verbose) {
		if (ok)
			std::cout << "Test is ok.\n";
		else
			std::cout << "Test failed.\n";
	}
	if (ok)
		return 0;
  return 1;
}


namespace theplu{
namespace svndigest{

	std::string path(void)
	{
		return "toy_project/bin/svnstat.cc";
	}

	bool test_add(test::Suite& suite)
	{
		suite.out() << "testing add\n";
		bool ok =true;
		AddStats cs(path());
		cs.parse(path());
		ok &= test_base_class(cs);

		std::vector<int> correct(62,0);
		correct[15] = 71;
		correct[16] = 3;
		correct[17] = 7;
		correct[28] = 35;
		correct[30] = 63;
		ok &= check(cs, correct, LineTypeParser::total, "total", "jari");
		correct[42] = 1;
		correct[43] = 1;
		correct[44] = 2;
		correct[47] = 2;
		ok &= check(cs, correct, LineTypeParser::total, "total", "all");
		std::fill(correct.begin(), correct.end(), 0);
		correct[42] = 1;
		correct[43] = 1;
		correct[44] = 2;
		correct[47] = 2;
		ok &= check(cs, correct, LineTypeParser::copyright, "copyright", "peter");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 49;
		correct[16] = 3;
		correct[17] = 7;
		correct[28] = 11;
		correct[30] = 54;
		ok &= check(cs, correct, LineTypeParser::code, "code", "jari");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 5;
		correct[28] = 13;
		correct[30] = 7;
		ok &= check(cs, correct, LineTypeParser::comment, "comment", "jari");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 17;
		correct[28] = 10;
		correct[30] = 2;
		ok &= check(cs, correct, LineTypeParser::other, "other", "jari");

		return ok;
	}
	
	
	bool test_blame(test::Suite& suite)
	{
		suite.out() << "testing blame\n";
		bool ok =true;
		BlameStats cs(path());
		cs.parse(path());
		ok &= test_base_class(cs);

		std::vector<int> correct(62,0);
		correct[15] = 71;
		correct[16] = -1;
		correct[17] = 2;
		correct[28] = 31;
		correct[30] = 25;
		correct[42] = -1;
		ok &= check(cs, correct, LineTypeParser::total, "total", "jari");
		correct[42] = 0;
		correct[43] = 1;
		ok &= check(cs, correct, LineTypeParser::total, "total", "all");
		std::fill(correct.begin(), correct.end(), 0);
		correct[42] = 1;
		correct[43] = 1;
		ok &= check(cs, correct, LineTypeParser::copyright, "copyright", "peter");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 49;
		correct[17] = 2;
		correct[28] = 8;
		correct[30] = 54-29;
		ok &= check(cs, correct, LineTypeParser::code, "code", "jari");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 5;
		correct[16] = -1;
		correct[28] = 13;
		correct[30] = 7-3;
		ok &= check(cs, correct, LineTypeParser::comment, "comment", "jari");
		std::fill(correct.begin(), correct.end(), 0);
		correct[15] = 17;
		correct[28] = 10-1;
		correct[30] = 2-6;
		ok &= check(cs, correct, LineTypeParser::other, "other", "jari");

		return ok;
	}

	
	bool test_cache(const Stats& stats)
	{
		std::stringstream out;
		stats.print(out);
		std::stringstream in(out.str());
		ClassicStats stats2(path());
		assert(in.good());
		if (!stats2.load_cache(in)){
			std::cout << "load_cache() failed\n";
			return false;
		}

		std::stringstream out2;
		stats2.print(out2);
		
		if (out.str()!=out2.str()) {
			std::cout << "test_cache() failed\n";
			return false;
		}
		return true;
	}


	bool test_classic(test::Suite& suite)
	{
		suite.out() << "testing classic\n";
		bool ok =true;
		ClassicStats cs(path());
		cs.parse(path());
		ok &= test_base_class(cs);

		// testing copyright lines for peter
		std::vector<int> correct(48,0);
		correct[47]=1;
		ok &= check(cs, correct, LineTypeParser::copyright, "copyright", "peter");

		// testing code lines for jari
		correct.resize(0);
		correct.resize(48,0);
		correct[15]=20;
		correct[16]=1;
		correct[17]=1;
		correct[28]=8;
		correct[30]=54;
		ok &= check(cs, correct, LineTypeParser::code, "code", "jari");

		// testing comment lines for jari
		correct.resize(0);
		correct.resize(48,0);
		correct[15]=1;
		correct[28]=13;
		correct[30]=7;
		ok &= check(cs, correct, LineTypeParser::comment, "comment", "jari");

		// testing blank lines for jari
		correct.resize(0);
		correct.resize(48,0);
		correct[15]=10;
		correct[28]=10;
		correct[30]=2;
		ok &= check(cs, correct, LineTypeParser::other, "other", "jari");

		// testing code all lines for total
		correct.resize(0);
		correct.resize(48,0);
		correct[15]=31;
		correct[16]=1;
		correct[17]=1;
		correct[28]=31;
		correct[30]=63;
		correct[47]=1;
		ok &= check(cs, correct, LineTypeParser::total, "total", "all");

		return ok;
	}
	
	bool check(const Stats& stats, const std::vector<int>& correct,
						 int linetype, const std::string& descr, const std::string& author)
	{
		bool ok=true;
		std::vector<size_t> sum(correct.size());
		std::partial_sum(correct.begin(), correct.end(), sum.begin());
		for (size_t rev=0; rev<sum.size(); ++rev){
			size_t n = stats(linetype, author, rev);
			if (n != sum[rev]){
				std::cout << "error: " << descr << " " << author << " rev:" << rev 
									<< ": found " << n << " expected " << sum[rev] << "\n";
				ok = false;
			}
		}
		return ok;
	}

	bool test_base_class(const Stats& s)
	{
		if (!test_cache(s))
			return false;
		if (s.code()+s.comments()+s.empty()!=s.lines()){
			std::cerr << "Code plus comments plus empty do not add up to lines\n";
			std::cerr << "code: " << s.code() << "\n";
			std::cerr << "comment: " << s.comments() << "\n";
			std::cerr << "empty: " << s.empty() << "\n";
			std::cerr << "lines: " << s.lines() << "\n";
			return false;
		}
		return true;
	}

}} // end of namespace svndigest and theplu
