#!/bin/sh
# $Id: config2_test.sh 1100 2010-06-13 17:28:19Z peter $
#
# Copyright (C) 2009, 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# validate that our own config file is valid

# svndigest's config file is not shipped
required=repo

. ./init.sh || exit 1
set -e

SVNDIGEST_run 0 -g --config-file $abs_top_srcdir/.svndigest/config || exit_fail

cat > tmp_config <<EOF
#
[image]
format = apple
EOF

SVNDIGEST_run 1 -g --config-file tmp_config
grep 'unknown format.* apple' stderr || exit_fail

exit_success
