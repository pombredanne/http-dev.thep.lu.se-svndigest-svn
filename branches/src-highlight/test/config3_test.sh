#!/bin/sh
# $Id: config3_test.sh 1100 2010-06-13 17:28:19Z peter $
#
# Copyright (C) 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# test that we can use /dev/null as config file

. ./init.sh || exit 1
set -e

test -e /dev/null || exit_skip
test -r /dev/null || exit_skip
SVNDIGEST_run 0 -g --config-file /dev/null
SVNDIGEST_run 1 -g --config-file .
grep 'is a directory' stderr || exit_fail

exit_success
