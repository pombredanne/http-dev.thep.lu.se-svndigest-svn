// $Id: htmlstream_test.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "HtmlStream.h"

#include <sstream>

using namespace theplu::svndigest;

void test_stream(std::string, std::string, test::Suite&);

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv);

	test_stream("\n", "<br />", suite);
	test_stream("<", "&lt;", suite);
	test_stream(">", "&gt;", suite);
	test_stream("&", "&amp;", suite);

	if (suite.ok())
		return 0;
  return 1;
}

void test_stream(std::string in, std::string correct, test::Suite& suite)
{
	std::ostringstream out;
	HtmlStream hs(out);
	hs << in;
	if (out.str() != correct) {
		suite.out() << in << " -> " << out.str() << "\n";
		suite.out() << "error: expected `" << correct << "'\n";
		suite.add(false);
	}
}
