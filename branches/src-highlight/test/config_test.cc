// $Id: config_test.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "Configuration.h"

namespace theplu{
namespace svndigest{
	void test_codon(test::Suite&);
	void test_read_write(test::Suite&);
}} // end of namespace svndigest and theplu


int main( int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv, false);

	test_codon(suite);
	test_read_write(suite);
                                                                                
	if (suite.ok()) {
		suite.out() << "Test is Ok!" << std::endl;
		return 0;
	}
	suite.out() << "Test failed." << std::endl;
	return 1;
}


namespace theplu{
namespace svndigest{

	void test_codon(test::Suite& suite)
	{
		const Configuration& c(Configuration::instance());
		if (!c.codon("foo.h")){
			suite.out() << "No codon for foo.h\n";
			suite.add(false);
		}
		if (!c.codon("/dir/test.cc")){
			suite.out() << "No codon for test.cc\n";
			suite.add(false);
		}
		if (!c.codon("bootstrap")){
			suite.out() << "No codon for bootstrap\n";
			suite.add(false);
		}
	}
	
	void test_read_write(test::Suite& suite)
	{
		Configuration& config = Configuration::instance();
		std::stringstream ss;
		ss << config;
		config.load(ss);
		std::stringstream ss2;
		ss2 << config;
		if (ss2.str() != ss.str()) {
			suite.out() << "error: configuration output not equal\n";
			suite.out() << "defalt output:\n" << ss.str() << "\n";
			suite.out() << "second output:\n" << ss2.str() << "\n";
			suite.add(false);
		}
	}

}} // end of namespace svndigest and theplu
