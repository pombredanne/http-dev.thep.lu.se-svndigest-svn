// $Id: svncopyright.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "svncopyrightParameter.h"

#include "Configuration.h"
#include "Directory.h"
#include "main_utility.h"

#include <cassert>
#include <iostream>
#include <string>

int main( int argc, char* argv[])
{
	using namespace theplu;
	using namespace svndigest;

	// Reading commandline options
	svncopyrightParameter option;
	try {
		option.parse(argc, argv);
		if (option.verbose())
			std::cout << "Done parsing parameters" << std::endl;
	}
	catch (yat::utility::cmd_error& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch (std::runtime_error& e) {
		std::cerr << "svncopyright: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	try {
		load_config(option.config_file());
		if (option.generate_config()) {
			std::cout << Configuration::instance();
			return EXIT_SUCCESS;
		}

		if (option.verbose())
			std::cout << "Initializing SVN singleton." << std::endl;
		SVN::instance(option.root());

		// build directory tree already here ... see comment in svndigest.cc
		if (option.verbose())
			std::cout << "Building directory tree" << std::endl;
		Directory tree(0,option.root(),"");

		if (option.verbose())
			std::cout << "Parsing directory tree" << std::endl;
		tree.parse(option.verbose(), option.ignore_cache());
		update_copyright(tree, option.verbose());
	}
	catch (std::runtime_error& e) {
		std::cerr << "svncopyright: " << e.what() << "\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;				// normal exit
}
