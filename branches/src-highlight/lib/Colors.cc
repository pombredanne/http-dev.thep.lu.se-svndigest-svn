// $Id: Colors.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Colors.h"

#include "Configuration.h"

#include <cstdlib>
#include <ctype.h>

namespace theplu {
namespace svndigest {

	Colors* Colors::instance_=NULL;


	Colors::Colors(void)
		: color_map_(13)
	{
		color_map_[ 0].label="green";
		color_map_[ 0].str="009248";
		color_map_[ 1].label="blue";
		color_map_[ 1].str="1b75ba";
		color_map_[ 2].label="red";
		color_map_[ 2].str="bd202e";
		color_map_[ 3].label="grey";
		color_map_[ 3].str="838383";
		color_map_[ 4].label="orange";
		color_map_[ 4].str="f59120";
		color_map_[ 5].label="brown";
		color_map_[ 5].str="754d29";
		color_map_[ 6].label="cyan";
		color_map_[ 6].str="28aae1";
		color_map_[ 7].label="lime";
		color_map_[ 7].str="d5dd26";
		color_map_[ 8].label="purple";
		color_map_[ 8].str="8f288c";
		color_map_[ 9].label="lgrey";
		color_map_[ 9].str="dcdcdc";
		color_map_[10].label="lbrown";
		color_map_[10].str="c2976a";
		color_map_[11].label="pink";
		color_map_[11].str="e67de6";
		color_map_[12].label="black";
		color_map_[12].str="000000";
		// calculate corresponding rgb values
		for (std::vector<color>::iterator col = color_map_.begin(); 
				 col!=color_map_.end(); ++col) {
			str2rgb(col->str, col->r, col->g, col->b);
		}

		typedef std::map<std::string, std::string> ACmap;
		const ACmap& authcols=Configuration::instance().author_colors();
		// reserve sufficient size in vector here to avoid reallocation
		// and iterators being invalidated
		color_map_.reserve(color_map_.size()+authcols.size());
		next_color_=color_map_.begin();
		for (ACmap::const_iterator i=authcols.begin(); i!=authcols.end(); i++) {
			color c;
			c.label=i->first;
			str2rgb(i->second, c.r, c.g, c.b);
			c.str = i->second;
			next_color_=color_map_.insert(next_color_,c);
			author_map_.insert(std::make_pair(c.label, next_color_++));
		}
	}


	const std::string& Colors::color_str(const std::string& label)
	{
		return get_color(label).str;
	}


	const Colors::color& Colors::get_color(const std::string& label)
	{
		std::map<std::string,std::vector<color>::iterator>::iterator i;
		i = author_map_.lower_bound(label);
		if (i==author_map_.end() || i->first != label) {
			// no color defined for label, set color for label
			i = author_map_.insert(i, std::make_pair(label, next_color_++));
			if (next_color_==color_map_.end())
				// end of color map reach, start over
				next_color_=color_map_.begin();
		}
		return *(i->second);
	}


	void Colors::get_color(const std::string& label, unsigned char& r,
													 unsigned char& g, unsigned char& b)
	{
		const color& col(get_color(label));
		r = col.r;
		g = col.g;
		b = col.b;
	}


	Colors& Colors::instance(void)
	{
		if (!instance_)
			instance_ = new Colors;
		return *instance_;
	}


	int to_decimal(int hex)
	{
		hex=toupper(hex);
		if (hex>47 && hex<58) // 0--9
			return hex-48;
		if (hex>64 && hex<71) // A--F
			return hex-55;
		std::ostringstream ss;
		ss << static_cast<char>(hex) << " is not hexadecimal";
		throw std::runtime_error(ss.str());
		return -1;
	}

	void str2rgb(const std::string& str, unsigned char& r, unsigned char& g,
							 unsigned char& b)
	{
		if (str.size()==6) {
			r = 16*to_decimal(str[0]) + to_decimal(str[1]);
			g = 16*to_decimal(str[2]) + to_decimal(str[3]);
			b = 16*to_decimal(str[4]) + to_decimal(str[5]);
		}
		else if (str.size()==3) {
			r = 17*to_decimal(str[0]);
			g = 17*to_decimal(str[1]);
			b = 17*to_decimal(str[2]);
		}
		else 
			throw std::runtime_error("invalid color format: " + str);
	}


}} // end of namespace svndigest and namespace theplu
