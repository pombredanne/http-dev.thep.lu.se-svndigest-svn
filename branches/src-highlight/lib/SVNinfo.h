#ifndef _theplu_svndigest_svninfo_
#define _theplu_svndigest_svninfo_

// $Id: SVNinfo.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

#include <subversion-1/svn_client.h>

namespace theplu {
namespace svndigest {

	class SVN;

	///
	/// The SVNinfo class is a utility class for taking care of 'svn
	/// info'.
	///
	class SVNinfo {
	public:

		///
		/// Retrieve meta information about the item with \a path.
		/// 
		/// @note The recursivness of the underlying subversion API is not
		/// allowed (nor supported).
		///
		explicit SVNinfo(const std::string& path);

		///
		/// @brief Get the repository root URL.
		///
		inline const std::string& repos_root_url(void) const
		{ return info_receiver_baton_.repos_root_url_; }

		///
		/// @brief Get the URL.
		///
		inline std::string url(void) const
		{ return info_receiver_baton_.url_; }

		///
		/// @brief Get the author of the latest commit.
		///
		inline const std::string& last_changed_author(void) const
		{ return info_receiver_baton_.last_changed_author_; }

		///
		/// @brief Get the revision of the latest commit.
		///
		inline svn_revnum_t last_changed_rev(void) const
		{ return info_receiver_baton_.last_changed_rev_; }

		///
		/// @brief Get the current revision of the item.
		///
		inline svn_revnum_t rev(void) const { return info_receiver_baton_.rev_; }


	private:

		///
		/// @brief Copy Constructor, not implemented.
		///
		SVNinfo(const SVNinfo&);

		///
		/// svn info is stored in the info_receiver_baton_. The
		/// information is retrieved with the info_* set of member
		/// functions. The struct is filled in the info_receiver function.
		///
		/// @see info_receiver
		///
		struct info_receiver_baton {
			// more info is available but we only use these
			std::string url_;
			std::string repos_root_url_;
			std::string last_changed_author_;
			svn_revnum_t last_changed_rev_;
			svn_revnum_t rev_;
		} info_receiver_baton_ ;

		///
		/// info_receiver is the function passed to the underlying
		/// subversion API. This function is called by the subversion API
		/// for every item matched by the conditions of the API call.
		///
		/// @see Subversion API documentation
		///
		static svn_error_t * info_receiver(void *baton, const char *path,
																			 const svn_info_t *info, apr_pool_t *pool);
	};

}} // end of namespace svndigest and namespace theplu

#endif
