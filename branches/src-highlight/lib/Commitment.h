#ifndef _theplu_svndigest_commitment_
#define _theplu_svndigest_commitment_

// $Id: Commitment.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>

#include <subversion-1/svn_types.h>

namespace theplu {
namespace svndigest {

	/**
		 The Commitment is an aggregrate containing the information from
		 one commitment in the log.

		 \see SVNlog
	*/
	class Commitment {
	public:

		/**
			 \brief Default contructor.
		*/
		Commitment(void);

		/**
			 \brief The contructor.
		*/
		Commitment(std::string author, std::string date, std::string msg, 
							 svn_revnum_t rev);

		/**
			 \return Author
		*/
		inline const std::string& author(void) const { return author_; }

		/**
			 \return Date
		*/
		inline const std::string& date(void) const { return date_; }

		/**
			 \return Message
		*/
		inline const std::string& message(void) const { return msg_; }

		/**
			 \return Revision
		*/
		inline svn_revnum_t revision(void) const { return rev_; }

	private:
		// Using compiler-generated Copy Constructor.
		// Commitment(const Commitment&);
		//
		// Using compiler-generated Copy assignment.
		// Commitment& operator=(const Commitment&);

		std::string author_;
		std::string date_;
		std::string msg_;
		svn_revnum_t rev_;

	};

	
	struct GreaterRevision
	{
		inline bool operator()(const Commitment& lhs, const Commitment& rhs)
		{ return lhs.revision()>rhs.revision(); }
	};

	struct LessRevision
	{
		inline bool operator()(const Commitment& lhs, const Commitment& rhs)
		{ return lhs.revision()<rhs.revision(); }
	};

}} // end of namespace svndigest and namespace theplu

#endif
