// $Id: SVNproperty.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNproperty.h"
#include "SVN.h"

#include <string>

namespace theplu {
namespace svndigest {


	SVNproperty::SVNproperty(const std::string& path)
		: binary_(false), svndigest_ignore_(false)
  {
		SVN::instance()->client_proplist(path, property_);
		std::map<std::string, std::string>::const_iterator i = property_.begin();
		std::map<std::string, std::string>::const_iterator e = property_.end();
		for ( ; i!=e ; ++i)
			if (i->first == "svndigest:ignore")
				svndigest_ignore_=true;
			else
				if ((i->first == "svn:mime-type") &&
						(svn_mime_type_is_binary(i->second.c_str())))
					binary_=true;
  }


}} // end of namespace svndigest and namespace theplu
