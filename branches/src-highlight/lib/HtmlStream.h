#ifndef _theplu_svndigest_html_stream_
#define _theplu_svndigest_html_stream_

// $Id: HtmlStream.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "HtmlBuf.h"

#include <map>
#include <fstream>
#include <sstream>

namespace theplu{
namespace svndigest{
	///
	/// \brief Convenient class for using HtmlBuf. 
	///
	/// Class is created from another ostream, whose buffer is used to
	/// create an internal HtmlBuf. In other words, when this class is
	/// used, typical with operator<<, data is sent to the HtmlBuffer,
	/// which transforms it as appropriate and sends it further to its
	/// target buffer. The target buffer is the buffer of the ostream
	/// this class is created of.
	///
	class HtmlStream : public std::ostream
	{
	public:
		///
		/// Creates a HtmlBuf from target.rdbuf().
		///
		HtmlStream(std::ostream& target);

		///
		/// @brief Destructor
		///
		~HtmlStream(void);

		///
		/// \return reference to underlying ostream
		///
		std::ostream& stream(void);

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		HtmlStream(const HtmlStream&);

		HtmlBuf hbuf_;
		std::ostream& os_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
