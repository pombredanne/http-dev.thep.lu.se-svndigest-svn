#ifndef _theplu_svndigest_colors_
#define _theplu_svndigest_colors_

// $Id: Colors.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <string>
#include <vector>

namespace theplu {
namespace svndigest {

	/**
		 The author-color mapping is provided by the Colors class.

		 The only way of setting specific colors for authors is through
		 the configuration file.

		 Colors use the singleton design pattern.
	 */
	class Colors
	{
	public:

		/**
			 Same as get_color(4) but color is returned as a hexadecimal
			 string rather than in rgb.

			 \return color string associated with label
		 */
		const std::string& color_str(const std::string& label);

		/**
			 \brief Get RGB color for \a label

			 If \a label has no assigned color, the next unused color is
			 automatically assigned to \a label. The number of available
			 unique colors is low, when all colors are used mapped with a
			 \a label once, the colors are reused.
		 */
		void get_color(const std::string& label, unsigned char& r,
										unsigned char& g, unsigned char& b);

		/**
			 \brief Get an instance of Color
		 */
		static Colors& instance(void);

	private:

		Colors(void);

		// Copy constructor not implemented
		Colors(const Colors&);

		// Assignment not implemented because assignment is always self-assignment
		Colors& operator=(const Colors&);

		// Destructor not implemented
		~Colors(void);

		struct color {
			std::string label;
			unsigned char r,g,b;
			std::string str;
		};

		const color& get_color(const std::string& label);

		std::map<std::string, std::vector<color>::iterator> author_map_;
		std::vector<color> color_map_;
		static Colors* instance_;
		std::vector<color>::iterator next_color_;
	};

	/**
		 Convert a string to color rgb values. The input str must be
		 either of length 3 or 6 and each character should be a
		 hexadecimal.
	 */
	void str2rgb(const std::string& str, unsigned char& r, unsigned char& g,
							 unsigned char& b);

	// Convert char from hexadecimal to decimal, if char is out of
	// range -1 is returned.
	int to_decimal(int);

}} // end of namespace svndigest and namespace theplu

#endif
