#ifndef _theplu_svndigest_directory_
#define _theplu_svndigest_directory_

// $Id: Directory.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

#include <map>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	///
	/// Class taking care of directories.
	///
	class Directory : public Node
	{
	public:

		///
		/// @brief Constructor
		///
		/// Recursively create a directory tree starting from \a path. All
		/// entries except explicit directories are treated as File nodes,
		/// i.e. symbolic links to directories are treated as File
		/// nodes. This will ensure that the directory structure is a tree
		/// and double counting of branches is avoided.
		///
		/// @note Nodes named '.', '..', and '.svn' are ignored and not
		/// traversed.
		///
		Directory(const unsigned int level, const std::string& path, 
							const std::string& output="");

		///
		/// @brief Destructor
		///
		~Directory(void);

		///
		/// @return true
		///
		bool dir(void) const;

		///
		/// @return directory-name/index.html
		///
		std::string href(void) const;

		/**
			 \brief Get the revision number of the latest commit.

			 Does not only check this directory but also daughter nodes.
		*/
		svn_revnum_t last_changed_rev(void) const;

		/**
			 @return The explicit string "directory", nothing else.
		*/
		std::string node_type(void) const;

		/**
			 @return output path for example 'lib/File.h.html' for this file
		 */
		std::string output_path(void) const;

		const StatsCollection& parse(bool verbose, bool ignore);

		using Node::print_copyright;
		/**
			 virtual function typically called from Node::print_copyright
		*/
		void print_copyright(std::map<std::string, Alias>&, bool verbose,
												 const std::map<int, svn_revnum_t>&) const;

	private:
		/**
			 add union of logs from daughter nodes.
		*/
		void log_core(SVNlog&) const;

		///
		/// @brief Copy Constructor, not implemented
		///
		Directory(const Directory&);

		void print_core(bool verbose=false) const;

		void print_core(const std::string& stats_type, const std::string& user, 
										const std::string& line_type, const SVNlog&) const;


		typedef std::vector<Node*> NodeContainer;
		typedef NodeContainer::iterator NodeIterator;
		typedef NodeContainer::const_iterator NodeConstIterator;
		NodeContainer daughters_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
