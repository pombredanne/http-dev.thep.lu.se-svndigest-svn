#ifndef _theplu_svndigest_functor_
#define _theplu_svndigest_functor_

// $Id: Functor.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <functional>
#include <string>
#include <utility>
#include <vector>

#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	struct AlNum
	{
		inline bool operator()(std::string::const_iterator i) const 
		{ return isalnum(*i); }
	};


	struct Digit
	{
		inline bool operator()(std::string::const_iterator i) const 
		{ return isdigit(*i); }
	};


	class notChar
	{
	public:
		notChar(char);
		inline bool operator()(std::string::const_iterator i) const 
		{ return *i!=char_; }
	private:
		char char_;
	};


	class not2Char
	{
	public:
		not2Char(char, char);
		inline bool operator()(std::string::const_iterator i) const 
		{ return *i!=char1_ && *i!=char2_; }
	private:
		const char char1_;
		const char char2_;
	};

	class not2Str
	{
	public:
		not2Str(std::string, std::string);
		inline bool operator()(std::string::const_iterator i) const 
		{ 
			return !(std::equal(str1_.begin(), str1_.end(), i) ||
							std::equal(str2_.begin(), str2_.end(), i));
		} 

	private:
		const std::string str1_;
		const std::string str2_;
	};

	///
	/// Functor to be used on contaioners and works as standard less,
	/// but on the reversed container.
	///
	/// Requirements on T is that has rend and rbegin. T::value_type
	/// must be comparable (i.e. have operator<)
	/// 
	template <typename T>
	struct LessReversed
	{
		///
		/// using std::lexicographical_compare on the reversed container
		///
		inline bool operator()(const T& x, const T& y) const
		{ return std::lexicographical_compare(x.rbegin(),x.rend(),
																					y.rbegin(),y.rend()); }
	};


	///
	/// @brief Functor comparing pairs using second.
	///
	/// STL provides operator< for the pair.first element, but none for
	/// pair.second. This template provides this and can be used as the
	/// comparison object in generic functions such as the STL sort.
	///
	template <typename T1, typename T2>
	struct pair_value_compare
	{
		///
		/// @return true if x.second<y.second or (x.second==y.second and
		/// x.first<y.first)
		///
		inline bool operator()(const std::pair<T1,T2>& x,
													 const std::pair<T1,T2>& y) {
			return ((x.second<y.second) ||
							(!(y.second<x.second) && (x.first<y.first))); 
		}
	};

	
	///
	/// Functor working on pair.second, using a user passed functor.
	///
	template <typename T1, typename T2, typename T3>
	struct PairSecondCompare
	{

		///
		/// @brief Constructor
		///
		explicit PairSecondCompare(const T3& comp)
			: compare_(comp) {}

		///
		/// @return compare(x.second, y.second) where compare is a
		/// internal comparison functor.
		///
		inline bool operator()(const std::pair<T1,T2>& x,
													 const std::pair<T1,T2>& y) const
		{ return compare_(x.second,y.second); }

	private:
		T3 compare_;

	};
	
	///
	/// Calculating sum of two vectors.
	///
	/// @return resulting vector
	///
	template <typename T >
	struct VectorPlus : 
		public std::binary_function<std::vector<T>,std::vector<T>,std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& u,
															const std::vector<T>& v) const 
		{
			std::vector<T> res;
			res.reserve(std::max(u.size(), v.size()));
			std::back_insert_iterator<std::vector<T> > inserter(res);
			if ( u.size() > v.size() ){
				std::transform(v.begin(), v.end(), u.begin(), inserter, std::plus<T>());
				std::copy(u.begin()+v.size(), u.end(), inserter);
			}
			else {
				std::transform(u.begin(), u.end(), v.begin(), inserter, std::plus<T>());
				std::copy(v.begin()+u.size(), v.end(), inserter);
			}
			return res;
		}

	};

	///
	/// @return resulting vector
	///
	template <typename Key, typename T>
	struct PairValuePlus :
		public std::binary_function<std::vector<T>,
																std::pair<const Key, std::vector<T> >, 
																std::vector<T> >
	{
		std::vector<T> operator()(const std::vector<T>& sum, 
															const std::pair<const Key,std::vector<T> >& p)
		{
			return VectorPlus<T>()(sum, p.second);
		}
	};

}} // end of namespace svndigest end of namespace theplu

#endif 
