#ifndef _theplu_svndigest_alias_
#define _theplu_svndigest_alias_

// $Id: Alias.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

#include <string>

namespace theplu{
namespace svndigest{

	/// 
	/// @brief Class holding a copyright alias
	/// 
	class Alias
	{
	public:
		///
		/// @brief default constructor
		///
		Alias(void); // to allow stl::container

		///
		/// \brief Constructor
		///
		Alias(std::string alias, size_t id);
		
		///
		/// @return name
		///
		std::string name(void) const;

		///
		/// @return id
		///
		size_t id(void) const;

	private:
		//Alias& Alias(const Alias&) using compiler generated copy ctor
		//Alias& operator=(const Alias&) using compiler generated operator
		
		std::string name_;
		size_t id_;
	};

	bool operator<(const Alias& lhs, const Alias& rhs);
	bool operator==(const Alias& lhs, const Alias& rhs);


	///
	/// @brief Functor to compare using Id
	/// 
	struct IdCompare
	{
		inline bool operator()(const Alias& lhs, const Alias& rhs) const
		{ return lhs.id()<rhs.id(); }
	};


}} // end of namespace svndigest and namespace theplu

#endif


