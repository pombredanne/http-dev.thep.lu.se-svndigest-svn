// $Id: CommandLine.cc 2265 2010-06-05 23:12:10Z peter $

/*
	Copyright (C) 2007 Jari Häkkinen, Peter Johansson, Markus Ringnér
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson
	Copyright (C) 2010 Jari Häkkinen, Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CommandLine.h"

#include "ColumnStream.h"
#include "Exception.h"
#include "Option.h"
#include "OptionSwitch.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <fstream>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	CommandLine::CommandLine(std::string str)
		: description_(str), free_arg_max_(0), parsed_(false)
	{}


	CommandLine::~CommandLine(void)
	{
	}


	void CommandLine::add(Option& option)
	{
		if (option.long_name().size()) {
			if (long_options_.find(option.long_name())!=long_options_.end()) {
				std::stringstream ss;
				ss << "yat::utility::Commandline: two options with long_name: "
					 << option.long_name();
				throw runtime_error(ss.str());
			}
			long_options_[option.long_name()] = &option;
		}
		if (option.short_name()) {
			if (short_options_.find(option.short_name())!=short_options_.end()) {
				std::stringstream ss;
				ss << "yat::utility::Commandline: two options with short_name: "
					 << option.short_name();
				throw runtime_error(ss.str());
			}
			short_options_[option.short_name()] = &option;
		}
		if (option.long_name().size() || option.short_name())
			options_.push_back(&option);
	}


	void CommandLine::allow_free_args(size_t n)
	{
		free_arg_max_ = n;
	}


	const std::vector<std::string>& CommandLine::free_args(void) const
	{
		return free_arg_;
	}


	bool CommandLine::is_long_option(std::string str) const
	{
		return (str.size()>2 && str[0]=='-' && str[1]=='-');
	}


	bool CommandLine::is_short_option(std::string str) const
	{
		return (str.size()>=2 && str[0]=='-' && str[1]!='-');
	}


	void CommandLine::parse(int argc, char* argv[])
	{		
		parsed_=true;
		using namespace std;
		// just in case it is not pristine
		for_each(options_.begin(), options_.end(),std::mem_fun(&Option::reset)); 

		std::vector<std::string> arguments;
		arguments.reserve(argc);
		for (int i=0; i<argc; ++i)
			arguments.push_back(argv[i]);
		std::vector<std::string>::iterator arg(arguments.begin());		
		stringstream ss(*arg++);
		// keeping string after last /
		while (getline(ss, program_name_,'/')) {}

		try {
			for (; arg!=arguments.end(); ++arg) {
				if (is_long_option(*arg)) {
					std::string key(arg->substr(2));
					std::stringstream ss2(key);
					getline(ss2, key, '=');
					std::string value;
					getline(ss2, value, '\0');
					if (!value.empty()){
						*arg = value;
						*(--arg) = std::string("--")+key;
					}					
					else
						*arg = key;
					std::map<std::string, Option*>::const_iterator
						iter(long_options_.find(key));
					if (iter!=long_options_.end())
						iter->second->parse(arg, arguments.end());
					else if (key.size()>3 && key.substr(0,3)=="no-") { 
						iter = long_options_.find(key.substr(3));
						if (iter!=long_options_.end())
							iter->second->parse(arg, arguments.end());
					}						
					else if (iter==long_options_.end()) {
						std::stringstream ss3;
						ss3 << ": unrecognized option `" << key << "'\n" << try_help();
						throw cmd_error(ss3.str());
					}
				}
				else if (is_short_option(*arg)) {
					size_t size=arg->size();
					for (size_t i=1; i<size; ++i){
						std::map<char, Option*>::const_iterator
							iter(short_options_.find((*arg)[i]));
						if (iter==short_options_.end()) {
							std::stringstream ss2;
							ss2 << ": invalid option -- " << (*arg)[i] << "\n"
									<< try_help() << "\n";
							throw cmd_error(ss2.str());
						}				
						else 
							iter->second->parse(arg, arguments.end());
					}
				}
				else {
					free_arg_.push_back(*arg);
					if (free_arg_.size()>free_arg_max_) {
						std::stringstream ss2;
						ss2 << ": invalid option -- " << *arg << "\n"
								<< try_help() << "\n";
						throw cmd_error(ss2.str());
					}
				}
			}
			for_each(options_.begin(),options_.end(),
							 std::mem_fun(&Option::validate)); 
		}
		catch (cmd_error& e){
			std::stringstream ss2;
			ss2 << program_name_ << ": " << e.what();
			throw cmd_error(ss2.str());
		}
			
	}


	bool CommandLine::parsed(void) const
	{
		return parsed_;
	}


	std::string CommandLine::program_name(void) const
	{
		return program_name_;
	}


	void CommandLine::sort(void)
	{
		sort(OptionCompare());
	}


	std::vector<std::string> CommandLine::split(std::string str, char del) const
	{
		std::vector<std::string> vec;
		std::stringstream ss(str);
		while (std::getline(ss, str, del)){
			vec.push_back(str);
		}
		return vec;
	}

	std::string CommandLine::try_help(void) const
	{
		return std::string("Try `"+program_name()+" --help' for more information.");
	}


	std::ostream& operator<<(std::ostream& os, const CommandLine& cmd)
	{
		os << cmd.description_ << "\n";
		ColumnStream cs2(os, 2);
		std::string::size_type width = 0;
		for (std::vector<Option*>::const_iterator i(cmd.options_.begin()); 
				 i!=cmd.options_.end();++i) {
			std::stringstream ss((*i)->print());
			std::string str;
			getline(ss, str, '\t');
			width = std::max(width, str.size()+3);				
		}
		cs2.width(0)=width;
		cs2.width(1)=76-width;
		cs2.margin(0)=2;

		for (std::vector<Option*>::const_iterator i(cmd.options_.begin()); 
				 i!=cmd.options_.end();++i) 
			cs2 << (*i)->print() << "\n";

		return os;
	}


	bool CommandLine::OptionCompare::operator()(const Option* lhs, 
																							const Option* rhs) const
	{
		assert(lhs);
		assert(rhs);
		std::string lhs_str = lhs->long_name();
		if (lhs_str.empty())
			lhs_str = lhs->short_name();
		std::string rhs_str = rhs->long_name();
		if (rhs_str.empty())
			rhs_str = rhs->short_name();
		return lhs_str < rhs_str;
	}


}}} // of namespace utility, yat, and theplu
