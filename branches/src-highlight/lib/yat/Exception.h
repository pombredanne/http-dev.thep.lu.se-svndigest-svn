#ifndef _theplu_yat_utility_exception_
#define _theplu_yat_utility_exception_

// $Id: Exception.h 2210 2010-03-05 22:59:01Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdexcept>
#include <string>

namespace theplu {
namespace yat {
namespace utility {

	

	/**
		 \brief Class used for all runtime error detected within yat library.
	 */
	class runtime_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor

			 \param message message to be displayed using function what().
		 */
		runtime_error(std::string message);
	};


	/**
		 \brief Class used for error reported from Commandline or Option.
	 */
	class cmd_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor

			 \param message message to be displayed using function what().
		 */
		cmd_error(std::string message);
	};


	/**
		 \brief Class that contains information reported bia global
		 variable errno.
	 */
	class errno_error : public std::runtime_error
	{
	public:
		/**
			 The error message, return from what(), is set to
			 \a message + strerror(errno)
		 */
		errno_error(std::string message);
	};

	/**
		 \brief Class for errors reported from underlying GSL calls.

		 GSL_error is used in the same way as C++ standard library
		 exceptions.
	*/
	class GSL_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor to create an exception with a message.
		*/
		GSL_error(std::string message);

		/**
			 \brief Constructor to create an exception with a message
			 containg the GSL error description.
		*/
		GSL_error(std::string message, int gsl_status);
	};


	/**
		 \brief Class to report errors associated with IO operations.

		 IO_error is used in the same way as C++ standard library
		 exceptions.
	*/
	class IO_error : public std::runtime_error
	{
	public:
		/**
			 \brief Constructor to create an exception with a message.
		*/
		IO_error(std::string message);
	};

}}} // of namespace utility, yat, and theplu

#endif
