// $Id: Exception.cc 2210 2010-03-05 22:59:01Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Exception.h"

#include <cerrno>
#include <stdexcept>
#include <string>

namespace theplu {
namespace yat {
namespace utility {

	runtime_error::runtime_error(std::string message)
		: std::runtime_error(message) 
	{}


	cmd_error::cmd_error(std::string message)
		: std::runtime_error(message) 
	{}


	errno_error::errno_error(std::string message)
		: std::runtime_error(message + strerror(errno)) 
	{}


	IO_error::IO_error(std::string message)
		: std::runtime_error("IO_error: " + message)
	{}

}}} // of namespace utility, yat, and theplu
