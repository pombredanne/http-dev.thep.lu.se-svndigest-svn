#ifndef _theplu_yat_utility_utility_
#define _theplu_yat_utility_utility_ 

// $Id: utility.h 2248 2010-04-22 00:57:13Z peter $

/*
	Copyright (C) 2005 Jari Häkkinen, Peter Johansson, Markus Ringnér
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

///
/// \file yat/utility/utility.h
///
/// @brief Some useful functions are placed here
///

#include "deprecate.h"
#include "Exception.h"

#include <cmath>
#include <limits>
#include <istream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <utility>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	/**
		 For each element in resulting range assign it to 0.0 if
		 corresponding element in input range is NaN else assign it to
		 1.0.

		 \return true if there is at least one NaN in input range
		 [first, last).

		 \since New in yat 0.5
	*/
	template<typename InputIterator, typename OutputIterator>
	bool binary_weight(InputIterator first, InputIterator last, 
										 OutputIterator result);


	/**
		 \brief convert string to (numerical) type

		 \throw runtime_error if conversion fails
	 */
	template<typename T>
	T convert(const std::string& s);

	/**
		 \brief check if string is convertible to (numerical) type

		 \since New in yat 0.5
	 */
	template<typename T>
	bool is(const std::string& s);

	///
	/// @return true if string is a double
	///
	/// \deprecated Provided for backward compatibility with the 0.4
	/// API. Use is<double>(const std::string&)
	///
	bool is_double(const std::string&) YAT_DEPRECATE;

	/**
		 This function takes the first word (separated by whitespace) in
		 \a s, replaces all upper case with lower case, and compares it
		 with \a other.

		 \return true if processed \a s is equal to \a other. It returns
		 false otherwise or if \a s contains more than one word.
	*/
	bool is_equal(std::string s, std::string other);

	///
	/// @return true if string is a float
	///
	/// \deprecated Provided for backward compatibility with the 0.4
	/// API. Use is<float>(const std::string&)
	///
	bool is_float(const std::string&) YAT_DEPRECATE;

	///
	/// @return true if string is an int
	///
	/// \deprecated Provided for backward compatibility with the 0.4
	/// API. Use is<int>(const std::string&)
	///
	bool is_int(const std::string&) YAT_DEPRECATE;

	///
	/// @return true if string is "nan" (case-insensitive) 
	///
	bool is_nan(const std::string& s);

	/**
		 The std::istream will be interpreted as outlined here:

		 Lines are separated by character \a line_sep and rows are
		 separated by character \a sep. 
		 
		 The first line is read into a stringstream, which is used to
		 load the first vector (vec[0]) with elements using
		 load(stringstream, vec[0], sep).
		 
		 Therefore, column elements separation has two modes depending
		 on the value of \a sep.
		 
		 - If \a sep is the default '\\0' value then column elements are
		 separated with white space characters except the new line
		 character. Multiple sequential white space characters are treated
		 as one separator.
		 
		 - Setting \a sep to something else than the default value will
		 change the behaviour to use the \a sep character as the separator
		 between column elements. Multiple sequential \a sep characters
		 will be treated as separating elements with missing values.

		 If \a rectangle is true, rows must contain same number of
		 elements or function will throw.

		 If \a ignore_empty is true empty lines are ignored.

		 \see load(std::istream&, std::vector<T>&, char sep='\\0')

		 \note Requirement on T: utility::convert<T> must be supported
		 (from yat 0.7 T=string is also supported)

		 \since New in yat 0.6
	 */
	template<typename T>
	void load(std::istream& is, std::vector<std::vector<T> >& vec, char sep='\0', 
						char line_sep='\n', bool ignore_empty=false, bool rectangle=true);

	/**
		 \brief Fill a vector<T> with elements from istream

		 Element separation has two modes depending on the value of \a
		 sep.
		 
		 - If \a sep is the default '\\0' value then elements are
		 separated with white space characters. Multiple sequential white
		 space characters are treated as one separator.
		 
		 - Setting \a sep to something else than the default value will
		 change the behaviour to use the \a sep character as the
		 separator between column elements. Multiple sequential \a sep
		 characters will be treated as separating elements with missing
		 values. Missing values are set to std::numeric_limits<T>::quiet_NaN
		 
		 \note Requirement on T: utility::convert<T> must be supported
		 (from yat 0.7 T=string is also supported)

		 \since New in yat 0.6
	 */
	template<typename T>
	void load(std::istream& is, std::vector<T>& vec, char sep='\0');
	
// private namespace
namespace detail {
	/**
		 Functor used in load function
	 */
	template<typename T>
	struct VectorPusher
	{
		/**
			 convert element to T and push on vec's back

			 \internal
		 */
		void operator()(const std::string& element, std::vector<T>& vec)
		{ 
			if (!element.size())
				vec.push_back(std::numeric_limits<T>::quiet_NaN());
			else {
				vec.push_back(theplu::yat::utility::convert<T>(element));
			}
		}
	};

	/**
		 specialization for string

		 \internal
	 */
	template<>
	struct VectorPusher<std::string>
	{
		/**
			 push element on vec's back
		 */
		void operator()(const std::string& element, std::vector<std::string>& vec)
		{ 
			vec.push_back(element);
		}
	};

} // end of namespace detail


	// template implementations

	template<typename InputIterator, typename OutputIterator>
	bool binary_weight(InputIterator first, InputIterator last, 
										 OutputIterator result)
	{
		bool nan=false;
		while (first!=last) {
			if (std::isnan(*first)) {
				*result=0;
				nan=true;
			}
			else
				*result = 1.0;
			++first;
			++result;
		}
		return nan;
	}


	// template implementations
	template<typename T>
	T convert(const std::string& s)
	{
		if (is_nan(s))
			return std::numeric_limits<T>::quiet_NaN();
		if (is_equal(s, "inf"))
			return std::numeric_limits<T>::infinity();
		if (is_equal(s, "-inf")) {
			if (std::numeric_limits<T>::is_signed)
				return -std::numeric_limits<T>::infinity();
			else
				throw runtime_error(std::string("yat::utility::convert(\"")+s+
														std::string("\"): type is unsigned") );
		}
		std::stringstream ss(s);
	  T a;
		ss >> a;
		bool ok = true;
		if(ss.fail()) 
			ok = false;
		// Check that nothing is left on stream
		std::string b;
		ss >> b;
		if (!b.empty() || !ok)
			throw runtime_error(std::string("yat::utility::convert(\"")+s+
													std::string("\")"));
		return a;
	}

	template<typename T>
	bool is(const std::string& s)
	{
		if (is_nan(s))
			return std::numeric_limits<T>::has_quiet_NaN;
		if (is_equal(s, "inf"))
			return std::numeric_limits<T>::has_infinity;
		if (is_equal(s, "-inf"))
			return std::numeric_limits<T>::has_infinity && 
				std::numeric_limits<T>::is_signed;
		std::stringstream ss(s);
	  T a;
		ss >> a;
		if(ss.fail())
			return false;
		// Check that nothing is left on stream
		std::string b;
		ss >> b;
		return b.empty();
	}

	template<typename T>
	void load(std::istream& is, std::vector<std::vector<T> >& matrix, 
						char sep, char line_sep, bool ignore_empty, 
						bool rectangle)
	{
		size_t nof_columns=0;
		std::string line;
		while(getline(is, line, line_sep)){
			if (line.empty() && ignore_empty)
				continue;
			matrix.resize(matrix.size()+1);
			std::vector<double>& v=matrix.back();
			v.reserve(nof_columns);
			std::stringstream ss(line);
			load(ss, v, sep);
			// add NaN for final separator
			if(sep!='\0' && !line.empty() && line[line.size()-1]==sep) 
				v.push_back(std::numeric_limits<T>::quiet_NaN());
			
			if (rectangle && nof_columns && v.size()!=nof_columns) {
				std::ostringstream s;
				s << "load data file error: "
					<< "line " << matrix.size() << " has " << v.size()
					<< " columns; expected " << nof_columns	<< " columns.";
				throw utility::IO_error(s.str());
			}				
			nof_columns = std::max(nof_columns, v.size());
		}

		// manipulate the state of the stream to be good
		is.clear(std::ios::goodbit);
	}

	template<typename T>
	void load(std::istream& is, std::vector<T>& vec, char sep='\0')
	{
		detail::VectorPusher<T> pusher;
		std::string element;
		bool ok=true;
		while(true) {
			if(sep=='\0')
				ok=(is>>element);
			else
				ok=getline(is, element, sep);
			if(!ok)
				break;
			
			pusher(element, vec);
		}
	}			 			

}}} // of namespace utility, yat, and theplu

#endif
