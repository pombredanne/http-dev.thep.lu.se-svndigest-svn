#ifndef _theplu_svndigest_html_buf_
#define _theplu_svndigest_html_buf_

// $Id: HtmlBuf.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <locale>
#include <map>
#include <streambuf>
#include <string>

namespace theplu{
namespace svndigest{
	///
	/// Buffer that translates certain chars to the corresponding string
	/// representing the character in HTML. The class is typically not
	/// used directly but through the convenience class HtmlStream.
	///
	class HtmlBuf : public std::streambuf
	{
	public:
		/// \param buf Buffer output is sent to.
		HtmlBuf(std::streambuf& buf);

		typedef std::char_traits<char>::int_type int_type;

	private:
		// dictionary how to transform chars
		std::map<char, std::string> map_;
		std::streambuf& buf_;

		// called from base class
		int_type overflow (int_type c);
	};

}} // end of namespace svndigest and namespace theplu

#endif
