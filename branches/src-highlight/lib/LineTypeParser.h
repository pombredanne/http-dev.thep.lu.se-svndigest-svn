#ifndef _theplu_svndigest_line_type_parser_
#define _theplu_svndigest_line_type_parser_

// $Id: LineTypeParser.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iosfwd>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	/**
		 \brief differentiate code, comments, and blank lines
		 
		 Class gets parsing rules from Configuration. Which rules depends
		 on the filename. Typical use is then to add lines using the add
		 function, and the class builds an internal vector<line_type>,
		 which can be accessed through function type(void).
	*/
	class LineTypeParser
	{
	public:
		///
		/// see 'doc/readme.txt' for info on what is code, comment, and other.
		///
		// do not change these without checking in Stats class
		enum line_type {
			copyright = 0,
			other = 1,
			comment = 2,
			code = 3,
			comment_or_copy = 4,
			total = 5 // total should always be the largest
		};

		/// 
		/// @brief Constructor 
		/// \param filename is used to decide which parsing rules to use 
		///
		explicit LineTypeParser(std::string filename);

		const std::string& block(void) const { return block_; }
		inline bool copyright_found(void) const { return copyright_found_; }

		inline size_t end_line(void) const { return end_line_; } 

		/**
			 \brief add a line to parser

			 \return line_type of parsed line

			 The line is parsed and added to internal vector.  
		 */
		line_type parse(std::string line);

		const std::string& prefix(void) const { return copyright_prefix_; }

		inline size_t start_line(void) const { return start_line_; } 

		///
		/// @return const ref to vector with the line types
		///
		inline const std::vector<line_type>& type(void) const { return type_; }

	private:
		// no copy allowed
		LineTypeParser(const LineTypeParser& other);
		LineTypeParser& operator=(const LineTypeParser&);

		line_type code_mode(const std::string& line);
		line_type text_mode(const std::string& line);

		size_t mode_;
		bool post_copyright_;
		bool copyright_found_;
		std::string copyright_prefix_;
		std::string block_;
		size_t start_line_;
		size_t end_line_;
		
		std::vector<line_type> type_;
		const std::vector<std::pair<std::string, std::string> >* codon_;
	};

	///
	/// @return true if @a c is [a-z], [A-Z] or numerical.
	///
	struct AlphaNum : public std::unary_function<char,bool>
	{
		inline bool operator()(const char c) const
		{ 
			return isalnum(c); 
		}
	};

	///
	/// Functor for white space identification
	///
	/// @see isspace
	/// 
	/// @return true if @a c is '\t', '\n', '\v', '\f' or ' '.
	///
	struct WhiteSpace : public std::unary_function<char,bool>
	{
		inline bool operator()(const char c) const
		{ 
			return isspace(c); 
		}
	};

}} // end of namespace svndigest end of namespace theplu

#endif 
