// $Id: Node.cc 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

#include "Date.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "SVNlog.h"
#include "SVNproperty.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	std::string Node::project_=std::string();

	Node::Node(const unsigned int level, const std::string& path, 
						 const std::string& local_path)
		: level_(level), path_(path), stats_(path), log_(NULL),  
			svninfo_(path)
	{ 
		SVNproperty property(path);
		binary_=property.binary();
		svndigest_ignore_=property.svndigest_ignore();
		if (Node::project_==std::string()) // no root directory in local path
			Node::project_ = file_name(path);
		else if (local_path.empty())
			local_path_ = file_name(path);
		else
			local_path_ = local_path + "/" + file_name(path);

		struct stat nodestat;                // C api from sys/stat.h
		lstat(path.c_str(),&nodestat);   // C api from sys/stat.h
		link_ = S_ISLNK(nodestat.st_mode);
	}


	Node::~Node(void)
	{
		if (log_)
			delete log_;
	}


	std::string Node::author(void) const
	{ 
		if (ignore())
			return svninfo_.last_changed_author(); 
		assert(log().commits().size());
		return log().latest_commit().author();
	}


	bool Node::dir(void) const
	{
		return false;
	}


	void Node::html_tablerow(std::ostream& os, 
													 const std::string& stats_type,
													 const std::string& css_class,
													 const std::string& user) const
	{
		const Stats& stats = stats_[stats_type];
		os << "<tr class=\"" << css_class << "\">\n"
			 << "<td class=\"" << node_type() << "\">";
		if (svndigest_ignore())
			os << name() << " (<i>svndigest:ignore</i>)";
		else if (binary())
			os << name() << " (<i>binary</i>)";
		else if (link_)
			os << name() << " (<i>link</i>)";
		// there is no output for nodes when user has zero contribution
		else if (user!="all" && !stats.lines(user))
			os << name();
		else
			os << anchor(href(), name()); 
		os << "</td>\n"; 
		if (user=="all") {
			os << "<td>" << stats.lines() << "</td>\n"
				 << "<td>" << stats.code() << "</td>\n"
				 << "<td>" << stats.comments() << "</td>\n"
				 << "<td>" << stats.empty() << "</td>\n";
		}
		else {
			os << "<td>" << stats.lines(user); 
			if (stats.lines(user)) 
				os << " (" << percent(stats.lines(user),stats.lines()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.code(user); 
			if (stats.code(user)) 
				os << " (" << percent(stats.code(user),stats.code()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.comments(user); 
			if (stats.comments(user)) 
				os << " (" << percent(stats.comments(user),stats.comments()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats.empty(user); 
			if (stats.empty(user)) 
				os << " (" << percent(stats.empty(user),stats.empty()) << "%)"; 
			os << "</td>\n";

		}

		os << "<td>" << trac_revision(last_changed_rev()) << "</td>\n"
			 << "<td>" << author() << "</td>\n"
			 << "</tr>\n";
	}


	const SVNlog& Node::log(void) const
	{
		if (!log_) {
			if (ignore())
				log_ = new SVNlog;
			else {
				log_ = new SVNlog(path());
				log_core(*log_);
			}
		}
		return *log_;
	}


	std::string Node::name(void) const 
	{ 
		std::string res = file_name(path_); 
		return res;
	}


	std::string Node::output_dir(void) const
	{
		return output_dir_;
	}


	void Node::path_anchor(std::ostream& os) const
	{
		os << "<h2 class=\"path\">\n";
		std::vector<std::string> words;
		words.reserve(level_+1);
		std::string word;
		words.push_back(Node::project_);
		std::stringstream ss(local_path());
		while(getline(ss,word,'/'))
			if (!word.empty()) // ignore double slash in path
				words.push_back(word);
		if (words.size()==1)
			os << anchor("index.html", Node::project_,0, "View " + Node::project_);
		else {
			for (size_t i=0; i<words.size()-1; ++i){
				os << anchor("index.html", words[i], level_-i, "View " + words[i]);
				os << "<span class=\"sep\">/</span>";
			}
			os << anchor(href(), words.back(), level_+2-words.size(), 
						 "View " + words.back()); 
		}
		os << "\n</h2>\n";
	}


	void Node::print(const bool verbose) const
	{
		if (ignore())
			return;
		if (verbose)
			std::cout << "Printing output for " << path_ << std::endl;
		const SVNlog& log = this->log();
		typedef std::map<std::string, Stats*>::const_iterator iter;

		const iter end(stats_.stats().end());
		for (iter i=stats_.stats().begin();i!=end; ++i){
			print_core(i->first, "all", "total", log);
			print_core(i->first, "all", "code", log);
			print_core(i->first, "all", "comments", log);
			print_core(i->first, "all", "empty", log);
			for (std::set<std::string>::const_iterator j=i->second->authors().begin();
				 j!=i->second->authors().end(); ++j) {
				print_core(i->first, *j, "total", log);
				print_core(i->first, *j, "code", log);
				print_core(i->first, *j, "comments", log);
				print_core(i->first, *j, "empty", log);
			}
		}
			print_core(verbose);
	}


	void Node::print_author_summary(std::ostream& os, 
																	const Stats& stats,
																	const std::string& line_type,
																	const SVNlog& log) const
	{ 
		HtmlStream hs(os);
		os << "<h3>Author Summary</h3>";
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Author</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Date</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (!dir()) {
			os << "<tr class=\"" << color << "\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("index.html", "../");
			os << "</td>\n</tr>\n";
		}

		// print authors
		const std::string timefmt("%Y-%m-%d  %H:%M");
		for (std::set<std::string>::const_iterator i=stats.authors().begin();
				 i!=stats.authors().end(); ++i){
			if (color=="dark")
				color="light";
			else
				color="dark";
			os << "<tr class=\"" << color << "\"><td>"; 
			os << anchor(*i+"/"+line_type+"/"+output_path()
									 ,*i, level_+2, "View statistics for "+*i); 
			os << "</td><td>" << stats.lines(*i)
				 << "</td><td>" << stats.code(*i)
				 << "</td><td>" << stats.comments(*i)
				 << "</td><td>" << stats.empty(*i);
			if (log.exist(*i)) {
				const Commitment& lc(log.latest_commit(*i));
				os << "</td>" << "<td>" << trac_revision(lc.revision()) 
					 << "</td>" << "<td>";
				hs << Date(lc.date())(timefmt);
			}
			else {
				os << "</td>" << "<td>N/A"
					 << "</td>" << "<td>N/A";
			}
			os << "</td></tr>\n";
		}

		os << "<tr class=\"" << color << "\">\n";
		os << "<td>"; 
		if (dir())
			if (local_path().empty())
				os << anchor("all/"+line_type+"/index.html"
										 ,"Total", level_+2, "View statistics for all"); 
			else
				os << anchor("all/"+line_type+"/"+local_path()+"/index.html"
										 ,"Total", level_+2, "View statistics for all"); 
		else
			os << anchor("all/"+line_type+"/"+local_path()+".html"
									 ,"Total", level_+2, "View statistics for all"); 
		os << "</td>\n";
		os << "<td>" << stats.lines() << "</td>\n";
		os << "<td>" << stats.code() << "</td>\n";
		os << "<td>" << stats.comments() << "</td>\n";
		os << "<td>" << stats.empty() << "</td>\n";
		const Commitment& lc(log.latest_commit());
		os << "<td>" << trac_revision(lc.revision()) << "</td>\n";
		os << "<td>";
		hs << Date(lc.date())(timefmt);
		os << "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
	}

	
	void Node::print_copyright(std::map<std::string,Alias>& alias, 
														 bool verbose) const
	{
		// map with last rev for every year
		std::map<int, svn_revnum_t> year2rev;
		// get log for entire project
		SVNlog log(SVNinfo(path()).repos_root_url());
		typedef SVNlog::container::const_iterator LogIterator;
		for (LogIterator i=log.commits().begin(); i!=log.commits().end(); ++i){
			time_t sec = str2time(i->date());
			tm* timeinfo = gmtime(&sec);
			// ignore commits in repository not present in wc
			year2rev[timeinfo->tm_year] = std::min(i->revision(), last_changed_rev());
		}
		print_copyright(alias, verbose, year2rev);
	}


	std::string Node::url(void) const
	{
		return svninfo_.url();
	}

}} // end of namespace svndigest and namespace theplu
