#ifndef _theplu_svndigest_main_utility_
#define _theplu_svndigest_main_utility_

// $Id: main_utility.h 1100 2010-06-13 17:28:19Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

/**
	 \file This file contains some high level functions that typically
	 are called from within main
 */

#include <string>

namespace theplu {
namespace svndigest {

	class Node;

	/**
		 If \a file exists load configuration file, otherwise load default
		 configuration.

		 \trow if something is wrong
	 */
	void load_config(const std::string& file);

	/**
		 \brief update copyright statements in \a node
	 */
	void update_copyright(const Node& node, bool verbose);

}} // end of namespace svndigest end of namespace theplu

#endif 
