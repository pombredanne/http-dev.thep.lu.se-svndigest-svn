// $Id: first_page.cc 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2006 Peter Johansson
	Copyright (C) 2007 Jari H�kkinen, Peter Johansson
	Copyright (C) 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "first_page.h"

#include "Commitment.h"
#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "Stats.h"
#include "SVNlog.h"
#include "Trac.h"
#include "utility.h"
#include <config.h>	// this header file is created by configure

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/param.h>
#include <unistd.h>
#include <vector>

namespace theplu{
namespace svndigest{

	void print_main_page(const std::string& dir, const SVNlog& log,
											 const Stats& stats, std::string url)
	{
		std::string filename="index.html";
		std::ofstream os(filename.c_str());
		print_header(os, dir, 0, "all", "main", "index.html");

		using namespace std;
		set<string> authors;
		authors.insert(log.author().begin(), log.author().end());
		// erase invalid authors
		authors.erase("");
		authors.erase("no author");

		vector<Commitment> latest_commit;
		latest_commit.reserve(authors.size());
		for (set<string>::const_iterator i(authors.begin()); i!=authors.end(); ++i)
			latest_commit.push_back(log.latest_commit(*i));

		print_summary_plot(os, stats);
		print_general_information(os, log, authors.size(), url);
		sort(latest_commit.begin(), latest_commit.end(), GreaterRevision());
		print_authors(os, latest_commit, stats);
		print_recent_logs(os, log);
		os << "<hr width=100% />";
		print_footer(os);
		os.close();

	}

	void print_general_information(std::ostream& os, const SVNlog& log, 
																 size_t nof_authors, std::string url)
	{ 
		Date begin(log.date()[0]);
		Date end(log.date().back());
		std::string timefmt("%a, %e %b %Y  %H:%M:%S");

		os << "<div class=\"main\">" 
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "General Information"
			 << "</th></tr></thead>\n"
			 << "<tbody>\n"
			 << "<tr><td>URL:</td><td>" 
			 << url << "</td></tr>\n"
			 << "<tr><td>First Revision Date:</td><td>" 
			 << begin(timefmt) << "</td></tr>\n"
			 << "<tr><td>Latest Revision Date:</td><td>" 
			 << end(timefmt) << "</td></tr>\n"
			 << "<tr><td>Age:</td><td>"; 
		os << end.difftime(begin);
		os << "</td></tr>\n"
			 << "<tr><td>Smallest Revision:</td><td>" << log.revision()[0] 
			 << "</td></tr>\n"
			 << "<tr><td>Largest Revision:</td><td>" << log.revision().back() 
			 << "</td></tr>\n"
			 << "<tr><td>Revision Count:</td><td>" << log.revision().size() 
			 << "</td></tr>\n"
			 << "<tr><td>Number of Authors:</td><td>" << nof_authors
			 << "</td></tr>\n"
			 << "</tbody>\n"
			 << "</table></div>\n";
	}


	void print_authors(std::ostream& os, 
										 const std::vector<Commitment>& lc,
										 const Stats& stats)
	{
		os << "<div class=\"main\">"
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "Authors"
			 << "</th></tr></thead>\n";

		os << "<tr><td>Author</td>" 
			 << "<td>Number of Lines</td>"
			 << "<td>Code Lines</td>"
			 << "<td>Comment Lines</td>"
			 << "<td>Latest Commitment</td>"
			 <<"</tr>";

		std::string timefmt("%b %d %H:%M:%S %Y");
		using namespace std;
		for (vector<Commitment>::const_iterator i=lc.begin(); i!=lc.end(); ++i) {
			os << "<tr><td>"; 
			if (!stats.lines(i->author()))
				os << i->author();
			else
				os << anchor(string(i->author()+"/total/index.html"),i->author());
			os << "</td><td>" << stats.lines(i->author()) << " (" 
				 << 100*stats.lines(i->author())/(stats.lines()?stats.lines():1)
				 << "%)</td><td>" << stats.code(i->author()) << " (" 
				 << 100*stats.code(i->author())/(stats.code()?stats.code():1)
				 << "%)</td><td>" << stats.comments(i->author()) << " (" 
				 << 100*stats.comments(i->author())/(stats.comments()?stats.comments():1)
				 << "%)</td><td>" << i->date()(timefmt) << "</td>"
				 <<"</tr>";
		}
		os << "<tr><td>Total</td>";
		os << "<td>" << stats.lines() << "</td>"
			 << "<td>" << stats.code() << "</td>"
			 << "<td>" << stats.comments() << "</td>"
			 <<"</tr>";

		os << "</table></div>\n";
		
	}


	void print_recent_logs(std::ostream& os, const SVNlog& log)
	{
		os << "<div class=\"main\">\n"
			 << "<table class=\"main\"><thead><tr><th colspan=\"2\">" 
			 << "Recent Log"
			 << "</th></tr></thead>\n";

		std::vector<std::string>::const_reverse_iterator a=log.author().rbegin();
		std::vector<std::string>::const_reverse_iterator d=log.date().rbegin();
		std::vector<std::string>::const_reverse_iterator m=log.message().rbegin();
		std::vector<size_t>::const_reverse_iterator r=log.revision().rbegin();
		assert(log.author().size()==log.date().size());
		assert(log.author().size()==log.message().size());
		assert(log.author().size()==log.revision().size());
		os << "<tr><td>Author</td><td>Date</td><td>Rev</td><td>Message</td></tr>\n";
		HtmlStream hs(os);
		std::string timefmt("%b %d %H:%M:%S %Y");
		const size_t maxlength = 80;
		const Configuration& conf = Configuration::instance();
		for (size_t i=0; i<10 && a!=log.author().rend(); ++i) {
			os << "<tr><td>" << anchor(*a+"/total/index.html",*a) << "</td>";
			Date date(*d);
			os << "<td>" << date(timefmt) << "</td>";
			os << "<td>"; 
			os << trac_revision(*r);
			os << "</td>";
			os << "<td>";
			std::string mess = *m;
			// replace newlines with space
			std::replace(mess.begin(), mess.end(), '\n', ' ');
			mess = htrim(mess);

			if (conf.trac_root().empty()) {
				// truncate message if too long
				if (mess.size()>maxlength)
					mess = mess.substr(0,maxlength-3) + "...";
				hs << mess;
			}
			else {// make anchors to trac
				Trac trac(hs);
				trac.print(mess, maxlength);
			}

			os << "</td></tr>\n";
			++a;
			++d;
			++m;
			++r;
		}
		os << "</table></div>\n";
	}


	void print_summary_plot(std::ostream& os, const Stats& stats)
	{
		std::string name("summary_plot.png");
		stats.plot_summary(name);
		os << "<div class=\"main\">\n";
		os << "<img src='" << name << "' alt='[plot]'/>"; 
		os << "</div>";
	}

}} // end of namespace svndigest and namespace theplu
