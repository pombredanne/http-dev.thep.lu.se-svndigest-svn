// $Id: Directory.cc 731 2008-12-15 19:03:04Z peter $

/*
	Copyright (C) 2005, 2006, 2007 Jari H�kkinen, Peter Johansson
	Copyright (C) 2008 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "Directory.h"

#include "Alias.h"
#include "File.h"
#include "html_utility.h"
#include "Node.h"
#include "SVN.h"
#include "SVNlog.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <map>

#include <cerrno>	// Needed to check error state below.
#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{


	Directory::Directory(const u_int level, const std::string& path, 
											 const std::string& output)
		: Node(level,path,output)
	{
		output_dir_=local_path();
		if (!output_dir_.empty())
			output_dir_+='/';

		using namespace std;
		DIR* directory=opendir(path.c_str());    // C API from dirent.h
		if (!directory)
			throw NodeException("ERROR: opendir() failed; " + path +
													" is not a directory");
		list<string> entries;
		struct dirent* entry;
		errno=0;	// Global variable used by C to track errors, from errno.h
		while ((entry=readdir(directory)))       // C API from dirent.h
			entries.push_back(string(entry->d_name));
		if (errno)
			throw NodeException("ERROR: readdir() failed on " + path);
		closedir(directory);

		SVN* svn=SVN::instance();
		for (list<string>::iterator i=entries.begin(); i!=entries.end(); ++i)
			if ((*i)!=string(".") && (*i)!=string("..") && (*i)!=string(".svn")) {
				string fullpath(path_+'/'+(*i));
				switch (svn->version_controlled(fullpath)) {
				case SVN::uptodate:
					struct stat nodestat;                // C api from sys/stat.h
					lstat(fullpath.c_str(),&nodestat);   // C api from sys/stat.h
					if (S_ISDIR(nodestat.st_mode))       // C api from sys/stat.h
						daughters_.push_back(new Directory(level_+1,fullpath,local_path()));
					else
						daughters_.push_back(new File(level_,fullpath,local_path()));
					break;
				case SVN::unresolved:
					throw NodeException(fullpath+" is not up to date");
				case SVN::unversioned: ; // do nothing
				}
			}
		daughters_.sort(NodePtrLess());
	}


	Directory::~Directory(void)
	{
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			delete *i;
	}

	bool Directory::dir(void) const
	{
		return true;
	}

	std::string Directory::href(void) const
	{ 
		return name() + "/index.html";
	}


	std::string Directory::node_type(void) const
	{
		return std::string("directory");
	}


	std::string Directory::output_path(void) const
	{
		return output_dir()+"index.html";
	}

	const Stats& Directory::parse(const bool verbose)
	{
		stats_.reset();
		// Directories themselved give no contribution to statistics.
		for (NodeIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			if (!(*i)->ignore())
				stats_ += (*i)->parse(verbose);
		return stats_;
	}


	void Directory::print_core(const bool verbose) const
	{
		// print daughter nodes, i.e., this function is recursive
		for (NodeConstIterator i=daughters_.begin(); i!=daughters_.end(); ++i)
			(*i)->print(verbose);
	}


	void Directory::print_core(const std::string& user, 
														 const std::string& line_type,
														 const SVNlog& log) const
	{

		std::string outdir = user+"/"+line_type+"/"+local_path_;
		if (local_path_=="")
			outdir = user+"/"+line_type;

		mkdir(outdir);
		std::string imagedir = "images/"+line_type+"/"+local_path_;
		if (user=="all")
			mkdir(imagedir);

		std::string html_name = outdir+"/index.html";
		std::ofstream os(html_name.c_str());
		assert(os.good());
		if (local_path().empty())
			print_header(os, name(), level_+2, user, line_type, "index.html");
		else
			print_header(os, name(), level_+2, user, line_type, 
									 local_path()+"/index.html");
		path_anchor(os);
		os << "<p class=\"plot\">\n<img src='"; 
		for (size_t i=0; i<level_; ++i)
			os << "../";
		os << "../../";
		if (user=="all")
			os << stats_.plot(imagedir+"/index.png", line_type);
		else
			os << imagedir << "/index.png";
		os << "' alt='[plot]' /></p>\n";
		os << "<h3>File Summary";
		if (user!="all")
			os << " for " << user;
		os << "</h3>";			
		os << "<table class=\"listings\">\n";
		os << "<thead>\n";
		os << "<tr>\n";
		os << "<th>Node</th>\n";
		os << "<th>Lines</th>\n";
		os << "<th>Code</th>\n";
		os << "<th>Comments</th>\n";
		os << "<th>Other</th>\n";
		os << "<th>Revision</th>\n";
		os << "<th>Author</th>\n";
		os << "</tr>\n</thead>\n";
		os << "<tbody>\n";

		std::string color("light");
		if (level_){
			os << "<tr class=\"light\">\n";
			os << "<td class=\"directory\" colspan=\"7\">";
			os << anchor("../index.html", "../");
			os << "</td>\n</tr>\n";
			color = "dark";
		}

		// print html links to daughter nodes
		for (NodeConstIterator d = daughters_.begin(); d!=daughters_.end(); ++d) {
			(*d)->html_tablerow(os,color, user);
			if (color=="dark")
				color = "light";
			else
				color = "dark";
		}
		os << "<tr class=\"" << color << "\">\n";
		os << "<td>Total</td>\n";
		if (user=="all"){
			os << "<td>" << stats_.lines() << "</td>\n";
			os << "<td>" << stats_.code() << "</td>\n";
			os << "<td>" << stats_.comments() << "</td>\n";
			os << "<td>" << stats_.empty() << "</td>\n";
		}
		else {
			os << "<td>" << stats_.lines(user); 
			if (stats_.lines(user)) 
				os << " (" << percent(stats_.lines(user),stats_.lines()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.code(user); 
			if (stats_.code(user)) 
				os << " (" << percent(stats_.code(user),stats_.code()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.comments(user); 
			if (stats_.comments(user)) 
				os << " (" << percent(stats_.comments(user),stats_.comments()) << "%)"; 
			os << "</td>\n";
			os << "<td>" << stats_.empty(user); 
			if (stats_.empty(user)) 
				os << " (" << percent(stats_.empty(user),stats_.empty()) << "%)"; 
			os << "</td>\n";
		}
		os << "<td>" << trac_revision(stats_.last_changed_rev()) << "</td>\n";
		os << "<td>" << author() << "</td>\n";
		os << "</tr>\n";
		os << "</tbody>\n";
		os << "</table>\n";
		print_author_summary(os, line_type, log);
		os << "\n";
		print_footer(os);
		os.close();	
	}


	void Directory::print_copyright(std::map<std::string, Alias>& alias) const {
		if (!ignore()){
			// print daughter nodes, i.e, this function is recursive
			for (NodeConstIterator i = daughters_.begin(); i!=daughters_.end(); ++i)
				(*i)->print_copyright(alias);
		}
	}

}} // end of namespace svndigest and namespace theplu
