$Id: README 731 2008-12-15 19:03:04Z peter $

= About svndigest =

This directory contains the 0.6 release of svndigest.

svndigest is a tool to extract development information and statistics
from a subversion repository. 

See the file NEWS for the user-visible changes from previous releases.
In addition, there have been bugs fixed.

For general building and installation instructions, see the file INSTALL.

svndigest is free software. See the file COPYING for copying conditions.


== Downloading ==

svndigest can be obtained from 

  http://dev.thep.lu.se/svndigest/wiki/DownloadPage


== Documentation ==

For documentation see the file doc/readme.txt.


== Development ==

The development of svndigest can be monitored through 

  http://dev.thep.lu.se/svndigest

You can find most information concerning the development of svndigest at
this site.


== Bug Reporting ==

You can report svndigest bugs on 

  http://dev.thep.lu.se/svndigest/newticket

Use user `svndigest` and password `svndigest`.


== Subversion Access ==

The svndigest source repository is available via anonymous subversion
access, issue:

  `svn checkout http://dev.thep.lu.se/svndigest/svn/trunk svndigest`

See file README.developer for developer specific information. We make no
guarantees about the contents or quality of the latest code in the
subversion repository: it is not unheard of for code that is known to
be broken to be committed to the repository. Use at your own risk.


----------------------------------------------------------------------
{{{
Copyright (C) 2005, 2006 Jari H�kkinen
Copyright (C) 2007, 2008 Jari H�kkinen, Peter Johansson

This file is part of svndigest, http://dev.thep.lu.se/svndigest

svndigest is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

svndigest is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.
}}}


