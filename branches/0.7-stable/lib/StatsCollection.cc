// $Id: StatsCollection.cc 1078 2010-06-08 03:29:59Z peter $

/*
	Copyright (C) 2007 Peter Johansson
	Copyright (C) 2008 Jari H�kkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "StatsCollection.h"

#include "AddStats.h"
#include "BlameStats.h"
#include "ClassicStats.h"
#include "Stats.h"

#include <cassert>
#include <map>
#include <stdexcept>
#include <string>

#include <iostream>

namespace theplu{
namespace svndigest{


	StatsCollection::StatsCollection(const std::string& path)
		: path_(path)
	{
		stats_["classic"] = new ClassicStats(path);
		stats_["blame"] = new BlameStats(path);
		stats_["add"] = new AddStats(path);
	}


	StatsCollection::~StatsCollection(void)
	{
		for (map::iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			assert(i->second);
			delete i->second;
			i->second = NULL;
		}
	}


	bool StatsCollection::load_cache(std::istream& is)
	{
		bool result = true;
		for (map::const_iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			svn_revnum_t cache_rev = i->second->load_cache(is);
			if (cache_rev < i->second->last_changed_rev()) {
				result = false;
				// reset if load cache failed
				if (!cache_rev)
					i->second->reset();
				i->second->parse(path_, cache_rev+1);
			}
		}
		return result;
	}


	const std::map<std::string, Stats*>& StatsCollection::stats(void) const
	{
		return stats_;
	}


	void StatsCollection::parse(const std::string& path)
	{
		for (map::iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			i->second->parse(path);
		}
	}


	void StatsCollection::print(std::ostream& os)
	{
		for (map::const_iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			i->second->print(os);
		}
	}


	void StatsCollection::reset(void)
	{
		for (map::const_iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			i->second->reset();
		}
	}


	const StatsCollection& StatsCollection::operator+=(const StatsCollection& rhs)
	{
		assert(stats_.size()==rhs.stats_.size());
		for (map::const_iterator i(stats_.begin()); i!=stats_.end(); ++i) {
			*(i->second) += rhs[i->first];
		}
		return *this;
	}


	const Stats& StatsCollection::operator[](const std::string& key) const
	{
		map::const_iterator iter = stats_.find(key);
		if (iter==stats_.end())
			throw std::runtime_error(key + 
															 std::string(" not found in StatsCollection"));
		return *(iter->second);
	}


	Stats& StatsCollection::operator[](const std::string& key)
	{
		assert(stats_.find(key)!=stats_.end());
		return *(stats_[key]);
	}


}} // end of namespace svndigest and namespace theplu
