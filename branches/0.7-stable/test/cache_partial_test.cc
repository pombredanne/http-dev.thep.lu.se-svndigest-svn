// $Id: cache_partial_test.cc 803 2009-07-10 23:23:45Z peter $

/*
	Copyright (C) 2009 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// Testing that partial cache works fine, ticket #338.

#include "Suite.h"

#include "File.h"
#include "SVN.h"
#include "SVNinfo.h"

#include "../lib/utility.h"

#include <iostream>

using namespace theplu::svndigest;

int main( int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);

	mkdir_p(test::filename("toy_project/.svndigest"));
	std::string root=test::filename("toy_project");
	std::string filename = root + "/AUTHORS";
	suite.out() << "Create SVN instance" << std::endl;
	SVN* svn=SVN::instance(root);
	if (!svn)
		return 1;
  // Extract repository location
	suite.out() << "Extract repository location" << std::endl;
	std::string	repo=SVNinfo(root).repos_root_url();

	// create file ignoring cache file
	std::string cache_file = 
		test::filename("toy_project/.svndigest/AUTHORS.svndigest-cache");
	File file(0,filename,"");
	const StatsCollection& stats1 = file.parse(suite.verbose(), true);
	suite.add(test::consistent(stats1, suite));

	// create file using partial cache
	copy_file(test::src_filename("data/AUTHORS.svndigest-cache-r61"), cache_file);
	File file2(0,filename,"");
	const StatsCollection& stats2 = file2.parse(suite.verbose(), false);
	suite.add(test::consistent(stats2, suite));

	suite.add(equal(stats1, stats2, suite));

	if (suite.ok()) {
		suite.out() << "Test is OK\n";
		return 0;
	}
	suite.out() << "Test failed\n";
  return 1;
}




