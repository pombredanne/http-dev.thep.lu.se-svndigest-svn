DELTA 4 3384 1062
SVN  ¦#’<\ N7u w< * kX2d#ifndef _theplu_svndigest_node_
#define _theplu_svndigest_node_

// $Id$

/*
	Copyright (C) 2005, 2006 Jari Hδkkinen, Peter Johansson
	Copyright (C) 2007 Peter Johansson

	This file is part of svndigest, http://lev.thep.lu.se/trac/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include "html_utility.h"
#include "Stats.h"
#include "SVNinfo.h"
#include "utility.h"

#include <map>
#include <ostream>
#include <stdexcept>
#include <string>

namespace theplu{
namespace svndigest{

	///
	/// If something goes wrong in the use of the Node or its derived
	/// classes, a NodeException is thrown.
	///
	struct NodeException : public std::runtime_error
	{ inline NodeException(const std::string& msg) : runtime_error(msg) {} };
    /// 
    /// @brief Constructor 
    /// 
		Node(const u_int, const std::string&, const std::string&);

		///
		/// @brief Destructor
		///
		virtual inline ~Node(void) {};

		///
		/// @brief Get the author of the latest commit.
		///
		inline const std::string& author(void) const
		{ return svninfo_.last_changed_author(); }

		/**
			 @brief Check whether node is binary.

			 @return True if node is binary.
		*/
		inline bool binary(void) const { return binary_; }

		///
		/// @return true if directory
		///
		virtual bool dir(void) const;

		///
		/// @return href to this node
		///
		virtual std::string href(void) const=0;

		void html_tablerow(std::ostream&, const std::string&, 
											 const std::string& user) const;

		/**
			 @brief Check whether node should be ignored in statistics.

			 If a node is to be ignored the statistics implementer should
			 respect this state. Currently binary files and items with
			 property svndigest:ignore are to be ignored by svndigest. If
			 the node is a directory then the direcotry and its siblings
			 should be ignored by statistics.

			 @return True of node should be ignored by statistics.

			 @see SVNproperty::svndigest_ingorable
		*/
		inline bool ignore(void) const { return binary_ || svndigest_ignore_; }

		///
		/// @brief Get the revision number of the latest commit.
		///
		inline svn_revnum_t last_changed_rev(void) const
		{ return svninfo_.last_changed_rev(); }

		///
		/// @return file or directory
		///
		virtual const std::string node_type(void) const=0;

		///
		/// @return 
		///
		inline const std::string& local_path(void) const { return local_path_; }
inline std::string name(void) const { return file_name(path_); }

		///
		/// @brief parsing file using svn blame.
		///
		virtual const Stats& parse(const bool verbose=false)=0;

		///
		/// @todo doc
		/// 
		inline const std::string& path(void) const { return path_; }void print_author_summary(std::ostream&) const; 

		virtual void 
		print_copyright(std::map<std::string, std::string>&) const=0;

		/**
			 @brief Check if item used to create this object has been
			 assigned property svndigest:ignore.

			 Currently files with property svndigest:ignore are to be
			 ignored by svndigest. It is the responsibility of the
			 statistics implementer to obey the ignore state.

			 @return True if item property svndigest:ignore was set.
		*/
		inline bool svndigest_ignore(void) const { return svndigest_ignore_; }

	protected:

		///
		/// print path in html format (with anchors) to @a os
		///
		void path_anchor(std::ostream& os) const; 

		u_int level_;
		std::string local_path_; // path from root
		std::string path_; // absolute path
		static std::string project_;
			bool binary_;
		bool svndigest_ignore_;
		SVNinfo svninfo_;
	};

	struct NodePtrLess
	{
		inline bool operator()(const Node* first, const Node* second) const
		{		
			if (first->dir()==second->dir())
				return first->name()<second->name();
			return first->dir();
		}
	};

}} // end of namespace svndigesENDREP
id: l.0.r41/4484
type: file
pred: l.0.r23/587
count: 4
text: 41 0 4459 4878 72eb50e54aa8a319181719c013c5db5c
props: 4 19205 59 0 e49f80f0d7ac23e43d85dfc3e13e93ac
cpath: /trunk/lib/Node.h
copyroot: 0 /

PLAIN
K 13
CommitStat.cc
V 17
file c.0.r16/3920
K 12
CommitStat.h
V 17
file d.0.r16/4392
K 12
Directory.cc
V 15
file e.0.r24/96
K 11
Directory.h
V 17
file f.0.r4/19015
K 7
File.cc
V 17
file g.0.r21/1343
K 6
File.h
V 17
file h.0.r4/17189
K 10
Gnuplot.cc
V 17
file i.0.r17/3650
K 9
Gnuplot.h
V 17
file j.0.r17/3856
K 12
GnuplotFE.cc
V 17
file u.0.r22/1106
K 11
GnuplotFE.h
V 17
file v.0.r22/1523
K 11
Makefile.am
V 17
file w.0.r16/4202
K 7
Node.cc
V 17
file k.0.r21/1547
K 6
Node.h
V 17
file l.0.r41/4484
K 8
Stats.cc
V 16
file m.0.r22/703
K 7
Stats.h
V 16
file n.0.r22/904
K 10
utility.cc
V 17
file o.0.r4/17446
K 9
utility.h
V 17
file p.0.r4/18493
END
ENDREP
id: b.0.r41/5345
type: dir
pred: b.0.r24/958
count: 19
text: 41 4686 646 646 6cb1edef68c9a89d3bdc45f168dd1e4b
cpath: /trunk/lib
copyroot: 0 /

PLAIN
K 7
AUTHORS
V 16
file 4.0.r2/2419
K 9
ChangeLog
V 17
file q.0.r17/4871
K 11
Makefile.am
V 16
file 5.0.r2/3114
K 4
NEWS
V 17
file 14.0.r29/751
K 6
README
V 16
file 6.0.r35/168
K 28
acgt for jari@thep.lu.se.pdf
V 16
file 15.0.r34/66
K 8
alhambra
V 19
dir 7.0.r27/3150094
K 3
bin
V 19
dir r.0.r38/1888295
K 7
copy.sh
V 19
file t.0.r37/115640
K 6
design
V 19
dir 16.0.r37/116615
K 17
dir_to_be_ignored
V 15
dir 1c.0.r40/45
K 5
empty
V 13
dir z.0.r25/0
K 3
lib
V 16
dir b.0.r41/5345
K 4
pics
V 18
dir 9.0.r3/2108143
END
ENDREP
id: 1.0.r41/6016
type: dir
pred: 1.0.r40/779
count: 40
text: 41 5488 515 515 4c9aa6f6912ddd009cdc5d3d170a778c
cpath: /trunk
copyroot: 0 /

PLAIN
K 8
branches
V 13
dir 2.0.r1/61
K 4
tags
V 14
dir 3.0.r1/126
K 5
trunk
V 16
dir 1.0.r41/6016
END
ENDREP
id: 0.0.r41/6265
type: dir
pred: 0.0.r40/1026
count: 41
text: 41 6155 97 97 e0e7950600e01cc8bb3283fcb627745d
cpath: /
copyroot: 0 /

l.0.t40-1 modify true false /trunk/lib/Node.h


6265 6398
