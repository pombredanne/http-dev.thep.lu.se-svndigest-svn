#!/bin/sh
# $Id: ignore_revs_test.sh 1515 2012-09-26 00:35:10Z peter $
#
# Copyright (C) 2011, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required="repo"

. ./init.sh || exit 99
set -e

run_svncopyright_with_ignore ()
{
ignore=$1
$SVN revert -R toy_project
cp toy_project/.svndigest/config config
cat >> config << _EOF
[svn-props]
README = svncopyright:ignore=$ignore
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 0 --verbose --root toy_project --config-file=config
}

# no ignore
SVNCOPYRIGHT_run 0 -v --ignore-cache --root toy_project
$GREP "2006 Jari" toy_project/README || exit_fail
$GREP "2007, 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore 42
$GREP "2006 Jari" toy_project/README || exit_fail
$GREP "Copyright (C) 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore "-42"
$GREP "2006 Jari" toy_project/README && exit_fail
$GREP "Copyright (C) 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore "41"
$GREP "2006 Jari" toy_project/README || exit_fail
$GREP "Copyright (C) 2007, 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore "1-34"
$GREP "2006 Jari" toy_project/README || exit_fail
$GREP "Copyright (C) 2007, 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore "1-35"
$GREP "2006 Jari" toy_project/README && exit_fail
$GREP "Copyright (C) 2007, 2008 Peter" toy_project/README || exit_fail

run_svncopyright_with_ignore "42-"
$GREP "2006 Jari" toy_project/README || exit_fail
$GREP "Copyright (C) 2007, 2008 Peter" toy_project/README && exit_fail

run_svncopyright_with_ignore "0-999"
$GREP "Copyright (C)" toy_project/README || exit_fail

# test that nonsense argument fails with grace
$SVN revert toy_project -R
cat > config << _EOF
[svn-props]
README = svncopyright:ignore=nonsense
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 1 -v --root toy_project --config-file=config
$GREP svncopyright: stderr || exit_fail
$GREP yat stderr && exit_fail

# test ignore property on both Node and subNode
$SVN revert toy_project -R
cat > config << _EOF
[copyright-alias]
jari = Jari
peter = Peter
[svn-props]
README = svncopyright:ignore=0-35
toy_project = svncopyright:ignore=42
_EOF
cat config
# do not use --ignore-cache so we also test that cache is ignored automatically
SVNCOPYRIGHT_run 0 -v --root toy_project --config-file=config
$GREP "Copyright (C) 2006" toy_project/README && exit_fail
$GREP "Copyright (C) 2008" toy_project/README || exit_fail

exit_success
