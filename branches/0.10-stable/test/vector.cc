// $Id: vector.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Vector.h"

#include <iostream>
#include <vector>

using namespace theplu::svndigest;

template<class T>
void run_test(T, test::Suite&);
void test_accumulate(test::Suite&);

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv);

	run_test(SparseVector(), suite);
	run_test(SumVector(), suite);
	test_accumulate(suite);

	return suite.exit_status();
}

template<class T>
void run_test(T vec, test::Suite& suite)
{
	vec.begin();
	vec.end();
	vec.size();
	vec[0];
	vec.set(0, 0);

	vec.set(5,10);
	T vec2(vec);
	vec2.set(7,11);
	T vec3;
	vec3 = vec2;
	suite.out() << "testing assignment\n";
	if (vec3.size()!=vec2.size()) {
		suite.add(false);
		suite.out() << "incorrect size: " << vec3.size() << " expected: " 
								<< vec2.size() << "\n";
	}

	suite.out() << "testing back()\n";
	vec.resize(0);
	vec.resize(10);
	vec.set(9,71);
	if (!suite.add(vec.back()==vec[9])) {
		suite.out() << "vec.back(): " << vec.back() 
								<< "\nexpected: " << vec[9] << "\n";
	}
	vec.resize(20);
	if (!suite.add(vec.back()==vec[19])) {
		suite.out() << "vec.back(): " << vec.back() 
								<< "\nexpected: " << vec[19] << "\n";
	}
}

void test_accumulate(test::Suite& suite)
{
	SparseVector vec;
	vec.set(5, 1);
	vec.set(9, 12);
	
	SumVector result;
	accumulate(vec, result);
	std::vector<unsigned int> expected;
	expected.resize(10);
	expected[5]=1;
	expected[6]=1;
	expected[7]=1;
	expected[8]=1;
	expected[9]=13;
	if (static_cast<size_t>(result.size()) != expected.size()) {
		suite.add(false);
		suite.out() << "error: size: " << result.size() << " expected: " 
								<< expected.size() << "\n";
	}
	else {
		for (svn_revnum_t i=0; i<result.size(); ++i) {
			if (result[i]!=expected[i]) {
				suite.add(false);
				suite.out() << "error: result[" << i << "]: " << result[i] 
										<< " expected: " << expected[i] << "\n";
			}
		}
	}

}
