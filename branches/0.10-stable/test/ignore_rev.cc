// $Id: ignore_rev.cc 1478 2012-05-29 10:17:04Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// Testing that partial ignore works

#include "Suite.h"

#include "lib/Configuration.h"
#include "lib/File.h"
#include "lib/SVN.h"
#include "lib/Stats.h"
#include "lib/StatsCollection.h"
#include "lib/utility.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

using namespace theplu::svndigest;

void compare(const Stats& stats, const std::vector<int>& jari,
						 const std::vector<int>& peter, test::Suite& suite);

void test_ignore(svn_revnum_t ignore_rev, const std::string& filename,
								 test::Suite& suite);

int main( int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);

	std::string root="toy_project";
	std::string filename = root + "/AUTHORS";
	suite.out() << "Create SVN instance" << std::endl;
	SVN* svn=SVN::instance(root);
	if (!svn)
		return 1;

	test_ignore(0, filename, suite);
	test_ignore(2, filename, suite);
	test_ignore(48, filename, suite);
	test_ignore(62, filename, suite);
	test_ignore(67, filename, suite);
	return suite.exit_status();
}



void test_ignore(svn_revnum_t ignore_rev, const std::string& filename,
								 test::Suite& suite)
{
	suite.out() << "svndigest:ignore = " << ignore_rev << "\n";
	// create a config and load it
	std::stringstream ss;
	ss << "[svn-props]\n" << filename << " = "
		 << "svndigest:ignore=" << ignore_rev << "\n";
	Configuration::instance().load(ss);

	File file(0,filename,"");
	const StatsCollection& stats = file.parse(suite.verbose(), false, ignore_rev);
	suite.add(test::consistent(stats, suite));

	suite.out() << "classic:\n";
	const Stats& classic = stats["classic"];
	std::vector<int> add_jari(classic.revision()+1,0);
	std::vector<int> add_peter(add_jari);
	std::vector<int> blame_jari(add_jari);
	std::vector<int> blame_peter(add_jari);
	std::vector<int> rm_jari(add_jari);
	std::vector<int> rm_peter(add_jari);

	blame_jari[2] = 4;
	blame_peter[62] = 3;
	blame_peter[67] = 1;

	add_jari[2] = 14;
	if (ignore_rev<2) {
		rm_jari[48] = 7;
		rm_jari[62] = 2;
		rm_jari[67] = 1;
	}
	add_peter[62] = 3;
	add_peter[67] = 1;

	std::vector<int> jari_lines(add_jari.size(), 0);
	std::vector<int> peter_lines(add_jari.size(), 0);
	for (size_t r=ignore_rev+1; r<jari_lines.size(); ++r) {
		jari_lines[r] = jari_lines[r-1] + blame_jari[r];
		peter_lines[r] = peter_lines[r-1] + blame_peter[r];
	}
	compare(classic, jari_lines, peter_lines, suite);

	suite.out() << "blame:\n";
	const Stats& blame = stats["blame"];

	for (size_t r=ignore_rev+1; r<jari_lines.size(); ++r) {
		jari_lines[r] = jari_lines[r-1] + add_jari[r] - rm_jari[r];
		peter_lines[r] = peter_lines[r-1] + add_peter[r] - rm_peter[r];
	}
	compare(blame, jari_lines, peter_lines, suite);

	suite.out() << "add:\n";
	const Stats& add = stats["add"];
	for (size_t r=ignore_rev+1; r<jari_lines.size(); ++r) {
		jari_lines[r] = jari_lines[r-1] + add_jari[r];
		peter_lines[r] = peter_lines[r-1] + add_peter[r];
	}
	compare(add, jari_lines, peter_lines, suite);
}


void compare(const Stats& stats, const std::vector<int>& expected,
						 const std::string& author, test::Suite& suite)
{
	bool all_zero=true;
	for (size_t rev=0; rev<expected.size(); ++rev)
		all_zero = all_zero && !expected[rev];
	if (all_zero)
		return;

	for (size_t rev=0; rev<expected.size(); ++rev) {
		int x = stats(LineTypeParser::total, author, rev);
		if (x != expected[rev]) {
			suite.add(false);
			suite.out() << "error: " << author << " rev: " << rev << " "
									<< x << " lines; expected: " << expected[rev] << "\n";
		}
	}
}


void compare(const Stats& stats, const std::vector<int>& jari,
						 const std::vector<int>& peter, test::Suite& suite)
{
	compare(stats, jari, "jari", suite);
	compare(stats, peter, "peter", suite);
}
