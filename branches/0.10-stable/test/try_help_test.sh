#!/bin/sh
# $Id: try_help_test.sh 1420 2011-11-02 00:24:53Z peter $
#
# Copyright (C) 2011 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

required=""

. ./init.sh || exit 99
set -e

SVNDIGEST_run 1 nonsense
tail -n 1 stderr | $GREP svndigest || exit_fail
SVNDIGEST_run 1 -nonsense
tail -n 1 stderr | $GREP svndigest || exit_fail
SVNDIGEST_run 1 --nonsense
tail -n 1 stderr | $GREP svndigest || exit_fail
exit_success;
