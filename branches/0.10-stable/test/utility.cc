// $Id: utility.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson
	Copyright (C) 2011 Jari Häkkinen, Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/utility.h"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <iostream>
#include <string>

void test_directory_name(theplu::svndigest::test::Suite& suite);
void test_file_name(theplu::svndigest::test::Suite& suite);
bool test_hex(int, unsigned int, std::string);
bool test_fnmatch(bool, std::string, std::string);
bool test_regexp(bool, std::string, std::string,
								 const std::vector<std::string>&);

int main(int argc, char* argv[])
{
	theplu::svndigest::test::Suite suite(argc, argv);
	bool ok=true;

	ok &= test_hex(15,2, "0f");
	ok &= test_hex(17,1, "1");
	ok &= test_hex(16,2, "10");

	ok &= test_fnmatch(true,"peter", "peter");
	ok &= test_fnmatch(false,"peter", "peterj");

	ok &= test_fnmatch(true,"*", "peterj");
	ok &= test_fnmatch(true,"p*", "peterj");
	ok &= test_fnmatch(true, "p*", "peter");
	ok &= test_fnmatch(false, "p*j", "peter");
	ok &= test_fnmatch(true, "p*j", "peterj");
	ok &= test_fnmatch(true, "*peter", "peter");

	ok &= test_fnmatch(true, "p?ter", "peter");
	ok &= test_fnmatch(false, "p?er", "peter");
	ok &= test_fnmatch(false, "p?eter", "peter");

	ok &= test_fnmatch(true, "filename", "filename");
	ok &= test_fnmatch(true, "*name", "filename");
	ok &= test_fnmatch(true, "[fa]il?name", "filename");
	ok &= test_fnmatch(true, "[fa]*il?name", "ffilename");

	ok &= test_fnmatch(true, "[fa]*il?name", "fafafailename");
	ok &= test_fnmatch(false, "[fa]?il?name", "ilename");
	ok &= test_fnmatch(false, "?[fa]il?name", "ilename");
	ok &= test_fnmatch(true, "[fa]il?name", "filename");
	ok &= test_fnmatch(false, "[fa]?il?name", "fafafailename");

	ok &= test_fnmatch(true, "*name", "/path/to/filename");
	ok &= test_fnmatch(true, "*name", "file.name");
	// posix dictates that leading period can not be matched by
	// wildcard, but here we allow match
	ok &= test_fnmatch(true, "*.txt", ".file.txt");


	std::vector<std::string> vec;
	ok &= test_regexp(true,"abcde", "abcde", vec);
	vec.push_back("c");
	ok &= test_regexp(true,"ab?de", "abcde", vec);
	vec[0] = "bcd";
	ok &= test_regexp(true,"a*e", "abcde", vec);
	vec.push_back("");
	ok &= test_regexp(true,"a*d*f", "abcddf", vec);
	vec[0] = "bc";
	vec[1] = "ef";
	ok &= test_regexp(true,"a*d*g", "abcdefg", vec);
	vec.push_back("");
	vec[1]="e";
	vec[2]="f";
	ok &= test_regexp(true,"a*d*?g", "abcdefg", vec);
	ok &= test_regexp(true,"a*d??g", "abcdefg", vec);
	vec.resize(2);
	vec[0]="f";
	vec[1]="e";
	ok &= test_regexp(true, "[fa]il?name", "filename", vec);

	suite.add(ok);
	test_directory_name(suite);
	test_file_name(suite);
	return suite.exit_status();
}


void test_directory_name(theplu::svndigest::test::Suite& suite)
{
	std::vector<std::string> path;
	path.push_back("/usr/lib");
	path.push_back("/usr/");
	path.push_back("usr");
	path.push_back("/");
	path.push_back(".");
	path.push_back("..");
	std::vector<std::string> dir;
	dir.push_back("/usr");
	dir.push_back("/");
	dir.push_back(".");
	dir.push_back("/");
	dir.push_back(".");
	dir.push_back(".");
	assert(dir.size()==path.size());
	using theplu::svndigest::directory_name;
	for (size_t i=0; i<dir.size(); ++i) {
		if (dir[i] != directory_name(path[i])) {
			suite.add(false);
			suite.out() << "error:\n";
			suite.out() << "path:           " << path[i] << "\n";
			suite.out() << "directory_name: " << directory_name(path[i]) << "\n";
			suite.out() << "expected:       " << dir[i] << "\n";
		}
	}
}


void test_file_name(theplu::svndigest::test::Suite& suite)
{
	std::vector<std::string> path;
	path.push_back("/usr/lib");
	path.push_back("/usr/");
	path.push_back("usr");
	path.push_back("/");
	path.push_back(".");
	path.push_back("..");
	std::vector<std::string> file;
	file.push_back("lib");
	file.push_back("usr");
	file.push_back("usr");
	file.push_back("/");
	file.push_back(".");
	file.push_back("..");
	assert(file.size()==path.size());
	using theplu::svndigest::file_name;
	for (size_t i=0; i<file.size(); ++i) {
		if (file[i] != file_name(path[i])) {
			suite.add(false);
			suite.out() << "error:\n";
			suite.out() << "path:           " << path[i] << "\n";
			suite.out() << "file_name:      " << file_name(path[i]) << "\n";
			suite.out() << "expected:       " << file[i] << "\n";
		}
	}
}


bool test_fnmatch(bool answ, std::string a, std::string b)
{
	using namespace theplu::svndigest;
	bool res = fnmatch(a.c_str(), b.c_str());
	// check that fnmatch and regexp agree
	std::vector<std::string> v;
	bool res2 = regexp(a, b, v);
	if (res == answ && res2==answ)
		return true;
	if (res!=answ)
		std::cerr << "fnmatch(" << a << ", " << b << ") results "
							<< res
							<< ". Expects " << answ << std::endl;
	if (res2!=answ)
		std::cerr << "regexp(" << b << ", " << a << ") results "
							<< res2
							<< ". Expects " << answ << std::endl;
	return false;
}

bool test_hex(int x, unsigned int w, std::string facit)
{
	if (theplu::svndigest::hex(x,w)==facit)
		return true;
	std::cerr << "hex(" << x << ", " << w << ") results "
						<< theplu::svndigest::hex(x,w) << ". Expects " << facit
						<< std::endl;
	return false;
}

bool test_regexp(bool ans, std::string a, std::string b,
								 const std::vector<std::string>& vec)
{
	using namespace theplu::svndigest;
	std::vector<std::string> v;
	bool res = regexp(a, b, v);
	if (res!=ans || v!=vec) {
		std::cerr << "regexp(" << a << ", " << b << ") results "
							<< res << ". Expected " << ans << "\n"
							<< "resulting vector:\n";
		std::copy(v.begin(), v.end(),
							std::ostream_iterator<std::string>(std::cerr, "\n"));
		std::cerr << "expected:\n";
		std::copy(vec.begin(), vec.end(),
							std::ostream_iterator<std::string>(std::cerr, "\n"));
		return false;
	}
	return true;
}
