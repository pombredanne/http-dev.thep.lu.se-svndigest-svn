// $Id: cache.cc 1519 2012-09-27 00:39:48Z peter $

/*
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

// Testing that loading a cache file does not fail, i.e., protecting
// development from breaking compatibility with old cache files.

#include "Suite.h"

#include "lib/File.h"
#include "lib/SVN.h"
#include "lib/StatsCollection.h"
#include "lib/utility.h"

#include <cassert>
#include <fstream>

using namespace theplu::svndigest;

void main2(test::Suite& suite);

int main( int argc, char* argv[])
{
	test::Suite suite(argc, argv, true);

	try {
		main2(suite);
	}
	catch (std::runtime_error& e) {
		suite.out() << "error: " << e.what() << "\n";
		suite.add(false);
	}
	return suite.exit_status();
}


void main2(test::Suite& suite)
{
	// update wc to rev 61
	suite.system("svn update -r 61 toy_project", 0);

	mkdir_p("toy_project/.svndigest");
	std::string root="toy_project";
	std::string filename = root + "/AUTHORS";
	suite.out() << "Create SVN instance" << std::endl;
	SVN* svn=SVN::instance(root);
	if (!svn)
		throw std::runtime_error("failed creating SVN instance\n");

	StatsCollection stats(filename);
	std::string cache_file=test::src_filename("data/AUTHORS.svndigest-cache-r61");
	std::ifstream is(cache_file.c_str());
	assert(is);
	if (!stats.load_cache(is)) {
		suite.add(false);
		suite.out() << "error: failed reading config file\n";
	}
}




