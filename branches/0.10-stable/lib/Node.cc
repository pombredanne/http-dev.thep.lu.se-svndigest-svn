// $Id: Node.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

#include "Configuration.h"
#include "Date.h"
#include "HtmlStream.h"
#include "html_utility.h"
#include "LineTypeParser.h"
#include "SVNlog.h"
#include "SVNproperty.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

#include <dirent.h>
#include <sys/stat.h>

namespace theplu{
namespace svndigest{

	std::string Node::project_=std::string();

	Node::Node(const unsigned int level, const std::string& path,
						 const std::string& local_path, const std::string& project)
		: level_(level), path_(path), stats_(path), log_(NULL),
			property_(path), svninfo_(path)
	{
		if (Node::project_==std::string()) // no root directory in local path
			Node::project_ = project;
		else if (local_path.empty())
			local_path_ = file_name(path);
		else
			local_path_ = local_path + "/" + file_name(path);

		struct stat nodestat;                // C api from sys/stat.h
		lstat(path,&nodestat);   // C api from sys/stat.h
		link_ = S_ISLNK(nodestat.st_mode);
	}


	Node::~Node(void)
	{
		if (log_)
			delete log_;
	}


	std::string Node::author(void) const
	{
		if (svndigest_ignore())
			return svninfo_.last_changed_author();
		assert(log().commits().size());
		return log().latest_commit().author();
	}


	bool Node::binary(void) const
	{
		return property_.binary();
	}


	bool Node::dir(void) const
	{
		return false;
	}


	void Node::html_tablerow(std::ostream& os,
													 const std::string& stats_type,
													 const std::string& css_class,
													 const std::string& user) const
	{
		os << "<tr class=\"" << css_class << "\">\n"
			 << "<td class=\"" << node_type() << "\">";
		if (svndigest_ignore())
			os << name() << " (<i>svndigest:ignore</i>)";
		else if (binary())
			os << name() << " (<i>binary</i>)";
		else if (link_)
			os << name() << " (<i>link</i>)";
		// there is no output for nodes when user has zero contribution
		else if (user!="all"
						 && !tiny_stats()(stats_type,user,LineTypeParser::total))
			os << name();
		else if (!Configuration::instance().output_file() && !dir())
			os << name();
		else
			os << anchor(href(), name());
		os << "</td>\n";

		html_tabletd(os, stats_type, user, LineTypeParser::total);
		html_tabletd(os, stats_type, user, LineTypeParser::code);
		html_tabletd(os, stats_type, user, LineTypeParser::comment);
		html_tabletd(os, stats_type, user, LineTypeParser::other);

		os << "<td>" << trac_revision(last_changed_rev()) << "</td>\n"
			 << "<td>" << author() << "</td>\n"
			 << "</tr>\n";
	}


	void Node::html_tabletd(std::ostream& os,
													const std::string& stats_type,
													const std::string& user,
													LineTypeParser::line_type lt) const
	{
		os << "<td>" << tiny_stats()(stats_type, user, lt);
		if (user!="all" && tiny_stats()(stats_type, user, lt))
			os << " (" << percent(tiny_stats()(stats_type, user, lt),
														tiny_stats()(stats_type, "all", lt)) << "%)";
		os << "</td>\n";
	}


	void Node::init_tiny_stats(void)
	{
		tiny_stats_.init(stats_);
	}


	const SVNlog& Node::log(void) const
	{
		if (!log_) {
			if (svndigest_ignore())
				log_ = new SVNlog;
			else {
				log_ = new SVNlog(path());
				log_core(*log_);
			}
		}
		return *log_;
	}


	std::string Node::name(void) const
	{
		return file_name(path_);
	}


	const SVNproperty& Node::property(void) const
	{
		return property_;
	}


	const StatsCollection& Node::stats(void) const
	{
		return stats_;
	}


	StatsCollection& Node::stats(void)
	{
		return stats_;
	}


	bool Node::svncopyright_ignore(void) const
	{
		return property_.svncopyright_ignore() || binary() || link_;
	}


	bool Node::svndigest_ignore(void) const
	{
		return property_.svndigest_ignore() || binary() || link_;
	}

}} // end of namespace svndigest and namespace theplu
