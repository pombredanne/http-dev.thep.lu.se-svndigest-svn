#ifndef _theplu_svndigest_directory_util_
#define _theplu_svndigest_directory_util_

// $Id: DirectoryUtil.h 1423 2011-12-16 03:19:31Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "yat/FileUtil.h"

#include <string>
#include <vector>

namespace theplu {
namespace svndigest {

	/**
	*/
	class DirectoryUtil
	{
	public:
		DirectoryUtil(const std::string& path);
		typedef
		std::vector<yat::utility::FileUtil>::const_iterator const_iterator;
		const_iterator begin(void) const;
		bool empty(void) const;
		const_iterator end(void) const;
	private:
		std::vector<yat::utility::FileUtil> entries_;
	};

}} // end of namespace svndigest and namespace theplu
#endif
