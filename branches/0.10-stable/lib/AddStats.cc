// $Id: AddStats.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AddStats.h"

#include "SVNblame.h"
#include "SVNlog.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <map>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{


	AddStats::AddStats(const std::string& path)
		: Stats(path)
	{
	}


	AddStats::AddStats(const AddStats& other)
	: Stats(other)
	{
	}


	void AddStats::do_parse(const std::string& path, svn_revnum_t first_rev)
	{
		first_rev = std::max(first_rev, ignore_rev()+1);
		// FIXME: why not using the log from File object
		SVNlog log(path);
		typedef std::set<svn_revnum_t, std::greater<svn_revnum_t> > RevSet;
		RevSet revs;
		std::vector<std::map<std::string, SparseVector> > data;
		std::transform(log.commits().begin(), log.commits().end(),
									 std::inserter(revs, revs.begin()),
									 std::mem_fun_ref(&Commitment::revision));
		for (RevSet::iterator rev_iter=revs.begin();
				 rev_iter!=revs.end() && *rev_iter>=first_rev; ++rev_iter){
			SVNblame svn_blame(path, *rev_iter);
			LineTypeParser parser(path);
			std::vector<size_t> add_vec(4,0);
			std::string author="";
			while (svn_blame.valid()) {
				LineTypeParser::line_type lt = parser.parse(svn_blame.line());
				if (*rev_iter==svn_blame.revision()) {
					author = svn_blame.author();
					assert(static_cast<size_t>(lt)<add_vec.size());
					++add_vec[lt];
				}
				// I dont trust blame and log behave consistently (stop-on-copy).
				revs.insert(svn_blame.revision());
				svn_blame.next_line();
			}
			if (author.empty()) { // don't add 0 to author ""
				assert(add_vec[0]==0);
				assert(add_vec[1]==0);
				assert(add_vec[2]==0);
				assert(add_vec[3]==0);
				continue;
			}
			if (data.size()<add_vec.size())
				data.resize(add_vec.size());
			for (size_t lt=0; lt<add_vec.size(); ++lt) {
				assert(author.size());
				SparseVector& vec=data[lt][author];
				// add add_vec[lt] to current value
				vec.set(*rev_iter, vec[*rev_iter] + add_vec[lt]);
				assert(static_cast<size_t>(data[lt][author][*rev_iter])>=add_vec[lt]);
			}
		}
		// add parsed data to member
		add(data);
	}


	unsigned int AddStats::max_element(const SumVector& v) const
	{
		if (v.size()==0)
			return 0;
		return v.back();
	}


}} // end of namespace svndigest and namespace theplu
