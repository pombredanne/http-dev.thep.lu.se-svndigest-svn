#ifndef _theplu_svndigest_svndigest_cache_remover_
#define _theplu_svndigest_svndigest_cache_remover_

// $Id: CacheRemover.h 1423 2011-12-16 03:19:31Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodeVisitor.h"

#include <string>

namespace theplu{
namespace svndigest{

	class Directory;
	class File;

  /**
	*/
  class CacheRemover : public NodeVisitor
  {
  public:
		CacheRemover(bool verbose, const std::string& suffix);

		/**
		 */
		bool enter(Directory& dir);

		/**
		 */
		void leave(Directory& dir);

		/**
		 */
		void visit(File& dir);

	private:
		bool verbose_;
		std::string suffix_;
	};
}} // end of namespace svndigest and namespace theplu

#endif
