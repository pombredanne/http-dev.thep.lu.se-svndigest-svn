// $Id: SVNblame.cc 1267 2010-11-02 03:56:19Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNblame.h"
#include "SVN.h"

#include <string>

namespace theplu {
namespace svndigest {


	SVNblame::SVNblame(const std::string& path)
		: instance_(SVN::instance())
	{
		instance_->client_blame(path.c_str(), blame_receiver,
														static_cast<void*>(&blame_receiver_baton_));
		blame_info_iterator_ = blame_receiver_baton_.blame_info.begin();
	}


	SVNblame::SVNblame(const std::string& path, svn_revnum_t rev)
		: instance_(SVN::instance())
	{
		instance_->client_blame(path.c_str(), blame_receiver,
														static_cast<void*>(&blame_receiver_baton_),
														rev);
		blame_info_iterator_ = blame_receiver_baton_.blame_info.begin();
	}


	SVNblame::~SVNblame(void)
	{
		std::vector<blame_information*>::iterator i=
			blame_receiver_baton_.blame_info.begin();
		while (i!=blame_receiver_baton_.blame_info.end()) {
			delete *i;
			++i;
		}
	}


	std::string SVNblame::author(void)
	{
		return (*blame_info_iterator_)->author;
	}


	svn_error_t *
	SVNblame::blame_receiver(void *baton, apr_int64_t line_no,
													 svn_revnum_t revision, const char *author,
													 const char *date, const char *line, apr_pool_t *pool)
	{
		blame_information *bi=new blame_information;
		bi->line_no=line_no;
		bi->revision=revision;
		bi->author=author;
		bi->date=date;
		bi->line=line;
		static_cast<struct blame_receiver_baton_*>(baton)->blame_info.push_back(bi);
		return SVN_NO_ERROR;
	}


	std::string SVNblame::date(void)
	{
		return (*blame_info_iterator_)->date;
	}


	std::string SVNblame::line(void)
	{
		return (*blame_info_iterator_)->line;
	}


	apr_int64_t SVNblame::line_no(void)
	{
		return (*blame_info_iterator_)->line_no;
	}


	bool SVNblame::next_line(void)
	{
		if (valid())
			++blame_info_iterator_;
		return valid();
	}


	svn_revnum_t SVNblame::revision(void)
	{
		return (*blame_info_iterator_)->revision;
	}


	void SVNblame::reset(void)
	{
		blame_info_iterator_ = blame_receiver_baton_.blame_info.begin();
	}


	bool SVNblame::valid(void)
	{
		return (blame_info_iterator_!=blame_receiver_baton_.blame_info.end());
	}

}} // end of namespace svndigest and namespace theplu
