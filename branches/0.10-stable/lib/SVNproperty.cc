// $Id: SVNproperty.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNproperty.h"

#include "Configuration.h"
#include "SVN.h"
#include "utility.h"

#include "yat/Exception.h"
#include "yat/Segment.h"
#include "yat/utility.h"

#include <limits>
#include <stdexcept>
#include <sstream>
#include <string>

namespace theplu {
namespace svndigest {

	using yat::utility::convert;

	SVNproperty::SVNproperty(const std::string& path)
		: binary_(false), svncopyright_ignore_(false), svndigest_ignore_rev_(0)
  {
		typedef std::map<std::string, std::string> str_map;
		str_map property;
		SVN::instance()->client_proplist(path, property);
		add(property.begin(), property.end());
		const Configuration& config = Configuration::instance();
		const str_map& props = config.svn_properties(file_name(path));
		add(props.begin(), props.end());
	}


	void
	SVNproperty::add(std::map<std::string, std::string>::const_iterator i,
									 std::map<std::string, std::string>::const_iterator last)
	{
		for ( ; i!=last ; ++i) {
			try {
				if (i->first == "svndigest:ignore")
					add_svndigest_ignore(i->second);
				else if (i->first == "svncopyright:ignore")
					add_svncopyright_ignore(i->second);
				else
					if ((i->first == "svn:mime-type") &&
							(svn_mime_type_is_binary(i->second.c_str())))
						binary_=true;
			}
			catch (yat::utility::runtime_error& e) {
				std::ostringstream ss;
				ss << "invalid value for property '" << i->first << "': '"
					 << i->second << "'";
				throw std::runtime_error(ss.str());
			}
		}
	}


	void SVNproperty::add_svncopyright_ignore(const std::string& value)
	{
		// reset previous settings
		svncopyright_ignore_ = false;
		svncopyright_ignore_rev_.clear();

		if (value.empty()) {
			svncopyright_ignore_ = true;
			return;
		}

		std::istringstream ss(value);
		std::string word;
		// entries delimited by ';'
		while (getline(ss, word, ';')) {
			std::istringstream ss2(word);
			std::string first;
			getline(ss2, first, '-');
			trim(first);
			std::string second;
			bool found_dash = getline(ss2, second);
			trim(second);
			yat::utility::Segment<svn_revnum_t> rev_interval;
			if (!found_dash) {
				rev_interval.begin() = convert<svn_revnum_t>(first);
				rev_interval.end() = rev_interval.begin()+1;
			}
			else {
				if (first.empty())
					rev_interval.begin() = 0;
				else
					rev_interval.begin() = convert<svn_revnum_t>(first);
				if (second.empty())
					rev_interval.end() = std::numeric_limits<svn_revnum_t>::max();
				else
					rev_interval.end() = convert<svn_revnum_t>(second)+1;
			}
			svncopyright_ignore_rev_.insert_merge(rev_interval);
		}
	}


	void SVNproperty::add_svndigest_ignore(const std::string& value)
	{
		if (value.empty() || value=="*") {
			svndigest_ignore_rev_ = std::numeric_limits<svn_revnum_t>::max();
			return;
		}
		svndigest_ignore_rev_ = convert<svn_revnum_t>(value);
	}


	bool SVNproperty::binary(void) const
	{
		return binary_;
	}


	bool SVNproperty::svndigest_ignore(void) const
	{
		return svndigest_ignore_rev_==std::numeric_limits<svn_revnum_t>::max();
	}


	svn_revnum_t SVNproperty::svndigest_ignore_rev(void) const
	{
		return svndigest_ignore_rev_;
	}


	bool SVNproperty::svncopyright_ignore(void) const
	{
		return svncopyright_ignore_;
	}


	const yat::utility::SegmentSet<svn_revnum_t>&
	SVNproperty::svncopyright_ignore_rev(void) const
	{
		return svncopyright_ignore_rev_;
	}

}} // end of namespace svndigest and namespace theplu
