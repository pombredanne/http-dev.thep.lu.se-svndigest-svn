// $Id: SVNlog.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SVNlog.h"

#include "Commitment.h"
#include "SVN.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>

namespace theplu {
namespace svndigest {


	SVNlog::SVNlog(void)
	{
	}


	SVNlog::SVNlog(const std::string& path)
	{
		SVN::instance()->client_log(path, log_message_receiver,
																static_cast<void*>(&lb_));
	}


	SVNlog::~SVNlog(void)
	{
	}


	bool SVNlog::exist(std::string name) const
	{

		SVNlog::container::const_reverse_iterator iter = commits().rbegin();
		for ( ; iter!= commits().rend(); ++iter)
			if (iter->author() == name)
				return true;
		return false;
	}


	const Commitment& SVNlog::latest_commit(void) const
	{
		assert(commits().size());
		return *commits().rbegin();
	}


	const Commitment& SVNlog::latest_commit(std::string name) const
	{
		SVNlog::container::const_reverse_iterator iter = commits().rbegin();
		for ( ; iter!= commits().rend(); ++iter)
			if (iter->author() == name)
				return *iter;
		std::stringstream ss;
		ss << __FILE__ << " could not find author: " << name;
		throw std::runtime_error(ss.str());
		// let us return something to avoid compiler warnings
		return *commits().begin();
	}


	svn_error_t*
	SVNlog::log_message_receiver(void *baton, apr_hash_t *changed_paths,
															 svn_revnum_t rev, const char *author,
															 const char *date, const char *msg,
															 apr_pool_t *pool)
	{
		struct log_receiver_baton *lb=static_cast<struct log_receiver_baton*>(baton);
		std::string d;
		if (date && date[0])
			d = date;
		else if (!lb->commits.empty()) {
			// this is a bit hackish to fix the problem reported in
			// ticket #458. If we lack read permission for rev, no date is
			// retrived and we use the date of the previous rev. That should
			// be OK if we call the super-root and previous rev truly is
			// rev-1 (super-root contains all revs). That should be the
			// typical case where this happens. If this would happen when
			// calling the log of a sub-node (e.g. the trunk) this could be
			// problematic because log is more or less sparse and when
			// merging two sparse logs together this workaround could have
			// strange effects such as rev+1 having a earlier date than
			// rev. To ensure that we only allow this workaround when
			// calling super-root, we have the assert below:
			std::cerr << "no date defined for revision: " << rev << "\n";
			assert(rev == lb->commits.rbegin()->revision()+1);
			d = lb->commits.rbegin()->date();
		}
		else {
			std::stringstream msg;
			msg << "No date defined for revision: " << rev;
			throw SVNException(msg.str());
		}
		std::string a;
		if (author && author[0])
			a=author;
		std::string message;
		if (msg)
			message = msg;
		lb->commits.insert(lb->commits.end(), Commitment(a, d, message, rev));
    return SVN_NO_ERROR;
	}


	void SVNlog::swap(SVNlog& rhs)
	{
		lb_.commits.swap(rhs.lb_.commits);
	}


	SVNlog& operator+=(SVNlog& lhs, const SVNlog& rhs)
	{
		lhs.commits().insert(rhs.commits().begin(), rhs.commits().end());
		return lhs;
	}

}} // end of namespace svndigest and namespace theplu
