// $Id: CacheRemover.cc 1561 2014-06-01 03:59:08Z peter $

/*
	Copyright (C) 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CacheRemover.h"

#include "Directory.h"
#include "DirectoryUtil.h"
#include "File.h"
#include "NodeVisitor.h"
#include "utility.h"

#include "yat/Exception.h"

#include <iostream>

namespace theplu {
namespace svndigest {

	CacheRemover::CacheRemover(bool verbose, const std::string& suffix)
		: verbose_(verbose), suffix_(suffix)
	{}


	bool CacheRemover::enter(Directory& dir)
	{
		return true;
	}


	void CacheRemover::leave(Directory& directory)
	{
		std::string cache_dir = concatenate_path(directory.path(), ".svndigest");
		if (!node_exist(cache_dir))
			return;

		std::string pattern = std::string("*") + suffix_;
		DirectoryUtil dir(cache_dir);
		SVN* svn=SVN::instance();
		for (DirectoryUtil::const_iterator node=dir.begin();
				 node!=dir.end(); ++node) {
			if (fnmatch(pattern, node->path())) {
				std::string fn =
					concatenate_path(directory.path(), file_name(node->path()));
				// chop off suffix_
				fn.resize(fn.size()-suffix_.size());
				if (svn->version_controlled(fn)==SVN::unversioned) {
					remove(node->path());
					if (verbose_)
						std::cout << "Removing '" << node->path() << "'\n";
				}
			}
		}
		DirectoryUtil dir2(cache_dir);
		// remove cache dir if it's empty and unversioned
		if (dir2.empty() && svn->version_controlled(cache_dir)==SVN::unversioned)
			remove(cache_dir);
	}


	void CacheRemover::visit(File& file)
	{
		// do nothing
	}

}} // end of namespace svndigest and namespace theplu
