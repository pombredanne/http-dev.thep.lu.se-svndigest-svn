// $Id: Alias.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Alias.h"

#include <string>

namespace theplu{
namespace svndigest{

	Alias::Alias(void)
		: name_(std::string()), id_(0)
	{}


	Alias::Alias(std::string name, size_t id)
		: name_(name), id_(id)
	{}


	std::string Alias::name(void) const
	{
		return name_;
	}


	size_t Alias::id(void) const
	{
		return id_;
	}

	bool operator<(const Alias& lhs, const Alias& rhs)
	{
		return lhs.name() < rhs.name();
	}


	bool operator==(const Alias& lhs, const Alias& rhs)
	{
		return lhs.name() == rhs.name();
	}

}} // end of namespace svndigest and namespace theplu
