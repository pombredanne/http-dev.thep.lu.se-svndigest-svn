// $Id: HtmlBuf.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "HtmlBuf.h"

#include "Configuration.h"

#include <cstdio>
#include <iostream>
#include <string>

namespace theplu{
namespace svndigest{

	HtmlBuf::HtmlBuf(std::streambuf& buf)
		: std::streambuf(), buf_(buf)
	{
		map_['"']=std::string("&quot;");
		map_['\'']=std::string("\'");
		map_['\n']=std::string("<br />");
		map_['<']=std::string("&lt;");
		map_['>']=std::string("&gt;");
		map_['&']=std::string("&amp;");
		map_[' ']=std::string("&nbsp;");
		std::string str;
		for (size_t i=0; i<Configuration::instance().tab_size(); ++i)
			str += "&nbsp;";
		map_['\t']=str;
	}

	HtmlBuf::int_type HtmlBuf::overflow (HtmlBuf::int_type c)
	{
		std::map<char, std::string>::const_iterator i = map_.find(c);
		if (i==map_.end())
			return buf_.sputc(c);
		// writing
		if (buf_.sputn(i->second.c_str(), i->second.size()) ==
				static_cast<std::streamsize>(i->second.size()))
			return c;
		return EOF;
	}

}} // end of namespace svndigest and namespace theplu
