// $Id: CacheCopyer.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CacheCopyer.h"

#include "Directory.h"
#include "File.h"
#include "NodeVisitor.h"
#include "utility.h"

#include "yat/Exception.h"

#include <sstream>
#include <string>

#include <iostream>

namespace theplu {
namespace svndigest {

	CacheCopyer::CacheCopyer(const std::string& target, bool verbose)
		: NodeVisitor(), target_(target), verbose_(verbose)
	{}


	void
	CacheCopyer::copy(const std::string& src, const std::string& trg_dir) const
	{
		if (!node_exist(src))
			return;
		if (access_rights(src, "r"))
			throw yat::utility::errno_error("'" + src + "': ");
		std::string target_file =
			concatenate_path(trg_dir, file_name(src));

		if (verbose_)
			std::cout << src << " -> " << target_file << "\n";
		copy_file(src, target_file);
	}


	bool CacheCopyer::enter(Directory& dir)
	{
		if (dir.svndigest_ignore() && dir.svncopyright_ignore())
			return false;

		std::string target_dir = concatenate_path(target_, dir.local_path());
		if (!node_exist(target_dir)) {
			throw yat::utility::errno_error(target_dir + ": ");
		}
		mkdir_p(concatenate_path(target_dir, ".svndigest"));
		return true;
	}


	void CacheCopyer::leave(Directory& dir)
	{
	}


	void CacheCopyer::visit(File& file)
	{
		if (file.svndigest_ignore() && file.svncopyright_ignore())
			return;
		std::string src_file = file.cache_name();

		std::string target_dir =
			concatenate_path(target_,
											 directory_name(file.local_path())+"/.svndigest");

		if (!file.svndigest_ignore())
			copy(src_file, target_dir);

		replace(src_file, "svndigest-cache", "svncopyright-cache");
		if (!file.svncopyright_ignore())
			copy(src_file, target_dir);

	}

}} // end of namespace svndigest and namespace theplu
