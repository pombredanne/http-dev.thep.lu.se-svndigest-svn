#ifndef _theplu_svndigest_directory_printer_
#define _theplu_svndigest_directory_printer_

// $Id: DirectoryPrinter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodePrinter.h"

#include <map>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{

	class Directory;
	class Node;

	///
	/// Class taking care of directories.
	///
	class DirectoryPrinter : public NodePrinter
	{
	public:
		/**
			 \brief Constructor
		*/
		explicit DirectoryPrinter(const Directory&);

		/**
			 \brief Destructor
		*/
		~DirectoryPrinter(void);

		/**
			 @return output path for example 'lib/File.h.html' for this file
		 */
		std::string output_path(void) const;

	private:
		///
		/// @brief Copy Constructor, not implemented
		///
		DirectoryPrinter(const DirectoryPrinter&);

		const Node& node(void) const;

		void print_core(bool verbose=false) const;

		void print_core(const std::string& stats_type, const std::string& user,
										const std::string& line_type, const SVNlog&) const;




		const Directory& directory_;
		typedef std::vector<Node*> NodeContainer;
		typedef NodeContainer::iterator NodeIterator;
		typedef NodeContainer::const_iterator NodeConstIterator;
	};

}} // end of namespace svndigest and namespace theplu

#endif
