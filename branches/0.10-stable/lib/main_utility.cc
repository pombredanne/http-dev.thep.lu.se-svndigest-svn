// $Id: main_utility.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "main_utility.h"

#include "Configuration.h"
#include "CopyrightVisitor.h"
#include "Node.h"
#include "utility.h"

#include "yat/utility.h"

#include <cctype>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>

namespace theplu {
namespace svndigest {

	void load_config(const std::string& file, bool verbose)
	{
		// Reading configuration file
		Configuration& config = Configuration::instance();
		if (node_exist(file)) {
			std::ifstream is(file.c_str());
			assert(is.good());
			if (verbose)
				std::cout << "Reading configuration file: '" << file << "'\n";
			try {
				config.load(is);
			}
			catch (std::runtime_error& e) {
				std::string msg = "invalid config file\n";
				msg += e.what();
				throw std::runtime_error(msg);
			}
			is.close();
		}
	}


	void update_copyright(Node& tree, bool verbose, bool ignore_cache)
	{
		const Configuration& config = Configuration::instance();
		std::map<std::string, Alias> alias(config.copyright_alias());

		// map with last rev for every year
		std::map<int, svn_revnum_t> year2rev;
		// get log for entire project
		SVNlog log(SVNinfo(tree.path()).repos_root_url());
		typedef SVNlog::container::const_iterator LogIterator;
		for (LogIterator i=log.commits().begin(); i!=log.commits().end(); ++i){
			// grep everything prior first '-'
			std::string year = i->date().substr(0,i->date().find('-'));
			using yat::utility::convert;
			// ignore commits in repository not present in wc
			year2rev[convert<int>(year)-1900] = std::min(i->revision(),
																									 tree.last_changed_rev());
		}

		CopyrightVisitor visitor(alias, verbose, year2rev, ignore_cache);
		tree.traverse(visitor);
	}

}} // end of namespace svndigest and namespace theplu
