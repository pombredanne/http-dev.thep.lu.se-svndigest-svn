// $Id: ClassicStats.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClassicStats.h"

#include "Functor.h"
#include "SVNblame.h"
#include "SVNinfo.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	ClassicStats::ClassicStats(const std::string& path)
		: Stats(path)
	{
	}


	ClassicStats::ClassicStats(const ClassicStats& other)
	: Stats(other)
	{
	}


	void ClassicStats::do_parse(const std::string& path, svn_revnum_t rev)
	{
		svn_revnum_t first_rev = ignore_rev() + 1;
		reset();
		LineTypeParser parser(path);
		SVNblame svn_blame(path);
		std::vector<std::map<std::string, SparseVector> > data(4);
		while (svn_blame.valid()) {
			LineTypeParser::line_type lt = parser.parse(svn_blame.line());
			assert(static_cast<size_t>(lt)<data.size());
			if (svn_blame.revision()>=first_rev) {
				SparseVector& vec = data[lt][svn_blame.author()];
				vec.set(svn_blame.revision(), vec[svn_blame.revision()] + 1);
			}
			svn_blame.next_line();
		}
		// add parsed data to member
		add(data);
	}


	unsigned int
	ClassicStats::max_element(const SumVector& v) const
	{
		if (v.size()==0)
			return 0;
		return v.back();
	}

}} // end of namespace svndigest and namespace theplu
