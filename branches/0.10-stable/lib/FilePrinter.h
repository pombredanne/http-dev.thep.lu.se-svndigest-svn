#ifndef _theplu_svndigest_file_printer_
#define _theplu_svndigest_file_printer_

// $Id: FilePrinter.h 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2005, 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodePrinter.h"

#include <string>

namespace theplu{
namespace svndigest{

	class File;

	class FilePrinter : public NodePrinter
	{
	public:
		///
		/// @brief Default Constructor
		///
		explicit FilePrinter(const File& file);

		/**
			 @return output path for example 'lib/FilePrinter.h.html' for this file
		 */
		std::string output_path(void) const;

	private:
		std::string blame_output_file_name(void) const;

		/**
			 do nothing
		*/
		void log_core(SVNlog&) const;

		///
		/// @brief Copy Constructor, not implemented
		///
		FilePrinter(const FilePrinter&);

		///
		/// @brief Parsing svn blame output
		///
		/// @return true if parsing is succesful
		///
		bool blame(void) const;

		/**
			 @brief Print blame output
		*/
		void print_blame(std::ofstream& os) const;

		void print_core(bool verbose=false) const;

		///
		/// print page for specific user (or all) and specific line_style
		/// (or total).
		///
		void print_core(const std::string& stats_type, const std::string& user,
										const std::string& line_type, const SVNlog&) const;

		const Node& node(void) const;

		const File& file_;

	};

}} // end of namespace svndigest and namespace theplu

#endif
