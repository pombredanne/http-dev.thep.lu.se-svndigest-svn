// $Id: NodeCounter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodeCounter.h"

#include "Directory.h"
#include "File.h"
#include "NodeVisitor.h"

namespace theplu {
namespace svndigest {

	NodeCounter::NodeCounter(void)
		: NodeVisitor(), directories_(0), files_(0) {}


	unsigned int NodeCounter::directories(void) const
	{
		return directories_;
	}


	bool NodeCounter::enter(Directory& dir)
	{
		if (dir.svndigest_ignore())
			return false;
		++directories_;
		return true;
	}


	unsigned int NodeCounter::files(void) const
	{
		return files_;
	}


	void NodeCounter::leave(Directory& dir)
	{
	}


	void NodeCounter::visit(File& file)
	{
		if (!file.svndigest_ignore())
			++files_;
	}

}} // end of namespace svndigest and namespace theplu
