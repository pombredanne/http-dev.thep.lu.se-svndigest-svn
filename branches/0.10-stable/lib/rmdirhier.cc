// $Id: rmdirhier.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006 Jari Häkkinen
	Copyright (C) 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "rmdirhier.h"
#include "DirectoryUtil.h"
#include "utility.h"

#include "yat/Exception.h"

#include <cstdio>
#include <cstring>
#include <dirent.h>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>


namespace theplu {
namespace svndigest {

	void rmdirhier(const std::string& dir)
	{
		struct stat buf;
		// using lstat because links should not be treated as dirs
		lstat(dir, &buf);
		// check if dir
		if ((buf.st_mode & S_IFDIR)) {
			// Delete sub-nodes
			DirectoryUtil du(dir);
			for (DirectoryUtil::const_iterator node=du.begin();node!=du.end();++node){
				if ((file_name(node->path())!=".") && (file_name(node->path())!=".."))
					rmdirhier(node->path());
			}
		}
		// Make sure file is removable before removing it
		chmod(dir.c_str(),S_IWRITE);
		remove(dir);
	}

}} // of namespace svndigest and namespace theplu
