// $Id: OptionVersion.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2008, 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	svndigest is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "OptionVersion.h"
#include "subversion_revision.h"
#include "utility.h"

#include "yat/ColumnStream.h"
#include "yat/CommandLine.h"

#include <subversion-1/svn_version.h>

#include <apr_version.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

namespace theplu {
namespace svndigest {

	OptionVersion::OptionVersion(yat::utility::CommandLine& cmd, std::string flag,
															 std::string desc, OptionSwitch* const verbose)
		: yat::utility::OptionSwitch(cmd, flag, desc, false), verbose_(verbose)
	{
	}


	void OptionVersion::do_parse2(std::vector<std::string>::iterator first,
																std::vector<std::string>::iterator last)
	{
		yat::utility::ColumnStream cs(std::cout, 1);
		cs.width(0)=79;
		cs << prog_name_ << " " << PACKAGE_VERSION;
		bool no_verbose = verbose_ && verbose_->present() && !verbose_->value();
		bool verbose = verbose_ && verbose_->present() && verbose_->value();
		if (!no_verbose) {
			cs << " (";
			if (DEV_BUILD)
				cs << "r" << svn_revision() << " ";
			cs << "compiled " << compilation_time() << ", " << compilation_date()
				 << ")";
		}
		cs << "\n\nCopyright (C) " << RELEASE_YEAR
			 << " Jari H\u00E4kkinen and Peter Johansson.\n"
			 << "License GPLv3+: GNU GPL version 3 or later "
			 << "<http://gnu.org/licenses/gpl.html>\n"
			 << "This is free software: you are free to change and redistribute it.\n"
			 << "There is NO WARRANTY, to the extent permitted by law.\n";
		if (verbose) {
			cs << "\n"
				 << "Compiled with libsvn_subr " << SVN_VER_NUM
				 << "; using libsvn_subr ";
			const svn_version_t* svn_ver = svn_subr_version();
			cs << svn_ver->major << '.' << svn_ver->minor << '.' << svn_ver->patch;
			cs << "\n"
				 << "Compiled with libapr " << APR_VERSION_STRING
				 << "; using libapr ";
			cs << apr_version_string();
			cs << "\n";
			cs << extra_;
		}
		exit(EXIT_SUCCESS);
	}


	void OptionVersion::extra(const std::string& str)
	{
		extra_ = str;
	}


	void OptionVersion::program_name(const std::string& str)
	{
		prog_name_ = str;
	}

}} // of namespace svndigest and theplu
