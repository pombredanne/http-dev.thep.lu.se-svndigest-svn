// $Id: utility.cc 1560 2014-06-01 02:56:15Z peter $

/*
	Copyright (C) 2006, 2007, 2008, 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include "utility.h"

#include "yat/Exception.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fnmatch.h>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

#include <iostream>

#include <libgen.h>

namespace theplu{
namespace svndigest{

	int access_rights(const std::string& path, const std::string& bits)
	{
		if (access(path.c_str(),F_OK)) {
			throw std::runtime_error(std::string("access_rights: ") + path +
															 "' does not exist.");
		}
		int mode=0;
		for (unsigned int i=0; i<bits.length(); ++i)
			switch (bits[i]) {
					case 'r':
						mode|=R_OK;
						break;
					case 'w':
						mode|=W_OK;
						break;
					case 'x':
						mode|=X_OK;
						break;
			}
		return access(path.c_str(),mode);
	}


	void chdir(const std::string& dir)
	{
		if (::chdir(dir.c_str()) )
			throw yat::utility::errno_error("chdir: " + dir + ":");
	}


	void chmod(const std::string& filename, mode_t mode)
	{
		if (::chmod(filename.c_str(), mode))
			throw yat::utility::errno_error("chmod: ");
	}


	std::string concatenate_path(std::string dir, std::string base)
	{
		if (dir.empty() || dir==".")
			return base;
		if (dir[dir.size()-1]!='/')
			dir.append("/");
		return dir+base;
	}


	void copy_file(const std::string& source, const std::string& target)
	{
		std::ofstream o(target.c_str());
		std::ifstream i(source.c_str());
		while (i.good()) {
			char ch=i.get();
			if (i.good())
				o.put(ch);
			if (!o.good())
				throw std::runtime_error(std::string("copy_file: ") +
																 "writing target file failed '" + target + "'");
		}
 		if (!i.eof() && (i.bad() || i.fail()))	// fail on everything except eof
			throw std::runtime_error(std::string("copy_file: ") +
															 "error reading source file '" + source + "'");
		i.close(); o.close();
		// copy permissions
		struct stat nodestat;
		stat(source.c_str(),&nodestat);
		chmod(target.c_str(), nodestat.st_mode);
	}


	std::string	directory_name(const std::string& path)
	{
		if (path.size()==1) {
			if (path=="/")
				return path;
			else
				return ".";
		}
		assert(path.size()>=2);
		size_t pos = path.find_last_of('/', path.size()-2);
		if (pos==std::string::npos)
			return ".";
		if (pos==0)
			return "/";
		return path.substr(0,pos);
		return "";
	}


	std::string	file_name(const std::string& path)
	{
		if (path.size()==1)
			return path;
		size_t pos = path.find_last_of('/');
		if (pos==std::string::npos)
			return path;
		if (pos==path.size()-1)
			return file_name(path.substr(0, path.size()-1));
		return path.substr(pos+1);
	}


	std::string getenv(const std::string& var)
	{
		char* buffer=std::getenv(var.c_str());
		if (!buffer)
			throw std::runtime_error("Environment variable "+var+" is not set");
		return std::string(buffer);
	}


	std::string hex(int x, unsigned int width)
	{
		std::stringstream ss;
		ss << std::hex << x;
		if (!width)
			return ss.str();
		if (ss.str().size()<width)
			return std::string(width-ss.str().size(), '0') + ss.str();
		return ss.str().substr(0, width);
	}


	std::string htrim(std::string str)
	{
		size_t length=str.size();
		while(length && isspace(str[length-1]))
			--length;
		return str.substr(0,length);
	}


	bool fnmatch(const std::string& pattern, const std::string& str)
	{
		int res = ::fnmatch(pattern.c_str(), str.c_str(), 0);
		if (res==0)
			return true;
		if (res!=FNM_NOMATCH) {
			std::stringstream ss;
			ss << "fnmatch with args: " << pattern << ", " << str;
			throw std::runtime_error(ss.str());
		}
		return false;
	}


	void lstat(const std::string path, struct stat* nodestat)
	{
		int res = ::lstat(path.c_str(), nodestat);
		if (res) {
			throw yat::utility::errno_error("lstat: ");
		}
	}


	std::string ltrim(std::string str)
	{
		size_t i = 0;
		while(i<str.size() && isspace(str[i]))
			++i;
		return str.substr(i);
	}

	void mkdir(const std::string& dir)
	{
		int code = ::mkdir(dir.c_str(),0777);
		if (code){
			std::stringstream ss;
			ss << "mkdir: '" << dir << "': ";
			throw yat::utility::errno_error(ss.str());
		}
	}


	void mkdir_p(const std::string& dir)
	{
		if (node_exist(dir))
			return;
		std::string mother = directory_name(dir);
		mkdir_p(mother);
		mkdir(dir);
	}


	bool node_exist(const std::string& path)
	{
		struct stat buf;
		return !stat(path.c_str(),&buf);
	}


	int percent(int a, int b)
	{
		if (b)
			return (100*a)/b;
		return 0;
	}


	std::string pwd(void)
	{
		char buffer[FILENAME_MAX];
		if (!getcwd(buffer, FILENAME_MAX))
			throw yat::utility::errno_error("pwd: ");
		return std::string(buffer);
	}


	bool regexp(const std::string& pattern,	const std::string& str,
							std::vector<std::string>& vec)
	{
		bool regexp__(std::string::const_iterator first1,
									std::string::const_iterator last1,
									std::string::const_iterator first2,
									std::string::const_iterator last2,
									std::vector<std::string>::iterator item);

		// find number of special chars
		size_t count=0;
		for (std::string::const_iterator i=pattern.begin(); i!=pattern.end(); ++i)
			if (*i=='*' || *i=='?' || *i=='[')
				++count;
		vec.resize(count);
		return regexp__(pattern.begin(), pattern.end(), str.begin(), str.end(),
										vec.begin());
	}

	bool regexp__(std::string::const_iterator first1,
								std::string::const_iterator last1,
								std::string::const_iterator first2,
								std::string::const_iterator last2,
								std::vector<std::string>::iterator item)
	{
		if (first1==last1) {
			return first2==last2;
		}
		if (*first1 == '*') {
			if (first2<last2) {
				item->push_back(*first2);
				if (regexp__(first1, last1, first2+1, last2, item))
					return true;
				item->resize(item->size()-1);
			}
			return regexp__(first1+1, last1, first2, last2, item+1);
		}
		if (*first1 == '?') {
			if (first2==last2)
				return false;
			*item = *first2;
			return regexp__(first1+1, last1, first2+1, last2, item+1);
		}
		if (*first1 == '[') {
			if (first2==last2)
				return false;
			while (*first1 != ']') {
				if (*first1 == *first2) {
					*item = *first2;
				}
				++first1;
				assert(first1!=last1);
			}
			return regexp__(first1+1, last1, first2+1, last2, item+1);
		}

		if (first2==last2)
			return false;
		if (*first1 != *first2)
			return false;
		return regexp__(first1+1, last1, first2+1, last2, item);
	}


	void remove(const std::string& fn)
	{
		if (::remove(fn.c_str())) {
			std::string msg("remove: ");
			msg += fn;
			throw yat::utility::errno_error(msg);
		}
	}


	void rename(const std::string& from, const std::string to)
	{
		int code = ::rename(from.c_str(), to.c_str());
		if (code){
			std::stringstream ss;
			ss << "rename" << from << " to " << to << ": ";
			throw yat::utility::errno_error(ss.str());
		}
	}


	void replace(std::string& str, std::string old_str, std::string new_str)
	{
		std::string::iterator iter(str.begin());
		while ((iter=search(iter, str.end(), old_str)) != str.end()) {
			size_t i = iter-str.begin();
			str = std::string(str.begin(), iter) + new_str +
				std::string(iter+old_str.size(), str.end());
			// pointing to char after substr we just inserted
			iter = str.begin() + (i+new_str.size());
		}
	}


	void touch(std::string str)
	{
		if (!node_exist(str)) {
			std::ofstream os(str.c_str());
			os.close();
		}
	}


	std::string match(std::string::const_iterator& first,
										const std::string::const_iterator& last,
										std::string str)
	{
		if (match_begin(first, last, str)){
			first+=str.size();
			return str;
		}
		return std::string();
	}

}} // end of namespace svndigest and namespace theplu
