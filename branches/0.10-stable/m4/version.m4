## $Id: version.m4 1524 2012-09-28 05:51:28Z peter $
#
# Copyright (C) 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009, 2010, 2012 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.
#

m4_include([m4/my_version.m4])

#
# Set version numbers, see http://apr.apache.org/versioning.html
#

# edit this line before release
MY_VERSION_early([0], [10], [1], [true])
