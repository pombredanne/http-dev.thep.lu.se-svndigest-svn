// $Id: AbstractParameter.cc 1515 2012-09-26 00:35:10Z peter $

/*
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010, 2011, 2012 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>	// this header file is created by configure

#include "AbstractParameter.h"

#include "lib/OptionVersion.h"
#include "lib/utility.h" // to avoid inclusion of yat file

#include "yat/ColumnStream.h"
#include "yat/Exception.h"
#include "yat/OptionArg.h"
#include "yat/OptionHelp.h"
#include "yat/OptionSwitch.h"

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>

namespace theplu {
namespace svndigest {

	AbstractParameter::AbstractParameter(void)
		: cmd_( std::string("Mandatory arguments to long options ") +
						std::string("are mandatory for short options too.")),
			help_(cmd_),
			root_(cmd_, "r,root",
						"svn controlled directory to perform statistics on [.]"),
			verbose_(cmd_, "v,verbose", "explain what is being done"),
			version_(cmd_, "version", "print version information and exit", &verbose_)
	{
	}


	AbstractParameter::~AbstractParameter(void)
	{
	}


	void AbstractParameter::analyse(void)
	{
		// call pure virtual function
		analyse1();
	}


	void AbstractParameter::analyse_root(const std::string root)
	{
		check_existence(root);
		check_readable(root);
		check_is_dir(root);

		std::string save_wd = pwd();
		chdir(root);
		root_full_ = pwd();
		chdir(save_wd);

		// take care of when root is a symlink (see ticket #477)
		struct stat stats;
		lstat(root.c_str(), &stats);
		if (S_ISLNK(stats.st_mode))
			root_basename_ = file_name(root);
		else
			root_basename_ = file_name(root_full_);
	}


	void AbstractParameter::check_existence(std::string path) const
	{
		if (node_exist(path))
			return;
		std::stringstream ss;
		ss << cmd_.program_name() << ": cannot stat '" << path << "': "
			 << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}


	void AbstractParameter::check_is_dir(std::string path) const
	{
		if (node_exist(path)) {
			struct stat buf;
			stat(path.c_str(), &buf);
			if (S_ISDIR(buf.st_mode))
				return;
			errno = ENOTDIR;
		}
		std::stringstream ss;
		assert(errno);
		ss << cmd_.program_name() << ": '" << path << "': "
			 << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}


	void AbstractParameter::check_readable(std::string path) const
	{
		if (!access_rights(path, "r"))
			return;
		std::stringstream ss;
		ss << cmd_.program_name() << ": cannot open '" << path << "': "
			 << strerror(errno);
		throw yat::utility::cmd_error(ss.str());
	}


	void AbstractParameter::init(void)
	{
		// we like the options sorted alphabetically
		cmd_.sort();
		root_.print_arg("=ROOT");
		std::stringstream ss;
		ss << "Report bugs to " << PACKAGE_BUGREPORT << ".\n"
			 << PACKAGE << " home page: <" << PACKAGE_URL << ">.\n";
		help_.post_arguments() = ss.str();
		init1();
	}


	void AbstractParameter::parse(int argc, char* argv[])
	{
		init();
		cmd_.parse(argc, argv);
		set_default();
		// analyse arguments
		analyse();
	}


	std::string AbstractParameter::root(void) const
	{
		return root_full_;
	}


	const std::string& AbstractParameter::root_basename(void) const
	{
		assert(root_basename_.size());
		return root_basename_;
	}


	void AbstractParameter::set_default(void)
	{
		if (!root_.present())
			root_.value(".");

		set_default1();
	}


	bool AbstractParameter::verbose(void) const
	{
		return verbose_.present();
	}


}} // of namespace svndigest and namespace theplu
