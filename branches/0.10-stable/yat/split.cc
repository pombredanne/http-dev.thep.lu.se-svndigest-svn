// $Id: split.cc 2434 2011-03-08 16:52:23Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "split.h"

#include <string>
#include <vector>

namespace theplu {
namespace yat {
namespace utility {

	void split(std::vector<std::string>& vec, const std::string& str, char delim)
	{
		size_t pos=0;
		while (true) {
			size_t end = str.find_first_of(delim, pos);
			vec.push_back(str.substr(pos, end-pos));
			if (end == std::string::npos)
				break;
			pos = end+1;
		}
	}

}}} // of namespace utility, yat, and theplu
