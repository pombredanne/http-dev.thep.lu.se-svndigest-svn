#ifndef _theplu_yat_utility_yat_assert_
#define _theplu_yat_utility_yat_assert_
// $Id: yat_assert.h 1463 2012-03-10 07:20:54Z peter $

/*
	Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of the yat library, http://dev.thep.lu.se/yat

	The yat library is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 3 of the
	License, or (at your option) any later version.

	The yat library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yat. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Exception.h"

#include <sstream>
#include <stdexcept>
#include <string>

namespace theplu {
namespace yat {
namespace utility {

	/**
		 \internal

		 \brief yat_assert is similar to assert in std.

		 If YAT_DEBUG is enabled and \a assertion is false, an exception
		 X is thrown using constructor X(\a msg ).
	 */
	template<class X> inline void yat_assert(bool assertion, std::string msg) 
#ifdef YAT_DEBUG
	{ if (!assertion) throw X(std::string("yat_assert:")+msg); }
#else
	{ }
#endif

}}}


#ifdef YAT_DEBUG
// Peter, this is a bit clumsy, but I wanna keep the stringstream
// invisible outside macro, or multiple calls to macro would result in
// multiple declaration of the stringstream. Also the macro is
// supposed to be called with a trailing `;', and that's why we need
// to end with something that allows that.
#define YAT_ASSERT(expr) \
	if (!(expr)) {																								\
		std::stringstream yat_msg_;																	\
		yat_msg_ << __FILE__ << ":" << __LINE__ << " failed assertion `" \
						 << #expr << "'";																						\
		theplu::yat::utility::yat_assert<theplu::yat::utility::runtime_error>(expr, \
																										yat_msg_.str());	\
	} \
	else theplu::yat::utility::yat_assert<std::runtime_error>(expr, "")
#else
// This could be anything empty, but why not use the empty yat_assert
#define YAT_ASSERT(expr) theplu::yat::utility::yat_assert<std::runtime_error>(expr, "")
#endif
	/*
	*/
#endif
