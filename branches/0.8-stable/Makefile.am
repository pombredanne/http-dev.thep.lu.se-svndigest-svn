## Process this file with automake to produce Makefile.in
##
## $Id: Makefile.am 1244 2010-10-24 22:28:19Z peter $

# Copyright (C) 2005, 2006 Jari Häkkinen
# Copyright (C) 2007, 2008 Jari Häkkinen, Peter Johansson
# Copyright (C) 2009, 2010 Peter Johansson
#
# This file is part of svndigest, http://dev.thep.lu.se/svndigest
#
# svndigest is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# svndigest is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with svndigest. If not, see <http://www.gnu.org/licenses/>.

# -I arg should be the same as arg in AC_CONFIG_MACRO_DIR in configure.ac
ACLOCAL_AMFLAGS = -I m4

SUBDIRS = yat lib bin man test

EXTRA_DIST = build_support/move-if-change .release_year
MOVE_IF_CHANGE = $(SHELL) $(top_srcdir)/build_support/move-if-change

lazycheck recheck: all
	cd test && $(MAKE) $(AM_MAKEFLAGS) $@

clean-local: 
	rm -rf svndigest *~

.PHONY: .release_year.tmp recheck
if HAVE_SVN_WC
# update copyright year automatically (if we build from svn wc)
$(srcdir)/.release_year: $(builddir)/.release_year.tmp
	@$(MOVE_IF_CHANGE) $(builddir)/.release_year.tmp $@

$(builddir)/.release_year.tmp:
	@if ($(SVNVERSION) $(top_srcdir) | $(GREP) M > /dev/null); then \
	   date -u "+%Y" > $@; \
	else \
		 cp $(srcdir)/.release_year $@; \
	fi
endif


##############################################################
##
## Some rules useful for maintainer
##

include $(srcdir)/maintainer.am

# called within maintainer-check
## We do wanna run all tests here since test repository is not
## included in tarball, and thus some tests are skiped in distcheck.
MAINTAINER_CHECK_LOCAL = check
# extra check in release rule
RELEASE_LOCAL = check-svn_revision

.PHONY: check-svn_revision

check-svn_revision:
	@$(SVNVERSION) | $(EGREP) '^[0-9]+$$' || \
	{ echo incorrect svn revision - expected single unmodified revision 1>&2; \
	  exit 1; }

# run in end of release target
RELEASE_HOOK = build_support/Portfile

edit = $(SED) \
	-e 's|@PACKAGE_URL[@]|$(PACKAGE_URL)|g' \
	-e 's|@VERSION[@]|$(VERSION)|g'

build_support/Portfile: Makefile build_support/Portfile.in
	@rm -f $@ $@.t
	@openssl version > /dev/null || exit 1
	@openssl sha1 $(distdir).tar.gz > $(distdir).tar.gz.sha1
	@openssl rmd160 $(distdir).tar.gz > $(distdir).tar.gz.rmd160
	@sha1=`cut -f 2 -d ' ' $(distdir).tar.gz.sha1`;\
	rmd160=`cut -f 2 -d ' ' $(distdir).tar.gz.rmd160`;\
	$(edit) -e "s|@SHA1SUM[@]|$$sha1|g" -e "s|@RMD160SUM[@]|$$rmd160|g" \
	'$(srcdir)/$@.in' > $@.t
	@chmod a-w $@.t
	@mv $@.t $@
	@echo "creating $@"
