// $Id: Graph.cc 1147 2010-08-01 21:56:01Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Graph.h"

#include "Date.h"

#include <algorithm>
#include <cmath>
#include <sstream>

namespace theplu {
namespace svndigest {

	std::vector<std::string> Graph::xticks_;

	Graph::Graph(const std::string& filename, const std::string& format)
#ifdef HAVE_PLPLOT
		: plots_(0), pls_(1,1,format.c_str(),filename.c_str()), timeformat_("%y-%b"),
			title_(filename), xmin_(0.0), xmax_(0.0), ymin_(0.0), ymax_(0.0)
	{
		// should match the maximum number of authors plotted, change this
		// when the maximum number of authors becomes configurable
		legend_.reserve(10);
		// we use color map 0 position 0 for background color
		pls_.scolbga(255,255,255,0);
		pls_.setopt("geometry", "600x500");
		pls_.init();
		pls_.adv(0);
		pls_.vsta();
		pls_.syax(6,0);
	}
#else
{}
#endif


	Graph::~Graph(void)
	{
		print_legend();
	}


	bool Graph::date_xticks(void)
	{
		return xticks_.size() != 0;
	}


	void Graph::current_color(const legend_data& legend)
	{
		// we use color map 0 position 1 for current color
#ifdef HAVE_PLPLOT
		pls_.scol0a(1,legend.r,legend.g,legend.b,1.0);
#endif
	}


	void Graph::current_color(unsigned char r, unsigned char g, unsigned char b)
	{
		// we use color map 0 position 1 for current color
#ifdef HAVE_PLPLOT
		pls_.scol0a(1,r,g,b,1.0);
#endif
	}


	void Graph::plot(const std::vector<unsigned int>& y, const std::string& label,
									 unsigned int lines)
	{
#ifdef HAVE_PLPLOT
		if (!plots_) {
			// date[0] is not the oldest when repo is imported with cvs2svn
			xmin_= date_xticks() ? 
				std::min( Date(xticks_[0]), Date(xticks_[1]) ).seconds() : 0;
			xmax_= date_xticks() ? Date(xticks_.back()).seconds() : y.size();
			xrange_=xmax_-xmin_;
			yrange_=ymax_-ymin_;
			pls_.wind(xmin_, xmax_, ymin_, ymax_);

			// draw plot frame, x and y ticks only for the first plot
			pls_.scol0a(2,0,0,0,1.0);
			pls_.col0(2);

			std::string xopt("bcnstv");
			if (date_xticks()) {
				pls_.timefmt(timeformat_.c_str());
				xopt="bcnstd";
			}

			unsigned int ytickspacing=tick_spacing(yrange_);
			unsigned int xtickspacing=tick_spacing(xrange_);
			pls_.box(xopt.c_str(), xtickspacing, 1, "bcnstv", ytickspacing, 2);
			pls_.lab("Date", "Number of lines", title_.c_str());
		}
		++plots_;

		pls_.col0(1);
		for (unsigned int i=1; i<y.size(); ++i) {
			PLFLT x0=i-1;
			PLFLT x1=i;
			if (date_xticks()) {
				x0=Date(xticks_[i-1]).seconds();
				x1=Date(xticks_[i]).seconds();
			}
			pls_.join(x0, y[i-1], x0, y[i]);
			pls_.join(x0, y[i]  , x1, y[i]);
		}

		legend_data legend;
		legend.label=label;
		legend.lines=lines;
		pls_.gcol0(1,legend.r,legend.g,legend.b);
		legend_.push_back(legend);
#endif
	}


	void Graph::print_legend(void)
	{
#ifdef HAVE_PLPLOT
		PLFLT line_length=0.05*xrange_;
		PLFLT x=xmin_+1.7*line_length;
		unsigned char characteristic=log10(ymax_);
		PLFLT legend_lines_length=0.016*xrange_*(characteristic+1);
		PLFLT dx=0.005*xrange_;
		PLFLT dy=0.003*yrange_;
		unsigned int row=0;
		std::reverse(legend_.begin(), legend_.end());
		for (std::vector<legend_data>::const_iterator i=legend_.begin();
				 i!=legend_.end(); i++, ++row) {
			PLFLT y=(0.95-0.04*row)*yrange_;
			current_color(*i);
			pls_.col0(1);
			pls_.join(x-line_length, y-dy, x, y-dy);
			std::stringstream ss;
			ss << i->lines;
			pls_.col0(2);
			pls_.ptex(x+legend_lines_length+dx*2, y, 0, 0, 0, i->label.c_str());
			pls_.ptex(x+legend_lines_length+dx  , y, 0, 0, 1, ss.str().c_str());
		}
#endif
	}


	void Graph::set_dates(const std::vector<std::string>& date)
	{
		xticks_=date;
	}


	unsigned int Graph::tick_spacing(const double range) const
	{
		double frac=range/5;
		unsigned char characteristic= static_cast<unsigned char>(std::log10(frac));
		unsigned int power=static_cast<unsigned int>(std::pow(10.0, characteristic));
		unsigned char msn=static_cast<unsigned char>(frac/power);
		return power*msn;
	}


	void Graph::timeformat(const std::string& format)
	{
		timeformat_=format;
	}


	const std::vector<std::string>& Graph::xticks(void)
	{
		return xticks_;
	}


	double Graph::ymax(double ymax)
	{
		return ymax_=ymax;
	}

}} // end of namespace svndigest and namespace theplu
