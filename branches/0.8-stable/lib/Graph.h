#ifndef _theplu_svndigest_graph_
#define _theplu_svndigest_graph_

// $Id: Graph.h 1017 2010-01-09 14:41:34Z peter $

/*
	Copyright (C) 2009 Jari Häkkinen, Peter Johansson
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include <string>
#include <vector>

#ifdef HAVE_PLPLOT
#include <plplot/plstream.h>
#else
typedef int PLINT;
typedef double PLFLT;
typedef double plstream;
#endif

namespace theplu {
namespace svndigest {

	/**
		 Graph is used to generate plots in svndigest and is a specialized
		 wrapper to the plplot plot library, http://plplot.sourceforge.net/

		 Graph allows some axis label manipulation, setting pen colour,
		 and plotting lines.

		 Graph supports three graphics formats - portable network graphics
		 (png), scalable vector graphics (svg), and protable document
		 format (pdf) - if the underlying plplot library was built with
		 the appropriate modules. If not, rebuild and reinstall plplot.
	*/
	class Graph 
	{
	public:

		/**
			 \brief Constructor

			 \a filename to be defined depending on whether we'll support
			 more output formats than SVG.

			 \note The plot legend is created in the destructioe, i.e., when
			 the graph object is destroyed.
		*/
		Graph(const std::string& filename, const std::string& format);

		/**
			 \brief Destructor
		*/
		~Graph(void);

		/**
			 \brief Set the pen colour to use in next drawing call
		*/
		void current_color(unsigned char r, unsigned char g, unsigned char b);

		static bool date_xticks(void);

		/**
			 \brief Plot \a data and use \a lines and \a label to compose
			 the legend label.

			 The legend will be a coloured line followed by \a lines
			 followed by \a label.

			 The label order in the legend is reverse to the plot order,
			 i.e., the last plotted line will get the top entry in the
			 legend, the second to last plotted line will be the second
			 legend entry, and so on.
		*/
		void plot(const std::vector<unsigned int>& data, const std::string& label,
							unsigned int lines);

		/**
			 \brief Function setting the dates.

			 The date strings must be in svn format since underlying time
			 conversion are done with the Date class.

			 \see Date::svntime(std::string)
		*/
		static void set_dates(const std::vector<std::string>& date);

		/**
			 \brief Set x-axis tick value format to \a format. The format is
			 only used when dates are used.
		*/
		void timeformat(const std::string& format);

		static const std::vector<std::string>& xticks(void);

		/**
			 \brief Set max y value in the plot

			 The plot area is (xstart,0) to (xend,\a ymax) where xend is
			 either the length of vector to plot (corresponds to latest
			 revision number) or the date of the last revision commit,
			 xstart is 0 or the date of the first commit.
		*/
		double ymax(double ymax);

	private:

		// Copy constructor not implemented
		Graph(const Graph&);

		struct legend_data {
			std::string label;
			unsigned int lines;
			PLINT r,g,b;
		};

		/**
			 \brief Set the pen colour to use in next drawing call
		*/
		void current_color(const legend_data&);
		void print_legend(void);
		unsigned int tick_spacing(const double range) const;

		std::vector<legend_data> legend_;
		unsigned int plots_; // keep track of number of plots drawn
		plstream pls_;
		std::string timeformat_;
		std::string title_;
		static std::vector<std::string> xticks_;
		PLFLT xmin_, xmax_, xrange_, ymin_, ymax_, yrange_;
	};

}} // end of namespace svndigest and namespace theplu

#endif
