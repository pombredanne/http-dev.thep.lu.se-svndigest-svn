// $Id: AddStats.cc 1003 2010-01-02 02:35:57Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AddStats.h"

#include "SVNblame.h"
#include "SVNlog.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <map>
#include <string>
#include <vector>

namespace theplu{
namespace svndigest{


	AddStats::AddStats(const std::string& path)
		: Stats(path)
	{
	}


	AddStats::AddStats(const AddStats& other)
	: Stats(other)
	{
	}


	void AddStats::do_parse(const std::string& path, svn_revnum_t rev)
	{
		SVNlog log(path);
		typedef std::set<svn_revnum_t, std::greater<svn_revnum_t> > RevSet;
		RevSet revs;
		std::transform(log.commits().begin(), log.commits().end(),
									 std::inserter(revs, revs.begin()), 
									 std::mem_fun_ref(&Commitment::revision));
		for (RevSet::iterator rev_iter=revs.begin();
				 rev_iter!=revs.end() && *rev_iter>=rev; ++rev_iter){
			SVNblame svn_blame(path, *rev_iter);
			LineTypeParser parser(path);
			while (svn_blame.valid()) {
				LineTypeParser::line_type lt = parser.parse(svn_blame.line());
				if (*rev_iter==svn_blame.revision())
					add(svn_blame.author(), *rev_iter, lt);
				// I dont trust blame and log behave consistently (stop-on-copy).
				revs.insert(svn_blame.revision());
				svn_blame.next_line();
			}
		}
		accumulate_stats(rev);
	}


	unsigned int AddStats::max_element(const std::vector<unsigned int>& v) const 
	{
		assert(v.size());
		return v.back();
	}


}} // end of namespace svndigest and namespace theplu
