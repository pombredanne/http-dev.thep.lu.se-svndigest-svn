// $Id: ClassicStats.cc 1036 2010-02-15 05:31:34Z peter $

/*
	Copyright (C) 2005 Peter Johansson
	Copyright (C) 2006, 2007, 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClassicStats.h"

#include "Functor.h"
#include "SVNblame.h"
#include "SVNinfo.h"
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <string>
#include <sstream>
#include <unistd.h>
#include <utility>
#include <vector>


namespace theplu{
namespace svndigest{


	ClassicStats::ClassicStats(const std::string& path)
		: Stats(path)
	{
	}


	ClassicStats::ClassicStats(const ClassicStats& other)
	: Stats(other)
	{
	}


	void ClassicStats::do_parse(const std::string& path, svn_revnum_t rev)
	{
		reset();
		LineTypeParser parser(path);
		SVNblame svn_blame(path);
		while (svn_blame.valid()) {
			add(svn_blame.author(), svn_blame.revision(), 
					parser.parse(svn_blame.line()));
			svn_blame.next_line();
		}
		accumulate_stats();
	}


	unsigned int 
	ClassicStats::max_element(const std::vector<unsigned int>& v) const 
	{
		assert(v.size());
		return v.back();
	}

}} // end of namespace svndigest and namespace theplu
