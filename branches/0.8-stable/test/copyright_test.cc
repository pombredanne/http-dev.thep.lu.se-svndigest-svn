// $Id: copyright_test.cc 1119 2010-07-04 18:34:07Z peter $

/*
	Copyright (C) 2008 Jari Häkkinen, Peter Johansson
	Copyright (C) 2009, 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"

#include "lib/Configuration.h"
#include "lib/File.h"
#include "lib/SVN.h"
#include "lib/SVNinfo.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

std::vector<std::string> copyright_lines(std::istream&);

int main( int argc, char* argv[])
{
	using namespace theplu::svndigest;
	test::Suite suite(argc, argv, true);

	std::string root="toy_project";
	std::string filename = root + "/README";

	// Saving original file
	std::ifstream is(filename.c_str());
	assert(is.good());
	std::string original_file;
	std::getline(is, original_file, '\0');
	is.close();
	is.clear(std::ios::goodbit);

	is.open(filename.c_str());
	std::vector<std::string> copyrights_old=copyright_lines(is);
	is.close();
	is.clear(std::ios::goodbit);

	if (copyrights_old.size()!=1) {
		suite.out() << copyrights_old.size() << " Copyright lines\n";
		for (size_t i=0; i<copyrights_old.size(); ++i)
			suite.out() << copyrights_old[i] << "\n";
		suite.add(false);
	}
	suite.out() << "File contains 1 copyright line.\n"; 

	// warn about missing Copyright statement only in verbose mode
	if (suite.verbose()){
		std::string config_str("[copyright]\nmissing-copyright-warning=yes");
		std::stringstream ss(config_str);
		Configuration& config = Configuration::instance();
		config.load(ss);
	}

	suite.out() << "Create SVN instance" << std::endl;
	SVN* svn=SVN::instance(root);
	if (!svn)
		return 1;

  // Extract repository location
	suite.out() << "Extract repository location" << std::endl;
	std::string	repo=SVNinfo(root).repos_root_url();
	suite.out() << "Create File object" << std::endl;
	File file(0,filename,"");
	
	suite.out() << "Get stats for file" << std::endl;
	file.parse(suite.verbose(), true);
	
	suite.out() << "Updating copyright statements" << std::endl;
	std::map<std::string, Alias> alias;
	alias["jari"]=Alias("jh", 1);
	alias["peter"]=Alias("pj", 2);
	
	file.print_copyright(alias, suite.verbose());

	is.open(filename.c_str());
	std::vector<std::string> copyrights=copyright_lines(is);
	is.close();
	is.clear(std::ios::goodbit);

	std::vector<std::string> copyright_correct;
	copyright_correct.push_back("Copyright (C) 2006 jh");
	copyright_correct.push_back("Copyright (C) 2007, 2008 pj");
	if (copyrights.size()!=copyright_correct.size()) {
		suite.add(false);
		suite.out() << "ERROR: expected " << copyright_correct.size() 
								<< " lines of Copyright (C)\n"
								<< "But found " << copyrights.size() << " lines.\n";
	}
	else {
		for (size_t i=0; i<copyrights.size(); ++i)
			if (copyrights[i]!=copyright_correct[i]){
				suite.add(false);
				suite.out() << "ERROR: found '" << copyrights[i] << "'\n"
										<< "expected: '" << copyright_correct[i] << "'\n";
			}

	}


	// Restoring file
	std::ofstream os(filename.c_str());
	assert(os.good());
	os << original_file;
	os.close();

	if (suite.ok()) {
		suite.out() << "Test is Ok!" << std::endl;
		return 0;
	}
	suite.out() << "Test failed." << std::endl;
	return 1;
}

std::vector<std::string> copyright_lines(std::istream& is)
{
	using namespace theplu::svndigest;
	std::vector<std::string> res;
	std::string line;
	while (std::getline(is, line)){
		if (match_begin(line.begin(), line.end(), "Copyright (C)"))
			res.push_back(line);
	}
	
	return res;
}

