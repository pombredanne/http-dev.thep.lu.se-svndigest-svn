// $Id: parameter_test.cc 1250 2010-10-31 14:11:20Z peter $

/*
	Copyright (C) 2010 Peter Johansson

	This file is part of svndigest, http://dev.thep.lu.se/svndigest

	svndigest is free software; you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	svndigest is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with svndigest. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Suite.h"
#include "environment.h"

#include "bin/svndigestParameter.h"
#include "lib/utility.h"

#include "yat/Exception.h"

#include <cassert>
#include <cerrno>
#include <cstring>
#include <string>
#include <vector>

using namespace theplu::svndigest;

void test_root(test::Suite& suite);
void test_root(test::Suite& suite, const std::string& arg, 
							 const std::string& root, const std::string& root_basename);
void parse(Parameter&, const std::vector<std::string>& argv);

int main(int argc, char* argv[])
{
	test::Suite suite(argc, argv);
	test_root(suite);
	if (suite.ok())
		return 0;
	return 1;
}


void test_root(test::Suite& suite)
{
	suite.out() << "test --root ROOT\n";
	test_root(suite, ".", test::abs_builddir()+"/testSubDir/parameter_test",
						"parameter_test");
	test_root(suite, "..", test::abs_builddir()+"/testSubDir","testSubDir");
	test_root(suite, "../..", test::abs_builddir(), "test");
	// test with absolute path
	test_root(suite, test::abs_builddir(), test::abs_builddir(), "test");
	// test with symbolic link
	if (!node_exist("symlink")) {
		if (symlink(".", "symlink")) {
			suite.add(false);
			suite.out() << "creating symlink failed: " << strerror(errno) << "\n";
		}
	}
	test_root(suite, "symlink", 
						test::abs_builddir()+"/testSubDir/parameter_test", "symlink");
	test_root(suite, "symlink/.", 
						test::abs_builddir()+"/testSubDir/parameter_test","parameter_test");
	test_root(suite, "symlink/..", 
						test::abs_builddir()+"/testSubDir", "testSubDir");
	test_root(suite, test::abs_builddir()+"/testSubDir/parameter_test/symlink/..", 
						test::abs_builddir()+"/testSubDir", "testSubDir");
	test_root(suite, test::abs_builddir()+"/testSubDir/parameter_test/symlink", 
						test::abs_builddir()+"/testSubDir/parameter_test", "symlink");
	
	try {
		test_root(suite, "../../Makefile", test::abs_builddir()+"/Makefile", 
							"Makefile");
		suite.add(false);
		suite.out() << "error: no exception thrown\n --root ../../Makefile\n";
	}
	catch (theplu::yat::utility::cmd_error& e) {
		suite.out() << "caught expected errno_error: what(): " << e.what();
	}
}

void test_root(test::Suite& suite, const std::string& arg, 
							 const std::string& root, const std::string& root_basename)
{
	svndigestParameter option;
	std::vector<std::string> argv;
	argv.push_back("svndigest");
	argv.push_back("--no-report");
	argv.push_back("--root");
	argv.push_back(arg);
	parse(option, argv);
	if (option.root() != root) {
		suite.add(false);
		for (size_t i=0; i<argv.size(); ++i)
			suite.out() << argv[i] << " ";
		suite.out() << "\n";
		suite.out() << "root: `" << option.root() << "'\n";
		suite.out() << "expected: `" << root << "'\n";
	}
	if (option.root_basename() != root_basename) {
		suite.add(false);
		for (size_t i=0; i<argv.size(); ++i)
			suite.out() << argv[i] << " ";
		suite.out() << "\n";
		suite.out() << "root: `" << option.root_basename() << "'\n";
		suite.out() << "expected: `" << root_basename << "'\n";
	}
}


void parse(Parameter& option, const std::vector<std::string>& vec)
{
	int argc = vec.size();
	char** argv = new char*[vec.size()];
	for (int i = 0; i<argc; ++i) {
		argv[i] = strdup(vec[i].c_str());
		assert(argv[i]);
	}
	option.parse(argc, argv);
	for (int i = 0; i<argc; ++i) {
		free(argv[i]);
	}
}
